<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    //
    protected $guarded = [];

    public function items(){
        return $this->hasMany('App\PurchaseOrderDetail');
    }

    public function company(){
        return $this->belongsTo('App\Company');
    }

    public function vehicle(){
        return $this->belongsTo('App\Vehicle');
    }

    public function coverage(){
        $vehicleSize=$this->vehicle->size;
        $coverageSize=0;
        foreach ($this->items as $item){
            if($item->item && $item->item->size_t)
                $coverageSize+=$item->quantity * $item->item->size_t;
        }

        return ($coverageSize*100)/$vehicleSize;
    }

    public function quantity(){
        $quantity=0;
        foreach ($this->items as $item){
            $quantity+=$item->quantity;
        }
        return $quantity;
    }
    public function user(){
        return $this->belongsTo('App\User');
    }

}
