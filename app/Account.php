<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //
    protected $guarded = [];
    //public $timestamps = false;

    public function company(){
        return $this->belongsTo('App\Company');
    }

    public function payments(){
        return $this->hasMany('App\Payments');
    }

    public function GetPaymentAttribute($value){
        $returnVal=$this->payments->all();
        usort($returnVal,function($a,$b){
            return $a->date<$b->date;
        });
        return $returnVal;
    }

    public function head(){
        return $this->belongsTo('App\Account','head_id');
    }


    public function Opening(){
        return $this->morphOne('App\Model\Opening','owner')->where('date','>=',\config('myapp.opening_date'));
    }
}
