<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderBookingDetail extends Model
{
    //

    protected $guarded = [];

    public function orderBooking(){
        return $this->belongsTo('App\OrderBooking');
    }

    public function item(){
        return $this->belongsTo('App\Item');
    }

}
