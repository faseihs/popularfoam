<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NonCashTransaction extends Model
{
    //
    protected $guarded = [];
    public function debitCompany(){
        return $this->belongsTo('App\Company','debit_id');
    }

    public function creditCompany(){
        return $this->belongsTo('App\Company','credit_id');
    }
}
