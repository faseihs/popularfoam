<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDetail extends Model
{
    //
    protected $guarded = [];

    public function purchaseOrder(){
        return $this->belongsTo('App\PurchaseOrder');
    }

    public function item(){
        return $this->belongsTo('App\Item');
    }
}
