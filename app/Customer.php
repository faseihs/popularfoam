<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $guarded = [];

    public function cashRegister(){
        return $this->morphOne('App\CashRegister','owner');
    }
    public function payments(){
        return $this->morphMany('App\Payments','owner')->orderBy('created_at','DESC');
    }

    public function  lastPayment(){
        return $this->morphMany('App\Payments','owner')->where('pay_later','=',0)->where('type','=','credit')->orderBy('created_at','DESC');
    }
    public function GetNumberAttribute($value){
        if($value)
            return '0'.$value;
        else return '';
    }
}
