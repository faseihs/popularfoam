<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseInvoice extends Model
{
    //
    protected $guarded = [];

    public function items(){
        return $this->hasMany('App\PurchaseInvoiceDetail');
    }

    public function company(){
        return $this->belongsTo('App\Company');
    }

    public function payment(){
        return $this->morphOne('App\Payments','sales');
    }

    public function photo(){
        return $this->morphOne('App\Photo','imageable');
    }

    public function GetPicAttribute(){
        if(!$this->photo)
            return null;
        else return asset('images/'.$this->photo->path);
    }
    
    public function getCountItems(){
        $count=0;
        foreach($this->items as $items){
            $count+=$items->quantity;
        }
        return $count;
    }
}
