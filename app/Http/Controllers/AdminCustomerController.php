<?php

namespace App\Http\Controllers;

use App\CashRegister;
use App\Customer;
use Illuminate\Http\Request;
use App\SalesInvoice;
use App\User;

class AdminCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $customers = Customer::orderBy('name')->get();;
        $walking=[];
        $si=SalesInvoice::all();
        foreach($si as $s){
            if($s->name){
                $tempObj=new \stdClass;
                $tempObj->name=$s->name;
                $tempObj->number=$s->number;
                array_push($walking,$tempObj);
            }
        }
        return view('admin.customer.index',compact(['customers','walking']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('owner', User::class);
        //
        return view('admin.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //
        $this->validate($request,[
            'name'=>'required',
            'type'=>'required',
            'balance_amount'=>'required',
            'balance_type'=>'required'
        ]);
        $id=Customer::create($request->all())->id;
        $to_pay=0;
        $to_receive=0;
        if($request['balance_type']==0){
            $to_pay=$request['balance_amount'];
        }
        else $to_receive=$request['balance_amount'];
        CashRegister::create(['owner_id'=>$id,'paid'=>0,'to_pay'=>$to_pay,'to_receive'=>$to_receive,'owner_type'=>'App\Customer','received'=>0]);
        return redirect('admin/customer')->with('success','Customer Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $customer=Customer::findOrFail($id);
        return view('admin.customer.edit',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        $this->validate($request,[
            'name'=>'required',
            'type'=>'required',
            'number'=>'required',
            'balance_amount'=>'required',
            'balance_type'=>'required'
        ]);
        $customer=Customer::findOrFail($id);
        $cr=$customer->cashRegister;
        $newtoPay=$cr->to_pay;
        $newtoRec=$cr->to_rec;
        $to_pay=0;
        $to_receive=0;
        if($request['balance_type']==0){
            $to_pay=$request['balance_amount'];
            $newtoPay-=$cr->balance_amount;
            $newtoPay+=$to_pay;
        }
        else {
            $to_receive = $request['balance_amount'];
            $newtoRec-=$cr->balance_amount;
            $newtoRec+=$to_receive;
        }
        $customer->update($request->all());
        return redirect('admin/customer')->with('success','Customer Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        $customer=Customer::find($id);
        if($cr=$customer->cashRegister)
            $cr->delete();
        if($pa=$customer->payments)
        {
            foreach ($pa as $p){
                $p->delete();
            }
        }
        $customer->delete();
        return redirect('/admin/customer')->with('deleted','Customer Deleted');
    }

    public function printNumbers(){

        $this->authorize('owner', User::class);
        
        $customers = Customer::orderBy('name')->get();;
        $walking=[];
        $si=SalesInvoice::all();
        foreach($si as $s){
            if($s->name){
                $tempObj=new \stdClass;
                $tempObj->name=$s->name;
                $tempObj->number=$s->number;
                array_push($walking,$tempObj);
            }
        }

        $pdf= \PDF::loadHTML( view('admin.customer.number_print',compact(['walking','customers'])))->setPaper('a4', 'portrait');
        return $pdf->stream();
    }
}
