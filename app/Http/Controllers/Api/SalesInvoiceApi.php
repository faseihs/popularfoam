<?php

namespace App\Http\Controllers\Api;

use App\CashRegister;
use App\SalesInvoice;
use App\Transformer\SalesInvoiceApiTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Payments;
use App\SalesInvoiceDetails;
use App\Transformer\ItemTransformer;
use App\User;
use Illuminate\Support\Facades\DB;

class SalesInvoiceApi extends Controller
{
    //


    public function salesToday(){
        
        $this->authorize('sales', User::class);

        $sales_invoices_today=SalesInvoice::where('order_booking_id','=',null)->where('date','=',Carbon::now()->format('Y-m-d'))->orderByDesc('id')->orderByDesc('date')->get();
        return fractal($sales_invoices_today,new SalesInvoiceApiTransformer())->toArray();
    }

    public function salesOld(){

        $this->authorize('sales', User::class);

        $sales_invoices_previous=SalesInvoice::where('order_booking_id','=',null)->where('date','<',Carbon::now()->format('Y-m-d'))->orderByDesc('id')->orderByDesc('date')->get();
        return fractal($sales_invoices_previous,new SalesInvoiceApiTransformer())->toArray();

    }
    public function oldPagination(){
        
        $this->authorize('sales', User::class);

        $sales_invoices_previous=SalesInvoice::where('order_booking_id','=',null)->where('date','<',Carbon::now()->format('Y-m-d'))->orderByDesc('id')->orderByDesc('date')
            ->paginate(20);
        return fractal($sales_invoices_previous->all(),new SalesInvoiceApiTransformer())->toArray();

    }

    public function paymentPending(){

        $this->authorize('sales', User::class);

        $salesInvoicesAll=SalesInvoice::orderBy('date','DESC')
        ->whereIn("customer_id",[0,1,23])
        ->get();
        $salesInvoices=array();
        foreach ($salesInvoicesAll as $sales_invoice){
            if(!$sales_invoice->allPaid())
                array_push($salesInvoices,$sales_invoice);
        }

        return fractal($salesInvoices,new SalesInvoiceApiTransformer())->toArray();

    }

    public function orderBooking(){

        $this->authorize('sales', User::class);

        $order_bookings= SalesInvoice::where('order_booking_id','<>',null)
            ->orderBy('date','DESC')
            ->get();
        return fractal($order_bookings,new SalesInvoiceApiTransformer())->toArray();

    }

    public function SalesInvoiceEdit($id){
        
        $this->authorize('sales', User::class);

        $sales_invoice=SalesInvoice::findOrFail($id);
        $invoice_items = $sales_invoice->items()->with("item")->get();
        $customer = $sales_invoice->customer;
        $items = fractal($invoice_items->pluck("item")->all(),new ItemTransformer($customer->id))->toArray();



        return [
            'sales_invoice'=>$sales_invoice,
            'invoice_items'=>$invoice_items,
            "items"=>$items,
            'customer'=>$customer
        ];

    }

    public function SalesInvoiceUpdate(Request $request, $id)
    { 
        $this->authorize('sales', User::class);

        //dd($request->all());
        try {
            DB::beginTransaction();
            $si=SalesInvoice::findOrFail($id);
            foreach ($si->items as $it){
                $item=$it->item;
                $q=$item->current_quantity+$it->quantity;
                $item->update(['current_quantity'=>$q]);
                $it->delete();
            }

            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->sales_invoice_id=$id;
                $si=SalesInvoiceDetails::create((array)$item);
                $it=$si->item;
                if($it->current_quantity>$si->quantity){
                    $q=$it->current_quantity-$si->quantity;
                    $it->update(['current_quantity'=>$q]);
                }
                else if($it->store_quantity>$si->quantity){
                    $q=$it->store_quantity-$si->quantity;
                    $it->update(['store_quantity'=>$q]);

                }
                else if(($it->store_quantity+$it->current_quantity)>=$si->quantity){
                    $q=($it->store_quantity+$it->current_quantity)-$si->quantity;
                    $it->update(['store_quantity'=>0,'current_quantity'=>$q]);
                }
                else{
                    $q=$it->current_quantity-$si->quantity;
                    $it->update(['current_quantity'=>$q]);
                }
            }
            $si=SalesInvoice::findOrFail($id);
            $cr=null;
            $cID=0;
            if($si->customer){
                $cr=$si->customer->cashRegister;
                $cID=$si->customer->id;
            }
            else{
                $cr=CashRegister::where('owner_id','=','0')->where('owner_type','=','App\Customer')->get()->first();
            }
            $payment=Payments::where('sales_type','=','App\SalesInvoice')->where(
                'sales_id','=',$id
            )->get()->first();
            //dd($payment);

            
            if(!$payment){
                if($request['pay_later']==0){
                    $payment=[];
                    $payment['owner_id']=$cID;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=$request['total_price'];
                    $payment['pay_later']=0;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];
                    Payments::create($payment);
                }
                else {
                    $payment=[];
                    $payment['owner_id']=$cID;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=$request['paid_amount'];
                    $payment['pay_later']=0;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];
                    Payments::create($payment);

                    /*$payment=[];
                    $payment['owner_id']=$cID;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=abs($request['returned']);
                    $payment['pay_later']=1;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];*/
                }
            }

            else{

                if($payment->pay_later==0){
                    $q=$cr->paid-$payment->amount;
                    if($q<0) $q=0;
                    $q+=$request['total_price'];
                    $cr->update(['paid'=>$q]);


                    if($request['pay_later']==0){
                        $payment->update([
                            'amount'=>$request['paid_amount']
                        ]);
                    }
                    else {
                        $payment->update([
                            'amount'=>$request['paid_amount']
                        ]);
                    }
                }
                else{
                    $q=$cr->to_pay-$payment->amount;
                    if($q<0) $q=0;
                    $q+=$request['total_price'];
                    $cr->update(['to_pay'=>$q]);

                    if($request['pay_later']==0){
                        $payment->update([
                            'amount'=>$request['paid_amount']
                        ]);
                    }
                    else {
                        $payment->update([
                            'amount'=>$request['paid_amount']
                        ]);
                    }
                }

            }


            $sales=[];
            $sales['date']=$request['date'];
            $sales['customer_id']=$request['customer_id'];
            $sales['total_price']=$request['total_price'];
            $sales['paid_amount']=$request['paid_amount'];
            $sales['returned']=$request['returned'];
            $sales['remaining']=$request['remaining'];
            $sales['name']=$request['name'];
            $sales['number']=$request['number'];
            $sales['discount']=$request['discount'];
            $sales['given']=$request['given'];
            $sales['pay_later']=$request['pay_later'];
            $sales['comments']=$request['comments'];
            $si->update($sales);
            DB::commit();
            return response()->json($id,201);
        } catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }
    }
}
