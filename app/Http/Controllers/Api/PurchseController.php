<?php

namespace App\Http\Controllers\Api;

use App\Company;
use App\Item;
use App\Quality;
use App\Transformer\ItemEditTransformer;
use App\Transformer\ItemPurchaseTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class PurchseController extends Controller
{
    //

    public function itemStock($id){
        
        $this->authorize('sales', User::class);

        $item = Item::findOrFail($id);
        return [
            "stock"=>$item->getStock()
        ];
    }

    public function getItems(Request $request,$id){

        $this->authorize('sales', User::class);

        $company=Company::find($id);

        if($company){
            if(!empty($request->search))
            {
               
                $search = $request->search;
                $items = $company->items()->where("items.name","LIKE","%$search%")->with(["quality","quality.company"])->get();
            }
            else $items=$company->items()->with(["quality","quality.company"])->get();
        }
        else {
            if(!empty($request->search))
            {
               
                $search = $request->search;
                $items = Item::where("items.name","LIKE","%$search%")->with(["quality","quality.company"])->get();
            }
            else $items=Item::with(["quality","quality.company"])->get();
        }
      

        $items=$items->sort(function ($a, $b) {
            // sort by column1 first, then 2, and so on
            return strcmp($a->quality->name, $b->quality->name)
                ?: strcmp($a->name, $b->name);
        });
        return fractal($items,new ItemPurchaseTransformer())->toArray();
    }

    public function getItemsByQuality($id){
        
        $this->authorize('sales', User::class);

        $quality=Quality::findOrFail($id);
        $items=$quality->items;
        $items=$items->sort(function ($a, $b) {
            // sort by column1 first, then 2, and so on
            return strcmp($a->quality->name, $b->quality->name)
                ?: strcmp($a->name, $b->name);
        });
        return fractal($items,new ItemPurchaseTransformer())->toArray();
    }
    
    public function getQualitiesByCompany($id){

        $this->authorize('sales', User::class);

        $company=Company::findOrFail($id);
        $qualities=$company->quality()->orderBy('name','ASC')->get();
        return response()->json($qualities,200);
    }
    
    public function getCompanies(){

        $this->authorize('sales', User::class);

        $companies=Company::orderBy('name','ASC')->get();
        return response()->json($companies,200);
    }
}
