<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\User;
use App\Worker;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AdminAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            DB::beginTransaction();
            $current_date=date("Y-m-d");
            $workers=Worker::all();
            $creation=false;
            $attendances=[];
            foreach ($workers As $w){
                if($w->todaysAttenance->first()){
                    array_push($attendances,$w->todaysAttenance->first());

                }
                else {
                    $a=Attendance::create([
                        'worker_id'=>$w->id,
                        'date'=>Carbon::now()->toDateString(),
                        'arrival'=>null,
                        'status'=>'0'
                    ]);
                    array_push($attendances,$a);
                }
            }
            DB::commit();
            return view('admin.attendance.index',compact('attendances'));

        } catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        //dd($request->all());

        try {
            DB::beginTransaction();
            $attendances=[];
            $on_date=null;
            if($request['today']==1){
                $attendances=Attendance::where('date','=',Carbon::now()->toDateString())->get();
            }
            else{
                $on_date=$request['date'];
                foreach (Worker::all() as $w){
                    if($w->datesAttendence($request['date'])->first()){
                        array_push($attendances,$w->datesAttendence($request['date'])->first());
                    }
                    else{
                        $a=Attendance::create([
                            'worker_id'=>$w->id,
                            'date'=>$request['date'],
                            'arrival'=>null,
                            'status'=>'0'
                        ]);
                        array_push($attendances,$a);
                    }
                }
            }
            DB::commit();
            return view('admin.attendance.index',compact(['attendances','on_date']));
        } catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $at=Attendance::findOrFail($id);
        return view('admin.attendance.edit',compact('at'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
            DB::beginTransaction();
            $at=Attendance::findOrFail($id);
            $arrival=null;
            if(($request['arrival']) && $request['status']==1)
                $arrival= Carbon::parse($request['arrival'])->toTimeString();

            $at->update([
                'status'=>$request['status'],
                'arrival'=>$arrival
            ]);
            DB::commit();
            return redirect('/admin/attendance')->with('success','Attendance Marked');
        } catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
