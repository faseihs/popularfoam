<?php

namespace App\Http\Controllers;

use App\Company;
use App\NonCashTransaction;
use App\User;
use Illuminate\Http\Request;

class AdminDailyCreditsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $transactions=NonCashTransaction::where('debit_id','-1')->where('credit_id','0')->get();
        return view('admin.daily_credits.index',compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('owner', User::class);
        //
        $companies=Company::pluck('name','id')->all();
        return view('admin.daily_credits.create',compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //

        NonCashTransaction::create($request->all());
        return redirect('/admin/daily_credits')->with('success','Credits Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $companies=Company::pluck('name','id')->all();
        $transaction=NonCashTransaction::findOrFail($id);
        return view('admin.daily_credits.edit',compact(['companies','transaction']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        NonCashTransaction::findOrFail($id)->update($request->all());
        return redirect('/admin/daily_credits')->with('success','Credits Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        NonCashTransaction::findOrFail($id)->delete();
        return redirect('/admin/daily_credits')->with('deleted','Credits Deleted');
    }

    public function printCredits($id){
        $transaction = NonCashTransaction::findOrFail($id);
        $pdf= \PDF::loadHTML( view('admin.daily_credits.print',compact(['transaction'])))->setPaper('a4', 'portrait');
        return $pdf->stream();
    }
}
