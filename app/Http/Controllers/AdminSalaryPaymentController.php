<?php

namespace App\Http\Controllers;

use App\SalaryPayment;
use App\User;
use App\Worker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminSalaryPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $salary_payments=SalaryPayment::where('month','>=',Carbon::now()->startOfMonth())->orderBy('worker_id')->orderBy('date','DESC')->get();
        //dd($salary_payments);
        return view('admin.salary_payment.index',compact('salary_payments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('owner', User::class);
        //
        $workers=Worker::pluck('name','id')->all();
        return view('admin.salary_payment.create',compact('workers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //

        $this->validate($request,[
            'worker_id'=>'required',
            'amount'=>'required',
            'type'=>'required',
            'month'=>'required'
        ]);

        try {
            DB::beginTransaction();
            $request['month']=Carbon::parse($request['month'])->toDateString();
            $sp=SalaryPayment::create($request->all());
            $cr=$sp->worker->cashRegister;
            if($request['type']==0){
                $to_pay=$cr->to_pay+$request['amount'];
                $cr->update(['to_pay'=>$to_pay]);
            }
            else{
                $padi=$cr->paid+$request['amount'];
                $cr->update(['paid'=>$padi]);
            }
            DB::commit();
            return redirect('admin/salary_payment')->with('success','Salary Payment Created');
        } catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $payment=SalaryPayment::findOrFail($id);
        $workers=Worker::pluck('name','id')->all();
        return view('admin.salary_payment.edit',compact(['workers','payment']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        $this->validate($request,[
            'worker_id'=>'required',
            'amount'=>'required',
            'type'=>'required',
            'month'=>'required'
        ]);

        try {
            DB::beginTransaction();
            $request['month']=Carbon::parse($request['month'])->toDateString();
            $sp=SalaryPayment::findOrFail($id);
            $cr=$sp->worker->cashRegister;
            if($request['type']==0){
                $old_topay=$cr->to_pay-$sp->amount;
                if($old_topay<0)
                    $old_topay=0;
                $to_pay=$cr->$old_topay+$request['amount'];
                $cr->update(['to_pay'=>$to_pay]);
            }
            else{
                $old_topay=$cr->paid-$sp->amount;
                if($old_topay<0)
                    $old_topay=0;
                $padi=$old_topay+$request['amount'];
                $cr->update(['paid'=>$padi]);
            }
            $sp->update($request->all());
            DB::commit();
            return redirect('admin/salary_payment')->with('success','Salary Payment Edited');
        } catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        try {
            DB::beginTransaction();
            $sp=SalaryPayment::findOrFail($id);
            $cr=$sp->worker->cashRegister;
            if($sp->type==0){
                $old_topay=$cr->to_pay-$sp->amount;
                if($old_topay<0)
                    $old_topay=0;
                $to_pay=$old_topay;
                $cr->update(['to_pay'=>$to_pay]);
            }
            else{
                $old_topay=$cr->paid-$sp->amount;
                if($old_topay<0)
                    $old_topay=0;
                $padi=$old_topay;
                $cr->update(['paid'=>$padi]);
            }
            $sp->delete();
            DB::commit();
            return redirect('admin/salary_payment')->with('deleted','Salary Payment Deleted');
        } catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }
    }

    public function query(Request $request){

        $this->authorize('owner', User::class);
        $salary_payments=null;
        if(isset($request['month'])) {
            $on_date = Carbon::parse($request['month']);
            //dd(substr($on_date->toDateString(),0,7));
            $salary_payments=SalaryPayment::where('month','>=',$on_date->startOfMonth()->toDateString())->where(
                'month','<=',$on_date->endOfMonth()->toDateString()
            )->orderBy('date','DESC')->get();

        }
        else{
            $salary_payments=SalaryPayment::where('month','>=',Carbon::now()->startOfMonth()->toDateString())->where(
                'month','<=',Carbon::now()->endOfMonth()->toDateString()
            )->orderBy('date','DESC')->get();
        }
        $salary_payments=$salary_payments->all();
        // usort($salary_payments,function($a,$b){
            
        //     return $a->worker->name>$b->worker->name;
        // });

        // usort($salary_payments,function($a,$b){
            
        //     if($a->worker->name==$b->worker->name)
        //         return $a->date>$b->date;
        //     else return false;

        // });

        return view('admin.salary_payment.index',compact(['salary_payments','on_date']));

    }
}
