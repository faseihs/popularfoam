<?php

namespace App\Http\Controllers;

use App\Category;
use App\Item;
use App\Photo;
use App\Quality;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class AdminItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $items=Item::orderBy('name')->paginate(10);
        // foreach($items as $item){
        //     $q=$item->package_unit;
        //     if($item->package_unit=="Kilogram")
        //         $q="Kgs";
        //     else if($item->package_unit=="Metres")
        //         $q="Ms";
        //     else if($item->package_unit=="Pieces") 
        //         $q="Pcs";
        //     $item->update(['package_unit'=>$q]);   
        // }
        return view('admin.item.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('owner', User::class);
        //
        $qualities=Quality::pluck('name','id')->all();
        $categories=Category::pluck('name','id')->all();
        return view('admin.item.create',compact(['qualities','categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //
        $this->validate($request,[
            'name'=>'required',
            'quality_id'=>'required',
            'category_id'=>'required',
            'purchase_price'=>'required|numeric',
            'sale_price'=>'required|numeric',
            'package_unit'=>'required',
            'package_amount'=>'required|numeric',
            'default_quantity'=>'required|numeric',
            'covered'=>'required',
            'size_l'=>'required|numeric',
            'size_w'=>'required|numeric',
            'size_h'=>'required|numeric',
            'size_t'=>'required|numeric',
            'opening_date'=>'required',
            'opening_quantity'=>'required|numeric'
        ]);
        $input=$request->all();
        if($input['retail_price']==''){
            $input['retail_price']=0;
        }
        if($input['bar_code']==''){
            $input['bar_code']=0;
        }
        if($input['current_quantity']==''){
            $input['current_quantity']=0;
        }
        if($input['store_quantity']==''){
            $input['store_quantity']=0;
        }
        $input['size']=''.$input['size_l'].'x'.$input['size_w'].'x'.$input['size_h'];
        $photo_id=Item::create(array_except($input,['photo']))->id;
        if($file=$request->file('photo')){
            $name=time().$file->getClientOriginalName();
            $file->move('images',$name);
            $photo=Photo::create(['path'=>$name,'imageable_type'=>'App\Item','imageable_id'=>$photo_id]);
        }

        return redirect('admin/item')->with('success','Item Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('owner', User::class);
        //
        $item=Item::findOrFail($id);
        return view('admin.item.show',compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $qualities=Quality::pluck('name','id')->all();
        $categories=Category::pluck('name','id')->all();
        $item=Item::findOrFail($id);
        return view('admin.item.edit',compact(['item','qualities','categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        $this->validate($request,[
            'name'=>'required',
            'quality_id'=>'required',
            'category_id'=>'required',
            'purchase_price'=>'required|numeric',
            'sale_price'=>'required|numeric',
            'package_unit'=>'required',
            'package_amount'=>'required|numeric',
            'default_quantity'=>'required|numeric',
            'covered'=>'required',
            'size_l'=>'required|numeric',
            'size_w'=>'required|numeric',
            'size_h'=>'required|numeric',
            'size_t'=>'required|numeric',
            'opening_date'=>'required',
            'opening_quantity'=>'required|numeric'
        ]);
        $input=$request->all();
        if($input['retail_price']==''){
            $input['retail_price']=0;
        }
        if($input['bar_code']==''){
            $input['bar_code']=0;
        }
        if($input['current_quantity']==''){
            $input['current_quantity']=0;
        }
        if($input['store_quantity']==''){
            $input['store_quantity']=0;
        }
        $input['size']=''.$input['size_l'].'x'.$input['size_w'].'x'.$input['size_h'];
        $item=Item::findOrFail($id);
        if(isset($input['delPic'])){
            $p=$item->photo;
            unlink(public_path() .'/images/'.$p->path);
            $p->delete();
        }
        if($file=$request->file('photo')){
            $name=time().$file->getClientOriginalName();
            $file->move('images',$name);
            if($item->photo){
                $p=$item->photo;
                unlink(public_path() .'/images/'.$p->path);
                $p->delete();
            }
            $photo=Photo::create(['path'=>$name,'imageable_type'=>'App\Item','imageable_id'=>$id]);
        }
        $item->update(array_except($input,['photo','delPic']));
        return redirect('admin/item')->with('success','Item Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        $item=Item::findOrFail($id);
        if($item->photo) {
            $p = $item->photo;
            unlink(public_path() . '/images/' . $p->path);
            $p->delete();
        }
        $item->delete();
        return redirect('/admin/item')->with('deleted','Item Deleted');
    }


    public function Query($query){
        $items=Item::where('name','LIKE','%'.$query.'%')->paginate(10);
        return view('admin.item.index',compact('items'));

    }


    public function apiData(Request $request){

        $this->authorize('owner', User::class);

        $items = Item::with(["quality","photo","category"]);
        return DataTables::eloquent($items)
        ->addColumn('quality', function (Item $item) {
            return $item->quality->name;
        })
        ->addColumn('category', function (Item $item) {
            return $item->category->name;
        })
        ->toJson();
    }
}
