<?php

namespace App\Http\Controllers;

use App\Payments;
use App\SalesInvoice;
use App\SalesInvoiceDetails;
use Illuminate\Http\Request;
use App\Customer;
use App\OrderBooking;
use App\OrderBookingDetail;
use App\Item;
use App\Transformer\ItemTransformer;
use App\User;
use Illuminate\Support\Facades\DB;

class AdminOrderBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('sales', User::class);

        //
        $customers=Customer::pluck('name','id')->all();
        $order_bookings=OrderBooking::where('cleared','=',0)->orderBy('date','DESC')->orderBy('id','DESC')->get();
        $sorder_bookings= SalesInvoice::where('order_booking_id','<>',null)->orderBy('date','DESC')->orderBy('id','DESC')->get();
        return view('admin.order_booking.index',compact(['customers','order_bookings','sorder_bookings']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        




    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('sales', User::class);

        //dd($request->all());
        try {
            DB::beginTransaction();
            $order=[];
            $order['customer_id']=$request['customer_id'];
            $order['total_price']=$request['total_price'];
            $order['date']=$request['date'];
            $order['name']=$request['name'];
            $order['number']=$request['number'];
            $order['advance']=$request['advance'];
            $order['remaining']=$request['remaining'];
            $order['delivery']=$request['delivery'];
            $order['cleared']=$request['cleared'];
            $sales['paid_amount']=abs($request['remaining']);
            $id=OrderBooking::create($order)->id;
            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->order_booking_id=$id;
                OrderBookingDetail::create((array)$item);
            }

           if($request['cleared']==1){
               $items=json_decode($request['items']);
                $sales=[];
                $sales['customer_id']=$request['customer_id'];
                $sales['total_price']=$request['total_price'];
                $sales['date']=$request['delivery'];
                $sales['name']=$request['name'];
                $sales['number']=$request['number'];
                $sales['pay_later']=0;
                $sales['order_booking_id']=$id;
                $salesID=SalesInvoice::create($sales)->id;
               foreach($items as $item){
                   $item->sales_invoice_id=$salesID;
                   $si=SalesInvoiceDetails::create((array)$item);
                   $it=$si->item;
                   if($it->current_quantity>$si->quantity){
                       $q=$it->current_quantity-$si->quantity;
                       $it->update(['current_quantity'=>$q]);
                   }
                   else if($it->store_quantity>$si->quantity){
                       $q=$it->store_quantity-$si->quantity;
                       $it->update(['store_quantity'=>$q]);

                   }
                   else if(($it->store_quantity+$it->current_quantity)>=$si->quantity){
                       $q=($it->store_quantity+$it->current_quantity)-$si->quantity;
                       $it->update(['store_quantity'=>0,'current_quantity'=>$q]);
                   }
                   else{
                       $q=$it->current_quantity-$si->quantity;
                       $it->update(['current_quantity'=>$q]);
                   }
               }

               $payment['owner_id']=$request['customer_id'];
               $payment['owner_type']='App\Customer';
               $payment['type']='credit';
               $payment['amount']=abs($request['remaining']);
               $payment['pay_later']=0;
               $payment['sales_id']=$salesID;
               $payment['sales_type']='App\SalesInvoice';
               $payment['date']=$request['delivery'];
               Payments::create($payment);

           }

            DB::commit();
            return redirect('/admin/order_booking')->with('success','Order Booking Created');

        }

        catch (\Exception $e){
            DB::rollback();
            dd($e);
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('sales', User::class);
        //
        
        $order=OrderBooking::findOrFail($id);
        $items=$order->items;
        $customer=$order->customer;
        return view('admin.order_booking.show',compact(['order','items','customer']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('sales', User::class);
        //

        $order=OrderBooking::findOrFail($id);
        $items=$order->items;
        $customer=$order->customer;
        $Items=Item::all();
        return view('admin.order_booking.edit',compact(['order','items','customer','Items']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('sales', User::class);
        //
        //dd($request->all());
        try {
            DB::beginTransaction();
            $order=[];
            $order['customer_id']=$request['customer_id'];
            $order['total_price']=$request['total_price'];
            $order['date']=$request['date'];
            $order['name']=$request['name'];
            $order['number']=$request['number'];
            $order['delivery']=$request['delivery'];
            $order['advance']=$request['advance'];
            $order['remaining']=$request['remaining'];
            $order['cleared']=$request['cleared'];
            $order['discount']=$request['discount'];
            $orderB=OrderBooking::findOrFail($id);
            $orderB->update($order);
            foreach($orderB->items as $it){
                $it->delete();
            }
            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->order_booking_id=$id;
                OrderBookingDetail::create((array)$item);
            }
            //Cleared Items Logic

            if($request['cleared']==1){
                $items=json_decode($request['items']);
                $sales=[];
                $sales['customer_id']=$request['customer_id'];
                $sales['total_price']=$request['total_price'];
                $sales['date']=$request['delivery'];
                $sales['name']=$request['name'];
                $sales['number']=$request['number'];
                $sales['pay_later']=0;
                $sales['paid_amount']=abs($request['remaining']);
                $sales['order_booking_id']=$id;

                //Previous Sales Logic
                if($SI=$orderB->salesInvoice){
                    foreach ($SI->items as $it){
                        $item=$it->item;
                        $q=$item->current_quantity+$it->quantity;
                        $item->update(['current_quantity'=>$q]);
                        $it->delete();
                    }

                    $items=json_decode($request['items']);
                    foreach($items as $item){
                        $item->sales_invoice_id=$SI->id;
                        $si=SalesInvoiceDetails::create((array)$item);
                        $it=$si->item;
                        if($it->current_quantity>$si->quantity){
                            $q=$it->current_quantity-$si->quantity;
                            $it->update(['current_quantity'=>$q]);
                        }
                        else if($it->store_quantity>$si->quantity){
                            $q=$it->store_quantity-$si->quantity;
                            $it->update(['store_quantity'=>$q]);

                        }
                        else if(($it->store_quantity+$it->current_quantity)>=$si->quantity){
                            $q=($it->store_quantity+$it->current_quantity)-$si->quantity;
                            $it->update(['store_quantity'=>0,'current_quantity'=>$q]);
                        }
                        else{
                            $q=$it->current_quantity-$si->quantity;
                            $it->update(['current_quantity'=>$q]);
                        }
                    }

                    //Sales Payment Logic

                    $payment=Payments::where('sales_type','=','App\SalesInvoice')->where(
                        'sales_id','=',$SI->id
                    )->get()->first();
                    $payment->update([
                        'amount'=>abs($orderB->remaining),
                        'date'=>$orderB->delivery
                    ]);

                    $SI->update($sales);
                }

                else {
                        $salesID=SalesInvoice::create($sales)->id;
                        foreach($items as $item) {
                        $item->sales_invoice_id = $salesID;
                        $si = SalesInvoiceDetails::create((array)$item);
                        $it = $si->item;
                        if ($it->current_quantity > $si->quantity) {
                            $q = $it->current_quantity - $si->quantity;
                            $it->update(['current_quantity' => $q]);
                        } else if ($it->store_quantity > $si->quantity) {
                            $q = $it->store_quantity - $si->quantity;
                            $it->update(['store_quantity' => $q]);

                        } else if (($it->store_quantity + $it->current_quantity) >= $si->quantity) {
                            $q = ($it->store_quantity + $it->current_quantity) - $si->quantity;
                            $it->update(['store_quantity' => 0, 'current_quantity' => $q]);
                        } else {
                            $q = $it->current_quantity - $si->quantity;
                            $it->update(['current_quantity' => $q]);
                        }
                    }
                    $payment['owner_id']=$request['customer_id'];
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=abs($request['remaining']);
                    $payment['pay_later']=0;
                    $payment['sales_id']=$salesID;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['delivery'];
                    Payments::create($payment);
                }



            }
            //Update to Not Cleared Logic
            else {
                if ($si = $orderB->salesInvoice) {
                    //dd($si);
                    foreach ($si->items as $it) {
                        $item = $it->item;
                        $q = $item->current_quantity + $it->quantity;
                        $item->update(['current_quantity' => $q]);
                        $it->delete();
                    }
                    $payment=Payments::where('sales_type','=','App\SalesInvoice')->where(
                        'sales_id','=',$si->id
                    )->get()->first()->delete();
                    $si->delete();

                }
            }



            DB::commit();
            return redirect('/admin/order_booking')->with('success','Order Booking Updated');

        }

        catch (\Exception $e){
            DB::rollback();
            dd($e);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('sales', User::class);
        //
        try {
            DB::beginTransaction();
            $orderB=OrderBooking::findOrFail($id);
            foreach($orderB->items as $it){
                $it->delete();
            }
            if ($si = $orderB->salesInvoice) {
                //dd($si);
                foreach ($si->items as $it) {
                    $item = $it->item;
                    $q = $item->current_quantity + $it->quantity;
                    $item->update(['current_quantity' => $q]);
                    $it->delete();
                }
                $payment=Payments::where('sales_type','=','App\SalesInvoice')->where(
                    'sales_id','=',$si->id
                )->get()->first()->delete();
                $si->delete();

            }

            $orderB->delete();
            DB::commit();
            return redirect('/admin/order_booking')->with('deleted','Order Booking Deleted');

        }

        catch (\Exception $e){
            DB::rollback();
            dd($e);
        } 
    }

    public function createC(Request $request){

        $this->authorize('sales', User::class);

        $customer=null;
        if($request['customer_id']==0)
            $customer=null;
        else {
            $customer = Customer::findOrFail($request['customer_id']);
        }
        $items=Item::all();
        return view('admin.order_booking.create',compact(['customer','items']));
    }


    public function printOrder(Request $request,$id){

        $this->authorize('sales', User::class);

        $order_booking=OrderBooking::findOrFail($id);
        $items=$order_booking->items;
        $print_size = "a4";

        if($request->has("print_size"))
            $print_size= $request->print_size;

        if($print_size == "80"){
            \PDF::setOptions(['dpi'=>72]);
            $pdf= \PDF::loadHTML(view('admin.order_booking.print_80',compact(['order_booking','items'])))->setPaper(array(0,0,204,650));
            return $pdf->stream();
        }

        else {
            $pdf= \PDF::loadHTML(view('admin.order_booking.print',compact(['order_booking','items'])))->setPaper('a4', 'portrait');;
            return $pdf->stream();
        }
       
    }

    public function apiEdit($id){

        $this->authorize('sales', User::class);

        $order_booking=OrderBooking::findOrFail($id);
        $customer=$order_booking->customer;
        $invoice_items = $order_booking->items()->with("item")->get();
        $items = fractal($invoice_items->pluck("item")->all(),new ItemTransformer($customer->id))->toArray();

        return ['order_booking'=>$order_booking,'items'=>$items,'customer'=>$customer,'invoice_items'=>$invoice_items];
    }

    public function apiUpdate(Request $request, $id)
    {

        $this->authorize('sales', User::class);
        //$order->updateOrder($request->all());
        //
        //dd($request->all());
        try {
            DB::beginTransaction();
            $order=[];
            $order['customer_id']=$request['customer_id'];
            $order['total_price']=$request['total_price'];
            $order['date']=$request['date'];
            $order['name']=$request['name'];
            $order['number']=$request['number'];
            $order['delivery']=$request['delivery'];
            $order['advance']=$request['advance'];
            $order['remaining']=$request['remaining'];
            $order['cleared']=$request['cleared'];
            $order['comments']=$request['comments'];
            $order['discount']=$request['discount'];
            
            $orderB=OrderBooking::findOrFail($id);
            $orderB->update($order);
            foreach($orderB->items as $it){
                $it->delete();
            }
            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->order_booking_id=$id;
                OrderBookingDetail::create((array)$item);
            }
            //Cleared Items Logic

            if($request['cleared']==1){
                $items=json_decode($request['items']);
                $sales=[];
                $sales['customer_id']=$request['customer_id'];
                $sales['total_price']=$request['total_price'];
                $sales['date']=$request['delivery'];
                $sales['name']=$request['name'];
                $sales['number']=$request['number'];
                $sales['pay_later']=0;
                $sales['paid_amount']=abs($request['remaining']);
                $sales['order_booking_id']=$id;

                //Previous Sales Logic
                if($SI=$orderB->salesInvoice){
                    foreach ($SI->items as $it){
                        $item=$it->item;
                        $q=$item->current_quantity+$it->quantity;
                        $item->update(['current_quantity'=>$q]);
                        $it->delete();
                    }

                    $items=json_decode($request['items']);
                    foreach($items as $item){
                        $item->sales_invoice_id=$SI->id;
                        $si=SalesInvoiceDetails::create((array)$item);
                        $it=$si->item;
                        if($it->current_quantity>$si->quantity){
                            $q=$it->current_quantity-$si->quantity;
                            $it->update(['current_quantity'=>$q]);
                        }
                        else if($it->store_quantity>$si->quantity){
                            $q=$it->store_quantity-$si->quantity;
                            $it->update(['store_quantity'=>$q]);

                        }
                        else if(($it->store_quantity+$it->current_quantity)>=$si->quantity){
                            $q=($it->store_quantity+$it->current_quantity)-$si->quantity;
                            $it->update(['store_quantity'=>0,'current_quantity'=>$q]);
                        }
                        else{
                            $q=$it->current_quantity-$si->quantity;
                            $it->update(['current_quantity'=>$q]);
                        }
                    }

                    //Sales Payment Logic

                    $payment=Payments::where('sales_type','=','App\SalesInvoice')->where(
                        'sales_id','=',$SI->id
                    )->get()->first();
                    $payment->update([
                        'amount'=>abs($orderB->remaining),
                        'date'=>$orderB->delivery
                    ]);

                    $SI->update($sales);
                }

                else {
                        $salesID=SalesInvoice::create($sales)->id;
                        foreach($items as $item) {
                        $item->sales_invoice_id = $salesID;
                        $si = SalesInvoiceDetails::create((array)$item);
                        $it = $si->item;
                        if ($it->current_quantity > $si->quantity) {
                            $q = $it->current_quantity - $si->quantity;
                            $it->update(['current_quantity' => $q]);
                        } else if ($it->store_quantity > $si->quantity) {
                            $q = $it->store_quantity - $si->quantity;
                            $it->update(['store_quantity' => $q]);

                        } else if (($it->store_quantity + $it->current_quantity) >= $si->quantity) {
                            $q = ($it->store_quantity + $it->current_quantity) - $si->quantity;
                            $it->update(['store_quantity' => 0, 'current_quantity' => $q]);
                        } else {
                            $q = $it->current_quantity - $si->quantity;
                            $it->update(['current_quantity' => $q]);
                        }
                    }
                    $payment['owner_id']=$request['customer_id'];
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=abs($request['remaining']);
                    $payment['pay_later']=0;
                    $payment['sales_id']=$salesID;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['delivery'];
                    Payments::create($payment);
                }



            }
            //Update to Not Cleared Logic
            else {
                if ($si = $orderB->salesInvoice) {
                    //dd($si);
                    foreach ($si->items as $it) {
                        $item = $it->item;
                        $q = $item->current_quantity + $it->quantity;
                        $item->update(['current_quantity' => $q]);
                        $it->delete();
                    }
                    $payment=Payments::where('sales_type','=','App\SalesInvoice')->where(
                        'sales_id','=',$si->id
                    )->get()->first()->delete();
                    $si->delete();

                }
            }



            DB::commit();
            return response()->json($id,201);

        }

        catch (\Exception $e){
            DB::rollback();
            dd($e);
        } 
    }

}
