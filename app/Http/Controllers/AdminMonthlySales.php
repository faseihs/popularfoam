<?php

namespace App\Http\Controllers;

use App\SalesInvoice;
use App\SalesReturn;
use App\PurchaseInvoice;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class AdminMonthlySales extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        // $sales_invoices=SalesInvoice::all();
        // $sales_returns =SalesReturn::all();
        // $purchase_invoices=PurchaseInvoice::all();

        $sales_invoices=[];
        $sales_returns = [];
        $purchase_invoices= [];
        return view('admin.reports.sales_reports.monthly',compact(['sales_invoices','sales_returns','purchase_invoices']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //
        $sales_invoices=null;
        $sales_returns=null;
        $active=null;
        $s_val=null;
        $r_val=null;
        $p_val=null;
        if($request->s_option){
            if($request['s_option']==1)
                $sales_invoices=SalesInvoice::all();
            elseif($request['s_option']==2)
                $sales_invoices=SalesInvoice::where('date','=',$request['on_date'])->get();
            else $sales_invoices=SalesInvoice::where('date','>=',$request['from_date'])->where('date','<=',$request['to_date'])->get();
            $sales_returns=SalesReturn::all();
            $purchase_invoices=PurchaseInvoice::all();
            $active=1;
            $s_val=$request['s_option'];
        }

        if($request->r_option){
            if($request['r_option']==1)
                $sales_returns=SalesReturn::all();
            elseif($request['r_option']==2)
                $sales_returns=SalesReturn::where('date','=',$request['on_date'])->get();
            else $sales_returns=SalesReturn::where('date','>',$request['from_date'])->where('date','<',$request['to_date'])->get();
            $sales_invoices=SalesInvoice::all();
            $purchase_invoices=PurchaseInvoice::all();
            $active=2;
            $r_val=$request['r_option'];
        }
        
        if($request->p_option){
            if($request['p_option']==1)
                $purhase_invoices=PurchaseInvoice::all();
            elseif($request['p_option']==2)
                $purchase_invoices=PurchaseInvoice::where('date','=',$request['on_date'])->get();
            else $purchase_invoices=PurchaseInvoice::where('date','>',$request['from_date'])->where('date','<',$request['to_date'])->get();
            $sales_invoices=SalesInvoice::all();
            $sales_returns=SalesReturn::all();
            $active=3;
            $p_val=$request['p_option'];
        }

        return view('admin.reports.sales_reports.monthly',compact(['purchase_invoices','p_val','sales_invoices','sales_returns','active','s_val','r_val']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function printReport(Request $request){
        
        $this->authorize('owner', User::class);
        
        ini_set('max_execution_time', 240);
        ini_set('memory_limit', '-1');
        $onDate=null;
        $fromDate=null;
        $toDate=null;
        $sOption=$request->s_option;
        $sales_invoices=array();
        if($request->s_option){
            if($request['s_option']==1)
                $sales_invoices=SalesInvoice::all();
            elseif($request['s_option']==2){
                $sales_invoices=SalesInvoice::where('date','=',$request['on_date'])->get();
                $onDate=$request['on_date'];
            }

            else {
                $sales_invoices=SalesInvoice::where('date','>=',$request['from_date'])->where('date','<=',$request['to_date'])->get();
                $fromDate=$request['from_date'];
                $toDate=$request['to_date'];
            }
            $sales_returns=SalesReturn::all();
            $purchase_invoices=PurchaseInvoice::all();
            $active=1;
            $s_val=$request['s_option'];
        }
        /*$pdf= \PDF::loadHTML(view('admin.reports.sales_monthly_print',compact(['sales_invoices','sOption','onDate','fromDate','toDate'])))->setPaper('a4', 'portrait');;
        return $pdf->stream();*/

        Excel::create('Sales Report', function($excel) use ($sales_invoices,$sOption,$onDate,$fromDate,$toDate) {


            $excel->sheet('Sheet1', function($sheet) use($sales_invoices,$sOption,$onDate,$fromDate,$toDate) {
                if($sOption==1)
                    $sheet->row(1,array(
                        'Date','All Months'
                    ));
                else if($sOption==2)
                    $sheet->row(1,array(
                        'On Date',$onDate
                    ));
                else if($sOption==3)
                    $sheet->row(1,array(
                        'From Date',$fromDate,'-','To Date',$toDate
                    ));
                $sheet->row(2,array(
                    'ID','Date','Customer','Items','Bill'
                ));
                foreach ($sales_invoices as $i=>$si){
                    $sheet->row($i+3, array(
                        $si->id, Carbon::parse($si->date)->format('d-m-Y'),$si->cust,count($si->items),number_format($si->total_price,0,'.',',')
                    ));
                }

            });

        })->download('xls');
    }
}
