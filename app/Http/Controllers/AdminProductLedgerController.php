<?php

namespace App\Http\Controllers;

use App\Item;
use App\PurchaseInvoiceDetail;
use App\Quality;
use App\SalesInvoiceDetails;
use App\SalesReturnDetails;
use Illuminate\Http\Request;
use App\PurchaseReturnDetails;
use App\User;
use Illuminate\Support\Facades\Response;


class AdminProductLedgerController extends Controller
{
    //


    public function index(){
        $this->authorize('owner', User::class);
        return view('admin.product_ledger.index');
    }


    public  function  getPayment(){

        $this->authorize('owner', User::class);

       $returnArray=[];
        $si=SalesInvoiceDetails::all()->all();
        $sr=SalesReturnDetails::all()->all();
        $pi=PurchaseInvoiceDetail::all()->all();
        $pr=PurchaseReturnDetails::all()->all();

        foreach ($si as $s){
            $s->type="salesInvoice";
            array_push($returnArray,$s);

        }

        foreach ($sr as $s){
            $s->type="salesReturn";
            array_push($returnArray,$s);

        }
        foreach ($pi as $s){
            $s->type="purchaseInvoice";
            array_push($returnArray,$s);
        }

        foreach ($pr as $s){
            $s->type="purchaseReturn";
            array_push($returnArray,$s);
        }

        usort($returnArray,function($a,$b){
            return $a->dat>$b->dat;
        });

        $response = Response::make($returnArray, 200);
        $response->header('Content-Length', strlen(json_encode($returnArray)));
        return $response;
    }

    public  function  getPaymentByItem($id){

        $this->authorize('owner', User::class);

        $item=Item::find($id);
        $returnArray=[];
        $si=$item->salesInvoiceDetails->all();
        $sr=$item->salesReturns->all();
        $pi=$item->purchaseInvoiceDetails->all();
        $pr=$item->purchaseReturns->all();

        // if($item->opening_quantity && $item->opening_quantity>0 )
        //     array_push($returnArray,[
        //         "type"=>"purchaseInvoice",
        //         "date"=>$item->opening_date,
        //         "description"=>"Opening Entry",
        //         "quantity"=>$item->opening_quantity
        //     ]);

        foreach ($si as $s){
            $s->type="salesInvoice";
            array_push($returnArray,$s);

        }

        foreach ($sr as $s){
            $s->type="salesReturn";
            array_push($returnArray,$s);

        }
        foreach ($pi as $s){
            $s->type="purchaseInvoice";
            array_push($returnArray,$s);
        }

        foreach ($pr as $s){
            $s->type="purchaseReturn";
            array_push($returnArray,$s);
        }

        usort($returnArray,function($a,$b){
            return $a->dat>$b->dat;
        });

        $response = Response::make($returnArray, 200);
        $response->header('Content-Length', strlen(json_encode($returnArray)));
        return $response;

    }

    public function getQuality(){
        $this->authorize('owner', User::class);
        return Quality::orderBy('name')->get();
    }

    public function getItem($id){
        $this->authorize('owner', User::class);
        $items=Quality::findOrFail($id)->items->all();
        usort($items,function($a,$b){
            return $a->name>$b->name;
        });
        return $items;
    }
}
