<?php

namespace App\Http\Controllers;

use App\Account;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminAccountController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $accounts = Account::orderBy('name')->get();;
        return view('admin.account.old.index',compact('accounts'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $this->authorize('owner', User::class);
        return view('admin.account.old.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
        $this->authorize('owner', User::class);
        $this->validate($request,[
            'name'=>'required',
            'type'=>'required'
        ]);
        Account::create($request->all());
        return redirect('admin/account')->with('success','Account Created');
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $this->authorize('owner', User::class);
        //
        $account=Account::findOrFail($id);
        $payments=$account->payments;
        return view('admin.account.old.show',compact(['account','payments']));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $account=Account::findOrFail($id);
        return view('admin.account.old.edit',compact('account'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        $this->validate($request,[
            'name'=>'required',
            'type'=>'required'
        ]);
        Account::findOrFail($id)->update($request->all());
        return redirect('admin/account')->with('success','Account Updated');
    }


    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        Account::find($id)->delete();
        return redirect('/admin/account')->with('deleted','Account Deleted');
    }

    public function query(Request $request,$id){
        $this->authorize('owner', User::class);
        if($request['date']==null)
            return redirect('/admin/account/'.$id);
        $account=Account::findOrFail($id);
        $Payments=$account->payments;
        $payments=[];
        $date=new Carbon(date($request['date']));
        //dd($date);
        foreach ($Payments as $p){

            if(Carbon::parse($p->date)->diffInDays($date)==0)
                array_push($payments,$p);
        }


        return view('admin.account.old.show',compact(['account','payments','date']));
    }
}
