<?php

namespace App\Http\Controllers;

use App\CashRegister;
use App\Customer;
use App\Item;
use App\OrderBooking;
use App\OrderBookingDetail;
use App\Payments;
use App\SalesInvoice;
use App\SalesInvoiceDetails;
use App\SalesReturn;
use App\SalesReturnDetails;
use App\Transformer\ItemTransformer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PointOfSalesController extends Controller
{
    //

    public function index(){
        $this->authorize('sales', User::class);
        return view('admin.point-of-sales');
    }
    public function items(Request $request,$id){
        $this->authorize('sales', User::class);

        $items = Item::query();
        
        if($request->has("search") && !empty($request->search)){
            $items = $items->where("name","LIKE","%$request->search%");
        }
        $items=$items->orderBy('name')->with("quality")->get();
        $items = $items->filter(function($obj){
            return $obj->quality != null;
        });
        $items=$items->sort(function ($a, $b) {
            // sort by column1 first, then 2, and so on
            return strcmp($a->quality?$a->quality->name:'', $b->quality?$b->quality->name:'')
                ?: strcmp($a->name, $b->name);
        });
        return fractal($items,new ItemTransformer($id))->toArray();
    }

    public function customers(){
        $this->authorize('sales', User::class);
        $customers= Customer::orderBy('name')->get();
        return $customers;
    }


    public function Invoice(Request $request){

        $this->authorize('sales', User::class);
        
        try{
            DB::beginTransaction();
            $sales=[];
            $sales['customer_id']=$request['customer_id'];
            $sales['total_price']=$request['total_price'];
            $sales['paid_amount']=$request['paid_amount'];
            $sales['returned']=$request['returned'];
            $sales['given']=$request['given'];
            $sales['remaining']=$request['remaining'];
            $sales['name']=$request['c_name'];
            $sales['discount']=$request['discount'];
            $sales['number']=$request['number'];
            $sales['comments']=$request['comments'];
            if($request['pay_later']==1){
                $sales['pay_later']=1;
                $sales['to_pay']=abs($request['returned']);
            }
            else{
                $sales['pay_later']=0;
                $sales['to_pay']=0;
            }
            $sales['date']=$request['date'];
            $sales["user_id"]=Auth::user()->id;
            $sales_invoice=SalesInvoice::create($sales);
            $id=$sales_invoice->id;
            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->sales_invoice_id=$id;
                $si=SalesInvoiceDetails::create((array)$item);
                $it=$si->item;
                if($it->current_quantity>$si->quantity){
                    $q=$it->current_quantity-$si->quantity;
                    $it->update(['current_quantity'=>$q]);
                }
                else if($it->store_quantity>$si->quantity){
                    $q=$it->store_quantity-$si->quantity;
                    $it->update(['store_quantity'=>$q]);

                }
                else if(($it->store_quantity+$it->current_quantity)>=$si->quantity){
                    $q=($it->store_quantity+$it->current_quantity)-$si->quantity;
                    $it->update(['store_quantity'=>0,'current_quantity'=>$q]);
                }
                else{
                    $q=$it->current_quantity-$si->quantity;
                    $it->update(['current_quantity'=>$q]);
                }
            }
            if($request['customer_id']==0){
                if($sales['to_pay']==0){
                    if($cr=CashRegister::where('owner_id','=',0)->where('owner_type','=','App\Customer')->get()->first()){
                        $payment=[];
                        $payment['owner_id']=0;
                        $payment['owner_type']='App\Customer';
                        $payment['type']='credit';
                        $payment['amount']=$request['total_price'];
                        $payment['pay_later']=0;
                        $payment['sales_id']=$id;
                        $payment['sales_type']='App\SalesInvoice';
                        $payment['date']=$request['date'];
                        Payments::create($payment);
                        $paid = $cr->paid + $request['total_price'];
                        $cr->update(['paid' => $paid]);

                    }
                    else{
                        $cr=CashRegister::create(['owner_id'=>0,'paid'=>0,'to_pay'=>0,'owner_type'=>'App\Customer','received'=>0,'to_receive'=>0]);
                        $payment=[];
                        $payment['owner_id']=0;
                        $payment['owner_type']='App\Customer';
                        $payment['type']='credit';
                        $payment['amount']=$request['total_price'];
                        $payment['pay_later']=0;
                        $payment['sales_id']=$id;
                        $payment['sales_type']='App\SalesInvoice';
                        $payment['date']=$request['date'];
                        Payments::create($payment);
                        $paid = $cr->paid + $request['total_price'];
                        $cr->update(['paid' => $paid]);
                    }
                }
            }
            else{
                if($sales['to_pay']==0){
                    $customer=Customer::findOrFail($request['customer_id']);
                    $cr=$customer->cashRegister;
                    $paid = $cr->paid + $request['total_price'];
                    $cr->update(['paid' => $paid]);
                    $payment=[];
                    $payment['owner_id']=$customer->id;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=$request['total_price'];
                    $payment['pay_later']=0;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];
                    Payments::create($payment);
                }
                else{
                    $customer=Customer::findOrFail($request['customer_id']);
                    $cr=$customer->cashRegister;
                    $to_pay = $cr->to_pay + $sales['to_pay'];
                    $paid = $cr->paid + $sales['paid_amount'];
                    $cr->update(['paid' => $paid,'to_pay'=>$to_pay]);
                    $payment=[];
                    $payment['owner_id']=$customer->id;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=$sales['paid_amount'];
                    $payment['pay_later']=0;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];
                    Payments::create($payment);

                    /*$payment=[];
                    $payment['owner_id']=$customer->id;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=$sales['to_pay'];
                    $payment['pay_later']=1;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];
                    Payments::create($payment);*/

                }
            }

            DB::commit();

            /*if($request->has('print')) {
                $items = $sales_invoice->items;
                $customer = null;
                if ($sales_invoice->customer) {
                    $customer = $sales_invoice->customer;
                }
                //return view('admin.sales_invoice.print',compact(['sales_invoice','items','customer']));
                $pdf = \PDF::loadHTML(view('admin.sales_invoice.print', compact(['sales_invoice', 'items', 'customer'])))->setPaper('a4', 'portrait');;
                return $pdf->download();
            }*/


            return response()->json($id,201);
        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json($e,500);

        }
    }

    public function Booking(Request $request)
    {
        $this->authorize('sales', User::class);
        //dd($request->all());
        try {
            DB::beginTransaction();
            $order=[];
            $order['customer_id']=$request['customer_id'];
            $order['total_price']=$request['total_price'];
            $order['date']=$request['date'];
            $order['name']=$request['name'];
            $order['number']=$request['number'];
            $order['advance']=$request['advance'];
            $order['remaining']=$request['remaining'];
            $order['delivery']=$request['delivery'];
            $order['cleared']=$request['cleared'];
            $order['comments']=$request['comments'];
            $order["discount"]=$request["discount"];
            $sales['paid_amount']=abs($request['remaining']);
            $order["user_id"]=Auth::user()->id;
            $order_booking =OrderBooking::create($order);
            $id=$order_booking->id;
            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->order_booking_id=$id;
                OrderBookingDetail::create((array)$item);
            }

            if($request['cleared']==1){
                $items=json_decode($request['items']);
                $sales=[];
                $sales['customer_id']=$request['customer_id'];
                $sales['total_price']=$request['total_price'];
                $sales['date']=$request['delivery'];
                $sales['name']=$request['name'];
                $sales['number']=$request['number'];
                $sales['pay_later']=0;
                $sales['order_booking_id']=$id;
                $salesID=SalesInvoice::create($sales)->id;
                foreach($items as $item){
                    $item->sales_invoice_id=$salesID;
                    $si=SalesInvoiceDetails::create((array)$item);
                    $it=$si->item;
                    if($it->current_quantity>$si->quantity){
                        $q=$it->current_quantity-$si->quantity;
                        $it->update(['current_quantity'=>$q]);
                    }
                    else if($it->store_quantity>$si->quantity){
                        $q=$it->store_quantity-$si->quantity;
                        $it->update(['store_quantity'=>$q]);

                    }
                    else if(($it->store_quantity+$it->current_quantity)>=$si->quantity){
                        $q=($it->store_quantity+$it->current_quantity)-$si->quantity;
                        $it->update(['store_quantity'=>0,'current_quantity'=>$q]);
                    }
                    else{
                        $q=$it->current_quantity-$si->quantity;
                        $it->update(['current_quantity'=>$q]);
                    }
                }

                $payment['owner_id']=$request['customer_id'];
                $payment['owner_type']='App\Customer';
                $payment['type']='credit';
                $payment['amount']=abs($request['remaining']);
                $payment['pay_later']=0;
                $payment['sales_id']=$salesID;
                $payment['sales_type']='App\SalesInvoice';
                $payment['date']=$request['delivery'];
                Payments::create($payment);

            }

            DB::commit();
            return response()->json($id,201);

        }

        catch (\Exception $e){
            DB::rollback();
            return response()->json($e,500);
        }
    }


    public function salesReturn(Request $request){

        $this->authorize('sales', User::class);

        try {
            DB::beginTransaction();
            $sales = [];
            if($request->sales_invoice_id)
                $sales['sales_invoice_id'] = $request['sales_invoice_id'];
            $sales['customer_id'] = $request['customer_id'];
            $sales['total_price'] = $request['total_price'];
            $sales['paid_amount'] = $request['paid_amount'];
            $sales['pay_later'] = $request['pay_later'];
            $sales['name'] = $request['name'];
            $sales['number'] = $request['number'];
            $sales['date'] = $request['date'];
            $sales["user_id"]=Auth::user()->id;
            $id = SalesReturn::create($sales)->id;
            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->sales_return_id=$id;
                $iTit=SalesReturnDetails::create((array)$item);
                $ITEM=$iTit->item;
                if($iTit->return_type=='amount'){
                    $q=$ITEM->current_quantity+$item->quantity;
                    $ITEM->update(['current_quantity'=>$q]);
                }
            }
            if($request['customer_id']==0) {
                if ($cr = CashRegister::where('owner_id', '=', 0)->where('owner_type', '=', 'App\Customer')->get()->first()) {
                    $payment = [];
                    $payment['owner_id'] = 0;
                    $payment['owner_type'] = 'App\Customer';
                    $payment['type'] = 'credit';
                    $payment['amount'] = $request['total_price'];
                    $payment['pay_later'] = 0;
                    $payment['sales_id'] = $id;
                    $payment['sales_type'] = 'App\SalesReturn';
                    $payment['date'] = $request['date'];
                    Payments::create($payment);
                    $received = $cr->received + $request['paid_amount'];
                    $to_receive = $cr->to_receive + $request['paid_amount'];
                    if ($request['pay_later'] == 0)
                        $cr->update(['received' => $received]);
                    else $cr->update(['to_receive' => $to_receive]);

                    if ($request['pay_later'] == 1) {
                        $payment = [];
                        $payment['owner_id'] = 0;
                        $payment['owner_type'] = 'App\Customer';
                        $payment['type'] = 'debit';
                        $payment['amount'] = $request['total_price'];
                        $payment['pay_later'] = 0;
                        $payment['sales_id'] = $id;
                        $payment['sales_type'] = 'App\SalesReturn';
                        $payment['date'] = $request['date'];
                    }
                }
            }
            else{
                $customer=Customer::findOrFail($request['customer_id']);
                $cr=$customer->cashRegister;
                $received = $cr->received + $request['paid_amount'];
                $to_receive=$cr->to_receive+$request['paid_amount'];
                if($request['pay_later']==0)
                    $cr->update(['received' => $received]);
                else $cr->update(['to_receive' => $to_receive]);
                $payment=[];
                $payment['owner_id']=$customer->id;
                $payment['owner_type']='App\Customer';
                $payment['type']='credit';
                $payment['amount']=$request['total_price'];
                $payment['pay_later']=0;
                $payment['sales_id']=$id;
                $payment['sales_type']='App\SalesReturn';
                $payment['date']=$request['date'];
                Payments::create($payment);
                if($request['pay_later']==1){
                    $payment=[];
                    $payment['owner_id']=$customer->id;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='debit';
                    $payment['amount']=$request['total_price'];
                    $payment['pay_later']=0;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesReturn';
                    $payment['date']=$request['date'];
                    Payments::create($payment);
                }
            }
            DB::commit();

            return response()->json($id,201);
        }
        catch (\Exception $e){
            DB::rollback();
            return response()->json($e,500);
        }
    }


}
