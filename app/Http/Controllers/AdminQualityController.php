<?php

namespace App\Http\Controllers;

use App\Company;
use App\Discount;
use App\Quality;
use Illuminate\Http\Request;
use App\Item;
use App\User;

class AdminQualityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $qualities = Quality::orderBy('name')->get();;
        return view('admin.quality.index',compact('qualities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('owner', User::class);
        //
        $companies=Company::get();
        return view('admin.quality.create',compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //
        $this->validate($request,[
            'name'=>'required',
            'company_id'=>'required'
        ]);
        $quality =  Quality::create($request->all());
        Discount::create([
            "quality_id" => $quality->id,
            "item_id" => 0,
            "covered" => 0,
            "uncovered" => 0
        ]);
        return redirect('admin/quality')->with('success','Quality Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('owner', User::class);
        //
        $quality=Quality::findOrFail($id);
        $items = $quality->items()->orderBy("name")->get();
        return view('admin.quality.show',compact(['quality','items']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $companies=Company::get();
        $quality=Quality::findOrFail($id);
        return view('admin.quality.edit',compact(['quality','companies']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        Quality::findOrFail($id)->update($request->all());
        return redirect('admin/quality')->with('success','Quality Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        Quality::find($id)->delete();
        return redirect('/admin/quality')->with('deleted','Quality Deleted');
    }

    public function editpost(Request $request){
        $this->authorize('owner', User::class);
        try{
        $input=$request['Item'];
        $item=Item::findOrFail($input['id']);
        $item->update(['purchase_price'=>$input['pprice'],'sale_price'=>$input['sprice']]);
        return ['status'=>'OK'];
        }
        catch(Exception $e){
            return $e;
        }
    }
}
