<?php

namespace App\Http\Controllers;

use App\User;
use App\Vehicle;
use Illuminate\Http\Request;

class AdminVehicleController extends Controller
{
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $vehicles = Vehicle::orderBy('name')->get();;
        return view('admin.vehicle.index',compact('vehicles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('owner', User::class);
        //
        return view('admin.vehicle.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //
        $this->validate($request,[
            'name'=>'required',
            'size'=>'required|numeric'
        ]);
        Vehicle::create($request->all());
        return redirect('admin/vehicle')->with('success','Vehicle Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $vehicle=Vehicle::findOrFail($id);
        return view('admin.vehicle.edit',compact('vehicle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        $this->validate($request,[
            'name'=>'required',
            'size'=>'required|numeric'
        ]);
        Vehicle::findOrFail($id)->update($request->all());
        return redirect('admin/vehicle')->with('success','Vehicle Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        Vehicle::find($id)->delete();
        return redirect('/admin/vehicle')->with('deleted','Vehicle Deleted');
    }
}
