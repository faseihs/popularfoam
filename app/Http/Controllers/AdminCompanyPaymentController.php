<?php

namespace App\Http\Controllers;

use App\Account;
use App\CashRegister;
use App\Company;
use App\Payments;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class AdminCompanyPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $company_payments=Payments::where('owner_type','=','App\Company')->where('sales_type','=',null)->orderBy('created_at','DESC')->get();
        // $company_payments=[];
        // foreach($company_payment as $cp){
        //     if($cp->sales_type){
                
        //     }
        // }
        return view('admin.company_payment.index',compact('company_payments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('owner', User::class);
        //
        $accounts=Account::pluck('name','id')->all();
        $companies=Company::pluck('name','id')->all();
        return view('admin.company_payment.create',compact(['accounts','companies']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //
        try{
            DB::beginTransaction();
        $balance=0;
        $input=$request->all();
        $input['owner_type']='App\Company';
        $input['type']=Account::findOrFail($input['account_id'])->type;
        if(!$input['owner_id']){
            $newC=Company::create(['name'=>$input['name']]);
            $input['owner_id']=$newC->id;
            $cr=CashRegister::create(['owner_id'=>$input['owner_id'],'paid'=>0,'to_pay'=>0,'owner_type'=>'App\Company','received'=>0,'to_receive'=>0]);
            if($input['type']=='debit'){
                if($input['pay_later'] == 0)
                $cr->update(['received' => $input['amount']]);
                else $cr->update(['to_receive'=>$input['amount']]);
            }
            else {
                if($input['pay_later'] == 0)
                $cr->update(['paid' => $input['amount']]);
                else $cr->update(['to_pay'=>$input['amount']]);
            }


            $pa['owner_type']='App\Company';
            $pa['owner_id']=$input['owner_id'];
            $pa['amount']=$input['amount'];
            $pa['type']=$input['type'];
            $pa['pay_later']=$input['pay_later'];
            $pa['prev_to_pay']=0;
            $pa['comments']=$input['comments'];
            $pa['balance']=$balance;
            $pa['date']=$input['date'];
            $pa['account_id']=$input['account_id'];
            $pa['purchase_invoice_id'] = $input['purchase_invoice_id'];
            Payments::create($pa);
        }

        else{
            $company=Company::findOrFail($input['owner_id']);
            if($cr=$company->cashRegister) {
            }
            else $cr=CashRegister::create(['owner_id'=>$input['owner_id'],'paid'=>0,'to_pay'=>0,'owner_type'=>'App\Company','received'=>0]);
            $to_pay=$cr->to_pay;
            $paid=$cr->paid;
            $to_rec=$cr->to_receive;
            $rec=$cr->received;
            $prev_to_pay=0;
            if($input['type']=='debit'){
                if ($input['pay_later'] == 1) {
                    $to_rec = $cr->to_receive + $input['amount'];
                  
                } 
                else{
                    $rec = $cr->received + $input['amount'];
                    $prev_to_pay=min($to_rec,$input['amount']);
                    $to_rec=$cr->to_receive - $input['amount'];
                    if($to_rec<0)
                        $to_rec=0;
                    
                }
            }
            else {
                    if ($input['pay_later'] == 0) {
                        $paid = $cr->paid + $input['amount'];
                        $prev_to_pay=min($to_pay,$input['amount']);
                        $to_pay = $cr->to_pay - $input['amount'];
                        if ($to_pay < 0)
                            $to_pay = 0;
                        
                       
    
                    }
                    else{
                        $to_pay = $cr->to_pay + $input['amount'];
                    }
            }
    
            $cr->update([
                'to_pay'=>$to_pay,
                'paid'=>$paid,
                'received'=>$rec,
                'to_receive'=>$to_rec,
                
                
            ]);

            $pa['owner_type']='App\Company';
            $pa['owner_id']=$input['owner_id'];
            $pa['amount']=$input['amount'];
            $pa['type']=$input['type'];
            $pa['pay_later']=$input['pay_later'];
            $pa['prev_to_pay']=$prev_to_pay;
            $pa['comments']=$input['comments'];
            $pa['balance']=$balance;
            $pa['date']=$input['date'];
            $pa['account_id']=$input['account_id'];
            //$pa['purchase_invoice_id'] = $input['purchase_invoice_id'];
            $photo_id=Payments::create($pa)->id;
    
        }

        if($file=$request->file('photo')){
                $name=time().$file->getClientOriginalName();
                $file->move('images',$name);
                $photo=Photo::create(['path'=>$name,'imageable_type'=>'App\Payments','imageable_id'=>$photo_id]);
        }
        
        DB::commit();
        return redirect('/admin/company_payment')->with('success','Payment Created');
        }
        catch(\Exception $e){
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('owner', User::class);
        //
        $payment=Payments::findOrFail($id);
        return view('admin.company_payment.show',compact('payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        //$payment=Payments::where('owner_type','=','App\Company')->where('owner_id','=',$id)->get()->first();
        $payment=Payments::findOrFail($id);
        $accounts=Account::pluck('name','id')->all();
        $companies=Company::pluck('name','id')->all();
        return view('admin.company_payment.edit',compact(['accounts','companies','payment']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //dd($request->all());
        try {
            DB::beginTransaction();
        $input=$request->all();
        $input['owner_type']='App\Company';
        $input['type']=Account::findOrFail($input['account_id'])->type;
        $company=Company::findOrFail($input['owner_id']);
        $cr=$company->cashRegister;
        $toRec=$cr->to_receive;
        $Rec=$cr->received;
        $toPay=$cr->to_pay;
        $Paid=$cr->paid;
        $old_payment=Payments::findOrFail($id);
        if($old_payment->type=='debit'){
            if($old_payment->pay_later==0){
                $Rec=$cr->received-$old_payment->amount;
                if($Rec<0)
                    $Rec=0;
                if($old_payment->prev_to_pay)
                    $toRec+=$old_payment->prev_to_pay;
            }
            else{
                $toRec=$cr->to_receive-$old_payment->amount;
                if($toRec<=0)
                    $toRec=0;
            }
        }
        else{
            if($old_payment->pay_later==0){
                $Paid=$cr->paid-$old_payment->amount;
                if($Paid<=0)
                    $Paid=0;
                if($old_payment->prev_to_pay)
                    $toPay+=$old_payment->prev_to_pay;
            }
            else{
                $toPay=$cr->to_pay-$old_payment->amount;
                if($toPay<=0)
                    $toPay=0;
            }
        }

        $cr->update([
            'to_pay'=>$toPay,
            'paid'=>$Paid,
            'received'=>$Rec,
            'to_receive'=>$toRec,
        ]);
        
       
        $cr=$company->cashRegister;
        $to_pay=$cr->to_pay;
        $paid=$cr->paid;
        $to_rec=$cr->to_receive;
        $rec=$cr->received;
        $prev_to_pay=0;
        if($input['type']=='debit'){
            if ($input['pay_later'] == 1) {
                $to_rec = $cr->to_receive + $input['amount'];
              
            } 
            else{
                $rec = $cr->received + $input['amount'];
                $prev_to_pay=min($to_rec,$input['amount']);
                $to_rec=$cr->to_receive - $input['amount'];
                if($to_rec<0)
                    $to_rec=0;
                
            }
        }
        else {
                if ($input['pay_later'] == 0) {
                    $paid = $cr->paid + $input['amount'];
                    $prev_to_pay=min($to_pay,$input['amount']);
                    $to_pay = $cr->to_pay - $input['amount'];
                    if ($to_pay < 0)
                        $to_pay = 0;
                    
                   

                }
                else{
                    $to_pay = $cr->to_pay + $input['amount'];
                }
        }

        $cr->update([
            'to_pay'=>$to_pay,
            'paid'=>$paid,
            'received'=>$rec,
            'to_receive'=>$to_rec,
            
        ]);

            $old_payment->update(['type'=>$input['type'],'amount'=>$input['amount'],'pay_later'=>$input['pay_later'],'prev_to_pay'=>$prev_to_pay
                ,'date'=>$input['date'],'account_id'=>$input['account_id'],'comments'=>$input['comments']
            ]);



            //Photo Logic
            if(isset($input['delPic'])){
                $p=$old_payment->photo;
                try {
                    unlink(public_path() . '/images/' . $p->path);
                }
                catch(\Exception $e){

                }
                $p->delete();
            }

            if($file=$request->file('photo')){
                $name=time().$file->getClientOriginalName();
                $file->move('images',$name);
                if($old_payment->photo){
                    $p=$old_payment->photo;
                    try {
                        unlink(public_path() . '/images/' . $p->path);
                    }
                    catch(\Exception $e){

                    }
                    $p->delete();
                }
                $photo=Photo::create(['path'=>$name,'imageable_type'=>'App\Payments','imageable_id'=>$id]);
            }
            DB::commit();



            return redirect('admin/company_payment')->with('success','Updated');
        }
        catch (\Exception $e){
            DB::rollback();
            dd($e);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //

        // $p=Payments::findOrFail($id);
        // $cr=$p->owner->cashRegister;
        // $rec=$cr->received;
        // $paid=$cr->paid;
        // $to_pay=$cr->to_pay;
        // if($p->type=='debit'){
        //     $rec=$cr->received-$p->amount;
        // }
        // if($p->type=='credit'){
        //     if($p->pay_later==0){
        //         $paid=$cr->paid-$p->amount;
        //     }
        //     else {
        //         $to_pay=$cr->to_pay-$p->amount;

        //     }

        // }
        // $cr->update(['to_pay'=>$to_pay,'paid'=>$paid,'received'=>$rec]);
        // $p->delete();
            // return redirect('/admin/company_payment')->with('deleted','Payment Deleted');
            try {
                DB::beginTransaction();
                $old_payment=Payments::findOrFail($id);
                $company=$old_payment->owner;
                $cr=$company->cashRegister; 
                $toRec=$cr->to_receive;
                $Rec=$cr->received;
                $toPay=$cr->to_pay;
                $Paid=$cr->paid;
                if($old_payment->type=='debit'){
                    if($old_payment->pay_later==0){
                        $Rec=$cr->received-$old_payment->amount;
                        if($Rec<0)
                            $Rec=0;
                        if($old_payment->prev_to_pay)
                            $toRec+=$old_payment->prev_to_pay;
                    }
                    else{
                        $toRec=$cr->to_receive-$old_payment->amount;
                        if($toRec<=0)
                            $toRec=0;
                    }
                }
                else{
                    if($old_payment->pay_later==0){
                        $Paid=$cr->paid-$old_payment->amount;
                        if($Paid<=0)
                            $Paid=0;
                        if($old_payment->prev_to_pay)
                            $toPay+=$old_payment->prev_to_pay;
                    }
                    else{
                        $toPay=$cr->to_pay-$old_payment->amount;
                        if($toPay<=0)
                            $toPay=0;
                    }
                }

                $cr->update([
                    'to_pay'=>$toPay,
                    'paid'=>$Paid,
                    'received'=>$Rec,
                    'to_receive'=>$toRec,
                ]);
                $old_payment->delete();

                //Photo Logic

                if($old_payment->photo) {
                    $p = $old_payment->photo;
                    unlink(public_path() . '/images/' . $p->path);
                    $p->delete();
                }
                DB::commit();
                
                return redirect('/admin/company_payment')->with('deleted','Payment Deleted');
            }

            catch (\Exception $e){
                DB::rollback();
                dd($e);
            } 
    }
}
