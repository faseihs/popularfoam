<?php

namespace App\Http\Controllers;

use App\CashRegister;
use App\Company;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\User;
class AdminCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $c=Company::all();
        foreach ($c as $C){
            if(!($C->cashRegister)){
                CashRegister::create(['owner_id'=>$C->id,'paid'=>0,'to_pay'=>0,'to_receive'=>0,'owner_type'=>'App\Company','received'=>0]);
            }
        }

        $companies = Company::orderBy('name')->get();;
        return view('admin.company.index',compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('owner', User::class);
        //
        return view('admin.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //
        $this->validate($request,[
           'name'=>'required',
            'balance_type'=>'required',
            'balance_amount'=>'required'
        ]);
        //dd($request->all());
        $id=Company::create($request->all())->id;
        $to_pay=0;
        $to_receive=0;
        $paid=0;
        $received=0;
        if($request['balance_type']==0){
            $to_pay=$request['balance_amount'];
        }
        else if($request['balance_type']==1) $paid=$request['balance_amount'];
        else if($request['balance_type']==2) $to_receive=$request['balance_amount'];
        else if($request['balance_type']==3) $received=$request['balance_amount'];
        CashRegister::create(['owner_id'=>$id,'paid'=>$paid,'to_pay'=>$to_pay,'to_receive'=>$to_receive,'owner_type'=>'App\Company','received'=>$received]);
        return redirect('admin/company')->with('success','Company Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('owner', User::class);
        //

        $company=Company::findOrFail($id);
        $items=$company->items;
        $item_count=$items->count();
        $item_pcost=0;
        $item_scost=0;
        foreach($items As $i){
            $item_pcost+=$i->purchase_price;
            $item_scost+=$i->sale_price;
        }
        $pi=$company->purchaseInvoices;
        $pi_count=$pi->count();
        $pi_bill=0;
         foreach($pi As $i){
            $pi_bill+=$i->total_price;
        }

        $po=$company->purchaseOrders;
        $po_count=$po->count();
        $po_bill=0;
         foreach($po As $i){
            $po_bill+=$i->total_price;
        }

        
        return view('admin.company.show',compact(['company','items','item_count','item_pcost','item_scost','pi','pi_count','pi_bill','po','po_count','po_bill']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        /*$c=Company::all();
        foreach ($c as $C){
            if(!($C->cashRegister)){
                CashRegister::create(['owner_id'=>$C->id,'paid'=>0,'to_pay'=>0,'to_receive'=>0,'owner_type'=>'App\Company','received'=>0]);
            }
        }*/
        $company=Company::findOrFail($id);
        return view('admin.company.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        $company=Company::findOrFail($id);
        $cr=$company->cashRegister;
        $newtoPay=$cr->to_pay;
        $newtoRec=$cr->to_receive;
        $newPaid=$cr->paid;
        $newReceived=$cr->received;
        $to_pay=0;
        $to_receive=0;
        $paid=0;
        $received=0;
        if($request['balance_type']==0){
            $to_pay=$request['balance_amount'];
            $newtoPay-=$company->balance_amount;
            if($newtoPay<0)
                $newtoPay=0;
            $newtoPay+=$to_pay;
        }
        else if($request['balance_type']==2) {
            $to_receive = $request['balance_amount'];
            $newtoRec-=$company->balance_amount;
            if($newtoRec<0)
                $newtoRec=0;
            $newtoRec+=$to_receive;
        }
        else if($request['balance_type']==1) {
            $paid = $request['balance_amount'];
            $newPaid-=$company->balance_amount;
            if($newPaid<0)
                $newPaid=0;
            $newPaid+=$paid;
            
        }
        else if($request['balance_type']==3) {
            $received = $request['balance_amount'];
            $newReceived-=$company->balance_amount;
            if($newReceived<0)
                $newReceived=0;
            $newReceived+=$received;
            
        }



        $cr->update(['to_pay'=>$newtoPay,'to_receive'=>$newtoRec,'paid'=>$newPaid,'received'=>$newReceived]);
        $company->update($request->all());
        return redirect('admin/company')->with('success','Company Updated');
    }

    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        if($cr=Company::find($id)->cashRegister)
            $cr->delete();
        if($pa=Company::find($id)->payments)
        {
            foreach ($pa as $p){
                $p->delete();
            }
        }
        Company::find($id)->delete();
        return redirect('/admin/company')->with('deleted','Company Deleted');
    }

    public function apiData(Request $rquest){
        
        $this->authorize('owner', User::class);

        $companies = Company::query();
        return DataTables::of($companies)->toJson();
    }
}
