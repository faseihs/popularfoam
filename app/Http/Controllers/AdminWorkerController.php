<?php

namespace App\Http\Controllers;

use App\CashRegister;
use App\Photo;
use App\User;
use App\Worker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class AdminWorkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $workers=Worker::all();
        return view('admin.worker.index',compact('workers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('owner', User::class);
        //
        return view('admin.worker.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //
        try {
            DB::beginTransaction();
            $this->validate($request,[
                'name'=>'required',
                'number'=>'required|numeric',
                'address'=>'required',
                'salary'=>'required',
                'joining'=>'required',
                'active'=>'required'
            ]);
            $input=$request->all();
            $photo_id=Worker::create(array_except($input,['photo']))->id;
            if($file=$request->file('photo')){
                $name=time().$file->getClientOriginalName();
                $file->move('images',$name);
                Photo::create(['path'=>$name,'imageable_type'=>'App\Worker','imageable_id'=>$photo_id]);
            }

            CashRegister::create([
                'paid'=>0,
                'to_pay'=>0,
                'received'=>0,
                'to_receive'=>0,
                'owner_type'=>'App\Worker',
                'owner_id'=>$photo_id
            ]);
            DB::commit();
            return redirect('admin/worker')->with('success','Worker Created');

        } catch (\PDOException $e) {
            DB::rollBack();
            return Redirect::back()->withInput(Input::all());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('owner', User::class);
        //
        $worker=Worker::findOrFail($id);
        $selDate=Carbon::now()->startOfMonth();
        $payments=$this->getPayments($worker->salaryPayments,$selDate);

        $at=$worker->PresentThisMonth();
        $start_date=Carbon::now()->startOfMonth();
        $days_month=$start_date->daysInMonth;
        $totalDays=$start_date->daysInMonth;
        $totalPresent=0;
        $totalAbsent=0;
        $totalNotMarked=0;
        $att=[];
        for($i=0;$i<$days_month;$i++){
            $new_date=$start_date;
            if($worker->DateAttendence($new_date->toDateString()))
            {
                

                array_push($att,$worker->datesAttendence($new_date->toDateString())->get()->all()[0]);
                
                if($new_date->dayOfWeek==5)
                    {
                        $totalDays--;
                    }
                    else{
                        if($worker->datesAttendence($new_date->toDateString())->get()->all()[0]->status==0)
                        $totalAbsent++;
                        else $totalPresent++;
                    }
            }
            else {
               
                $newDate=new \stdClass();
                
                $newDate->status=2;
                $newDate->date=$new_date->toDateString();
                $newDate->arrival=null;
                if($new_date->dayOfWeek==5)
                {
                    $newDate->status=2;
                    $newDate->date=$new_date->toDateString();
                    $newDate->arrival='';
                    $totalDays--;
                    array_push($att,$newDate);

                }
                else{
                    $totalNotMarked++;
                    array_push($att,$newDate);
                }
                   
            }
            $new_date=$new_date->addDays(1);

        }
        //dd($att);
        
        return view('admin.worker.show',compact(['selDate','payments','worker','at','att','totalAbsent','totalNotMarked','totalDays','totalPresent']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $worker=Worker::findOrFail($id);
        return view('admin.worker.edit',compact(['worker']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        try {
            DB::beginTransaction();
            $this->validate($request,[
                'name'=>'required',
                'number'=>'required|numeric',
                'address'=>'required',
                'salary'=>'required',
                'joining'=>'required',
                'active'=>'required'
            ]);
            $input=$request->all();
            $worker=Worker::findOrFail($id);
            if(isset($input['delPic'])){
                $p=$worker->photo;
                unlink(public_path() .'/images/'.$p->path);
                $p->delete();
            }
            if($file=$request->file('photo')){
                $name=time().$file->getClientOriginalName();
                $file->move('images',$name);
                if($worker->photo){
                    $p=$worker->photo;
                    unlink(public_path() .'/images/'.$p->path);
                    $p->delete();
                }
                Photo::create(['path'=>$name,'imageable_type'=>'App\Worker','imageable_id'=>$id]);
            }
            $worker->update(array_except($input,['photo','delPic']));

            DB::commit();
            return redirect('admin/worker')->with('success','Worker Updated');
        } catch (\PDOException $e) {

            DB::rollBack();
            return redirect('/admin/worker')->with('deleted','Error Updating');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        try {
            DB::beginTransaction();
            $user=Worker::findOrFail($id);
            if($user->photo) {
                $p = $user->photo;
                unlink(public_path() . '/images/' . $p->path);
                $p->delete();
            }
            $user->delete();
            DB::commit();
            return redirect('/admin/worker')->with('deleted','Worker Deleted');

        } catch (\PDOException $e) {

            DB::rollBack();
            return redirect('/admin/worker')->with('deleted','Error Deleting');
        }
    }


    public function query(Request $request){

        $this->authorize('owner', User::class);

        $worker=Worker::findOrFail($request['worker_id']);
        $at=null;
        $on_date=null;
        if(isset($request['month'])){
            $at=$worker->PresentOnMonth(Carbon::parse($request['month']));
            $on_date=Carbon::parse($request['month']);
            $start_date=Carbon::parse($request['month'])->startOfMonth();
        }
        else{
            $at=$worker->PresentThisMonth();
            $start_date=Carbon::now()->startOfMonth();
        }

        $selDate=Carbon::now()->startOfMonth();
        $payments=$this->getPayments($worker->salaryPayments,$selDate);
        $days_month=$start_date->daysInMonth;
        $totalDays=$start_date->daysInMonth;
        $totalPresent=0;
        $totalAbsent=0;
        $totalNotMarked=0;
        $att=[];
        for($i=0;$i<$days_month;$i++){
            $new_date=$start_date;
            if($worker->DateAttendence($new_date->toDateString()))
            {array_push($att,$worker->datesAttendence($new_date->toDateString())->get()->all()[0]);
                
                if($new_date->dayOfWeek==5)
                    {
                        $totalDays--;
                    }
                    else{
                        if($worker->datesAttendence($new_date->toDateString())->get()->all()[0]->status==0)
                        $totalAbsent++;
                        else $totalPresent++;
                    }
            }
            else {
               
                $newDate=new \stdClass();
                
                $newDate->status=2;
                $newDate->date=$new_date->toDateString();
                $newDate->arrival=null;
                if($new_date->dayOfWeek==5)
                {
                    $newDate->status=2;
                    $newDate->date=$new_date->toDateString();
                    $newDate->arrival='';
                    $totalDays--;
                    array_push($att,$newDate);

                }
                else{
                    $totalNotMarked++;
                    array_push($att,$newDate);
                }
                   
            }
            $new_date=$new_date->addDays(1);

        }
        $query=1;

        return view('admin.worker.show',compact(['selDate','payments','worker','at','on_date','att','query','totalAbsent','totalNotMarked','totalDays','totalPresent']));


    }


    public function queryPayment(Request $request){

        $this->authorize('owner', User::class);

        //dd($request->all());
        $worker=Worker::findOrFail($request['worker_id']);
        $at=null;
        $on_date=null;
        $start_date=Carbon::now()->startOfMonth();
        $selDate=Carbon::parse($request['on_date'])->startOfMonth();
        //dd($selDate);
        $payments=$this->getPayments($worker->salaryPayments,$selDate);
        
        $days_month=$start_date->daysInMonth;
        $totalDays=$start_date->daysInMonth;
        $totalPresent=0;
        $totalAbsent=0;
        $totalNotMarked=0;
        $att=[];
        for($i=0;$i<$days_month;$i++){
            $new_date=$start_date;
            if($worker->DateAttendence($new_date->toDateString()))
            {array_push($att,$worker->datesAttendence($new_date->toDateString())->get()->all()[0]);
                
                if($new_date->dayOfWeek==5)
                    {
                        $totalDays--;
                    }
                    else{
                        if($worker->datesAttendence($new_date->toDateString())->get()->all()[0]->status==0)
                        $totalAbsent++;
                        else $totalPresent++;
                    }
            }
            else {
               
                $newDate=new \stdClass();
                
                $newDate->status=2;
                $newDate->date=$new_date->toDateString();
                $newDate->arrival=null;
                if($new_date->dayOfWeek==5)
                {
                    $newDate->status=2;
                    $newDate->date=$new_date->toDateString();
                    $newDate->arrival='';
                    $totalDays--;
                    array_push($att,$newDate);

                }
                else{
                    $totalNotMarked++;
                    array_push($att,$newDate);
                }
                   
            }
            $new_date=$new_date->addDays(1);

        }
        $payment=1;
        return view('admin.worker.show',compact(['payment','selDate','payments','worker','at','on_date','att','query','totalAbsent','totalNotMarked','totalDays','totalPresent']));


    }


    public function getPayments($payments,$date){

        $this->authorize('owner', User::class);
        
        $rpayments=[];
        foreach($payments as $p){
           if( Carbon::parse($p->date)>=$date->startOfMonth() &&  Carbon::parse($p->date)<= $date->endOfMonth())
           array_push($rpayments,$p);
        }

        usort($rpayments,function($a,$b){
            return $a->date > $b->date;
        });
        return $rpayments;
    }
}
