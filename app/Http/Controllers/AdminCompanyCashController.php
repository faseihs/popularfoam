<?php

namespace App\Http\Controllers;

use App\CashRegister;
use App\Company;
use App\User;
use Illuminate\Http\Request;

class AdminCompanyCashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $cash_registers=CashRegister::where('owner_type','=','App\Company')->get();
        return view('admin.company_cash.index',compact('cash_registers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        //$company_cash=CashRegister::where('owner_id','=',$id)->where('owner_type','=','App\Company')->get();
        //dd($company_cash);
        $company_cash=CashRegister::findOrFail($id);
        return view('admin.company_cash.edit',compact('company_cash'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        $company_cash=CashRegister::findOrFail($id)->update($request->except('company_name'));
        return redirect('/admin/company_cash')->with('success','Cash Register Updated');

    }


    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        $company_cash=CashRegister::findOrFail($id)->delete();
        return redirect('/admin/company_cash')->with('deleted','Cash Register Deleted');
    }
}
