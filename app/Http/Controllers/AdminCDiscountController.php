<?php

namespace App\Http\Controllers;

use App\Customer;
use App\CustomerDiscount;
use App\Quality;
use App\User;
use Illuminate\Http\Request;

class AdminCDiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $qualities=Quality::pluck('name','id')->all();
        $discounts=CustomerDiscount::orderBy('quality_id')->get();
        $customers=Customer::pluck('name','id')->all();
        return view('admin.customer_discount.index',compact(['qualities','discounts','customers']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('owner', User::class);
        //
        /*$companies=Company::all();
        $items=Item::all();*/
        return view('admin.customer_discount.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);

        CustomerDiscount::create($request->all());
        return redirect('/admin/customer_discount')->with('success','Discount Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $discount=CustomerDiscount::findOrFail($id);
        $items=$discount->quality->items->pluck('name','id')->all();
        return view('admin.customer_discount.edit',compact(['discount','items']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        $discount=CustomerDiscount::findOrFail($id);
        $discount->update($request->all());
        return redirect('/admin/customer_discount')->with('success','Discount Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        CustomerDiscount::findOrFail($id)->delete();
        return redirect('/admin/customer_discount')->with('deleted','Discount Deleted');
    }

    public function createC(Request $request){
        
        $this->authorize('owner', User::class);
        
        $quality=Quality::findOrFail($request['quality_id']);
        //dd($quality->items);
        $items=$quality->items->pluck('name','id')->all();
        if($request['customer_id']==0)
            $customer=null;
        else $customer=Customer::findOrFail($request['customer_id']);
        //$items=Item::pluck('name','id')->all();
        return view('admin.customer_discount.create',compact(['items','quality','customer']));
    }
}
