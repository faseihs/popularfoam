<?php

namespace App\Http\Controllers\Admin;

use App\Account;
use App\Model\Opening;
use App\Model\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class AccountController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //

        $accounts = Account::where('can_delete',1)->orderBy('name')->get();
        $headAccounts= Account::where('can_delete',0)->orderBy('type')->orderby('name')->get();
        return view('admin.account.index',compact(['accounts','headAccounts']));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $accounts= Account::where('can_delete',0)->pluck('name','id')->all();
        return view('admin.account.create',compact(['accounts']));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name'=>'required',
            'head_id'=>'required'
        ]);
        $input=$request->except('opening');
        $input["can_delete"]=true;


        try{
            DB::beginTransaction();
            $account=Account::create($input);
            if($request->opening!=null){
                $opening=new Opening();
                $opening->type="price";
                $opening->mode="account-opening";
                $opening->amount=$request->opening;
                $opening->date=Carbon::parse(\config('myapp.opening_date'))->toDateString();
                $opening->year=Carbon::parse(\config('myapp.opening_date'))->year;
                $opening->owner_id=$account->id;
                $opening->owner_type="App\Account";
                $opening->save();

            /* $transaction = new Transaction();
                $transaction->related_id=$opening->id;
                $transaction->related_type=$opening->type;
                $transaction->type="account-opening";
                $amount->amount=$opening->amount;*/


            }

            DB::commit();
            return redirect('admin/account')->with('success','Account Created');
        }
        catch(\Exception $e){
            DB::rollback();
            if(env('APP_ENV')=="local")
                dd($e);
            else abort(500);
        }

    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
        $account=Account::findOrFail($id);
        $payments=$account->payments;
        return view('admin.account.show',compact(['account','payments']));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
        $accounts= Account::where('can_delete',0)->pluck('name','id')->all();

        $account=Account::findOrFail($id);
        $account->opening=$account->Opening?$account->Opening->amount:null;
        return view('admin.account.edit',compact(['account','accounts']));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[
            'name'=>'required',
            'head_id'=>'required'
        ]);
        try{
            DB::beginTransaction();
            $account=Account::findOrFail($id);
            $account->update($request->except('opening'));
            if($account->opening){
                if($request->opening==null)
                    $account->opening->delete();
                else $account->opening->update(["amount"=>$request->opening]);
            }
        else {
            if($request->opening!=null){
                $opening=new Opening();
                $opening->type="price";
                $opening->mode="account-opening";
                $opening->amount=$request->opening;
                $opening->date=Carbon::parse(\config('myapp.opening_date'))->toDateString();
                $opening->year=Carbon::parse(\config('myapp.opening_date'))->year;
                $opening->owner_id=$account->id;
                $opening->owner_type="App\Account";
                $opening->save();
            }
        }

            DB::commit();
            return redirect('admin/account')->with('success','Account Updated');
        }
        catch(\Exception $e){
            DB::rollback();
            if(env('APP_ENV')=="local")
                dd($e);
            else abort(500);
        }

    }


    public function destroy($id)
    {
        //
        $account=Account::findOrFail($id);
        if($account->opening)
            $account->opening->delete();
        $account->delete();
        return redirect('/admin/account')->with('deleted','Account Deleted');
    }

    public function query(Request $request,$id){
        if($request['date']==null)
            return redirect('/admin/account/'.$id);
        $account=Account::findOrFail($id);
        $Payments=$account->payments;
        $payments=[];
        $date=new Carbon(date($request['date']));
        //dd($date);
        foreach ($Payments as $p){

            if(Carbon::parse($p->date)->diffInDays($date)==0)
                array_push($payments,$p);
        }


        return view('admin.account.show',compact(['account','payments','date']));
    }
}
