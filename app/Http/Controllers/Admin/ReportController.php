<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Item;
use App\Quality;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class ReportController extends Controller
{
    //Purchase Report Summary that shows item purchases of quality during given dates
    public function purchaseReportSummary(){

        $this->authorize('owner', User::class);

        return view('admin.reports.purchase-report-summary');
    }

    public function purchaseReportSummaryEvaluate(Request $request){
        
        $this->authorize('owner', User::class);

        //$company=Company::findOrFail($request->company_id);
        $quality=Quality::findOrFail($request->quality_id);
        $fromDate=Carbon::parse($request->fromDate);
        $toDate=Carbon::parse($request->toDate);
        $data=[];
        $total=0;
        $quantity=0;
        $items=Item::whereIn('quality_id',$request->quality_id)->orderBy('quality_id')->get();
        foreach ($items as $item){

            $purchases=$item->fromDatePurchases($fromDate,$toDate)->get();
           /* if($item->id==81)
                dd($purchases);*/
            $itemTotal=0;
            $itemQuantity=0;
            foreach ($purchases as $p){

                $itemQuantity+=$p->quantity;
                $quantity+=$p->quantity;
                $itemTotal+=$p->total_price;
                $total+=$p->total_price;

            }
            $item->total=$itemTotal;
            $item->quantity=$itemQuantity;
            $item->qualityName=$item->quality->name;
            if($item->quantity>0)
            array_push($data,$item);
        }

        $Data= new \stdClass();
        $Data->data=$data;
        $Data->quantity=$quantity;
        $Data->total=$total;
        return response()->json($Data,200);

    }
}
