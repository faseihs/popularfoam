<?php

namespace App\Http\Controllers;

use App\Company;
use App\Payments;
use Illuminate\Http\Request;
use App\PurchaseReturn;
use App\PurchaseReturnDetails;
use App\User;
use Illuminate\Support\Facades\DB;

class AdminPurchaseReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $purchase_returns=PurchaseReturn::orderBy('company_id')->orderBy('created_at','DESC')->get();
        $companies=Company::pluck('name','id')->all();
        foreach($purchase_returns as $p){
            $p->payment->update(['type'=>'debit']);
        }
        return view('admin.purchase_return.index',compact(['purchase_returns','companies']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //
        try{
            DB::beginTransaction();
            $po=array();
            $po['company_id']=$request['company_id'];
            $po['total_price']=$request['total_price'];
            $po['invoice_id']=$request['invoice_id'];
            $po['date']=$request['date'];
            if(isset($request['invoice_id']))
                $po['comments']='Purchase Returm , Challan# : '.$request['invoice_id'];
            $PP=PurchaseReturn::create($po);

            $poID=$PP->id;
            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->purchase_return_id=$poID;
                $it=PurchaseReturnDetails::create((array)$item);
                $itIt=$it->item;
                $ct=$itIt->current_quantity-$item->quantity;
                $itIt->update(['current_quantity'=>$ct]);
            }


            $payment=[];
            $payment['owner_id']=$request['company_id'];
            $payment['owner_type']='App\Company';
            $payment['type']='debit';
            $payment['amount']=$request['total_price'];
            $payment['pay_later']=0;
            $payment['date']=$request['date'];
            $payment['sales_id']=$poID;
            $payment['sales_type']='App\PurchaseReturn';
            if(isset($request['invoice_id']))
                $payment['comments']='Purchase Returm , Challan# : '.$request['invoice_id'];
            Payments::create($payment);

            $cr=$PP->company->cashRegister;

            $received=$cr->paid+$request['total_price'];
            $cr->update(['received'=>$received]);

            DB::commit();
            return redirect('/admin/purchase_return')->with('success','Purchase Return Created');
        }
        catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('owner', User::class);
        //
        $purchaseReturn=PurchaseReturn::findOrFail($id);
        $items=$purchaseReturn->items;
        return view('admin.purchase_return.show',compact(['items','purchaseReturn']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $purchaseReturn=PurchaseReturn::findOrFail($id);
        $items=$purchaseReturn->items;
        //dd($items);
        $Items=$purchaseReturn->company->items;
        return view('admin.purchase_return.edit',compact(['items','purchaseReturn','Items']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        try{
            DB::beginTransaction();
            $po=array();
            $po['company_id']=$request['company_id'];
            $po['invoice_id']=$request['invoice_id'];
            $po['total_price']=$request['total_price'];
            $po['date']=$request['date'];
            if(isset($request['invoice_id']))
                $po['comments']='Purchase Return , Challan# : '.$request['invoice_id'];
            $PO=PurchaseReturn::findOrFail($id);
            $PO->update($po);
            $poID=$id;

            foreach($PO->items() as $it){
                $itIt=$it->item;
                $ct=$itIt->current_quantity+$it->quantity;
                $itIt->update(['current_quantity'=>$ct]);
            }
            $PO->items()->delete();
            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->purchase_return_id=$poID;
                $it=PurchaseReturnDetails::create((array)$item);
                $itIt=$it->item;
                $ct=$itIt->current_quantity-$item->quantity;
                $itIt->update(['current_quantity'=>$ct]);
            }

            $payment=$PO->payment;
            $cr=$PO->company->cashRegister;
            $received=$cr->received-$payment->amount;
            $cr->update(['received'=>$received]);
            if(isset($request['invoice_id']))
                $comments='Return Invoice # : '.$request['invoice_id'];

            $payment->update(['type'=>'debit','amount'=>$request['total_price'],'comments'=>$comments]);

            $received=$cr->paid+$request['total_price'];
            $cr->update(['received'=>$received]);


            DB::commit();
            return redirect('admin/purchase_return')->with("success",'Purchase Return Updated');
        }
        catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        try{
            DB::beginTransaction();
            $PO=PurchaseReturn::findOrFail($id);
            foreach($PO->items() as $it){
                $itIt=$it->item;
                $ct=$itIt->current_quantity+$it->quantity;
                $itIt->update(['current_quantity'=>$ct]);
            }
            $PO->items()->delete();
            $payment=$PO->payment;
            $cr=$PO->company->cashRegister;
            $received=$cr->received-$payment->amount;
            $cr->update(['received'=>$received]);
            $payment->delete();
            $PO->delete();
            DB::commit();
            return redirect('admin/purchase_return')->with("deleted",'Purchase Return Deleted');
        }
        catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }
    }

    public function createC(Request $request){
        $this->authorize('owner', User::class);
        $company=Company::find($request['company_id']);
        $items=$company->items;
        return view('admin.purchase_return.create',compact(['items','company']));
    }
}
