<?php

namespace App\Http\Controllers;

use App\Company;
use App\PurchaseInvoice;
use App\PurchaseInvoiceDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Payments;
use App\Photo;
use App\User;
use stdClass;
use Yajra\DataTables\Facades\DataTables;

class AdminPurchaseInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $purchase_invoices=PurchaseInvoice::orderBy('created_at','DESC')->get();
        $companies=Company::pluck('name','id')->all();
        //$companies[0]="Other";
        return view('admin.purchase_invoice.index',compact(['purchase_invoices','companies']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //dd($request->all());
        try{
            DB::beginTransaction();
            $po=array();
            $po['company_id']=$request['company_id'];
            $po['total_price']=$request['total_price'];
            $po['invoice_id']=$request['invoice_id'];
            $po['date']=$request['date'];
            $po['receive_date']=$request['receive_date'];
            $po['comments']='Purchase Invoice , Challan# : '.$request['invoice_id'];
          
            $PP=PurchaseInvoice::create($po);

            $poID=$PP->id;
            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->purchase_invoice_id=$poID;
                $it=PurchaseInvoiceDetail::create((array)$item);
                $itIt=$it->item;
                $ct=$itIt->current_quantity+$item->quantity;
                $itIt->update(['current_quantity'=>$ct]);
            }

            if($file=$request->file('photo')){
                $name=time().$file->getClientOriginalName();
                $file->move('images',$name);
                $photo=Photo::create(['path'=>$name,'imageable_type'=>'App\PurchaseInvoice','imageable_id'=>$poID]);
            }

            $payment=[];
            $payment['owner_id']=$request['company_id'];
            $payment['owner_type']='App\Company';
            $payment['type']='credit';
            $payment['amount']=$request['total_price'];
            $payment['pay_later']=0;
            $payment['date']=$request['date'];
            $payment['sales_id']=$poID;
            $payment['sales_type']='App\PurchaseInvoice';
            if($request->company_id!=0)
                $payment['comments']='Purchase Invoice # , Challan# : '.$request['invoice_id'];
            else  $payment['comments']="Company: $request->name Purchase Invoice # , Challan# : ".$request['invoice_id'];
            Payments::create($payment);

            if($PP->company){
                $cr=$PP->company->cashRegister;
                $to_receive=$cr->to_receive+$request['total_price'];
                $cr->update(['to_receive'=>$to_receive]);
            }
           

            DB::commit();
            return redirect('/admin/purchase_invoice')->with('success','Purchase Invoice Created');
        }
        catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('owner', User::class);
        //
        //
        $purchaseInvoice=PurchaseInvoice::findOrFail($id);
        $items=$purchaseInvoice->items;
        if($purchaseInvoice->company)
            $Items=$purchaseInvoice->company->items;
        else $Items = [];
        return view('admin.purchase_invoice.show',compact(['items','purchaseInvoice']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $purchaseInvoice=PurchaseInvoice::findOrFail($id);
        $items=$purchaseInvoice->items;
        //dd($items);
        if($purchaseInvoice->company)
            $Items=$purchaseInvoice->company->items;
        else $Items = [];
        return view('admin.purchase_invoice.edit',compact(['items','purchaseInvoice','Items']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //dd($request->all());
        try{
            DB::beginTransaction();
            $po=array();
            $po['company_id']=$request['company_id'];
            $po['invoice_id']=$request['invoice_id'];
            $po['total_price']=$request['total_price'];
            $po['date']=$request['date'];
            $po['receive_date']=$request['receive_date'];
            $po['comments']='Purchase Invoice , Challan# : '.$request['invoice_id'];
         
            $PO=PurchaseInvoice::findOrFail($id);
            $PO->update($po);
            $poID=$id;
            
            foreach($PO->items() as $it){
                $itIt=$it->item;
                $ct=$itIt->current_quantity-$it->quantity;
                $itIt->update(['current_quantity'=>$ct]);
            }
            $PO->items()->delete();
            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->purchase_invoice_id=$poID;
                $it=PurchaseInvoiceDetail::create((array)$item);
                $itIt=$it->item;
                $ct=$itIt->current_quantity+$item->quantity;
                $itIt->update(['current_quantity'=>$ct]);
            }

            $payment=$PO->payment;
            if($PO->company){
                $cr=$PO->company->cashRegister;
                $to_receive=$cr->to_receive-$payment->amount;
                $cr->update(['to_receive'=>$to_receive]);
                $comments='Purchase Invoice # : '.$request['invoice_id'];
                $payment->update(['amount'=>$request['total_price'],'comments'=>$comments]);
    
                $to_receive=$cr->to_receive+$payment->amount;
                $cr->update(['to_receive'=>$to_receive]);
            }
          
            
            if(isset($request['delPic'])){
                $p=$PO->photo;
                unlink(public_path() .'/images/'.$p->path);
                $p->delete();
            }
            if($file=$request->file('photo')){
                $name=time().$file->getClientOriginalName();
                $file->move('images',$name);
                if($PO->photo){
                    $p=$PO->photo;
                    unlink(public_path() .'/images/'.$p->path);
                    $p->delete();
                }
                $photo=Photo::create(['path'=>$name,'imageable_type'=>'App\PurchaseInvoice','imageable_id'=>$poID]);
            }

            DB::commit();
            return redirect('admin/purchase_invoice')->with("success",'Purchase Invoice Updated');
        }
        catch (\PDOException $e) {

                DB::rollBack();
                dd($e);
            }
    }

    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        try{
            DB::beginTransaction();
        $PO=PurchaseInvoice::findOrFail($id);
        foreach($PO->items() as $it){
            $itIt=$it->item;
            $ct=$itIt->current_quantity-$it->quantity;
            $itIt->update(['current_quantity'=>$ct]);
        }
        $PO->items()->delete();
        $payment=$PO->payment;
        $cr=$PO->company->cashRegister;
        $to_receive=$cr->to_receive-$payment->amount;
        $cr->update(['to_receive'=>$to_receive]);
        $payment->delete();
        $PO->delete();
        if($PO->photo) {
            $p = $PO->photo;
            unlink(public_path() . '/images/' . $p->path);
            $p->delete();
        }
        DB::commit();
        return redirect('admin/purchase_invoice')->with("deleted",'Purchase Invoice Deleted');
    }
        catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }
    }

    public function createC(Request $request){

        $this->authorize('owner', User::class);

        //dd($request->all());
        $company=Company::find($request['company_id']);
        if($company)
            $items=$company->items;
        else {
            $items = [];
            $company = new stdClass();
            $company->id = 0 ;
            $company->name = "Other";
        }
        return view('admin.purchase_invoice.create',compact(['items','company']));
    }

    public function apiData(Request $request){

        $this->authorize('owner', User::class);
        
        $query = PurchaseInvoice::with("company");
        return DataTables::eloquent($query)
        ->addColumn('company', function (PurchaseInvoice $item) {
            return $item->company?$item->company->name:"-";
        })
        ->toJson();
    }
}
