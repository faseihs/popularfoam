<?php

namespace App\Http\Controllers;

use App\Account;
use App\CashRegister;
use App\Customer;
use App\Payments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Photo;
use App\SalesInvoice;
use App\User;

class AdminCustomerPaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $customer_payments=Payments::where('owner_type','=','App\Customer')->where('sales_type','=',null)->orderBy('created_at','DESC')->get();
        return view('admin.customer_payment.index',compact('customer_payments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('owner', User::class);
        //
        #$customers=Customer::where('type','=','perm')->pluck('name','id')->all();
       $customers=Customer::orderBy("name")->pluck('name','id')->all();
       $accounts=Account::orderBy("name")->pluck('name','id');
       $si=null;
       $sales_invoice = null;
       if($request->has('si'))
           {
               $si=$request->si;
               $sales_invoice= SalesInvoice::findOrFail($si);
           }
        
        return view('admin.customer_payment.create',compact(['customers','accounts','si',"sales_invoice"]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //dd($request->all());
        try {
            DB::beginTransaction();
            $input = $request->all();
            $input['owner_type'] = 'App\Customer';
            //dd($input);
            $input['type']=Account::findOrFail($input['account_id'])->type;

            if ($input['owner_id']==null) {
                $newC = Customer::create(['name' => $input['name'], 'number' => $input['number'], 'address' => $input['address'], 'type' => $input['type']]);
                $input['owner_id'] = $newC->id;
                $cr = CashRegister::create(['owner_id' => $input['owner_id'], 'paid' => 0, 'to_pay' => 0, 'owner_type' => 'App\Customer', 'received' => 0]);
                if ($input['type'] == 'debit') {
                    $rec = $cr->received + $input['amount'];
                    $to_rec = $cr->to_receive - $input['amount'];
                    if ($to_rec < 0)
                        $to_rec = 0;
                    $cr->update(['received' => $rec, 'to_receive' => $to_rec]);
                } else {
                    if ($input['pay_later'] == 1) {
                        $to_pay = $cr->to_pay + $input['amount'];
                        $cr->update(['to_pay' => $to_pay]);
                    } else {
                        $paid = $cr->paid + $input['amount'];
                        $to_pay = $cr->to_pay - $paid;
                        if ($to_pay < 0)
                            $to_pay = 0;
                        $cr->update(['paid' => $paid, 'to_pay' => $to_pay]);

                    }
                }
            } else {
                if($customer = Customer::find($input['owner_id'])) {
                    if ($cr = $customer->cashRegister) {
                    } else $cr = CashRegister::create(['owner_id' => $input['owner_id'], 'paid' => 0, 'to_pay' => 0, 'owner_type' => 'App\Customer', 'received' => 0]);

                }
                else $cr= CashRegister::where('owner_id','=',0)->get()->first();
                $to_pay = $cr->to_pay;
                $paid = $cr->paid;
                $to_rec = $cr->to_receive;
                $rec = $cr->received;
                $prev_to_pay = 0;
                if ($input['type'] == 'debit') {
                    if ($input['pay_later'] == 1) {
                        $to_rec = $cr->to_receive + $input['amount'];

                    } else {
                        $rec = $cr->received + $input['amount'];
                        $prev_to_pay = min($to_rec, $input['amount']);
                        $to_rec = $cr->to_receive - $input['amount'];
                        if ($to_rec < 0)
                            $to_rec = 0;

                    }
                } else {
                    if ($input['pay_later'] == 0) {
                        $paid = $cr->paid + $input['amount'];
                        $prev_to_pay = min($to_pay, $input['amount']);
                        $to_pay = $cr->to_pay - $input['amount'];
                        if ($to_pay < 0)
                            $to_pay = 0;


                    } else {
                        $to_pay = $cr->to_pay + $input['amount'];
                    }
                }
            }
            $cr->update([
                'to_pay' => $to_pay,
                'paid' => $paid,
                'received' => $rec,
                'to_receive' => $to_rec,


            ]);
            $pa['owner_type'] = 'App\Customer';
            $pa['owner_id'] = $input['owner_id'];
            $pa['amount'] = $input['amount'];
            $pa['type'] = $input['type'];
            $pa['pay_later'] = $input['pay_later'];
            $pa['comments'] = $input['comments'];
            $pa['date'] = $input['date'];
            $pa['account_id'] = $input['account_id'];
            $pa['sales_invoice_id'] = $input['sales_invoice_id'];

            $photo_id = Payments::create($pa)->id;
            if ($file = $request->file('photo')) {
                $name = time() . $file->getClientOriginalName();
                $file->move('images', $name);
                $photo = Photo::create(['path' => $name, 'imageable_type' => 'App\Payments', 'imageable_id' => $photo_id]);
            }
            DB::commit();
            return redirect('/admin/customer_payment')->with('success', 'Payment Created');
        }
        catch(\Exception $e){
            DB::rollback();
            dd($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('owner', User::class);
        //
        $payment=Payments::findOrFail($id);
        return view('admin.customer_payment.show',compact('payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $customers=Customer::orderBy("name")->pluck('name','id')->all();
        $payment=Payments::findOrFail($id);
        $accounts=Account::orderBy("name")->pluck('name','id');

        return view('admin.customer_payment.edit',compact(['customers','payment','accounts']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        try {
            DB::beginTransaction();
        $input=$request->all();
        $input['owner_type']='App\Customer';
        $input['type']=Account::findOrFail($input['account_id'])->type;
            if($customer = Customer::find($input['owner_id'])) {
                if ($cr = $customer->cashRegister) {
                } else $cr = CashRegister::create(['owner_id' => $input['owner_id'], 'paid' => 0, 'to_pay' => 0, 'owner_type' => 'App\Customer', 'received' => 0]);

            }
            else $cr= CashRegister::where('owner_id','=',0)->get()->first();
        $toRec=$cr->to_receive;
        $Rec=$cr->received;
        $toPay=$cr->to_pay;
        $Paid=$cr->paid;
        $old_payment=Payments::findOrFail($id);
        if($old_payment->type=='debit'){
            if($old_payment->pay_later==0){
                $Rec=$cr->received-$old_payment->amount;
                if($Rec<0)
                    $Rec=0;
                if($old_payment->prev_to_pay)
                    $toRec+=$prev_to_pay;
            }
            else{
                $toRec=$cr->to_receive-$old_payment->amount;
                if($toRec<=0)
                    $toRec=0;
            }
        }
        else{
            if($old_payment->pay_later==0){
                $Paid=$cr->paid-$old_payment->amount;
                if($Paid<=0)
                    $Paid=0;
                if($old_payment->prev_to_pay)
                    $toPay+=$prev_to_pay;
            }
            else{
                $toPay=$cr->to_pay-$old_payment->amount;
                if($toPay<=0)
                    $toPay=0;
            }
        }

        $cr->update([
            'to_pay'=>$toPay,
            'paid'=>$Paid,
            'received'=>$Rec,
            'to_receive'=>$toRec,
        ]);


            if($customer = Customer::find($input['owner_id'])) {
                if ($cr = $customer->cashRegister) {
                } else $cr = CashRegister::create(['owner_id' => $input['owner_id'], 'paid' => 0, 'to_pay' => 0, 'owner_type' => 'App\Customer', 'received' => 0]);

            }
            else $cr= CashRegister::where('owner_id','=',0)->get()->first();
        $to_pay=$cr->to_pay;
        $paid=$cr->paid;
        $to_rec=$cr->to_receive;
        $rec=$cr->received;
        $prev_to_pay=0;
        if($input['type']=='debit'){
            if ($input['pay_later'] == 1) {
                $to_rec = $cr->to_receive + $input['amount'];
              
            } 
            else{
                $rec = $cr->received + $input['amount'];
                $prev_to_pay=min($to_rec,$input['amount']);
                $to_rec=$cr->to_receive - $input['amount'];
                if($to_rec<0)
                    $to_rec=0;
                
            }
        }
        else {
                if ($input['pay_later'] == 0) {
                    $paid = $cr->paid + $input['amount'];
                    $prev_to_pay=min($to_pay,$input['amount']);
                    $to_pay = $cr->to_pay - $input['amount'];
                    if ($to_pay < 0)
                        $to_pay = 0;
                    
                   

                }
                else{
                    $to_pay = $cr->to_pay + $input['amount'];
                }
        }

        $cr->update([
            'to_pay'=>$to_pay,
            'paid'=>$paid,
            'received'=>$rec,
            'to_receive'=>$to_rec,
            
        ]);

            $old_payment->update(['account_id'=>$input['account_id'],'type'=>$input['type'],'amount'=>$input['amount'],'pay_later'=>$input['pay_later'],'prev_to_pay'=>$prev_to_pay
                ,'date'=>$input['date'],'sales_invoice_id'=>$input['sales_invoice_id']
            ]);

            //Photo Logic
            if(isset($input['delPic'])){
                $p=$old_payment->photo;
                unlink(public_path() .'/images/'.$p->path);
                $p->delete();
            }
            if($file=$request->file('photo')){
                $name=time().$file->getClientOriginalName();
                $file->move('images',$name);
                if($old_payment->photo){
                    $p=$old_payment->photo;
                    unlink(public_path() .'/images/'.$p->path);
                    $p->delete();
                }
                $photo=Photo::create(['path'=>$name,'imageable_type'=>'App\Payments','imageable_id'=>$id]);
            }
            DB::commit();
            return redirect('admin/customer_payment')->with('success','Updated');
        }
        catch (\Exception $e){
            DB::rollback();
            dd($e);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        // $p=Payments::findOrFail($id);
        // if($p->owner_id==0)
        //     $cr=CashRegister::where('owner_id','=','0')->where('owner_type','=','App\Customer')->get()->first();
        //  else $cr=$p->owner->cashRegister;
        // $rec=$cr->received;
        // $paid=$cr->paid;
        // $to_pay=$cr->to_pay;
        // if($p->type=='debit'){
        //     $rec=$cr->received-$p->amount;
        // }
        // if($p->type=='credit'){
        //     if($p->pay_later==0){
        //         $paid=$cr->paid-$p->amount;
        //     }
        //     else {
        //         $paid=$cr->paid-$p->amount;

        //     }

        // }
        // $cr->update(['to_pay'=>$to_pay,'paid'=>$paid,'received'=>$rec]);
        // $p->delete();
        // return redirect('/admin/customer_payment')->with('deleted','Payment Deleted');
        try {
            DB::beginTransaction();
            $old_payment=Payments::findOrFail($id);
            $customer=$old_payment->owner;
            $cr=$customer->cashRegister; 
            $toRec=$cr->to_receive;
            $Rec=$cr->received;
            $toPay=$cr->to_pay;
            $Paid=$cr->paid;
            if($old_payment->type=='debit'){
                if($old_payment->pay_later==0){
                    $Rec=$cr->received-$old_payment->amount;
                    if($Rec<0)
                        $Rec=0;
                    if($old_payment->prev_to_pay)
                        $toRec+=$old_payment->prev_to_pay;
                }
                else{
                    $toRec=$cr->to_receive-$old_payment->amount;
                    if($toRec<=0)
                        $toRec=0;
                }
            }
            else{
                if($old_payment->pay_later==0){
                    $Paid=$cr->paid-$old_payment->amount;
                    if($Paid<=0)
                        $Paid=0;
                    if($old_payment->prev_to_pay)
                        $toPay+=$old_payment->prev_to_pay;
                }
                else{
                    $toPay=$cr->to_pay-$old_payment->amount;
                    if($toPay<=0)
                        $toPay=0;
                }
            }

            $cr->update([
                'to_pay'=>$toPay,
                'paid'=>$Paid,
                'received'=>$Rec,
                'to_receive'=>$toRec,
            ]);
            $old_payment->delete();
            if($old_payment->photo) {
                $p = $old_payment->photo;
                unlink(public_path() . '/images/' . $p->path);
                $p->delete();
            }
            DB::commit();
            
            return redirect('/admin/customer_payment')->with('deleted','Payment Deleted');
        }

        catch (\Exception $e){
            DB::rollback();
            dd($e);
        } 
    }
}
