<?php

namespace App\Http\Controllers;

use App\Company;
use App\Item;
use App\PurchaseOrder;
use App\PurchaseOrderDetail;
use App\Vehicle;
use Illuminate\Http\Request;
use App\Mail\OrderSend;
use App\User;
use Illuminate\Support\Facades\Mail;


class AdminPurchaseOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('admin', User::class);
        //
        $purchase_orders=PurchaseOrder::orderBy('company_id')->orderBy('created_at','DESC')->get();
        $companies=Company::pluck('name','id')->all();;
        return view('admin.purchase_order.index',compact(['purchase_orders','companies']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('admin', User::class);
        //
        $items=Item::all();
        $companies=Company::pluck('name','id')->all();
        $vehicles=Vehicle::pluck('name','id')->all();
        $vehicles_=Vehicle::all();
        return view('admin.purchase_order.create',compact('items','companies','vehicles','vehicles_'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('admin', User::class);

        //dd($request->all());
        $po=array();
        $po['company_id']=$request['company_id'];
        $po['vehicle_id']=$request['vehicle_id'];
        $po['total_price']=$request['total_price'];
        $po['date']=$request['date'];
        $poID=PurchaseOrder::create($po)->id;
        $items=json_decode($request['items']);
        foreach($items as $item){
            $item->purchase_order_id=$poID;
            PurchaseOrderDetail::create((array)$item);
        }

        return redirect('/admin/purchase_order')->with('success','Purchase Order Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('admin', User::class);
        //
        $purchaseOrder=PurchaseOrder::findOrFail($id);
        $items=$purchaseOrder->items;
        //dd($items);
        $vehicles=Vehicle::pluck('name','id')->all();
        $vehicles_=Vehicle::all();
        $vehicle=Vehicle::findOrFail($purchaseOrder->vehicle->id);
        return view('admin.purchase_order.show',compact(['items','vehicles','vehicles_','purchaseOrder','vehicle']));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('admin', User::class);
        //
        $purchaseOrder=PurchaseOrder::findOrFail($id);
        $items=$purchaseOrder->items;
        //dd($items);
        $Items=$purchaseOrder->company->items;
        $vehicles=Vehicle::pluck('name','id')->all();
        $vehicles_=Vehicle::all();
        $vehicle=Vehicle::findOrFail($purchaseOrder->vehicle->id);
        return view('admin.purchase_order.edit',compact(['items','vehicles','vehicles_','purchaseOrder','vehicle','Items']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('admin', User::class);
        //
        //dd($request->all());
        $po=array();
        $po['company_id']=$request['company_id'];
        $po['vehicle_id']=$request['vehicle_id'];
        $po['total_price']=$request['total_price'];
        $po['date']=$request['date'];
        $PO=PurchaseOrder::findOrFail($id);
        $PO->update($po);
        $poID=$id;
        $PO->items()->delete();
        $items=json_decode($request['items']);
        foreach($items as $item){
            $item->purchase_order_id=$poID;
            PurchaseOrderDetail::create((array)$item);
        }
        return redirect('admin/purchase_order')->with("success",'Purchase Order Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('admin', User::class);
        //
        $PO=PurchaseOrder::findOrFail($id);
        $PO->items()->delete();
        $PO->delete();
        return redirect('admin/purchase_order')->with("deleted",'Purchase Order Deleted');
    }

    public function createC(Request $request){
        
        $this->authorize('admin', User::class);

        $company=Company::find($request['company_id']);
        $items=$company->items;
        $vehicles=Vehicle::pluck('name','id')->all();
        $vehicles_=Vehicle::all();
        return view('admin.purchase_order.create',compact(['items','company','vehicles','vehicles_']));
    }



    public function printOrder($id){
        
        $this->authorize('admin', User::class);

        $purchaseOrder=PurchaseOrder::findOrFail($id);
        $items=$purchaseOrder->items;
        return view('admin.purchase_order.printcomments',compact(['purchaseOrder','items']));
        
    }

    public function finalPrint(Request $request){
        //dd($request->all());
        $this->authorize('admin', User::class);

        if($request['query']==2){
            $this->validate($request,[
                'email'=>'required|email'
            ]);
        }

        $purchaseOrder=PurchaseOrder::findOrFail($request['purchase_order_id']);
        $items=json_decode($request['items']);
        //dd($items);

        usort($items,function($a,$b){
            return strcmp($a->quality,$b->quality);
        });

       
        $covered=0;
        $uncovered=0;
        
        foreach($items as $it){
            if($it->covered==0)
                $uncovered+=$it->quantity;
            else $covered+=$it->quantity;

        }
        //dd($items);
        $type=$request['type'];
        $vehicles=Vehicle::pluck('name','id')->all();
        $vehicles_=Vehicle::all();
        $vehicle=Vehicle::findOrFail($purchaseOrder->vehicle->id);
        
        $pdf= \PDF::loadHTML( view('admin.purchase_order.print',compact(['uncovered','covered','type','items','vehicles','vehicles_','purchaseOrder','vehicle'])))->setPaper('a4', 'portrait');
        if($request['query']==1){
            return $pdf->stream();
        }
        else{
           
            Mail::to($request->email)->send(new OrderSend($purchaseOrder,$pdf));
            return redirect('/admin/purchase_order')->with('success','Order sent to '.$request['email']);
        }
    }

    public function sendMail(Request $request){

        $this->authorize('admin', User::class);
        
        $purchaseOrder=PurchaseOrder::findOrFail($request['purchase_order_id']);
        $items=json_decode($request['items']);
        //dd($items);

        usort($items,function($a,$b){
            return strcmp($a->quality,$b->quality);
        });

       
        $covered=0;
        $uncovered=0;
        
        foreach($items as $it){
            if($it->covered==0)
                $uncovered+=$it->quantity;
            else $covered+=$it->quantity;

        }
        //dd($items);
        $type=$request['type'];
        $vehicles=Vehicle::pluck('name','id')->all();
        $vehicles_=Vehicle::all();
        $vehicle=Vehicle::findOrFail($purchaseOrder->vehicle->id);
        
        $pdf= \PDF::loadHTML( view('admin.purchase_order.print',compact(['uncovered','covered','type','items','vehicles','vehicles_','purchaseOrder','vehicle'])))->setPaper('a4', 'portrait');
       
        Mail::to($request->email,new OrderSend($purchaseOrder,$pdf))->send();

        return "Success";


    }
    
}
