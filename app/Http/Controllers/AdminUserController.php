<?php

namespace App\Http\Controllers;

use App\Photo;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    //
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $users=User::all();
        return view('admin.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('owner', User::class);
        //
        $roles=Role::where('id','!=',1)->pluck('name','id');
        return view('admin.user.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'role_id'=>'required',
            'password'=>'required',
        ]);
        $input=$request->all();
        $input['password']=bcrypt($input['password']);
        $photo_id=User::create(array_except($input,['photo']))->id;
        if($file=$request->file('photo')){
            $name=time().$file->getClientOriginalName();
            $file->move('images',$name);
            Photo::create(['path'=>$name,'imageable_type'=>'App\User','imageable_id'=>$photo_id]);
        }

        return redirect('admin/user')->with('success','User Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('owner', User::class);
        //
        return redirect('/admin/user');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $roles=Role::where('id','!=',1)->pluck('name','id');
        $user=User::findOrFail($id);
        return view('admin.user.edit',compact(['user','roles']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'role_id'=>'required',
        ]);

        if(trim($request->password)==''){
            $input=$request->except('password');
        }
        else{
            $input=$request->all();
            $input['password']=bcrypt($input['password']);
        }
        $user=User::findOrFail($id);
        if(isset($input['delPic'])){
            $p=$user->photo;
            unlink(public_path() .'/images/'.$p->path);
            $p->delete();
        }
        if($file=$request->file('photo')){
            $name=time().$file->getClientOriginalName();
            $file->move('images',$name);
            if($user->photo){
                $p=$user->photo;
                unlink(public_path() .'/images/'.$p->path);
                $p->delete();
            }
            Photo::create(['path'=>$name,'imageable_type'=>'App\User','imageable_id'=>$id]);
        }
        $user->update(array_except($input,['photo','delPic']));
        return redirect('admin/user')->with('success','User Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        $user=User::findOrFail($id);
        if($user->photo) {
            $p = $user->photo;
            unlink(public_path() . '/images/' . $p->path);
            $p->delete();
        }
        $user->delete();
        return redirect('/admin/user')->with('deleted','User Deleted');
    }
}
