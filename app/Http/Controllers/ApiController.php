<?php

namespace App\Http\Controllers;

use App\Item;
use App\Transformer\ItemEditTransformer;
use App\User;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    //

    public function salesInvoiceIndex(){

    }

    public function getAllItemsEdit($id){
        
        $this->authorize('sales', User::class);

        $items=Item::orderBy('name')->get();
        $items=$items->sort(function ($a, $b) {
            // sort by column1 first, then 2, and so on
            return strcmp($a->quality->name, $b->quality->name)
                ?: strcmp($a->name, $b->name);
        });
        return fractal($items,new ItemEditTransformer($id))->toArray();
    }

}
