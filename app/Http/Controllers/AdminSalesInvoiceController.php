<?php

namespace App\Http\Controllers;

use App\CashRegister;
use App\Customer;
use App\Item;
use App\Payments;
use App\SalesInvoice;
use App\SalesInvoiceDetails;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminSalesInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('sales', User::class);
        //

        /*$customers=Customer::pluck('name','id')->all();
        //$sales_invoices=SalesInvoice::orderBy('id','DESC')->orderBy('date','DESC')->get();
        $sales_invoices_today=SalesInvoice::where('order_booking_id','=',null)->where('date','=',Carbon::now()->format('Y-m-d'))->orderByDesc('id')->orderByDesc('date')->get();
        $sales_invoices_previous=SalesInvoice::where('order_booking_id','=',null)->where('date','<',Carbon::now()->format('Y-m-d'))->orderByDesc('id')->orderByDesc('date')->get();

        //Deleting Pay later Payments
        //$this->deletePayLaterPayments($sales_invoices);
        $salesInvoicesAll=SalesInvoice::orderBy('date','DESC')->get();
        $order_bookings= SalesInvoice::where('order_booking_id','<>',null)->get();
        //dd($order_bookings);
        $defaultView='today';
        if($request->has('page'))
            $defaultView='previous';
        return view('admin.sales_invoice.index',compact(['salesInvoicesAll','defaultView','customers','sales_invoices_today','sales_invoices_previous','order_bookings']));*/
        $customers=Customer::pluck('name','id')->all();
        $defaultView='today';
        if($request->has('page'))
            $defaultView='previous';
        return view('admin.sales_invoice.newIndex',compact(['defaultView','customers']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->authorize('sales', User::class);

        //

        //dd($request->all());
        //dd($request->all());
        try{
            DB::beginTransaction();
        $sales=[];
        $sales['customer_id']=$request['customer_id'];
        $sales['total_price']=$request['total_price'];
        $sales['paid_amount']=$request['paid_amount'];
        $sales['returned']=$request['returned'];
        $sales['given']=$request['given'];
        $sales['remaining']=$request['remaining'];
        $sales['name']=$request['name'];
        $sales['discount']=$request['discount'];
        $sales['number']=$request['number'];
        if($request['pay_later']==1){
            $sales['pay_later']=1;
            $sales['to_pay']=abs($request['returned']);
        }
        else{
            $sales['pay_later']=0;
            $sales['to_pay']=0;
        }
        $sales['date']=$request['date'];
        $id=SalesInvoice::create($sales)->id;
        $items=json_decode($request['items']);
        foreach($items as $item){
            $item->sales_invoice_id=$id;
            $si=SalesInvoiceDetails::create((array)$item);
            $it=$si->item;
            if($it->current_quantity>$si->quantity){
                $q=$it->current_quantity-$si->quantity;
                $it->update(['current_quantity'=>$q]);
            }
            else if($it->store_quantity>$si->quantity){
                $q=$it->store_quantity-$si->quantity;
                $it->update(['store_quantity'=>$q]);

            }
            else if(($it->store_quantity+$it->current_quantity)>=$si->quantity){
                $q=($it->store_quantity+$it->current_quantity)-$si->quantity;
                $it->update(['store_quantity'=>0,'current_quantity'=>$q]);
            }
            else{
                $q=$it->current_quantity-$si->quantity;
                $it->update(['current_quantity'=>$q]);
            }
        }
        if($request['customer_id']==0){
            if($sales['to_pay']==0){
                if($cr=CashRegister::where('owner_id','=',0)->where('owner_type','=','App\Customer')->get()->first()){
                    $payment=[];
                    $payment['owner_id']=0;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=$request['total_price'];
                    $payment['pay_later']=0;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];
                    Payments::create($payment);
                    $paid = $cr->paid + $request['total_price'];
                    $cr->update(['paid' => $paid]);

                }
                else{
                    $cr=CashRegister::create(['owner_id'=>0,'paid'=>0,'to_pay'=>0,'owner_type'=>'App\Customer','received'=>0,'to_receive'=>0]);
                    $payment=[];
                    $payment['owner_id']=0;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=$request['total_price'];
                    $payment['pay_later']=0;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];
                    Payments::create($payment);
                    $paid = $cr->paid + $request['total_price'];
                    $cr->update(['paid' => $paid]);
                }
            }
        }
        else{
            if($sales['to_pay']==0){
                $customer=Customer::findOrFail($request['customer_id']);
                $cr=$customer->cashRegister;
                $paid = $cr->paid + $request['total_price'];
                $cr->update(['paid' => $paid]);
                $payment=[];
                $payment['owner_id']=$customer->id;
                $payment['owner_type']='App\Customer';
                $payment['type']='credit';
                $payment['amount']=$request['total_price'];
                $payment['pay_later']=0;
                $payment['sales_id']=$id;
                $payment['sales_type']='App\SalesInvoice';
                $payment['date']=$request['date'];
                Payments::create($payment);
            }
            else{
                $customer=Customer::findOrFail($request['customer_id']);
                $cr=$customer->cashRegister;
                $to_pay = $cr->to_pay + $sales['to_pay'];
                $paid = $cr->paid + $sales['paid_amount'];
                $cr->update(['paid' => $paid,'to_pay'=>$to_pay]);
                $payment=[];
                $payment['owner_id']=$customer->id;
                $payment['owner_type']='App\Customer';
                $payment['type']='credit';
                $payment['amount']=$sales['paid_amount'];
                $payment['pay_later']=0;
                $payment['sales_id']=$id;
                $payment['sales_type']='App\SalesInvoice';
                $payment['date']=$request['date'];
                Payments::create($payment);

                /*$payment=[];
                $payment['owner_id']=$customer->id;
                $payment['owner_type']='App\Customer';
                $payment['type']='credit';
                $payment['amount']=$sales['to_pay'];
                $payment['pay_later']=1;
                $payment['sales_id']=$id;
                $payment['sales_type']='App\SalesInvoice';
                $payment['date']=$request['date'];
                Payments::create($payment);*/

            }
        }

        DB::commit();
        return redirect('/admin/sales_invoice')->with('success','Sales Invoice Created');
    }
    catch(\Exception $e){
        DB::rollback();
        dd($e);

    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('sales', User::class);

        //
        $sales_invoice=SalesInvoice::findOrFail($id);
        $items=$sales_invoice->items;
        $customer=null;
        if($sales_invoice->customer){
            $customer=$sales_invoice->customer;
        }

        $extra_payments = $sales_invoice->extraPayments;
        return view('admin.sales_invoice.show',compact(['sales_invoice','items','customer','extra_payments']));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $this->authorize('sales', User::class);
        
        $sales_invoice=SalesInvoice::findOrFail($id);
        $user = Auth::user();
        $now = Carbon::now();
        if($user->role_id !=1 && Carbon::parse($sales_invoice->date)->diffInHours($now)>=24){
            return redirect()->back()->with('danger','Permission not allowed');
        }
        $items=$sales_invoice->items;
        $Items=Item::all();
        $customer=null;
        $cr=null;
        $payment=null;
        if($sales_invoice->customer)
        {
            $customer=$sales_invoice->customer;
            $cr=$sales_invoice->customer->cashRegister;
            $payment=$customer->lastPayment->first();
        }
        else{
            $cr=CashRegister::where('owner_type','=','App\Customer')->where('owner_id','=','0')->get()->first();
        }
        return view('admin.sales_invoice.edit',compact(['items','sales_invoice','Items','cr','customer','payment']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $this->authorize('sales', User::class);

        try {
            DB::beginTransaction();
            $si=SalesInvoice::findOrFail($id);
            foreach ($si->items as $it){
                $item=$it->item;
                $q=$item->current_quantity+$it->quantity;
                $item->update(['current_quantity'=>$q]);
                $it->delete();
            }

            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->sales_invoice_id=$id;
                $si=SalesInvoiceDetails::create((array)$item);
                $it=$si->item;
                if($it->current_quantity>$si->quantity){
                    $q=$it->current_quantity-$si->quantity;
                    $it->update(['current_quantity'=>$q]);
                }
                else if($it->store_quantity>$si->quantity){
                    $q=$it->store_quantity-$si->quantity;
                    $it->update(['store_quantity'=>$q]);

                }
                else if(($it->store_quantity+$it->current_quantity)>=$si->quantity){
                    $q=($it->store_quantity+$it->current_quantity)-$si->quantity;
                    $it->update(['store_quantity'=>0,'current_quantity'=>$q]);
                }
                else{
                    $q=$it->current_quantity-$si->quantity;
                    $it->update(['current_quantity'=>$q]);
                }
            }
            $si=SalesInvoice::findOrFail($id);
            $cr=null;
            $cID=0;
            if($si->customer){
                $cr=$si->customer->cashRegister;
                $cID=$si->customer->id;
            }
            else{
                $cr=CashRegister::where('owner_id','=','0')->where('owner_type','=','App\Customer')->get()->first();
            }
            $payment=Payments::where('sales_type','=','App\SalesInvoice')->where(
                'sales_id','=',$id
            )->get()->first();
            //dd($payment);

            
            if(!$payment){
                if($request['pay_later']==0){
                    $payment=[];
                    $payment['owner_id']=$cID;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=$request['total_price'];
                    $payment['pay_later']=0;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];
                    Payments::create($payment);
                }
                else {
                    $payment=[];
                    $payment['owner_id']=$cID;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=$request['paid_amount'];
                    $payment['pay_later']=0;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];
                    Payments::create($payment);

                    /*$payment=[];
                    $payment['owner_id']=$cID;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=abs($request['returned']);
                    $payment['pay_later']=1;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];*/
                }
            }

            else{

                if($payment->pay_later==0){
                    $q=$cr->paid-$payment->amount;
                    if($q<0) $q=0;
                    $q+=$request['total_price'];
                    $cr->update(['paid'=>$q]);


                    if($request['pay_later']==0){
                        $payment->update([
                            'amount'=>$request['paid_amount']
                        ]);
                    }
                    else {
                        $payment->update([
                            'amount'=>$request['paid_amount']
                        ]);
                    }
                }
                else{
                    $q=$cr->to_pay-$payment->amount;
                    if($q<0) $q=0;
                    $q+=$request['total_price'];
                    $cr->update(['to_pay'=>$q]);

                    if($request['pay_later']==0){
                        $payment->update([
                            'amount'=>$request['paid_amount']
                        ]);
                    }
                    else {
                        $payment->update([
                            'amount'=>$request['paid_amount']
                        ]);
                    }
                }

            }


            $sales=[];
            $sales['date']=$request['date'];
            $sales['customer_id']=$request['customer_id'];
            $sales['total_price']=$request['total_price'];
            $sales['paid_amount']=$request['paid_amount'];
            $sales['returned']=$request['returned'];
            $sales['remaining']=$request['remaining'];
            $sales['name']=$request['name'];
            $sales['number']=$request['number'];
            $sales['discount']=$request['discount'];
            $sales['given']=$request['given'];
            $sales['pay_later']=$request['pay_later'];
            $si->update($sales);
            DB::commit();
            return redirect('admin/sales_invoice/'.$si->id.'/edit')->with("success",'Sales Invoice Updated');
        } catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('sales', User::class);

        //
        try {
            DB::beginTransaction();
            $si=SalesInvoice::findOrFail($id);


            $user = Auth::user();
            $now = Carbon::now();
            if($user->role_id !=1 && Carbon::parse($si->date)->diffInHours($now)>=24){
                return redirect()->back()->with('danger','Permission not allowed');
            }

            foreach ($si->items as $it){
                $item=$it->item;
                $q=$item->current_quantity+$it->quantity;
                $item->update(['current_quantity'=>$q]);
                $it->delete();
            }

            $cr=null;
            $cID=0;
            if($si->customer){
                $cr=$si->customer->cashRegister;
                $cID=$si->customer->id;
            }
            else{
                $cr=CashRegister::where('owner_id','=','0')->where('owner_type','=','App\Customer')->get()->first();
            }
            $payment=Payments::where('sales_type','=','App\SalesInvoice')->where(
                'sales_id','=',$id
            )->get()->first();




            if($payment->pay_later==0){
                $q=$cr->paid-$payment->amount;
                if($q<0) $q=0;
                $cr->update(['paid'=>$q]);
            }
            else{
                $q=$cr->to_pay-$payment->amount;
                if($q<0) $q=0;
                $cr->update(['to_pay'=>$q]);
            }
            
            $payment->delete();
            $si->delete();
            DB::commit();
            return redirect('admin/sales_invoice')->with("deleted",'Sales Invoice Deleted');
        } catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }

    }

    public function createC(Request $request){
        //dd($request->all());
        $this->authorize('sales', User::class);
        //dd(date('d-m-Y'));
        $cr=null;
        $payment=null;
        if($request['customer_id']==0)
            $customer=null;
        else {
            $customer = Customer::findOrFail($request['customer_id']);
            $payment=$customer->lastPayment->first();
            $cr=$customer->cashRegister;
        }
        $items=Item::all();
        return view('admin.sales_invoice.create',compact(['customer','items','payment','cr']));
    }


    public function printInvoice(Request $request, $id){

        $sales_invoice=SalesInvoice::findOrFail($id);
        $items=$sales_invoice->items;
        $customer=null;
        if($sales_invoice->customer){
            $customer=$sales_invoice->customer;
        }

        $print_size = "a4";

        if($request->has("print_size"))
            $print_size= $request->print_size;
        if($print_size == "80"){
            \PDF::setOptions(['dpi'=>72]);
            //return view('admin.sales_invoice.print_80',compact(['sales_invoice','items','customer']));
            $pdf= \PDF::loadHTML(view('admin.sales_invoice.print_80',compact(['sales_invoice','items','customer'])))->setPaper(array(0,0,204,650));
            return $pdf->stream();
        }
        else {
            \PDF::setOptions([]);
            $pdf= \PDF::loadHTML(view('admin.sales_invoice.print',compact(['sales_invoice','items','customer'])))->setPaper("A4","portrait");
            return $pdf->stream();
        }

       
    }

    public function printInvoiceOption(Request $request,$id){
        
        $this->authorize('sales', User::class);

        $url = "/admin/print/salesInvoice/$id";
        if($request->has("print_url"))
            $url = $request->print_url;
        return view("admin.sales_invoice.print_option",compact("id","url"));
    }

    public function deletePayLaterPayments($sales_invoices){
        try{
            DB::beginTransaction();
            foreach($sales_invoices as $si){
                if($si->pay_later==1){
                    if($p=$si->payLaterPayment())
                        $p->delete();
                }
            }
            DB::commit();
        }
        catch(\Exception $e){
            DB::rollback();
            dd($e);
        }

    }


    public function apiCreate(Request $request)
    {
        //
        $this->authorize('owner', User::class);
        // $this->authorize('sales', User::class);
        //dd($request->all());
        //dd($request->all());
        try{
            DB::beginTransaction();
            $sales=[];
            $sales['customer_id']=$request['customer_id'];
            $sales['total_price']=$request['total_price'];
            $sales['paid_amount']=$request['paid_amount'];
            $sales['returned']=$request['returned'];
            $sales['given']=$request['given'];
            $sales['remaining']=$request['remaining'];
            $sales['name']=$request['name'];
            $sales['discount']=$request['discount'];
            $sales['number']=$request['number'];
            if($request['pay_later']==1){
                $sales['pay_later']=1;
                $sales['to_pay']=abs($request['returned']);
            }
            else{
                $sales['pay_later']=0;
                $sales['to_pay']=0;
            }
            $sales['date']=$request['date'];
            $id=SalesInvoice::create($sales)->id;
            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->sales_invoice_id=$id;
                $si=SalesInvoiceDetails::create((array)$item);
                $it=$si->item;
                if($it->current_quantity>$si->quantity){
                    $q=$it->current_quantity-$si->quantity;
                    $it->update(['current_quantity'=>$q]);
                }
                else if($it->store_quantity>$si->quantity){
                    $q=$it->store_quantity-$si->quantity;
                    $it->update(['store_quantity'=>$q]);

                }
                else if(($it->store_quantity+$it->current_quantity)>=$si->quantity){
                    $q=($it->store_quantity+$it->current_quantity)-$si->quantity;
                    $it->update(['store_quantity'=>0,'current_quantity'=>$q]);
                }
                else{
                    $q=$it->current_quantity-$si->quantity;
                    $it->update(['current_quantity'=>$q]);
                }
            }
            if($request['customer_id']==0){
                if($sales['to_pay']==0){
                    if($cr=CashRegister::where('owner_id','=',0)->where('owner_type','=','App\Customer')->get()->first()){
                        $payment=[];
                        $payment['owner_id']=0;
                        $payment['owner_type']='App\Customer';
                        $payment['type']='credit';
                        $payment['amount']=$request['total_price'];
                        $payment['pay_later']=0;
                        $payment['sales_id']=$id;
                        $payment['sales_type']='App\SalesInvoice';
                        $payment['date']=$request['date'];
                        Payments::create($payment);
                        $paid = $cr->paid + $request['total_price'];
                        $cr->update(['paid' => $paid]);

                    }
                    else{
                        $cr=CashRegister::create(['owner_id'=>0,'paid'=>0,'to_pay'=>0,'owner_type'=>'App\Customer','received'=>0,'to_receive'=>0]);
                        $payment=[];
                        $payment['owner_id']=0;
                        $payment['owner_type']='App\Customer';
                        $payment['type']='credit';
                        $payment['amount']=$request['total_price'];
                        $payment['pay_later']=0;
                        $payment['sales_id']=$id;
                        $payment['sales_type']='App\SalesInvoice';
                        $payment['date']=$request['date'];
                        Payments::create($payment);
                        $paid = $cr->paid + $request['total_price'];
                        $cr->update(['paid' => $paid]);
                    }
                }
            }
            else{
                if($sales['to_pay']==0){
                    $customer=Customer::findOrFail($request['customer_id']);
                    $cr=$customer->cashRegister;
                    $paid = $cr->paid + $request['total_price'];
                    $cr->update(['paid' => $paid]);
                    $payment=[];
                    $payment['owner_id']=$customer->id;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=$request['total_price'];
                    $payment['pay_later']=0;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];
                    Payments::create($payment);
                }
                else{
                    $customer=Customer::findOrFail($request['customer_id']);
                    $cr=$customer->cashRegister;
                    $to_pay = $cr->to_pay + $sales['to_pay'];
                    $paid = $cr->paid + $sales['paid_amount'];
                    $cr->update(['paid' => $paid,'to_pay'=>$to_pay]);
                    $payment=[];
                    $payment['owner_id']=$customer->id;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=$sales['paid_amount'];
                    $payment['pay_later']=0;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];
                    Payments::create($payment);

                    /*$payment=[];
                    $payment['owner_id']=$customer->id;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='credit';
                    $payment['amount']=$sales['to_pay'];
                    $payment['pay_later']=1;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesInvoice';
                    $payment['date']=$request['date'];
                    Payments::create($payment);*/

                }
            }

            DB::commit();
            return response()->json("Done",201);
        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json($e,500);

        }
    }


}
