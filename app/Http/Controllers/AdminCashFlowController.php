<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SalaryPayment;
use App\PurchaseInvoice;
use App\NonCashTransaction;
use Carbon\Carbon;
use App\SalesReturn;
use App\SalesInvoice;
use App\Payments;
use App\OrderBooking;
use App\PurchaseReturn;
use App\User;

class AdminCashFlowController extends Controller
{
    //

    public function expenseIndex(){

        $this->authorize('owner', User::class);

        $default='all';
        //$salary_payments=SalaryPayment::all();
        //$purchase_invoices=PurchaseInvoice::all();
        // $transactions=NonCashTransaction::where('credit_id','0')->where('debit_id',0)->get();
        // $sales_returns=SalesReturn::all();
        // $payments=Payments::where('type','debit')->where('sales_type','=',null)->get()->all();

        $transactions  = [];
        $sales_returns = [];
        $payments =[];
        $purchase_invoices=[];
        $salary_payments=[];
        $customer_payments = null;
        $total_expense=0;
        $sp_total=0;
        $pi_total=0;
        $t_total=0;
        $sr_total=0;
        $p_total=0;

        foreach($salary_payments as $sp){
            $total_expense+=$sp->amount;
            $sp_total+=$sp->amount;
        }

        // foreach($purchase_invoices as $sp){
        //     $total_expense+=$sp->total_price;
        //     $pi_total+=$sp->total_price;
        // }
        foreach($transactions as $sp){
            $total_expense+=$sp->amount;
            $t_total+=$sp->amount;
        }
        foreach($sales_returns as $sp){
            $total_expense+=$sp->total_price;
            $sr_total+=$sp->total_price;
        }

        foreach($payments as $sp){
            $total_expense+=$sp->amount;
            $p_total+=$sp->amount;
        }
        return view('admin.reports.expense.index',compact(["customer_payments",'p_total','payments','default','sales_returns','sr_total','sp_total','pi_total','t_total','total_expense','purchase_invoices','salary_payments','transactions']));
    }

    public function expenseQuery(Request $request){

        $this->authorize('owner', User::class);
        
        $total_expense=0;
        $sp_total=0;
        $pi_total=0;
        $t_total=0;
        $sr_total=0;
        $p_total=0;
        $to_date = null;
        $on_date = null;
        $from_date = null;
        if($request['today']=='all'){
            $default='all';
            $salary_payments=SalaryPayment::all();
            $purchase_invoices=PurchaseInvoice::all();
            $transactions=NonCashTransaction::where('credit_id','0')->where('debit_id',0)->get();
            $sales_returns=SalesReturn::all();
            $payments=Payments::where('type','debit')->where('sales_type','=',null)->get()->all();

            

            foreach($salary_payments as $sp){
                $total_expense+=$sp->amount;
                $sp_total+=$sp->amount;
            }

            // foreach($purchase_invoices as $sp){
            //     $total_expense+=$sp->total_price;
            //     $pi_total+=$sp->total_price;
            // }
            foreach($transactions as $sp){
                $total_expense+=$sp->amount;
                $t_total+=$sp->amount;
            }
            foreach($sales_returns as $sp){
                $total_expense+=$sp->amount;
                $sr_total+=$sp->total_price;
            }
            foreach($payments as $sp){
                $total_expense+=$sp->amount;
                $p_total+=$sp->amount;
            }
            
        }
         else if($request['today']==1){
            $default=1;
            $today=Carbon::now()->setTimezone('Asia/Karachi');
            //dd($today);
            $salary_payments=SalaryPayment::where('date','=',$today->format('Y-m-d'))->get();
            $purchase_invoices=PurchaseInvoice::where('date','=',$today->format('Y-m-d'))->get();
            $transactions=NonCashTransaction::where('credit_id','0')->where('debit_id',0)->where('date','=',$today->format('Y-m-d'))->get();
            $sales_returns=SalesReturn::where('date','=',$today->format('Y-m-d'))->get();
            $payments=Payments::where('type','debit')->where('sales_type','=',null)->where('date','=',$today->format('Y-m-d'))->get()->all();
            
            foreach($salary_payments as $sp){
                $total_expense+=$sp->amount;
                $sp_total+=$sp->amount;
            }
    
            // foreach($purchase_invoices as $sp){
            //     $total_expense+=$sp->total_price;
            //     $pi_total+=$sp->total_price;
            // }
            foreach($transactions as $sp){
                $total_expense+=$sp->amount;
                $t_total+=$sp->amount;
            }
            foreach($sales_returns as $sp){
                $total_expense+=$sp->amount;
                $sr_total+=$sp->total_price;
            }
            foreach($payments as $sp){
                $total_expense+=$sp->amount;
                $p_total+=$sp->amount;
            }

        }

        else if($request['today']==2){
            $default=2;
            $on_date=Carbon::parse($request['on_date'])->setTimezone('Asia/Karachi');
            $salary_payments=SalaryPayment::where('date','=',$on_date->toDateString())->get();
            $purchase_invoices=PurchaseInvoice::where('date','=',$on_date->toDateString())->get();
            $transactions=NonCashTransaction::where('credit_id','0')->where('debit_id',0)->where('date','=',$on_date->toDateString())->get();
            $sales_returns=SalesReturn::where('date','=',$on_date->toDateString())->get();
            $payments=Payments::where('type','debit')->where('sales_type','=',null)->where('date','=',$on_date->toDateString())->get()->all();

            foreach($salary_payments as $sp){
                $total_expense+=$sp->amount;
                $sp_total+=$sp->amount;
            }
    
            // foreach($purchase_invoices as $sp){
            //     $total_expense+=$sp->total_price;
            //     $pi_total+=$sp->total_price;
            // }
            foreach($transactions as $sp){
                $total_expense+=$sp->amount;
                $t_total+=$sp->amount;
            }
            foreach($sales_returns as $sp){
                $total_expense+=$sp->amount;
                $sr_total+=$sp->total_price;
            }

            foreach($payments as $sp){
                $total_expense+=$sp->amount;
                $p_total+=$sp->amount;
            }

        }

        else if($request['today']==3){
            $default=3;
            $from_date=Carbon::parse($request['from_date'])->setTimezone('Asia/Karachi');
            $to_date=Carbon::parse($request['to_date'])->setTimezone('Asia/Karachi');
            $salary_payments=SalaryPayment::where('date','>=',$from_date->toDateString())->where('date','<=',$to_date->toDateString())->get();
            $purchase_invoices=PurchaseInvoice::where('date','>=',$from_date->toDateString())->where('date','<=',$to_date->toDateString())->get();
            $transactions=NonCashTransaction::where('credit_id','0')->where('debit_id',0)->where('date','>=',$from_date->toDateString())->where('date','<=',$to_date->toDateString())->get();
            $sales_returns=SalesReturn::where('date','>=',$from_date->toDateString())->where('date','<=',$to_date->toDateString())->get();
            $payments=Payments::where('type','debit')->where('sales_type','=',null)->where('date','>=',$from_date->toDateString())->where('date','<=',$to_date->toDateString())->get()->all();

            foreach($salary_payments as $sp){
                $total_expense+=$sp->amount;
                $sp_total+=$sp->amount;
            }
    
            // foreach($purchase_invoices as $sp){
            //     $total_expense+=$sp->total_price;
            //     $pi_total+=$sp->total_price;
            // }
            foreach($transactions as $sp){
                $total_expense+=$sp->amount;
                $t_total+=$sp->amount;
            }
            foreach($sales_returns as $sp){
                $total_expense+=$sp->amount;
                $sr_total+=$sp->total_price;
            }

            foreach($payments as $sp){
                $total_expense+=$sp->amount;
                $p_total+=$sp->amount;
            }
            
        }
        //dd($salary_payments);
        return view('admin.reports.expense.index',compact(['p_total','payments','default','sales_returns','sr_total','on_date','from_date','to_date','sp_total','pi_total','t_total','total_expense','purchase_invoices','salary_payments','transactions']));


    }


    public function cashIndex(){

        $this->authorize('owner', User::class);

        $default='all';
        // $sales_invoices = SalesInvoice::all();
        // $payments=Payments::where('owner_type','App\Company')->where('type','credit')->get()->all();
        // $orders=OrderBooking::all();
        // $pr=PurchaseReturn::all()->all();
        // $daily_credits= NonCashTransaction::where('debit_id','-1')->get()->all();


        $sales_invoices = collect([]);
        $payments=[];
        $orders=collect([]);
        $pr=[];
        $daily_credits= [];


        $total=0;
        $si_total=0;
        $p_total=0;
        $o_total=0;
        $pr_total=0;
        $dc_total=0;

        foreach($sales_invoices as $sp){
            if($sp->remaining){
                $total+=$sp->remaining;
                $si_total+=$sp->remaining;
            }
            else{
                if($sp->paid_amount){
                    $total+=$sp->paid_amount;
                    $si_total+=$sp->paid_amount;
                }
            }
        }

        foreach($orders as $sp){
            $total+=$sp->advance;
            $o_total+=$sp->advance;
        }

        foreach($pr as $p){
            $pr_total+=$p->total_price;
            $total+=$p->total_price;
        }
        foreach($daily_credits as $p){
            $dc_total+=$p->amount;
            $total+=$p->amount;
        }
        

        // foreach($payments as $sp){
        //     $total+=$sp->amount;
        //     $p_total+=$sp->amount;
        // }

        
        $orders=$orders->all();
        $sales_invoices=$sales_invoices->all();

        usort($sales_invoices,function($a,$b){
            return strcmp($a->cust,$b->cust);
        });
        usort($orders,function($a,$b){
            return strcmp($a->cust,$b->cust);
        });
        usort($pr,function($a,$b){
            return $a->date>$b->date;
        });
        usort($daily_credits,function($a,$b){
            return $a->date>$b->date;
        });

        $customer_payments = [];
        return view('admin.reports.cash_in',compact(['customer_payments','daily_credits','dc_total','pr_total','pr','o_total','orders','default','total','si_total','p_total','sales_invoices','payments']));




    }
    public function cashQuery(Request $request){

        $this->authorize('owner', User::class);
        
        $total=0;
        $si_total=0;
        $p_total=0;
        $o_total=0;
        $pr_total=0;
        $dc_total=0;
        $cp_total = 0;
        $on_date = null;
        $to_date = null;
        $from_date = null;
        if($request['today']=='all'){
            $default='all';
            $sales_invoices = SalesInvoice::all();
            $payments=Payments::where('owner_type','App\Company')->where('type','credit')->get()->all();
            $orders=OrderBooking::all();
            $pr=PurchaseReturn::all()->all();
            $daily_credits= NonCashTransaction::where('debit_id','-1')->get()->all();
            $customer_payments = Payments::where("owner_type","App\Customer")->whereNull("sales_id")->whereNull("sales_invoice_id")->whereNull("purchase_invoice_id")->get();
            
            $total=0;
            $si_total=0;
            $p_total=0;

            foreach($sales_invoices as $sp){
                if($sp->remaining){
                    $total+=$sp->remaining;
                    $si_total+=$sp->remaining;
                }
                else{
                    if($sp->paid_amount){
                        $total+=$sp->paid_amount;
                        $si_total+=$sp->paid_amount;
                    }
                }
            }

            foreach($orders as $sp){
                $total+=$sp->advance;
                $o_total+=$sp->advance;
            }

            foreach($pr as $p){
                $pr_total+=$p->total_price;
                $total+=$p->total_price;
            }

            foreach($daily_credits as $p){
                $dc_total+=$p->amount;
                $total+=$p->amount;
            }

            foreach($customer_payments as $p){
                $cp_total+=$p->amount;
                $total+=$p->amount;
            }

            
        }

        else if($request['today']==1){
            $default=1;
            $today=Carbon::now()->setTimezone('Asia/Karachi');
            $sales_invoices = SalesInvoice::where('date','=',$today->format('Y-m-d'))->get();
            $payments=Payments::where('owner_type','App\Company')->where('type','credit')->where('date','=',$today->format('Y-m-d'))->get()->all();
            $orders = OrderBooking::where('date','=',$today->format('Y-m-d'))->get();
            $pr = PurchaseReturn::where('date','=',$today->format('Y-m-d'))->get();
            $daily_credits= NonCashTransaction::where('debit_id','-1')->where('date','=',$today->format('Y-m-d'))->get()->all();
            $customer_payments = Payments::where('date','=',$today->format('Y-m-d'))->where("owner_type","App\Customer")->whereNull("sales_id")->whereNull("sales_invoice_id")->whereNull("purchase_invoice_id")->get();

            foreach($sales_invoices as $sp){
                if($sp->remaining){
                    $total+=$sp->remaining;
                    $si_total+=$sp->remaining;
                }
                else{
                    if($sp->paid_amount){
                        $total+=$sp->paid_amount;
                        $si_total+=$sp->paid_amount;
                    }
                }
            }
            foreach($orders as $sp){
                $total+=$sp->advance;
                $o_total+=$sp->advance;
            }

            foreach($pr as $p){
                $pr_total+=$p->total_price;
                $total+=$p->total_price;
            }
            foreach($daily_credits as $p){
                $dc_total+=$p->amount;
                $total+=$p->amount;
            }
            foreach($customer_payments as $p){
                $cp_total+=$p->amount;
                $total+=$p->amount;
            }
        }

        else if($request['today']==2){
            $default=2;
            $on_date=Carbon::parse($request['on_date'])->setTimezone('Asia/Karachi');
            $sales_invoices = SalesInvoice::where('date','=',$on_date->toDateString())->get();
            $payments=Payments::where('owner_type','App\Company')->where('type','credit')->where('date','=',$on_date->toDateString())->get()->all();
            $orders = OrderBooking::where('date','=',$on_date->toDateString())->get();
            $pr = PurchaseReturn::where('date','=',$on_date->toDateString())->get();
            $daily_credits= NonCashTransaction::where('debit_id','-1')->where('date','=',$on_date->format('Y-m-d'))->get()->all();
            $customer_payments = Payments::where('date','=',$on_date->format('Y-m-d'))->where("owner_type","App\Customer")->whereNull("sales_id")->whereNull("sales_invoice_id")->whereNull("purchase_invoice_id")->get();

            foreach($sales_invoices as $sp){
                if($sp->remaining){
                    $total+=$sp->remaining;
                    $si_total+=$sp->remaining;
                }
                else{
                    if($sp->paid_amount){
                        $total+=$sp->paid_amount;
                        $si_total+=$sp->paid_amount;
                    }
                }
            }
            foreach($orders as $sp){
                $total+=$sp->advance;
                $o_total+=$sp->advance;
            }

            foreach($pr as $p){
                $pr_total+=$p->total_price;
                $total+=$p->total_price;
            }
            foreach($daily_credits as $p){
                $dc_total+=$p->amount;
                $total+=$p->amount;
            }
            foreach($customer_payments as $p){
                $cp_total+=$p->amount;
                $total+=$p->amount;
            }
        }

        else if($request['today']==3){
            $default=3;
            $from_date=Carbon::parse($request['from_date'])->setTimezone('Asia/Karachi');
            $to_date=Carbon::parse($request['to_date'])->setTimezone('Asia/Karachi');
            $sales_invoices = SalesInvoice:: where('date','>=',$from_date->toDateString())->where('date','<=',$to_date->toDateString())->get();
            $payments=Payments::where('owner_type','App\Company')->where('type','credit')-> where('date','>=',$from_date->toDateString())->where('date','<=',$to_date->toDateString())->get()->all();
            $orders = OrderBooking:: where('date','>=',$from_date->toDateString())->where('date','<=',$to_date->toDateString())->get();
            $pr = PurchaseReturn:: where('date','>=',$from_date->toDateString())->where('date','<=',$to_date->toDateString())->get();
            $daily_credits= NonCashTransaction::where('debit_id','-1')->where('date','>=',$from_date->format('Y-m-d'))->where('date','<=',$to_date->toDateString())->get()->all();
            $customer_payments = Payments::where('date','>=',$from_date->format('Y-m-d'))->where('date','<=',$to_date->toDateString())->where("owner_type","App\Customer")->whereNull("sales_id")->whereNull("sales_invoice_id")->whereNull("purchase_invoice_id")->get();

            foreach($sales_invoices as $sp){
                if($sp->remaining){
                    $total+=$sp->remaining;
                    $si_total+=$sp->remaining;
                }
                else{
                    if($sp->paid_amount){
                        $total+=$sp->paid_amount;
                        $si_total+=$sp->paid_amount;
                    }
                }
            }
            foreach($orders as $sp){
                $total+=$sp->advance;
                $o_total+=$sp->advance;
            }

            foreach($pr as $p){
                $pr_total+=$p->total_price;
                $total+=$p->total_price;
            }
            foreach($daily_credits as $p){
                $dc_total+=$p->amount;
                $total+=$p->amount;
            }
            foreach($customer_payments as $p){
                $cp_total+=$p->amount;
                $total+=$p->amount;
            }
           
        }

        $orders=$orders->all();
        $sales_invoices=$sales_invoices->all();

        usort($sales_invoices,function($a,$b){
            return strcmp($a->cust,$b->cust);
        });
        usort($orders,function($a,$b){
            return strcmp($a->cust,$b->cust);
        });
        usort($daily_credits,function($a,$b){
            return $a->date>$b->date;
        });

        return view('admin.reports.cash_in',compact(['dc_total','daily_credits','pr_total','pr','o_total','orders','to_date','from_date','on_date','default','total','si_total','p_total','sales_invoices','payments',"cp_total","customer_payments"]));


    }
}
