<?php

namespace App\Http\Controllers;

use App\CashRegister;
use App\User;
use Illuminate\Http\Request;

class AdminCustomerCashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $cash_registers=[];
        $cash_register=CashRegister::where('owner_type','=','App\Customer')->get();
        foreach($cash_register as $cr){
            if($cr->owner){
                if($cr->owner->type=='perm')
                    array_push($cash_registers,$cr);
            }

            if($cr->owner_id==0)
                array_push($cash_registers,$cr);
        }
        return view('admin.customer_cash.index',compact('cash_registers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $customer_cash=CashRegister::findOrFail($id);
        return view('admin.customer_cash.edit',compact('customer_cash'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        $customer_cash=CashRegister::findOrFail($id)->update($request->except('customer_name'));
        return redirect('/admin/customer_cash')->with('success','Cash Register Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        $company_cash=CashRegister::findOrFail($id)->delete();
        return redirect('/admin/customer_cash')->with('deleted','Cash Register Deleted');
    }
}
