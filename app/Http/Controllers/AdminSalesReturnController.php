<?php

namespace App\Http\Controllers;
use App\CashRegister;
use App\Customer;
use App\Payments;
use App\SalesInvoice;
use App\SalesReturn;
use App\SalesReturnDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use App\Item;
use App\Transformer\ItemTransformer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class AdminSalesReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index()
    {
        //
        $sales_returns=SalesReturn::all();
        return view('admin.sales_return.index',compact('sales_returns'));
    }*/

    public function index(){
        
        $this->authorize('sales', User::class);

        $sales_returns=SalesReturn::orderBy('date','DESC')->get();
        $customers=Customer::pluck('name','id')->all();
        /*$sales_return=SalesReturn::findOrFail(17);
        dd($sales_return->creditPayment());*/
        // try{
        //     DB::beginTransaction();
        //     foreach($sales_returns as $sr){
        //         if($sr->debitPayment()) {
        //             $sr->debitPayment()->update([
        //                 'type' => 'credit'
        //             ]);
        //         }
        //         if($sr->pay_later==1){
        //             $payment=[];
        //             $payment['owner_id']=$sr->customer_id;
        //             $payment['owner_type']='App\Customer';
        //             $payment['type']='debit';
        //             $payment['amount']=$sr->total_price;
        //             $payment['pay_later']=0;
        //             $payment['sales_id']=$sr->id;
        //             $payment['sales_type']='App\SalesReturn';
        //             $payment['date']=$sr->date;
        //         }
        //     }
        //     DB::commit();
        // }
        // catch(\Exception $e){
        //     DB::rollback();
        //     dd($e);
        // }


        return view('admin.sr.index',compact(['customers','sales_returns']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create()
    {
        //
        return redirect('admin/sales_invoice');
    }*/

    public function create(){
        $this->authorize('sales', User::class);
        return view('admin.sr.create');
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*public function store(Request $request)
    {
        //
        //dd($request->all());
        try {
            DB::beginTransaction();
            $sales = [];
            $sales['sales_invoice_id'] = $request['sales_invoice_id'];
            $sales['customer_id'] = $request['customer_id'];
            $sales['total_price'] = $request['total_price'];
            $sales['paid_amount'] = $request['paid_amount'];
            $sales['returned'] = $request['returned'];
            $sales['date'] = $request['date'];
            $id = SalesReturn::create($sales)->id;
            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->sales_return_id=$id;
                $iTit=SalesReturnDetails::create((array)$item);
                $ITEM=$iTit->item;
                $q=$ITEM->current_quantity+$item->quantity;
                $ITEM->update(['current_quantity'=>$q]);
            }
            if($request['customer_id']==0) {
                if ($cr = CashRegister::where('owner_id', '=', 0)->where('owner_type', '=', 'App\Customer')->get()->first()) {
                    $payment=[];
                    $payment['owner_id']=0;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='debit';
                    $payment['amount']=$request['total_price'];
                    $payment['pay_later']=0;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesReturn';
                    $payment['date']=$request['date'];
                    Payments::create($payment);
                    $received = $cr->received + $request['total_price'];
                    $cr->update(['received' => $received]);
                }
            }
            else{
                $customer=Customer::findOrFail($request['customer_id']);
                $cr=$customer->cashRegister;
                $received = $cr->received + $request['total_price'];
                $cr->update(['received' => $received]);
                $payment=[];
                $payment['owner_id']=$customer->id;
                $payment['owner_type']='App\Customer';
                $payment['type']='debit';
                $payment['amount']=$request['total_price'];
                $payment['pay_later']=0;
                $payment['sales_id']=$id;
                $payment['sales_type']='App\SalesReturn';
                $payment['date']=$request['date'];
                Payments::create($payment);
            }
            DB::commit();
            return redirect('admin/sales_return')->with('success','Sales Return Created');
            //dd('Success');
        }
        catch (\Exception $e){
            DB::rollback();
            dd($e);
        }
    }*/

    public function store(Request $request){

        $this->authorize('sales', User::class);

        //dd($request['pay_later']);
        try {
            DB::beginTransaction();
            $sales = [];
            $sales['sales_invoice_id'] = $request['sales_invoice_id'];
            $sales['customer_id'] = $request['customer_id'];
            $sales['total_price'] = $request['total_price'];
            $sales['paid_amount'] = $request['paid_amount'];
            $sales['pay_later'] = $request['pay_later'];
            $sales['name'] = $request['name'];
            $sales['number'] = $request['number'];
            $sales['date'] = $request['date'];
            $sales['comments'] = $request['comments'];
            $id = SalesReturn::create($sales)->id;
            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->sales_return_id=$id;
                $iTit=SalesReturnDetails::create((array)$item);
                $ITEM=$iTit->item;
                if($iTit->return_type=='amount'){
                    $q=$ITEM->current_quantity+$item->quantity;
                    $ITEM->update(['current_quantity'=>$q]);
                }
            }
            if($request['customer_id']==0) {
                if ($cr = CashRegister::where('owner_id', '=', 0)->where('owner_type', '=', 'App\Customer')->get()->first()) {
                    $payment = [];
                    $payment['owner_id'] = 0;
                    $payment['owner_type'] = 'App\Customer';
                    $payment['type'] = 'credit';
                    $payment['amount'] = $request['total_price'];
                    $payment['pay_later'] = 0;
                    $payment['sales_id'] = $id;
                    $payment['sales_type'] = 'App\SalesReturn';
                    $payment['date'] = $request['date'];
                    Payments::create($payment);
                    $received = $cr->received + $request['paid_amount'];
                    $to_receive = $cr->to_receive + $request['paid_amount'];
                    if ($request['pay_later'] == 0)
                        $cr->update(['received' => $received]);
                    else $cr->update(['to_receive' => $to_receive]);

                    if ($request['pay_later'] == 1) {
                        $payment = [];
                        $payment['owner_id'] = 0;
                        $payment['owner_type'] = 'App\Customer';
                        $payment['type'] = 'debit';
                        $payment['amount'] = $request['total_price'];
                        $payment['pay_later'] = 0;
                        $payment['sales_id'] = $id;
                        $payment['sales_type'] = 'App\SalesReturn';
                        $payment['date'] = $request['date'];
                    }
                }
            }
            else{
            $customer=Customer::findOrFail($request['customer_id']);
            $cr=$customer->cashRegister;
            $received = $cr->received + $request['paid_amount'];
            $to_receive=$cr->to_receive+$request['paid_amount'];
                if($request['pay_later']==0)
                    $cr->update(['received' => $received]);
                else $cr->update(['to_receive' => $to_receive]);
            $payment=[];
            $payment['owner_id']=$customer->id;
            $payment['owner_type']='App\Customer';
            $payment['type']='credit';
            $payment['amount']=$request['total_price'];
            $payment['pay_later']=0;
            $payment['sales_id']=$id;
            $payment['sales_type']='App\SalesReturn';
            $payment['date']=$request['date'];
            Payments::create($payment);
            if($request['pay_later']==1){
                $payment=[];
                $payment['owner_id']=$customer->id;
                $payment['owner_type']='App\Customer';
                $payment['type']='debit';
                $payment['amount']=$request['total_price'];
                $payment['pay_later']=0;
                $payment['sales_id']=$id;
                $payment['sales_type']='App\SalesReturn';
                $payment['date']=$request['date'];
                Payments::create($payment);
            }
        }
            DB::commit();
            return redirect('/admin/sales_return')->with('success','Sales Return Created');
        } catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('sales', User::class);

        //
        $sales_return =SalesReturn::findOrFail($id);
        $customer=$sales_return->customer;
        $Items=$sales_return->items;
        $items=Item::all();
        return view('admin.sr.show',compact(['sales_return','customer','items','Items']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($id)
    {
        //
        $sales_return =SalesReturn::findOrFail($id);
        $customer=$sales_return->customer;
        $payment=null;
        $cr=null;
        $items=$sales_return->items;
        $Item=$sales_return->salesInvoice->items;
        if($customer){
            $cr=$customer->cashRegister;
            $payment=$customer->lastPayment->first();

        }

        //dd($payment);

        return view('admin.sales_return.edit',compact(['sales_return','customer','payment','cr','items','Item']));
    }*/

    public function edit($id){

        $this->authorize('sales', User::class);

        $sales_return =SalesReturn::findOrFail($id);
        $user = Auth::user();
        $now = Carbon::now();
        if($user->role_id !=1 && Carbon::parse($sales_return->date)->diffInHours($now)>=24){
            return redirect()->back()->with('danger','Permission not allowed');
        }
        $customer=$sales_return->customer;
        $Items=$sales_return->items;
        $items=Item::all();
        return view('admin.sr.edit',compact(['sales_return','customer','items','Items']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function update(Request $request, $id)
    {
        //
        //dd($request->all());
        try {
            DB::beginTransaction();
            $sales = [];
            $sales['sales_invoice_id'] = $request['sales_invoice_id'];
            $sales['customer_id'] = $request['customer_id'];
            $sales['total_price'] = $request['total_price'];
            $sales['paid_amount'] = $request['paid_amount'];
            $sales['returned'] = $request['returned'];
            $sales['date'] = $request['date'];
            $sr=SalesReturn::findOrFail($id);
            foreach ($sr->items as $item){
                $ITEM=$item->item;
                $q=$ITEM->current_quantity-$item->quantity;
                $ITEM->update(['current_quantity'=>$q]);
                $item->delete();
            }

            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->sales_return_id=$id;
                SalesReturnDetails::create((array)$item);
            }

            if($sr->customer){
                $customer=Customer::findOrFail($request['customer_id']);
                $cr=$customer->cashRegister;
                $received = $cr->received - $sr->total_price;
                $received = $received + $request['total_price'];
                $cr->update(['received' => $received]);
                $sr->payment->delete();
                $payment=[];
                $payment['owner_id']=$customer->id;
                $payment['owner_type']='App\Customer';
                $payment['type']='debit';
                $payment['amount']=$request['total_price'];
                $payment['pay_later']=0;
                $payment['sales_id']=$id;
                $payment['sales_type']='App\SalesReturn';
                $payment['date']=$request['date'];
                Payments::create($payment);
            }
            else{
                $cr = CashRegister::where('owner_id', '=', 0)->where('owner_type', '=', 'App\Customer')->get()->first();
                    $received = $cr->received - $sr->payment->received;
                    $received = $received + $request['total_price'];
                    $cr->update(['received' => $received]);
                    $sr->payment->delete();
                    $payment=[];
                    $payment['owner_id']=$customer->id;
                    $payment['owner_type']='App\Customer';
                    $payment['type']='debit';
                    $payment['amount']=$request['total_price'];
                    $payment['pay_later']=0;
                    $payment['sales_id']=$id;
                    $payment['sales_type']='App\SalesReturn';
                    $payment['date']=$request['date'];
                    Payments::create($payment);

            }
            $sr->update($sales);
            DB::commit();
            return redirect('admin/sales_return')->with('success','Sales Return Edited');

        } catch (\PDOException $e) {

            DB::rollBack();
            return Redirect::back()->withInput(Input::all());
        }
    }*/

    public function update(Request $request, $id){

        $this->authorize('sales', User::class);

        // dd($request->all());
        try {
            DB::beginTransaction();
            $sales = [];
            $sales['sales_invoice_id'] = $request['sales_invoice_id'];
            $sales['customer_id'] = $request['customer_id'];
            $sales['total_price'] = $request['total_price'];
            $sales['paid_amount'] = $request['paid_amount'];
            $sales['pay_later'] = $request['pay_later'];
            $sales['name'] = $request['name'];
            $sales['number'] = $request['number'];
            $sales['date'] = $request['date'];
            $sr=SalesReturn::findOrFail($id);
            foreach ($sr->items as $item){
                $ITEM=$item->item;
                $q=$ITEM->current_quantity-$item->quantity;
                $ITEM->update(['current_quantity'=>$q]);

                if($item->return_type=='quantity'){
                    $q=$ITEM->current_quantity+$item->quantity;
                    $ITEM->update(['current_quantity'=>$q]);
                }
                $item->delete();
            }

            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->sales_return_id=$id;
                SalesReturnDetails::create((array)$item);
            }
            if($sr->customer){
                $customer=Customer::findOrFail($request['customer_id']);
                $cr=$customer->cashRegister;
                $received = $cr->received - $sr->paid_amount;
                $to_receive=$cr->to_receive- $sr->paid_amount;
                if($sr->pay_later==0)
                    $cr->update(['received' => $received]);
                else $cr->update(['to_receive' => $to_receive]);

                
                $received = $cr->received + $request['paid_amount'];
                $to_receive=$cr->to_receive+$request['paid_amount'];
                
                if($request['pay_later']==0)
                    $cr->update(['received' => $received]);
                else $cr->update(['to_receive' => $to_receive]);


                if($creditPayment=$sr->creditPayment()){
                    $creditPayment->update([
                        'amount'=>$request['total_price'],
                        'owner_id'=>$customer->id
                    ]);
                    
                }
                else {
                    $payment=[];
                    $payment['owner_id'] = $customer->id;
                    $payment['owner_type'] = 'App\Customer';
                    $payment['type'] = 'credit';
                    $payment['amount'] = $request['total_price'];
                    $payment['pay_later'] = 0;
                    $payment['sales_id'] = $id;
                    $payment['sales_type'] = 'App\SalesReturn';
                    $payment['date'] = $request['date'];
                    Payments::create($payment);
                }
                
                if($request['pay_later']==1){
                    if($debitPayment=$sr->debitPayment()){
                        $debitPayment->update([
                            'amount'=>$request['total_price'],
                            'owner_id'=>$customer->id
                        ]);
                    }
                    else {
                        //dd('here');
                        $payments = [];
                        $payments['owner_id'] = $customer->id;
                        $payments['owner_type'] = 'App\Customer';
                        $payments['type'] = 'debit';

                        $payments['amount'] = $request['total_price'];
                        $payments['pay_later'] = 0;
                        $payments['sales_id'] = $id;
                        $payments['sales_type'] = 'App\SalesReturn';
                        $payments['date'] = $request['date'];
                        $debitPayment=Payments::create($payments);

                    }
                }
                else{
                    if($debitPayment=$sr->debitPayment()){
                        $debitPayment->delete();
                    }
                }
            }
            else{
                $cr = CashRegister::where('owner_id', '=', 0)->where('owner_type', '=', 'App\Customer')->get()->first();
                $received = $cr->received - $sr->paid_amount;
                $to_receive=$cr->to_receive- $sr->paid_amount;
                if($sr->pay_later==0)
                    $cr->update(['received' => $received]);
                else $cr->update(['to_receive' => $to_receive]);


                $received = $cr->received + $request['paid_amount'];
                $to_receive=$cr->to_receive+$received['paid_amount'];
                if($request['pay_later']==0)
                    $cr->update(['received' => $received]);
                else $cr->update(['to_receive' => $to_receive]);
                //$sr->payment->delete();

                if($creditPayment=$sr->creditPayment()){
                    $creditPayment->update([
                        'amount'=>$request['total_price'],
                        'owner_id'=>0
                    ]);
                }
                else {
                    $payment=[];
                    $payment['owner_id'] = 0;
                    $payment['owner_type'] = 'App\Customer';
                    $payment['type'] = 'credit';
                    $payment['amount'] = $request['total_price'];
                    $payment['pay_later'] = 0;
                    $payment['sales_id'] = $id;
                    $payment['sales_type'] = 'App\SalesReturn';
                    $payment['date'] = $request['date'];
                    Payments::create($payment);
                }

                if($request['pay_later']==1){
                    if($debitPayment=$sr->debitPayment()){
                        $debitPayment->update([
                            'amount'=>$request['total_price'],
                            'owner_id'=>0
                        ]);
                    }
                    else {
                        $payment = [];
                        $payment['owner_id'] = 0;
                        $payment['owner_type'] = 'App\Customer';
                        $payment['type'] = 'debit';
                        $payment['amount'] = $request['total_price'];
                        $payment['pay_later'] = 0;
                        $payment['sales_id'] = $id;
                        $payment['sales_type'] = 'App\SalesReturn';
                        $payment['date'] = $request['date'];
                        Payments::create($payment);
                    }
                }
                else{
                    if($debitPayment=$sr->debitPayment()){
                        $debitPayment->delete();
                    }
                }

            }
            $sr->update($sales);

            DB::commit();
            return redirect('admin/sales_return')->with('success','Sales Return Edited');

        } catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy($id)
    {
        //
        try {
            DB::beginTransaction();
            $sr=SalesReturn::findOrFail($id);
            if($customer=$sr->customer){
                $cr=$customer->cashRegister;
                $received = $cr->received - $sr->total_price;
                $cr->update(['received' => $received]);
                $sr->payment->delete();
                foreach ($sr->items as $item){
                    $ITEM=$item->item;
                    $q=$ITEM->current_quantity-$item->quantity;
                    $ITEM->update(['current_quantity'=>$q]);
                    $item->delete();
                }
                $sr->delete();

            }
            else{
                $cr = CashRegister::where('owner_id', '=', 0)->where('owner_type', '=', 'App\Customer')->get()->first();
                $received = $cr->received - $sr->total_price;
                $cr->update(['received' => $received]);
                $sr->payment->delete();
                foreach ($sr->items as $item){
                    $item->delete();
                }
                $sr->delete();

            }
            DB::commit();
            return redirect('/admin/sales_return')->with('deleted','Sales Return Deleted');
        } catch (\PDOException $e) {

            DB::rollBack();
            return Redirect::back()->withInput(Input::all());
        }
    }*/

    public function destroy($id){

        $this->authorize('sales', User::class);

        try {
            DB::beginTransaction();
            $sr=SalesReturn::findOrFail($id);
            $user = Auth::user();
            $now = Carbon::now();
            if($user->role_id !=1 && Carbon::parse($sr->date)->diffInHours($now)>=24){
                return redirect()->back()->with('danger','Permission not allowed');
            }
            if($customer=$sr->customer){
                $cr=$customer->cashRegister;
                $received = $cr->received - $sr->paid_amount;
                $to_receive=$cr->to_receive- $sr->paid_amount;
                if($sr->pay_later==0)
                    $cr->update(['received' => $received]);
                else $cr->update(['to_receive' => $to_receive]);
                //$sr->payment->delete();
                foreach ($sr->items as $item){
                    $ITEM=$item->item;
                    $q=$ITEM->current_quantity-$item->quantity;
                    $ITEM->update(['current_quantity'=>$q]);

                    if($item->return_type=='quantity'){
                        $q=$ITEM->current_quantity+$item->quantity;
                        $ITEM->update(['current_quantity'=>$q]);
                    }
                    $item->delete();
                }


                //Payments Deletion
                if($debitPayment=$sr->debitPayment()){
                    $debitPayment->delete();
                }
                if($creditPayment=$sr->creditPayment()){
                    $creditPayment->delete();
                }



                $sr->delete();

            }
            else{
                $cr = CashRegister::where('owner_id', '=', 0)->where('owner_type', '=', 'App\Customer')->get()->first();
                $received = $cr->received - $sr->paid_amount;
                $to_receive=$cr->to_receive- $sr->paid_amount;
                if($sr->pay_later==0)
                    $cr->update(['received' => $received]);
                else $cr->update(['to_receive' => $to_receive]);
                $sr->payment->delete();
                foreach ($sr->items as $item){
                    $ITEM=$item->item;
                    $q=$ITEM->current_quantity+$item->quantity;
                    $ITEM->update(['current_quantity'=>$q]);

                    if($item->return_type=='quantity'){
                        $q=$ITEM->current_quantity-$item->quantity;
                        $ITEM->update(['current_quantity'=>$q]);
                    }
                    $item->delete();
                }
                $sr->delete();

            }
            DB::commit();
            return redirect('/admin/sales_return')->with('deleted','Sales Return Deleted');
        } catch (\Exception $e) {

            DB::rollBack();
            dd($e);
        }

    }

    /*public function createC($id){
        $sales_invoice=SalesInvoice::findOrFail($id);
        if($sales_invoice->salesReturn){
            return redirect('admin/sales_return')->with('deleted','Sales Return already exists, try editing the previous sales return');
        }
        $items=$sales_invoice->items;
        $customer=$sales_invoice->customer;
        $cr=null;
        $payment=null;
        if($customer){
            $payment=$customer->lastPayment->first();
            $cr=$customer->cashRegister;
        }
        return view('admin.sales_return.create',compact(['sales_invoice','items','customer','payment','cr']));
    }*/


    public function createC(Request $request){

        $this->authorize('sales', User::class);
        
        $customer=null;
        if($request['customer_id']!=0)
            $customer=Customer::findOrFail($request['customer_id']);
        $items=Item::all()->all();
        usort($items,function($a,$b){
            strcmp($a->name,$b->name);
        });

        return view('admin.sr.create',compact(['items','customer']));
    }


    public function printReturn(Request $request ,$id){
        
        $this->authorize('sales', User::class);

        $sales_return=SalesReturn::findOrFail($id);
        $items=$sales_return->items;
        $customer=null;
        if($sales_return->customer){
            $customer=$sales_return->customer;
        }

        $print_size = "a4";

        if($request->has("print_size"))
            $print_size= $request->print_size;
        if($print_size == "80"){
            \PDF::setOptions(['dpi'=>72]);
            //return view('admin.sales_invoice.print_80',compact(['sales_invoice','items','customer']));
            $pdf= \PDF::loadHTML(view('admin.sr.print_80',compact(['sales_return','items',"customer"])))->setPaper(array(0,0,204,650));
            return $pdf->stream();
        }
        else {
            \PDF::setOptions([]);
            $pdf= \PDF::loadHTML(view('admin.sr.print',compact(['sales_return','items',"customer"])))->setPaper("A4","portrait");
            return $pdf->stream();
        }
    
        return $pdf->stream();
    }

    public function apiEdit($id){

        $this->authorize('sales', User::class);

        $sales_return =SalesReturn::findOrFail($id);
        $customer=$sales_return->customer;
        $invoice_items = $sales_return->items()->with("item")->get();       
        $items = fractal($invoice_items->pluck("item")->all(),new ItemTransformer($customer->id))->toArray(); 
        return ['sales_return' => $sales_return,'customer' => $customer,'invoice_items' => $invoice_items,'items' => $items];
    }

    public function apiUpdate(Request $request, $id)
    {
        $this->authorize('sales', User::class);
        // dd($request->all());
        try {
            DB::beginTransaction();
            $sales = [];
            $sales['sales_invoice_id'] = $request['sales_invoice_id'];
            $sales['customer_id'] = $request['customer_id'];
            $sales['total_price'] = $request['total_price'];
            $sales['paid_amount'] = $request['paid_amount'];
            $sales['pay_later'] = $request['pay_later'];
            $sales['name'] = $request['name'];
            $sales['number'] = $request['number'];
            $sales['date'] = $request['date'];
            $sales['comments'] = $request['comments'];
            $sr=SalesReturn::findOrFail($id);
            foreach ($sr->items as $item){
                $ITEM=$item->item;
                $q=$ITEM->current_quantity-$item->quantity;
                $ITEM->update(['current_quantity'=>$q]);

                if($item->return_type=='quantity'){
                    $q=$ITEM->current_quantity+$item->quantity;
                    $ITEM->update(['current_quantity'=>$q]);
                }
                $item->delete();
            }

            $items=json_decode($request['items']);
            foreach($items as $item){
                $item->sales_return_id=$id;
                SalesReturnDetails::create((array)$item);
            }
            if($sr->customer){
                $customer=Customer::findOrFail($request['customer_id']);
                $cr=$customer->cashRegister;
                $received = $cr->received - $sr->paid_amount;
                $to_receive=$cr->to_receive- $sr->paid_amount;
                if($sr->pay_later==0)
                    $cr->update(['received' => $received]);
                else $cr->update(['to_receive' => $to_receive]);

                
                $received = $cr->received + $request['paid_amount'];
                $to_receive=$cr->to_receive+$request['paid_amount'];
                
                if($request['pay_later']==0)
                    $cr->update(['received' => $received]);
                else $cr->update(['to_receive' => $to_receive]);


                if($creditPayment=$sr->creditPayment()){
                    $creditPayment->update([
                        'amount'=>$request['total_price'],
                        'owner_id'=>$customer->id
                    ]);
                    
                }
                else {
                    $payment=[];
                    $payment['owner_id'] = $customer->id;
                    $payment['owner_type'] = 'App\Customer';
                    $payment['type'] = 'credit';
                    $payment['amount'] = $request['total_price'];
                    $payment['pay_later'] = 0;
                    $payment['sales_id'] = $id;
                    $payment['sales_type'] = 'App\SalesReturn';
                    $payment['date'] = $request['date'];
                    Payments::create($payment);
                }
                
                if($request['pay_later']==1){
                    if($debitPayment=$sr->debitPayment()){
                        $debitPayment->update([
                            'amount'=>$request['total_price'],
                            'owner_id'=>$customer->id
                        ]);
                    }
                    else {
                        //dd('here');
                        $payments = [];
                        $payments['owner_id'] = $customer->id;
                        $payments['owner_type'] = 'App\Customer';
                        $payments['type'] = 'debit';

                        $payments['amount'] = $request['total_price'];
                        $payments['pay_later'] = 0;
                        $payments['sales_id'] = $id;
                        $payments['sales_type'] = 'App\SalesReturn';
                        $payments['date'] = $request['date'];
                        $debitPayment=Payments::create($payments);

                    }
                }
                else{
                    if($debitPayment=$sr->debitPayment()){
                        $debitPayment->delete();
                    }
                }
            }
            else{
                $cr = CashRegister::where('owner_id', '=', 0)->where('owner_type', '=', 'App\Customer')->get()->first();
                $received = $cr->received - $sr->paid_amount;
                $to_receive=$cr->to_receive- $sr->paid_amount;
                if($sr->pay_later==0)
                    $cr->update(['received' => $received]);
                else $cr->update(['to_receive' => $to_receive]);


                $received = $cr->received + $request['paid_amount'];
                $to_receive=$cr->to_receive+$received['paid_amount'];
                if($request['pay_later']==0)
                    $cr->update(['received' => $received]);
                else $cr->update(['to_receive' => $to_receive]);
                //$sr->payment->delete();

                if($creditPayment=$sr->creditPayment()){
                    $creditPayment->update([
                        'amount'=>$request['total_price'],
                        'owner_id'=>0
                    ]);
                }
                else {
                    $payment=[];
                    $payment['owner_id'] = 0;
                    $payment['owner_type'] = 'App\Customer';
                    $payment['type'] = 'credit';
                    $payment['amount'] = $request['total_price'];
                    $payment['pay_later'] = 0;
                    $payment['sales_id'] = $id;
                    $payment['sales_type'] = 'App\SalesReturn';
                    $payment['date'] = $request['date'];
                    Payments::create($payment);
                }

                if($request['pay_later']==1){
                    if($debitPayment=$sr->debitPayment()){
                        $debitPayment->update([
                            'amount'=>$request['total_price'],
                            'owner_id'=>0
                        ]);
                    }
                    else {
                        $payment = [];
                        $payment['owner_id'] = 0;
                        $payment['owner_type'] = 'App\Customer';
                        $payment['type'] = 'debit';
                        $payment['amount'] = $request['total_price'];
                        $payment['pay_later'] = 0;
                        $payment['sales_id'] = $id;
                        $payment['sales_type'] = 'App\SalesReturn';
                        $payment['date'] = $request['date'];
                        Payments::create($payment);
                    }
                }
                else{
                    if($debitPayment=$sr->debitPayment()){
                        $debitPayment->delete();
                    }
                }

            }
            $sr->update($sales);

            DB::commit();
            return $id;

        } catch (\PDOException $e) {

            DB::rollBack();
            dd($e);
        }
    }
}
