<?php

namespace App\Http\Controllers;

use App\Company;
use App\Discount;
use App\Item;
use App\Quality;
use App\User;
use Illuminate\Http\Request;

class AdminDiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $qualities=Quality::orderBy('name')->pluck('name','id')->all();
        $discounts=Discount::orderBy('quality_id')->get();
        return view('admin.discount.index',compact(['qualities','discounts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('owner', User::class);
        //
        $companies=Company::all();
        $items=Item::all();
        return view('admin.discount.create',compact(['companies','items']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('owner', User::class);
        //
        //dd($request->all());
        Discount::create($request->all());
        return redirect('/admin/discount')->with('success','Discount Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('owner', User::class);
        //
        $discount=Discount::findOrFail($id);
        $items=$discount->quality->items->pluck('name','id')->all();
        return view('admin.discount.edit',compact(['discount','items']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('owner', User::class);
        //
        $discount=Discount::findOrFail($id);
        $discount->update($request->all());
        return redirect('/admin/discount')->with('success','Discount Updated');
    }

    public function destroy($id)
    {
        $this->authorize('owner', User::class);
        //
        Discount::findOrFail($id)->delete();
        return redirect('/admin/discount')->with('deleted','Discount Deleted');
    }

    public function createC(Request $request){

        $this->authorize('owner', User::class);

        $quality=Quality::findOrFail($request['quality_id']);
        //dd($quality->items);
        $items=$quality->items->pluck('name','id')->all();
        //dd($items);
        //$items=Item::pluck('name','id')->all();
        return view('admin.discount.create',compact(['items','quality']));
    }
}
