<?php

namespace App\Http\Controllers;

use App\SalesInvoice;
use App\SalesReturn;
use Illuminate\Http\Request;
use App\Customer;
use App\Payments;
use App\OrderBooking;
use App\User;

class AdminCustomerLedgerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        return view('admin.customer_ledger.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAll(){
        /*$payments= Payments::where('owner_type','=','App\Customer')->with('owner')->orderBy('date')->get();
        foreach($payments as $p){
            $p->customer=$p->cust;
        }*/
        $this->authorize('owner', User::class);

        $returnArray=[];
        $si=SalesInvoice::all()->all();
        $sr=SalesReturn::all()->all();
        $payments=Payments::where('owner_type','=','App\Customer')
            ->where('sales_type',null)->get()->all();

        $order=OrderBooking::all()->all();

        foreach ($order as $s){
            $s->Type="orderBooking";
            $s->owner_id=$s->customer_id;
            array_push($returnArray,$s);

        }

        foreach ($si as $s){
            $s->Type="salesInvoice";
            $s->owner_id=$s->customer_id;
            array_push($returnArray,$s);

        }

        foreach ($sr as $s){
            if($debitPayment=$s->debitPayment()){
                $debitPayment->Type='salesReturnPayment';
                array_push($returnArray,$debitPayment);
            }
            $s->Type="salesReturn";
            $s->owner_id=$s->customer_id;
            $s->Items=$s->items->all();
            array_push($returnArray,$s);

            

        }
        foreach ($payments as $s){
            $s->Type="payments";
            array_push($returnArray,$s);
        }
        usort($returnArray,function($a,$b){
            return $a->date>$b->date;
        });
        
        return $returnArray;
    }

    public function getCust(){
        
        $this->authorize('owner', User::class);

        return Customer::orderBy('name')->get();
    }
}
