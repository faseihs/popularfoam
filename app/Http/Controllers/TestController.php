<?php

namespace App\Http\Controllers;

use App\Item;
use App\SalesInvoice;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TestController extends Controller
{
    //

    public function index(){
        $item=Item::find(38);

        $from_date=Carbon::parse('23-10-2019');
        $to_date=Carbon::parse('23-10-2019');
        //dd($to_date);
        $allSales=[];
        $totalQuantity=0;
        $totalPurchaseValue=0;
        $totalSaleValue=0;
        $totalProfit=0;
        $sales=SalesInvoice::select('sales_invoices.*')
            ->join('sales_invoice_details','sales_invoices.id','=','sales_invoice_details.sales_invoice_id')
            ->where('date','>=',$from_date->toDateString())->where('date','<=',$to_date->toDateString())
            ->where('sales_invoice_details.item_id',13)
            ->get();
        //dd($sales);
        foreach($sales as $s){
            $salesItems=[];
            $date=Carbon::parse($s->date);
            $finalProfit=0;
            $finalPurchase=0;
            $finalQuantity=0;


            foreach($s->items as $sItems){
                $itemStats=[];
                $arr=$sItems->item->getTotalBefore($date,$date);
                dd($arr);
                $salesCount=$arr['countSales'];
                $purchases=$arr['purchases'];
                $sales=$arr['sales'];
                $it=$sItems->item;
                $iProfit=0;
                $iPurchase=0;
                $iSale=0;


                for($i=0;$i<(int)$sItems->quantity;$i++){
                    $profit=0;
                    if(count($purchases)==0){

                        //$profit=$sales[$i]->price-$it->getDiscountedPrice($it->purchase_price);
                        $salesPrice=$sItems->price * (1-($sItems->discount/100));
                        $profit=$salesPrice-$it->getDiscountedPrice($it->purchase_price);
                        $finalPurchase+=$it->getDiscountedPrice($it->purchase_price);
                        $iSale+=$salesPrice;
                        $iPurchase+=$it->getDiscountedPrice($it->purchase_price);
                        // if($sItems->item_id==99)
                        // dd($salesPrice.'-'.$it->getDiscountedPrice($it->purchase_price));
                        array_push($itemStats,
                            ['sale'=>$salesPrice,
                                'purchase'=>$it->getDiscountedPrice($it->purchase_price),
                                'profit'=>$profit


                            ]);

                    }
                    else{

                        if((count($purchases)-1)>=$i){

                            //$profit=$sales[$i]->price-$purchases[$i]->price;
                            $salesPrice=$sItems->price * (1-($sItems->discount/100));
                            $profit=$salesPrice-$purchases[$i]->price;
                            $finalPurchase+=$purchases[$i]->price;
                            $iSale+=$salesPrice;
                            $iPurchase+=$purchases[$i]->price;
                            array_push($itemStats,
                                ['sale'=>$salesPrice,
                                    'purchase'=>$purchases[$i]->price,
                                    'profit'=>$profit


                                ]);
                        }
                        else {

                            //$profit=$sales[$i]->price-$purchases[(count($purchases)-1)]->price;
                            $salesPrice=$sItems->price * (1-($sItems->discount/100));
                            $profit=$salesPrice-$purchases[(count($purchases)-1)]->price;
                            $finalPurchase+=$purchases[(count($purchases)-1)]->price;
                            $iSale+=$salesPrice;
                            $iPurchase+=$purchases[(count($purchases)-1)]->price;

                            array_push($itemStats,
                                ['sale'=>$salesPrice,
                                    'purchase'=>$purchases[(count($purchases)-1)]->price,
                                    'profit'=>$profit


                                ]);
                        }
                    }
                    //dd($itemStats);
                    $iProfit+=$profit;


                }
                $itemObj = new \stdClass;
                $itemObj->stats=$itemStats;
                $itemObj->date=$s->date;
                $itemObj->id=$sItems->item_id;
                $itemObj->name=$sItems->item->name;
                $itemObj->quality=$sItems->item->quality->name;
                $itemObj->iProfit=$iProfit;
                $itemObj->quantity=$sItems->quantity;
                $itemObj->iSale=$sItems->total_price;

                if($sItems->quantity<1){
                    $itemObj->iPurchase=$sItems->quantity*$sItems->item->getDiscountedPrice($sItems->item->purchase_price);

                    $finalPurchase+=$itemObj->iPurchase;
                    $itemObj->$iProfit=$sItems->total_price-$itemObj->iPurchase;
                }
                else{
                    if(floor($sItems->quantity)!=$sItems->quantity){
                        $rem_quantity=$sItems->quantity-floor($sItems->quantity);
                        $itemObj->iPurchase=$iPurchase+($rem_quantity*($itemObj->stats[0]['purchase']));
                        $finalPurchase+=$rem_quantity*($itemObj->stats[0]['purchase']);
                        $itemObj->iProfit+=(($rem_quantity*($itemObj->stats[0]['sale']))-($rem_quantity*($itemObj->stats[0]['purchase'])));
                    }
                    else
                        $itemObj->iPurchase=$iPurchase;
                }

                $finalProfit+=$itemObj->iProfit;
                $finalQuantity+=$sItems->quantity;
                dd($itemObj);
                //$oneItemObj=new \stdClass;
                array_push($salesItems,$itemObj);
            }

            $salesInvoice= new \stdClass;
            $salesInvoice->items=$salesItems;
            $salesInvoice->id=$s->id;
            $salesInvoice->customer=$s->cust;
            $salesInvoice->total_price=$s->total_price;
            $salesInvoice->date=$s->date;
            $salesInvoice->finalProfit=$finalProfit;
            $salesInvoice->finalPurchase=$finalPurchase;
            $salesInvoice->quantity=$finalQuantity;
            $totalQuantity+=$salesInvoice->quantity;
            $totalPurchaseValue+=$salesInvoice->finalPurchase;
            $totalSaleValue+=$salesInvoice->total_price;
            $totalProfit+=$salesInvoice->finalProfit;
            array_push($allSales,$salesInvoice);
        }

    }
}
