<?php

namespace App\Http\Controllers;

use App\Worker;
use Illuminate\Http\Request;
use App\User;
use App\Company;
use App\Item;
use App\Vehicle;
use App\Category;
use App\SalesInvoice;
use App\SalesReturn;
use App\PurchaseInvoice;
use App\PurchaseOrder;

class AdminMonthlyOverall extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('owner', User::class);
        //
        $workers=Worker::all();
        $worker_count=$workers->count();
        $ws=0;
        foreach ($workers as $w){
            $ws+=$w->salary;
        }
        $company_count=Company::count();
        $item_count=Item::count();
        $category_count=Category::count();
        $vehicle_count=Vehicle::count();
        $si_count=SalesInvoice::count();
        $si_bill=0;
        foreach(SalesInvoice::all() as $si){
            $si_bill+=$si->total_price;
        }

        $sr_count=SalesReturn::count();
        $sr_bill=0;
        foreach(SalesReturn::all() as $sr){
            $sr_bill+=$sr->total_price;
        }

        $pi_count=PurchaseInvoice::count();
        $pi_bill=0;
        foreach(PurchaseInvoice::all() as $pi){
            $pi_bill+=$pi->total_price;
        }

        $po_count=PurchaseOrder::count();
        $po_bill=0;
        foreach(PurchaseOrder::all() as $po){
            $po_bill+=$po->total_price;
        }



        return view('admin.reports.monthly',compact(['ws',
            'worker_count','company_count','item_count','vehicle_count','category_count',
            'si_count','si_bill','sr_count','sr_bill','pi_count','pi_bill','po_count','po_bill'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
