<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CashRegister;
use App\User;

class AdminLedgerController extends Controller
{
    //

    public function index(){

        $this->authorize('owner', User::class);

        $customer_cash=CashRegister::where('owner_type','App\Customer')->get();
        $company_cash=CashRegister::where('owner_type','App\Company')->get();
        $companyTotal=new \stdClass();
        $companyTotal->paid=0;$companyTotal->to_pay=0;$companyTotal->to_receive=0;$companyTotal->received=0;
        $customerTotal=new \stdClass();
        $customerTotal->paid=0;$customerTotal->to_pay=0;$customerTotal->to_receive=0;$customerTotal->received=0;


        $grandTotal=new \stdClass();
        $grandTotal->paid=0;$grandTotal->to_pay=0;$grandTotal->to_receive=0;$grandTotal->received=0;

        foreach($company_cash as $c){
            $companyTotal->paid+=$c->received;
            $companyTotal->to_pay+=$c->to_receive;
            $companyTotal->received+=$c->paid;
            $companyTotal->to_receive+=$c->to_pay;

            $grandTotal->paid+=$companyTotal->paid;
            $grandTotal->to_pay+=$companyTotal->to_pay;
            $grandTotal->received+=$companyTotal->received;
            $grandTotal->to_receive+=$companyTotal->to_receive;

        }

        foreach($customer_cash as $c){
            $customerTotal->paid+=$c->received;
            $customerTotal->to_pay+=$c->to_receive;
            $customerTotal->received+=$c->paid;
            $customerTotal->to_receive+=$c->to_pay;

            $grandTotal->paid+= $customerTotal->paid;
            $grandTotal->to_pay+=$customerTotal->to_pay;
            $grandTotal->received+=$customerTotal->received;
            $grandTotal->to_receive+=$customerTotal->to_receive;

        }

        return view('admin.ledger.index',compact([
            'customer_cash','customerTotal'
            ,'company_cash','companyTotal',
            'grandTotal'
        ]));
    }

    public function getCompanyPayments(){

        $this->authorize('owner', User::class);

        return Payments::where('owner_type','=','App\Company')->with('owner')->orderBy('date')->get();

    }

    public function getCompanies(){
        $this->authorize('owner', User::class);
        return Company::all();
    }

    public function getCustomers(){
        $this->authorize('owner', User::class);
        return Customer::orderBy('name')->get();
    }

    public function getCustomerPayments(){
        /*$payments= Payments::where('owner_type','=','App\Customer')->with('owner')->orderBy('date')->get();
        foreach($payments as $p){
            $p->customer=$p->cust;
        }*/
        $this->authorize('owner', User::class);
        $returnArray=[];
        $si=SalesInvoice::all()->all();
        $sr=SalesReturn::all()->all();
        $payments=Payments::where('owner_type','=','App\Customer')
            ->where('sales_type',null)->get()->all();


        foreach ($si as $s){
            $s->Type="salesInvoice";
            $s->owner_id=$s->customer_id;
            array_push($returnArray,$s);

        }

        foreach ($sr as $s){
            $s->Type="salesReturn";
            $s->owner_id=$s->customer_id;
            $s->Items=$s->items->all();
            array_push($returnArray,$s);

        }
        foreach ($payments as $s){
            $s->Type="payments";
            array_push($returnArray,$s);
        }
        usort($returnArray,function($a,$b){
            return $a->date>$b->date;
        });
        return $returnArray;
    }

    public function ledger(){
        $this->authorize('owner', User::class);
        return view('admin.ledger.ledger');
    }
}
