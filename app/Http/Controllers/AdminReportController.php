<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use Carbon\Carbon;
use App\SalesInvoice;
use App\User;

class AdminReportController extends Controller
{
    //
    public function itemIndex(){

        $this->authorize('owner', User::class);

        set_time_limit(0);
        $po=[];
        $items=Item::all();

        // foreach($items as $it){
        //     $opening_price=$it->purchase_price;
        //     $it->update(['opening_price'=>$opening_price]);
        // }
        $FProfit=0;
        $FPVal=0;
        $FSales=0;
        $FSVal=0;
        foreach($items as $it){
            $stats=[];
            $salesCount=$it->countSales();
            $purchases=$it->getNumPurchases($salesCount);
            //dd(count($purchases).' '.$salesCount);
            $sales=$it->firstSales($salesCount);
            //dd(count($sales));

            //dd($sales);

            $cummProfit=0;
            $indPurchase=0;
            $indSale=0;
            for($i=0;$i<$salesCount;$i++){
                
                $profit=0;
                if(count($purchases)==0){
                    $salesPrice=$sales[$i]->price * (1-($sales[$i]->discount/100));
                    $profit=$salesPrice-$it->getDiscountedPrice($it->purchase_price);
                    array_push($stats,
                    ['sale'=>$salesPrice,
                    'purchase'=>$it->getDiscountedPrice($it->purchase_price),
                    'profit'=>$profit


                    ]);
                }
                else{
                   
                    if((count($purchases)-1)>=$i){
                        $salesPrice=$sales[$i]->price * (1-($sales[$i]->discount/100));
                        $profit=$salesPrice-$purchases[$i]->price;
                        array_push($stats,
                    ['sale'=>$salesPrice,
                    'purchase'=>$purchases[$i]->price,
                    'profit'=>$profit


                    ]);
                    }
                    else {
                        $salesPrice=$sales[$i]->price * (1-($sales[$i]->discount/100));
                        $profit=$salesPrice-$purchases[(count($purchases)-1)]->price;
                        array_push($stats,
                    ['sale'=>$salesPrice,
                    'purchase'=>$purchases[(count($purchases)-1)]->price,
                    'profit'=>$profit


                    ]);
                    }
                }
                $cummProfit+=$profit;
                
            }    
            $item= new \stdClass;
            $item->id=$it->id;
            $item->stats=$stats;
            $item->sales= $salesCount;
            $item->purchases=$it->countPurchases();
            $opArray=$it->getOpeningDiscount();
            $item->opening_quantity=$it->opening_quantity;
            $item->opening_total=$opArray['price'];
            $item->name=$it->name;
            $item->quality=$it->quality->name;
            $item->profit=$cummProfit;
            foreach($item->stats as $s){
                $indPurchase+=$s['purchase'];
                $indSale+=$s['sale'];
            }
            $FProfit+=$cummProfit;
           
            $item->cummSale=$indSale;
            $item->cummPurchase=$indPurchase;
            $FPVal+=$item->cummPurchase;
            $FSVal+=$item->cummSale;
            $FSales+=$item->sales;

            if($item->sales>0)
                array_push($po,$item);
        }
        $items=$po;
        //dd($items);
        $default='all';
        return view('admin.profit.item',compact(['FSVal','FSales','FPVal','FProfit','default','items']));
    }


    public function itemIndexMain(){

        $this->authorize('owner', User::class);

            $po=[];
            $items=[];

            // foreach($items as $it){
            //     $opening_price=$it->purchase_price;
            //     $it->update(['opening_price'=>$opening_price]);
            // }
            $FProfit=0;
            $FPVal=0;
            $FSales=0;
            $FSVal=0;
            foreach($items as $it){
                $stats=[];
                $salesCount=$it->countSales();
                $purchases=$it->getNumPurchases($salesCount);
                //dd(count($purchases).' '.$salesCount);
                $sales=$it->firstSales($salesCount);
                //dd(count($sales));

                //dd($sales);

                $cummProfit=0;
                $indPurchase=0;
                $indSale=0;
                for($i=0;$i<$salesCount;$i++){

                    $profit=0;
                    if(count($purchases)==0){
                        $salesPrice=$sales[$i]->price * (1-($sales[$i]->discount/100));
                        $profit=$salesPrice-$it->getDiscountedPrice($it->purchase_price);
                        array_push($stats,
                            ['sale'=>$salesPrice,
                                'purchase'=>$it->getDiscountedPrice($it->purchase_price),
                                'profit'=>$profit


                            ]);
                    }
                    else{

                        if((count($purchases)-1)>=$i){
                            $salesPrice=$sales[$i]->price * (1-($sales[$i]->discount/100));
                            $profit=$salesPrice-$purchases[$i]->price;
                            array_push($stats,
                                ['sale'=>$salesPrice,
                                    'purchase'=>$purchases[$i]->price,
                                    'profit'=>$profit


                                ]);
                        }
                        else {
                            $salesPrice=$sales[$i]->price * (1-($sales[$i]->discount/100));
                            $profit=$salesPrice-$purchases[(count($purchases)-1)]->price;
                            array_push($stats,
                                ['sale'=>$salesPrice,
                                    'purchase'=>$purchases[(count($purchases)-1)]->price,
                                    'profit'=>$profit


                                ]);
                        }
                    }
                    $cummProfit+=$profit;

                }
                $item= new \stdClass;
                $item->id=$it->id;
                $item->stats=$stats;
                $item->sales= $salesCount;
                $item->purchases=$it->countPurchases();
                $opArray=$it->getOpeningDiscount();
                $item->opening_quantity=$it->opening_quantity;
                $item->opening_total=$opArray['price'];
                $item->name=$it->name;
                $item->quality=$it->quality->name;
                $item->profit=$cummProfit;
                foreach($item->stats as $s){
                    $indPurchase+=$s['purchase'];
                    $indSale+=$s['sale'];
                }
                $FProfit+=$cummProfit;

                $item->cummSale=$indSale;
                $item->cummPurchase=$indPurchase;
                $FPVal+=$item->cummPurchase;
                $FSVal+=$item->cummSale;
                $FSales+=$item->sales;

                if($item->sales>0)
                    array_push($po,$item);
            }
            $items=$po;
            //dd($items);
            $default='all';
            return view('admin.profit.item',compact(['FSVal','FSales','FPVal','FProfit','default','items']));
    }


    public function itemQuery(Request $request){

        $this->authorize('owner', User::class);

        //dd($request->all());
        if($request['today']=='all')
            return redirect('/admin/profit/item/all');
        else{
            $from_date=Carbon::parse($request['from_date']);
            $to_date=Carbon::parse($request['to_date']);

            // $item=Item::findOrFail(1);
            // $item->getTotalBefore($from,$to);
            $po=[];
            $items=Item::all();
            $FProfit=0;
            $FPVal=0;
        $FSales=0;
        $FSVal=0;
        foreach($items as $it){
            $stats=[];
            $arr=$it->getTotalBefore($from_date,$to_date);
            //dd($arr);
            $salesCount=$arr['countSales'];
            $purchases=$arr['purchases'];
           
            $sales=$arr['sales'];
            
            $cummProfit=0;
            $indPurchase=0;
            $indSale=0;
            $iQuantity=0;
           /* if($it->id==251){
                dd($salesCount);
            }*/
            for($i=0;$i<$salesCount;$i++){
                //dd($sales);
                $iQuantity+=$sales[$i]->quantity;
                $profit=0;
                if($sales[$i]->quantity<1){
                    $salesPrice = $sales[$i]->price * (1 - ($sales[$i]->discount / 100));
                    $profit = $salesPrice - ($sales[$i]->quantity*$it->getDiscountedPrice($it->purchase_price));
                    array_push($stats,
                        ['sale' => $salesPrice,
                            'purchase' => $sales[$i]->quantity*$it->getDiscountedPrice($it->purchase_price),
                            'profit' => $profit


                        ]);
                }
                else {
                    if (count($purchases) == 0) {
                        $salesPrice = $sales[$i]->price * (1 - ($sales[$i]->discount / 100));
                        $profit = $salesPrice - $it->getDiscountedPrice($it->purchase_price);
                        array_push($stats,
                            ['sale' => $salesPrice,
                                'purchase' => $it->getDiscountedPrice($it->purchase_price),
                                'profit' => $profit


                            ]);
                    } else {

                        if ((count($purchases) - 1) >= $i) {
                            $salesPrice = $sales[$i]->price * (1 - ($sales[$i]->discount / 100));
                            $profit = $salesPrice - $purchases[$i]->price;
                            array_push($stats,
                                ['sale' => $salesPrice,
                                    'purchase' => $purchases[$i]->price,
                                    'profit' => $profit


                                ]);
                        } else {
                            $salesPrice = $sales[$i]->price * (1 - ($sales[$i]->discount / 100));
                            $profit = $salesPrice - $purchases[(count($purchases) - 1)]->price;
                            array_push($stats,
                                ['sale' => $salesPrice,
                                    'purchase' => $purchases[(count($purchases) - 1)]->price,
                                    'profit' => $profit


                                ]);
                        }
                    }
                }
                $cummProfit+=$profit;
                
            }    
            $item= new \stdClass;
            $item->id=$it->id;
            $item->stats=$stats;

            //$item->sales= $salesCount;
            $item->sales=$iQuantity;
            /*if($it->id==251){
            dd($iQuantity);
        }*/
            $item->purchases=$it->countPurchases();
            $opArray=$it->getOpeningDiscount();
            $item->opening_quantity=$it->opening_quantity;
            $item->opening_total=$opArray['price'];
            $item->name=$it->name;
            $item->quality=$it->quality->name;
            $item->profit=$cummProfit;




            $FProfit+=$cummProfit;
            foreach($item->stats as $s){
                $indPurchase+=$s['purchase'];
                $indSale+=$s['sale'];
            }
            $item->cummSale=$indSale;
            $item->cummPurchase=$indPurchase;
            //if($salesCount>0)
            //dd($item);
            $FPVal+=$item->cummPurchase;
            $FSVal+=$item->cummSale;
            $FSales+=$item->sales;
            if($item->sales>0)
                array_push($po,$item);
        }
            $items=$po;
            $default='3';
            return view('admin.profit.item',compact(['FSVal','FSales','FPVal','FProfit','default','items','from_date','to_date']));
        }
    }


    public function billIndex(){ 

        $this->authorize('owner', User::class);

        ini_set('max_execution_time', 300);
        $allSales=[];
        $sales=SalesInvoice::all();
        $totalQuantity=0;
        $totalPurchaseValue=0;
        $totalSaleValue=0;
        $totalProfit=0;
        foreach($sales as $s){
            $salesItems=[];
            $date=Carbon::parse($s->date);
            $finalProfit=0;
            $finalPurchase=0;
            $finalQuantity=0;
            foreach($s->items as $sItems){
                $itemStats=[];
                $arr=$sItems->item->getTotalBefore($date,$date);
                $salesCount=$arr['countSales'];
                $purchases=$arr['purchases'];
                $sales=$arr['sales'];
                $it=$sItems->item;
                $iProfit=0;
                $iPurchase=0;
                $iSale=0;
                
               
                for($i=0;$i<floor($sItems->quantity);$i++){
                    $profit=0;
                   
                    if(count($purchases)==0){
                        
                        //$profit=$sales[$i]->price-$it->getDiscountedPrice($it->purchase_price);
                        $salesPrice=$sItems->price * (1-($sItems->discount/100));
                        $profit=$salesPrice-$it->getDiscountedPrice($it->purchase_price);
                        $finalPurchase+=$it->getDiscountedPrice($it->purchase_price);
                        $iSale+=$salesPrice;
                        $iPurchase+=$it->getDiscountedPrice($it->purchase_price);
                        array_push($itemStats,
                        ['sale'=>$salesPrice,
                        'purchase'=>$it->getDiscountedPrice($it->purchase_price),
                        'profit'=>$profit


                        ]);
                       
                       
                    }
                    else{
                    
                        if((count($purchases)-1)>=$i){
                            
                            //$profit=$sales[$i]->price-$purchases[$i]->price;
                            $salesPrice=$sItems->price * (1-($sItems->discount/100));
                            $profit=$salesPrice-$purchases[$i]->price;
                            $finalPurchase+=$purchases[$i]->price;
                            $iSale+=$salesPrice;
                            $iPurchase+=$purchases[$i]->price;
                            array_push($itemStats,
                        ['sale'=>$salesPrice,
                        'purchase'=>$purchases[$i]->price,
                        'profit'=>$profit


                        ]);
                        }
                        else {
                        
                            //$profit=$sales[$i]->price-$purchases[(count($purchases)-1)]->price;
                            $salesPrice=$sItems->price * (1-($sItems->discount/100));
                            $profit=$salesPrice-$purchases[(count($purchases)-1)]->price;
                            $finalPurchase+=$purchases[(count($purchases)-1)]->price;
                            $iSale+=$salesPrice;
                            $iPurchase+=$purchases[(count($purchases)-1)]->price;
                            array_push($itemStats,
                        ['sale'=>$salesPrice,
                        'purchase'=>$purchases[(count($purchases)-1)]->price,
                        'profit'=>$profit


                        ]);
                        }
                    }
                    //dd($itemStats);
                    $iProfit+=$profit;
                   
                }
                $itemObj = new \stdClass;
                $itemObj->stats=$itemStats;
                $itemObj->date=$s->date;
                $itemObj->id=$sItems->item_id;
                $itemObj->name=$sItems->item->name;
                $itemObj->quality=$sItems->item->quality->name;
                $itemObj->iProfit=$iProfit;
                $itemObj->quantity=$sItems->quantity;
                $itemObj->iSale=$sItems->total_price;
                if($sItems->quantity<1){
                    $itemObj->iPurchase=$sItems->quantity*$sItems->item->getDiscountedPrice($sItems->item->purchase_price);
                    
                    $finalPurchase+=$itemObj->iPurchase;
                    $itemObj->$iProfit=$sItems->total_price-$itemObj->iPurchase;
                }
                else{
                    if(floor($sItems->quantity)!=$sItems->quantity){
                        $rem_quantity=$sItems->quantity-floor($sItems->quantity);
                        $itemObj->iPurchase=$iPurchase+($rem_quantity*($itemObj->stats[0]['purchase']));
                        $finalPurchase+=$rem_quantity*($itemObj->stats[0]['purchase']);
                        $itemObj->iProfit+=(($rem_quantity*($itemObj->stats[0]['sale']))-($rem_quantity*($itemObj->stats[0]['purchase'])));
                    }
                    else  
                    $itemObj->iPurchase=$iPurchase;
                }
                $finalProfit+=$itemObj->iProfit;
                $finalQuantity+=$sItems->quantity;
                // if($s->id==198)
                //     dd($itemObj);
                //$oneItemObj=new \stdClass;
                array_push($salesItems,$itemObj);
            }

            $salesInvoice= new \stdClass;
            $salesInvoice->items=$salesItems;
            $salesInvoice->id=$s->id;
            $salesInvoice->total_price=$s->total_price;
            $salesInvoice->date=$s->date;
            $salesInvoice->finalProfit=$finalProfit;
            $salesInvoice->finalPurchase=$finalPurchase;
            $salesInvoice->quantity=$finalQuantity;
            $salesInvoice->customer=$s->cust;


            $totalQuantity+=$salesInvoice->quantity;
            $totalPurchaseValue+=$salesInvoice->finalPurchase;
            $totalSaleValue+=$salesInvoice->total_price;
            $totalProfit+=$salesInvoice->finalProfit;
            array_push($allSales,$salesInvoice); 
        }

        

        usort($allSales,function($a,$b){
            return Carbon::parse($a->date)<Carbon::parse($b->date);
        });
        //dd($allSales);
        $default='all';
        return view('admin.profit.bill',compact(['default','allSales'
            ,'totalQuantity'
            ,'totalPurchaseValue'
            ,'totalSaleValue'
            ,'totalProfit'
        ]));
    }


    public function billIndexMain(){

        $this->authorize('owner', User::class);

        ini_set('max_execution_time', 300);
        $allSales=[];
        $sales=[];
        $totalQuantity=0;
        $totalPurchaseValue=0;
        $totalSaleValue=0;
        $totalProfit=0;
        foreach($sales as $s){
            $salesItems=[];
            $date=Carbon::parse($s->date);
            $finalProfit=0;
            $finalPurchase=0;
            $finalQuantity=0;
            foreach($s->items as $sItems){
                $itemStats=[];
                $arr=$sItems->item->getTotalBefore($date,$date);
                $salesCount=$arr['countSales'];
                $purchases=$arr['purchases'];
                $sales=$arr['sales'];
                $it=$sItems->item;
                $iProfit=0;
                $iPurchase=0;
                $iSale=0;


                for($i=0;$i<floor($sItems->quantity);$i++){
                    $profit=0;

                    if(count($purchases)==0){

                        //$profit=$sales[$i]->price-$it->getDiscountedPrice($it->purchase_price);
                        $salesPrice=$sItems->price * (1-($sItems->discount/100));
                        $profit=$salesPrice-$it->getDiscountedPrice($it->purchase_price);
                        $finalPurchase+=$it->getDiscountedPrice($it->purchase_price);
                        $iSale+=$salesPrice;
                        $iPurchase+=$it->getDiscountedPrice($it->purchase_price);
                        array_push($itemStats,
                            ['sale'=>$salesPrice,
                                'purchase'=>$it->getDiscountedPrice($it->purchase_price),
                                'profit'=>$profit


                            ]);


                    }
                    else{

                        if((count($purchases)-1)>=$i){

                            //$profit=$sales[$i]->price-$purchases[$i]->price;
                            $salesPrice=$sItems->price * (1-($sItems->discount/100));
                            $profit=$salesPrice-$purchases[$i]->price;
                            $finalPurchase+=$purchases[$i]->price;
                            $iSale+=$salesPrice;
                            $iPurchase+=$purchases[$i]->price;
                            array_push($itemStats,
                                ['sale'=>$salesPrice,
                                    'purchase'=>$purchases[$i]->price,
                                    'profit'=>$profit


                                ]);
                        }
                        else {

                            //$profit=$sales[$i]->price-$purchases[(count($purchases)-1)]->price;
                            $salesPrice=$sItems->price * (1-($sItems->discount/100));
                            $profit=$salesPrice-$purchases[(count($purchases)-1)]->price;
                            $finalPurchase+=$purchases[(count($purchases)-1)]->price;
                            $iSale+=$salesPrice;
                            $iPurchase+=$purchases[(count($purchases)-1)]->price;
                            array_push($itemStats,
                                ['sale'=>$salesPrice,
                                    'purchase'=>$purchases[(count($purchases)-1)]->price,
                                    'profit'=>$profit


                                ]);
                        }
                    }
                    //dd($itemStats);
                    $iProfit+=$profit;

                }
                $itemObj = new \stdClass;
                $itemObj->stats=$itemStats;
                $itemObj->date=$s->date;
                $itemObj->id=$sItems->item_id;
                $itemObj->name=$sItems->item->name;
                $itemObj->quality=$sItems->item->quality->name;
                $itemObj->iProfit=$iProfit;
                $itemObj->quantity=$sItems->quantity;
                $itemObj->iSale=$sItems->total_price;
                if($sItems->quantity<1){
                    $itemObj->iPurchase=$sItems->quantity*$sItems->item->getDiscountedPrice($sItems->item->purchase_price);

                    $finalPurchase+=$itemObj->iPurchase;
                    $itemObj->$iProfit=$sItems->total_price-$itemObj->iPurchase;
                }
                else{
                    if(floor($sItems->quantity)!=$sItems->quantity){
                        $rem_quantity=$sItems->quantity-floor($sItems->quantity);
                        $itemObj->iPurchase=$iPurchase+($rem_quantity*($itemObj->stats[0]['purchase']));
                        $finalPurchase+=$rem_quantity*($itemObj->stats[0]['purchase']);
                        $itemObj->iProfit+=(($rem_quantity*($itemObj->stats[0]['sale']))-($rem_quantity*($itemObj->stats[0]['purchase'])));
                    }
                    else
                        $itemObj->iPurchase=$iPurchase;
                }
                $finalProfit+=$itemObj->iProfit;
                $finalQuantity+=$sItems->quantity;
                // if($s->id==198)
                //     dd($itemObj);
                //$oneItemObj=new \stdClass;
                array_push($salesItems,$itemObj);
            }

            $salesInvoice= new \stdClass;
            $salesInvoice->items=$salesItems;
            $salesInvoice->id=$s->id;
            $salesInvoice->total_price=$s->total_price;
            $salesInvoice->date=$s->date;
            $salesInvoice->finalProfit=$finalProfit;
            $salesInvoice->finalPurchase=$finalPurchase;
            $salesInvoice->quantity=$finalQuantity;
            $salesInvoice->customer=$s->cust;


            $totalQuantity+=$salesInvoice->quantity;
            $totalPurchaseValue+=$salesInvoice->finalPurchase;
            $totalSaleValue+=$salesInvoice->total_price;
            $totalProfit+=$salesInvoice->finalProfit;
            array_push($allSales,$salesInvoice);
        }



        usort($allSales,function($a,$b){
            return Carbon::parse($a->date)<Carbon::parse($b->date);
        });
        //dd($allSales);
        $default='all';
        return view('admin.profit.bill',compact(['default','allSales'
            ,'totalQuantity'
            ,'totalPurchaseValue'
            ,'totalSaleValue'
            ,'totalProfit'
        ]));
    }

    public function billQuery(Request $request){

        $this->authorize('owner', User::class);

        ini_set('max_execution_time', 300);
        if($request['today']=='all')
            return redirect('/admin/profit/bill/all');
        $from_date=Carbon::parse($request['from_date']);
        $to_date=Carbon::parse($request['to_date']);
        //dd($to_date);
        $allSales=[];
        $totalQuantity=0;
        $totalPurchaseValue=0;
        $totalSaleValue=0;
        $totalProfit=0;
        $sales=SalesInvoice::where('date','>=',$from_date->toDateString())->where('date','<=',$to_date->toDateString())->get();
        foreach($sales as $s){
            $salesItems=[];
            $date=Carbon::parse($s->date);
            $finalProfit=0;
            $finalPurchase=0;
            $finalQuantity=0;
            $finalDiscount = $s->discount;

            $total_items_sold = $s->items()->sum("quantity");
            $discount_per_item = 0;
            if($s->discount > 0){
                $discount_per_item = $s->discount/$total_items_sold;
            }

            
            foreach($s->items as $sItems){
                $itemStats=[];
                $arr=$sItems->item->getTotalBefore($date,$date);
                $salesCount=$arr['countSales'];
                $purchases=$arr['purchases'];
                $sales=$arr['sales'];
                $it=$sItems->item;
                $iProfit=0;
                $iPurchase=0;
                $iSale=0;
                
               
                for($i=0;$i<(int)$sItems->quantity;$i++){
                    $profit=0;
                    if(count($purchases)==0){
                        
                        //$profit=$sales[$i]->price-$it->getDiscountedPrice($it->purchase_price);
                        $salesPrice=$sItems->price * (1-($sItems->discount/100));
                        $profit=$salesPrice-$it->getDiscountedPrice($it->purchase_price);
                        $finalPurchase+=$it->getDiscountedPrice($it->purchase_price);
                        $iSale+=$salesPrice;
                        $iPurchase+=$it->getDiscountedPrice($it->purchase_price);
                        // if($sItems->item_id==99)
                        // dd($salesPrice.'-'.$it->getDiscountedPrice($it->purchase_price));
                        array_push($itemStats,
                        ['sale'=>$salesPrice,
                        'purchase'=>$it->getDiscountedPrice($it->purchase_price),
                        'profit'=>$profit


                        ]);
                        
                    }
                    else{
                    
                        if((count($purchases)-1)>=$i){
                            
                            //$profit=$sales[$i]->price-$purchases[$i]->price;
                            $salesPrice=$sItems->price * (1-($sItems->discount/100));
                            $profit=$salesPrice-$purchases[$i]->price;
                            $finalPurchase+=$purchases[$i]->price;
                            $iSale+=$salesPrice;
                            $iPurchase+=$purchases[$i]->price;
                            array_push($itemStats,
                        ['sale'=>$salesPrice,
                        'purchase'=>$purchases[$i]->price,
                        'profit'=>$profit


                        ]);
                        }
                        else {
                        
                            //$profit=$sales[$i]->price-$purchases[(count($purchases)-1)]->price;
                            $salesPrice=$sItems->price * (1-($sItems->discount/100));
                            $profit=$salesPrice-$purchases[(count($purchases)-1)]->price;
                            $finalPurchase+=$purchases[(count($purchases)-1)]->price;
                            $iSale+=$salesPrice;
                            $iPurchase+=$purchases[(count($purchases)-1)]->price;
                           
                            array_push($itemStats,
                        ['sale'=>$salesPrice,
                        'purchase'=>$purchases[(count($purchases)-1)]->price,
                        'profit'=>$profit


                        ]);
                        }
                    }
                    //dd($itemStats);
                    $iProfit+=$profit;
                    //$iProfit -= ($discount_per_item*$sItems->quantity);
                    
                   
                }
                $itemObj = new \stdClass;
                $itemObj->stats=$itemStats;
                $itemObj->date=$s->date;
                $itemObj->id=$sItems->item_id;
                $itemObj->name=$sItems->item->name;
                $itemObj->quality=$sItems->item->quality->name;
                $itemObj->iProfit=$iProfit;
                $itemObj->quantity=$sItems->quantity;
                $itemObj->iSale=$sItems->total_price;
                $itemObj->discount = $discount_per_item*$sItems->quantity;
                
                if($sItems->quantity<1){
                    $itemObj->iPurchase=$sItems->quantity*$sItems->item->getDiscountedPrice($sItems->item->purchase_price);
                    
                    $finalPurchase+=$itemObj->iPurchase;
                    $itemObj->$iProfit=$sItems->total_price-$itemObj->iPurchase;
                }
                else{
                    if(floor($sItems->quantity)!=$sItems->quantity){
                        $rem_quantity=$sItems->quantity-floor($sItems->quantity);
                        $itemObj->iPurchase=$iPurchase+($rem_quantity*($itemObj->stats[0]['purchase']));
                        $finalPurchase+=$rem_quantity*($itemObj->stats[0]['purchase']);
                        $itemObj->iProfit+=(($rem_quantity*($itemObj->stats[0]['sale']))-($rem_quantity*($itemObj->stats[0]['purchase'])));
                    }
                    else  
                    $itemObj->iPurchase=$iPurchase;
                }
               
                $finalProfit+=$itemObj->iProfit;
                $finalQuantity+=$sItems->quantity;
                //$oneItemObj=new \stdClass;
                array_push($salesItems,$itemObj);
            }

            $salesInvoice= new \stdClass;
            $salesInvoice->items=$salesItems;
            $salesInvoice->id=$s->id;
            $salesInvoice->customer=$s->cust;
            $salesInvoice->total_price=$s->total_price;
            $salesInvoice->date=$s->date;
            $salesInvoice->finalProfit=$finalProfit-$finalDiscount;
            $salesInvoice->finalPurchase=$finalPurchase;
            $salesInvoice->quantity=$finalQuantity;
            $salesInvoice->discount=$s->discount;
            $totalQuantity+=$salesInvoice->quantity;
            $totalPurchaseValue+=$salesInvoice->finalPurchase;
            $totalSaleValue+=$salesInvoice->total_price;
            $totalProfit+=$salesInvoice->finalProfit;
            array_push($allSales,$salesInvoice); 
        }

        

        usort($allSales,function($a,$b){
            return Carbon::parse($a->date)<Carbon::parse($b->date);
        });
        //dd($allSales);
        $default=3;
        return view('admin.profit.bill',compact(['default','allSales','from_date','to_date'
        ,'totalQuantity'
        ,'totalPurchaseValue'
        ,'totalSaleValue'
        ,'totalProfit'
        
        ]));

    }

    function console_log( $data ){
        $this->authorize('owner', User::class);
        
        echo '<script>';
        echo 'console.log('. json_encode( $data ) .')';
        echo '</script>';
      }
}
