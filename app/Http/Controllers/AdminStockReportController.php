<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SalesInvoice;
use App\PurchaseInvoice;
use App\Item;
use App\Quality;
use Carbon\Carbon;
use App\Company;
use App\PurchaseReturn;
use App\User;

class AdminStockReportController extends Controller
{
    //

    public function evaluateQuantityBeforeDate($item,$date){
       
        $this->authorize('owner', User::class);

        $openingStock=$item->opening_quantity;
        $sale_items=0;
        $purchase_items=0;
        $return_items=0;
        $preturn_items=0;
        $si=$item->todaysSales;

        foreach($si as $SI){
            if(Carbon::parse($SI->salesInvoice->date)<=$date){

                $sale_items+=$SI->quantity;

            }
        }


        $pi=$item->todaysPurchases;

        foreach($pi as $SI){
            if(Carbon::parse($SI->purchaseInvoice->date)<=$date){
                $purchase_items+=$SI->quantity;
            }
        }

        $sr=$item->salesReturns;
        foreach($sr as $SR){
            if(Carbon::parse($SR->date)<=$date){

                $return_items+=$SR->quantity;
            }
        }

        $pr=$item->purchaseReturns;
        foreach($pr as $SR){
            if(Carbon::parse($SR->date)<=$date){

                $preturn_items+=$SR->quantity;
            }
        }



        if($openingStock)
            $opening= $openingStock+$purchase_items+$return_items -$sale_items -$preturn_items;
        else
        $opening= $purchase_items+$return_items -$sale_items -$preturn_items;
        return $opening;

    }


    //Stock report Quality Wise
    //index function
    public function qualityIndex(){

        $this->authorize('owner', User::class);

        $qualities=Quality::orderBy('name')->pluck('name','id')->all();
        return view('admin.reports.stock.qualitywise',compact(['qualities']));
    }
    //Query Function
    public function qualityQuery(Request $request){
        $item_reports=[];
        $qualities=Quality::orderBy('name')->pluck('name','id')->all();
        $to_date = null;
        $on_date = null;
        $from_date = null;
        if($request['today']==1){
            
            foreach(Quality::findOrFail($request['quality_id'])->items as $item){
                $opening=$this->evaluateQuantityBeforeDate($item,Carbon::yesterday());
                $sale_items=0;
                $purchase_items=0;
                $return_items=0;
                $preturn_items=0;
                $si=$item->todaysSales;
                
                foreach($si as $SI){
                    if(Carbon::parse($SI->salesInvoice->date)->diffInDays(Carbon::now())==0){
                        
                        $sale_items+=$SI->quantity;
                    
                }
                }
               
                
                $pi=$item->todaysPurchases;

                foreach($pi as $SI){
                    if(Carbon::parse($SI->purchaseInvoice->date)->diffInDays(Carbon::now())==0){
                        
                        $purchase_items+=$SI->quantity;
                    
                }
                }

                $sr=$item->salesReturns;
                foreach($sr as $SR){
                    if(Carbon::parse($SR->date)->diffInDays(Carbon::now())==0){

                        $return_items+=$SR->quantity;                    
                    }   
                }

                $pr=$item->purchaseReturns;
                foreach($pr as $SR){
                    if(Carbon::parse($SR->date)->diffInDays(Carbon::now())==0){

                        $preturn_items+=$SR->quantity;
                    }   
                }



               
                $tempObj=new \stdClass();
                $tempObj->name=$item->name;
                $tempObj->id=$item->id;

                
                $tempObj->price=$item->price;
                $tempObj->purchase_price=$item->getDiscountedPrice($item->purchase_price);
                $tempObj->sale_price=$item->sale_price;
                $tempObj->sale_items=$sale_items;
                $tempObj->purchase_items=$purchase_items;
                $tempObj->return_items=$return_items;
                $tempObj->opening=$opening;
                $tempObj->preturn_items=$preturn_items;
                $tempObj->stock=$opening+$return_items+$purchase_items-$sale_items-$preturn_items;
                $tempObj->current=$purchase_items-$item->current;
                array_push($item_reports,$tempObj);
            }

            
        }

        else if($request['today']==2){
            $sale_items=0;
            $purchase_items=0;

            $on_date=new Carbon(date($request['on_date']));
            foreach(Quality::findOrFail($request['quality_id'])->items as $item){
                $opening=$this->evaluateQuantityBeforeDate($item,$on_date->subDay(1));
                $on_date->addDay(1);
                $sale_items=0;
                $purchase_items=0;
                $return_items=0;
                $preturn_items=0;
                $si=$item->todaysSales;
                
                foreach($si as $SI){
                    if(Carbon::parse($SI->salesInvoice->date)->diffInDays($on_date)==0){
                        
                        $sale_items+=$SI->quantity;
                    
                }
                }
               
                
                $pi=$item->todaysPurchases;

                foreach($pi as $SI){
                    if(Carbon::parse($SI->purchaseInvoice->date)->diffInDays($on_date)==0){
                        
                        $purchase_items+=$SI->quantity;
                    
                }
                }

                $sr=$item->salesReturns;
                foreach($sr as $SR){
                    if(Carbon::parse($SR->date)->diffInDays($on_date)==0){

                        $return_items+=$SR->quantity;                    
                    }   
                }

                $pr=$item->purchaseReturns;
                foreach($pr as $SR){
                    if(Carbon::parse($SR->date)->diffInDays($on_date)==0){

                        $preturn_items+=$SR->quantity;
                    }   
                }

                $tempObj=new \stdClass();
                $tempObj->name=$item->name;
                

                $tempObj->price=$item->price;
                $tempObj->id=$item->id;
                $tempObj->purchase_price=$item->getDiscountedPrice($item->purchase_price);
                $tempObj->sale_price=$item->sale_price;
                $tempObj->sale_items=$sale_items;
                $tempObj->purchase_items=$purchase_items;
                $tempObj->preturn_items=$preturn_items;
                $tempObj->opening=$opening;
                $tempObj->stock=$opening+$return_items+$purchase_items-$sale_items-$preturn_items;
                $tempObj->return_items=$return_items;
                array_push($item_reports,$tempObj);
            }
     
        }

        else if($request['today']==3){
            $sale_items=0;
            $purchase_items=0;
            
            $from_date=Carbon::parse($request['from_date']);
            $to_date=Carbon::parse($request['to_date']);
            foreach(Quality::findOrFail($request['quality_id'])->items as $item){
                $sale_items=0;
                $purchase_items=0;
                $return_items=0;
                $preturn_items=0;
                $si=$item->todaysSales;
                
                foreach($si as $SI){
                    if(Carbon::parse($SI->salesInvoice->date)>=$from_date && Carbon::parse($SI->salesInvoice->date)<=$to_date){
                        
                        $sale_items+=$SI->quantity;
                    
                }
                }
               
                
                $pi=$item->todaysPurchases;

                foreach($pi as $SI){
                    if(Carbon::parse($SI->purchaseInvoice->date)>=$from_date && Carbon::parse($SI->purchaseInvoice->date)<=$to_date){
                        
                        $purchase_items+=$SI->quantity;
                    
                }
                }

                $sr=$item->salesReturns;
                foreach($sr as $SR){
                    if(Carbon::parse($SR->date)>=$from_date && Carbon::parse($SR->date)<=$to_date){

                        $return_items+=$SR->quantity;                    
                    }   
                }

                $pr=$item->purchaseReturns;
                foreach($pr as $SR){
                    if(Carbon::parse($SR->date)>=$from_date && Carbon::parse($SR->date)<=$to_date){

                        $preturn_items-=$SR->quantity;                    
                    }   
                }
                

                $tempObj=new \stdClass();
                $tempObj->name=$item->name;
                

                $tempObj->price=$item->price;
                
                $tempObj->id=$item->id;
                $tempObj->purchase_price=$item->getDiscountedPrice($item->purchase_price);
                $tempObj->sale_price=$item->sale_price;
                $tempObj->sale_items=$sale_items;
                $tempObj->purchase_items=$purchase_items;
                $tempObj->stock=$return_items+$purchase_items-$sale_items-$preturn_items;
                $tempObj->return_items=$return_items;
                array_push($item_reports,$tempObj);
            }
        }

        
        usort($item_reports, function($a,$b){
            return strcmp($a->name, $b->name);
        });
        return view('admin.reports.stock.qualitywise',compact(['qualities','item_reports','on_date','from_date','to_date']));
    }


    //Stock Valuation Company Wise
    //Index function
    public function companyStockIndex(){

        $this->authorize('owner', User::class);

        $companies=Company::orderBy('name')->pluck('name','id')->all();
        return view('admin.reports.stock.companywise',compact(['companies']));
    }


    //Query Function

    public function companyStockQuery(Request $request){

        $this->authorize('owner', User::class);

        $companies=Company::orderBy('name')->pluck('name','id')->all();
        $qualities=[];
        $item_reports=[];
        $to_date = null;
        $on_date = null;
        $from_date = null;
        if($request['company_id']=='all')
            $qualities=Quality::all();
        else{
            foreach(Company::findOrFail($request['company_id'])->quality as $quality){
                array_push($qualities,$quality);
            }
        }
       

        //dd($qualities);

        if($request['today']==1){
            foreach($qualities as $quality){
                foreach($quality->items as $item){
                    $opening=$this->evaluateQuantityBeforeDate($item,Carbon::yesterday());
                    $sale_items=0;
                    $purchase_items=0;
                    $return_items=0;
                    $preturn_items=0;
                    $si=$item->todaysSales;
                    
                    foreach($si as $SI){
                            if(Carbon::parse($SI->salesInvoice->date)->diffInDays(Carbon::now())==0){
                                
                                $sale_items+=$SI->quantity;
                            
                        }
                    }
                
                    
                    $pi=$item->todaysPurchases;

                    foreach($pi as $SI){
                        if(Carbon::parse($SI->purchaseInvoice->date)->diffInDays(Carbon::now())==0){
                            
                            $purchase_items+=$SI->quantity;
                        
                    }
                    }

                    $sr=$item->salesReturns;
                    foreach($sr as $SR){
                        if(Carbon::parse($SR->date)->diffInDays(Carbon::now())==0){

                            $return_items+=$SR->quantity;                    
                        }   
                    }

                    $pr=$item->purchaseReturns;
                    foreach($pr as $SR){
                        if(Carbon::parse($SR->date)->diffInDays(Carbon::now())==0){

                            $preturn_items+=$SR->quantity;
                        }   
                    }



                
                    $tempObj=new \stdClass();
                    $tempObj->name=$item->name;
                    $tempObj->id=$item->id;

                    
                    $tempObj->price=$item->price;
                    $tempObj->purchase_price=$item->getDiscountedPrice($item->purchase_price);
                    $tempObj->sale_price=$item->sale_price;
                    $tempObj->sale_items=$sale_items;
                    $tempObj->purchase_items=$purchase_items;
                    $tempObj->return_items=$return_items;
                    $tempObj->opening=$opening;
                    $tempObj->quality=$item->quality->name;
                    $tempObj->covered=$item->covered;
                    $tempObj->preturn_items=$preturn_items;
                    $tempObj->stock=$opening+$return_items+$purchase_items-$sale_items-$preturn_items;
                    $tempObj->current=$purchase_items-$item->current;
                    array_push($item_reports,$tempObj);
                }
            }

            
        }

        else if($request['today']==2){
            $sale_items=0;
            $purchase_items=0;

            $on_date=new Carbon(date($request['on_date']));
            foreach($qualities as $quality){
                foreach($quality->items as $item){
                    $opening=$this->evaluateQuantityBeforeDate($item,$on_date->subDay(1));
                    $on_date->addDay(1);
                    $sale_items=0;
                    $purchase_items=0;
                    $return_items=0;
                    $preturn_items=0;
                    $si=$item->todaysSales;
                    
                    foreach($si as $SI){
                        if(Carbon::parse($SI->salesInvoice->date)->diffInDays($on_date)==0){
                            
                            $sale_items+=$SI->quantity;
                        
                    }
                    }
                
                    
                    $pi=$item->todaysPurchases;

                    foreach($pi as $SI){
                        if(Carbon::parse($SI->purchaseInvoice->date)->diffInDays($on_date)==0){
                            
                            $purchase_items+=$SI->quantity;
                        
                    }
                    }

                    $sr=$item->salesReturns;
                    foreach($sr as $SR){
                        if(Carbon::parse($SR->date)->diffInDays($on_date)==0){

                            $return_items+=$SR->quantity;                    
                        }   
                    }

                    $pr=$item->purchaseReturns;
                    foreach($pr as $SR){
                        if(Carbon::parse($SR->date)->diffInDays($on_date)==0){

                            $preturn_items+=$SR->quantity;
                        }   
                    }

                    $tempObj=new \stdClass();
                    $tempObj->name=$item->name;
                    

                    $tempObj->price=$item->price;
                    $tempObj->id=$item->id;
                    $tempObj->quality=$item->quality->name;
                    $tempObj->purchase_price=$item->getDiscountedPrice($item->purchase_price);
                    $tempObj->sale_price=$item->sale_price;
                    $tempObj->sale_items=$sale_items;
                    $tempObj->purchase_items=$purchase_items;
                    $tempObj->preturn_items=$preturn_items;
                    $tempObj->opening=$opening;
                    $tempObj->covered=$item->covered;
                    $tempObj->stock=$opening+$return_items+$purchase_items-$sale_items-$preturn_items;
                    $tempObj->return_items=$return_items;
                    array_push($item_reports,$tempObj);
                }
            }
     
        }
        usort($item_reports, function($a,$b){
            $cmp=strcmp($a->quality, $b->quality);
            if($cmp==0) return strcmp($a->name,$b->name);
            else return $cmp;
        });
        return view('admin.reports.stock.companywise',compact(['companies','item_reports','on_date','from_date','to_date']));
   
    }




    //--------------------------------------------------------------------------------------//
    // Stock Valuation Functions
    public function valuationQuery(Request $request){

        $this->authorize('owner', User::class);

        //dd($request->all());
        $noDisplay=false;
        $total=0;
        $total_price=0;
        $item_reports=[];
        $to_date = null;
        $on_date = null;
        $from_date = null;
        $qualities=Quality::orderBy('name')->pluck('name','id')->all();
        if($request['quality_id']=='all'){
            if($request['today']==1){
                    $allqualities = Quality::all();
                    foreach($allqualities as $quality){
                        foreach($quality->items as $item){
                            /*$stock=$this->evaluateQuantityBeforeDate($item,Carbon::today());
                            if($item->id==401)
                                dd($stock);
                            $si=$item->dateSales(Carbon::today());
                            $return_items=0;

                            foreach($si as $SI){
                                    $stock-=$SI->quantity;
                            }


                            $pi=$item->datePurchases(Carbon::today());

                            foreach($pi as $SI){  
                                $stock+=$SI->quantity;
                            }

                            $sr=$item->salesReturns;
                            foreach($sr as $SR){
                                if(Carbon::parse($SR->date)->diffInDays(Carbon::now())==0){

                                    $return_items+=$SR->quantity;                    
                                }   
                            }

                            $preturn_items=0;
                            $pr=$item->purchaseReturns;
                            foreach($pr as $SR){
                                if(Carbon::parse($SR->date)->diffInDays(Carbon::now())==0){

                                    $preturn_items+=$SR->quantity;
                                }   
                            }


                        
                            $tempObj=new \stdClass();
                            $tempObj->name=$item->name;
                            $tempObj->quality=$item->quality->name;
                            $tempObj->id=$item->id;
                            $tempObj->covered=$item->covered;
                            $tempObj->discount=0;
                            if($tempObj->covered==1)
                                $tempObj->discount=$item->CDiscount();
                            else $tempObj->discount=$item->UDiscount();
                            
                            $tempObj->purchase_price=$item->getDiscountedPrice($item->purchase_price);
                            $tempObj->sale_price=$item->sale_price;
                            $stock+=$return_items;
                            $stock-=$preturn_items;
                            $tempObj->return_items=$return_items;
                            $tempObj->stock=$stock;
                            $tempObj->opening_date=$item->opening_date;
                            $tempObj->opening_quantity=$item->opening_quantity;
                            $total+=$stock;
                            $tempObj->mytotal=$tempObj->stock * $tempObj->purchase_price;
                            $total_price+=$tempObj->mytotal;
                            array_push($item_reports,$tempObj);*/
                            $obj = $this->evaluatedItem($item,Carbon::today()->toDateString());
                            $total+=$obj->total;
                            $total_price+=$obj->total_price;
                            array_push($item_reports,$obj->tempObj);
                        }
                    }
                }
                else{
                        $allqualities = Quality::all();
                        $selectedDate=Carbon::parse($request['on_date']);
                        foreach($allqualities as $quality){
                            foreach($quality->items as $item){
                                /*$stock=$this->evaluateQuantityBeforeDate($item,$selectedDate);
                                $si=$item->dateSales($selectedDate);
                                $return_items=0;
                                $on_date=Carbon::parse($request['on_date']);
                                foreach($si as $SI){
                                    if(Carbon::parse($SI->salesInvoice->date)->startOfDay()<=$on_date->startOfDay())
                                        $stock-=$SI->quantity;
                                }


                                $pi=$item->datePurchases($selectedDate);

                                foreach($pi as $SI){
                                    if(Carbon::parse($SI->purchaseInvoice->date)->startOfDay()<=$on_date->startOfDay())  
                                    $stock+=$SI->quantity;
                                }

                                $sr=$item->salesReturns;
                                foreach($sr as $SR){
                                    if(Carbon::parse($SR->date)->startOfDay()<=$on_date->startOfDay()){

                                        $return_items+=$SR->quantity;                    
                                    }   
                                }
                                $preturn_items=0;
                                $pr=$item->purchaseReturns;
                                foreach($pr as $SR){
                                    if(Carbon::parse($SR->date)->startOfDay()<=$on_date->startOfDay()){

                                        $preturn_items-=$SR->quantity;                    
                                    }   
                                }

                            
                                $tempObj=new \stdClass();
                                $tempObj->name=$item->name;
                                $tempObj->quality=$item->quality->name;
                                $tempObj->id=$item->id;
                                $tempObj->covered=$item->covered;
                                $tempObj->discount=0;
                                if($tempObj->covered==1)
                                    $tempObj->discount=$item->CDiscount();
                                else $tempObj->discount=$item->UDiscount();
                                $tempObj->purchase_price=$item->getDiscountedPrice($item->purchase_price);
                                $tempObj->sale_price=$item->sale_price;
                                $stock+=$return_items;
                                $stock-=$preturn_items;
                                $tempObj->return_items=$return_items;
                                $tempObj->stock=$stock;
                                $tempObj->opening_date=$item->opening_date;
                                $tempObj->opening_quantity=$item->opening_quantity;
                                $total+=$stock;
                                $tempObj->mytotal=$tempObj->stock * $tempObj->purchase_price;
                                $total_price+=$tempObj->mytotal;
                                array_push($item_reports,$tempObj);*/

                                $obj = $this->evaluatedItem($item,$request['on_date']);
                                $total+=$obj->total;
                                $total_price+=$obj->total_price;
                                array_push($item_reports,$obj->tempObj);
                    }
                }
            }
        }

        else{
            $quality=Quality::findOrFail($request['quality_id']);
            if($request['today']==1){
                    foreach($quality->items as $item){
                        /*$stock=$this->evaluateQuantityBeforeDate($item,Carbon::today());


                        $si=$item->dateSales(Carbon::today());

                        $return_items=0;
                        foreach($si as $SI){
                                $stock-=$SI->quantity;
                        }

                    
                        
                        $pi=$item->datePurchases(Carbon::today());

                        foreach($pi as $SI){  
                            $stock+=$SI->quantity;
                        }

                        $sr=$item->salesReturns;
                        foreach($sr as $SR){
                            if(Carbon::parse($SR->date)->diffInDays(Carbon::now())==0){

                                $return_items+=$SR->quantity;                    
                            }   
                        }
                        $preturn_items=0;
                        $pr=$item->purchaseReturns;
                        foreach($pr as $SR){
                            if(Carbon::parse($SR->date)->diffInDays(Carbon::now())==0){

                                $preturn_items+=$SR->quantity;
                            }   
                        }

                    
                        $tempObj=new \stdClass();
                        $tempObj->name=$item->name;
                        $tempObj->quality=$item->quality->name;
                        $tempObj->id=$item->id;
                        $tempObj->covered=$item->covered;
                        $tempObj->discount=0;
                        if($tempObj->covered==1)
                            $tempObj->discount=$item->CDiscount();
                        else $tempObj->discount=$item->UDiscount();

                        $tempObj->sale_price=$item->sale_price;
                        $stock+=$return_items;
                        $stock-=$preturn_items;
                        $tempObj->return_items=$return_items;
                        $tempObj->stock=$stock;
                        if($stock>0)
                            $tempObj->purchase_price=$item->getDiscountedPrice($item->purchase_price);
                        else $tempObj->purchase_price=0;
                        $tempObj->opening_date=$item->opening_date;
                        $tempObj->opening_quantity=$item->opening_quantity;
                        $total+=$stock;
                        $tempObj->mytotal=$tempObj->stock * $tempObj->purchase_price;
                        $total_price+=$tempObj->mytotal;
                        array_push($item_reports,$tempObj);*/

                        $obj = $this->evaluatedItem($item,Carbon::today()->toDateString());
                        $total+=$obj->total;
                        $total_price+=$obj->total_price;
                        array_push($item_reports,$obj->tempObj);
                    }
                
            }
            else{
                $selectedDate=Carbon::parse($request['on_date']);
                foreach($quality->items as $item){
                    /*$stock=$this->evaluateQuantityBeforeDate($item,$selectedDate);

                    $si=$item->dateSales($selectedDate);
                    $return_items=0;

                    $on_date=Carbon::parse($request['on_date']);

                    foreach($si as $SI){

                            $stock-=$SI->quantity;
                    }



                    $pi=$item->datePurchases($selectedDate);

                    foreach($pi as $SI){  

                            $stock+=$SI->quantity;
                    }




                    $sr=$item->dateSalesReturns($selectedDate);
                    foreach($sr as $SR){
                            $return_items+=$SR->quantity;
                    }

                    $preturn_items=0;
                    $pr=$item->datePurchaseReturns($selectedDate);
                    foreach($pr as $SR){


                            $preturn_items+=$SR->quantity;

                    }
                    $tempObj=new \stdClass();
                    $tempObj->name=$item->name;
                    $tempObj->quality=$item->quality->name;
                    $tempObj->id=$item->id;
                    $tempObj->covered=$item->covered;
                    $tempObj->discount=0;
                    if($tempObj->covered==1)
                        $tempObj->discount=$item->CDiscount();
                    else $tempObj->discount=$item->UDiscount();
                    
                    $tempObj->purchase_price=$item->getDiscountedPrice($item->purchase_price);

                    $tempObj->sale_price=$item->sale_price;

                    $stock+=$return_items;
                    $stock-=$preturn_items;


                     $tempObj->return_items=$return_items;
                    $tempObj->stock=$stock;

                    $tempObj->opening_date=$item->opening_date;
                    $tempObj->opening_quantity=$item->opening_quantity;

                    $total+=$stock;
                    $tempObj->mytotal=$tempObj->stock * $tempObj->purchase_price;
                    $total_price+=$tempObj->mytotal;
                    //dd($tempObj);
                    array_push($item_reports,$tempObj);*/
                    $obj = $this->evaluatedItem($item,$request['on_date']);
                    $total+=$obj->total;
                    $total_price+=$obj->total_price;
                    array_push($item_reports,$obj->tempObj);
                }
            }

        }
        return view('admin.reports.stock.valuation',compact(['noDisplay','qualities','item_reports','on_date','total','total_price']));
    }


    public function valuationIndex(){

        $this->authorize('owner', User::class);

        $noDisplay=true;
        $total=0;
        $item_reports=[];
        $total_price=0;
        $qualities=Quality::orderBy('name')->pluck('name','id')->all();
        $allqualities = Quality::all();
        /*foreach($allqualities as $quality){
            foreach($quality->items as $item){
                $stock=$item->opening_quantity;
                $si=$item->todaysSales;
                $return_items=0;
                foreach($si as $SI){
                        $stock-=$SI->quantity;
                }
               
                
                $pi=$item->todaysPurchases;

                foreach($pi as $SI){  
                    $stock+=$SI->quantity;
                }

                $sr=$item->salesReturns;
                foreach($sr as $SR){
                    if(Carbon::parse($SR->date)->diffInDays(Carbon::now())==0){

                        $return_items+=$SR->quantity;                    
                    }   
                }
                $preturn_items=0;
                $pr=$item->purchaseReturns;
                foreach($pr as $SR){
                    if(Carbon::parse($SR->date)->diffInDays(Carbon::now())==0){

                        $preturn_items-=$SR->quantity;                    
                    }   
                }

                
                $tempObj=new \stdClass();
                $tempObj->name=$item->name;
                $tempObj->quality=$item->quality->name;
                $tempObj->id=$item->id;
                $tempObj->covered=$item->covered;
                $tempObj->discount=0;
                if($tempObj->covered==1)
                    $tempObj->discount=$item->CDiscount();
                else $tempObj->discount=$item->UDiscount();
                $tempObj->purchase_price=$item->getDiscountedPrice($item->purchase_price);
                $tempObj->sale_price=$item->sale_price;
                $stock+=$return_items;
                $stock-=$preturn_items;
                $tempObj->return_items=$return_items;
                $tempObj->stock=$stock;
                $tempObj->opening_date=$item->opening_date;
                $tempObj->opening_quantity=$item->opening_quantity;
                $total+=$stock;
                $tempObj->mytotal=$tempObj->stock * $tempObj->purchase_price;
                $total_price+=$tempObj->mytotal;
                array_push($item_reports,$tempObj);
            }
        }*/
        return view('admin.reports.stock.valuation',compact(['noDisplay','qualities','item_reports','total','total_price']));
    }


    public function companyIndex(){

        $this->authorize('owner', User::class);

        $noDisplay=true;
        $total=0;
        $item_reports=[];
        $total_price=0;
        $companies=Company::orderBy('name')->pluck('name','id')->all();
        $allqualities = Quality::all();
        /*foreach($allqualities as $quality){
            foreach($quality->items as $item){
                $stock=$item->opening_quantity;
                $si=$item->todaysSales;
                $return_items=0;
                foreach($si as $SI){
                        $stock-=$SI->quantity;
                }
               
                
                $pi=$item->todaysPurchases;

                foreach($pi as $SI){  
                    $stock+=$SI->quantity;
                }

                $sr=$item->salesReturns;
                foreach($sr as $SR){
                    if(Carbon::parse($SR->date)->diffInDays(Carbon::now())==0){

                        $return_items+=$SR->quantity;                    
                    }   
                }
                $preturn_items=0;
                $pr=$item->purchaseReturns;
                foreach($pr as $SR){
                    if(Carbon::parse($SR->date)->diffInDays(Carbon::now())==0){

                        $preturn_items-=$SR->quantity;                    
                    }   
                }

               
                $tempObj=new \stdClass();
                $tempObj->name=$item->name;
                $tempObj->quality=$item->quality->name;
                $tempObj->id=$item->id;
                $tempObj->covered=$item->covered;
                $tempObj->discount=0;
                if($tempObj->covered==1)
                    $tempObj->discount=$item->CDiscount();
                else $tempObj->discount=$item->UDiscount();
                $tempObj->purchase_price=$item->getDiscountedPrice($item->purchase_price);
                $tempObj->sale_price=$item->sale_price;
                $stock+=$return_items;
                $stock-=$preturn_items;
                $tempObj->stock=$stock;
                $tempObj->opening_date=$item->opening_date;
                $tempObj->opening_quantity=$item->opening_quantity;
                $total+=$stock;
                $tempObj->return_items=$return_items;
                $tempObj->mytotal=$tempObj->stock * $tempObj->purchase_price;
                $total_price+=$tempObj->mytotal;
                array_push($item_reports,$tempObj);
            }
        }*/
        return view('admin.reports.stock.companyValuation',compact(['noDisplay','companies','item_reports','total','total_price']));
    }

    public function companyQuery(Request $request){

        $this->authorize('owner', User::class);
        $noDisplay=false;
        $total=0;
        $total_price=0;
        $item_reports=[];
        $on_date =  $request->on_date;
        $companies=Company::orderBy('name')->pluck('name','id')->all();
        if($request['company_id']=='all'){
            if($request['today']==1){
                    $allqualities = Quality::orderBy('name')->get();
                    foreach($allqualities as $quality){
                        foreach($quality->items as $item){
                            /*$selectedDate=Carbon::today();
                            $stock=$this->evaluateQuantityBeforeDate($item,$selectedDate);

                            $si=$item->dateSales($selectedDate);
                            $return_items=0;

                            $on_date=Carbon::parse($request['on_date']);

                            foreach($si as $SI){

                                $stock-=$SI->quantity;
                            }



                            $pi=$item->datePurchases($selectedDate);

                            foreach($pi as $SI){

                                $stock+=$SI->quantity;
                            }




                            $sr=$item->dateSalesReturns($selectedDate);
                            foreach($sr as $SR){
                                $return_items+=$SR->quantity;
                            }

                            $preturn_items=0;
                            $pr=$item->datePurchaseReturns($selectedDate);
                            foreach($pr as $SR){


                                $preturn_items+=$SR->quantity;

                            }
            

                        
                            $tempObj=new \stdClass();
                            $tempObj->name=$item->name;
                            $tempObj->quality=$item->quality->name;
                            $tempObj->id=$item->id;
                            $tempObj->covered=$item->covered;
                            $tempObj->discount=0;
                            if($tempObj->covered==1)
                                $tempObj->discount=$item->CDiscount();
                            else $tempObj->discount=$item->UDiscount();
                            
                            $tempObj->purchase_price=$item->purchase_price;
                            $tempObj->sale_price=$item->sale_price;
                            $stock+=$return_items;
                            $stock-=$preturn_items;
                            $tempObj->stock=$stock;
                            $tempObj->opening_date=$item->opening_date;
                            $tempObj->opening_quantity=$item->opening_quantity;
                            $total+=$stock;
                            $tempObj->return_items=$return_items;
                            $tempObj->mytotal=$tempObj->stock * $tempObj->purchase_price;
                            $total_price+=$tempObj->mytotal;
                            array_push($item_reports,$tempObj);*/
                            $obj = $this->evaluatedItem($item,Carbon::today()->toDateString());
                            $total+=$obj->total;
                            $total_price+=$obj->total_price;
                            array_push($item_reports,$obj->tempObj);
                        }
                    }
                }
                else{
                        $allqualities = Quality::orderBy('name')->get();
                        foreach($allqualities as $quality){
                            foreach($quality->items as $item){
                                /*$selectedDate=Carbon::parse($request['on_date']);
                                $stock=$this->evaluateQuantityBeforeDate($item,$selectedDate);

                                $si=$item->dateSales($selectedDate);
                                $return_items=0;

                                $on_date=Carbon::parse($request['on_date']);

                                foreach($si as $SI){

                                    $stock-=$SI->quantity;
                                }



                                $pi=$item->datePurchases($selectedDate);

                                foreach($pi as $SI){

                                    $stock+=$SI->quantity;
                                }




                                $sr=$item->dateSalesReturns($selectedDate);
                                foreach($sr as $SR){
                                    $return_items+=$SR->quantity;
                                }

                                $preturn_items=0;
                                $pr=$item->datePurchaseReturns($selectedDate);
                                foreach($pr as $SR){


                                    $preturn_items+=$SR->quantity;

                                }
                

                            
                                $tempObj=new \stdClass();
                                $tempObj->name=$item->name;
                                $tempObj->quality=$item->quality->name;
                                $tempObj->id=$item->id;
                                $tempObj->covered=$item->covered;
                                $tempObj->discount=0;
                                if($tempObj->covered==1)
                                    $tempObj->discount=$item->CDiscount();
                                else $tempObj->discount=$item->UDiscount();
                                $tempObj->purchase_price=$item->purchase_price;
                                $tempObj->sale_price=$item->sale_price;
                                $stock+=$return_items;
                                $stock-=$preturn_items;
                                $tempObj->stock=$stock;
                                $tempObj->opening_date=$item->opening_date;
                                $tempObj->opening_quantity=$item->opening_quantity;
                                $total+=$stock;
                                $tempObj->return_items=$return_items;
                                $tempObj->mytotal=$tempObj->stock * $tempObj->purchase_price;
                                $total_price+=$tempObj->mytotal;
                                array_push($item_reports,$tempObj);*/
                                $obj = $this->evaluatedItem($item,$request['on_date']);
                                $total+=$obj->total;
                                $total_price+=$obj->total_price;
                                array_push($item_reports,$obj->tempObj);
                    }
                }
            }
        }

        else{
            $qualities=Company::findOrFail($request['company_id'])->quality;
            $ITEMS=[];
            //dd($qualities);
            foreach($qualities as $q){
                foreach($q->items as $i){
                    array_push($ITEMS,$i);
                }
            }
            //dd($ITEMS);
            
            if($request['today']==1){
                    foreach($ITEMS as $item){
                        /*$selectedDate=Carbon::today();
                        $stock=$this->evaluateQuantityBeforeDate($item,$selectedDate);

                        $si=$item->dateSales($selectedDate);
                        $return_items=0;

                        $on_date=Carbon::parse($request['on_date']);

                        foreach($si as $SI){

                            $stock-=$SI->quantity;
                        }



                        $pi=$item->datePurchases($selectedDate);

                        foreach($pi as $SI){

                            $stock+=$SI->quantity;
                        }




                        $sr=$item->dateSalesReturns($selectedDate);
                        foreach($sr as $SR){
                            $return_items+=$SR->quantity;
                        }

                        $preturn_items=0;
                        $pr=$item->datePurchaseReturns($selectedDate);
                        foreach($pr as $SR){


                            $preturn_items+=$SR->quantity;

                        }

                        
        

                    
                        $tempObj=new \stdClass();
                        $tempObj->name=$item->name;
                        $tempObj->quality=$item->quality->name;
                        $tempObj->id=$item->id;
                        $tempObj->covered=$item->covered;
                        $tempObj->discount=0;
                        if($tempObj->covered==1)
                            $tempObj->discount=$item->CDiscount();
                        else $tempObj->discount=$item->UDiscount();
                        
                        $tempObj->purchase_price=$item->purchase_price;
                        $tempObj->sale_price=$item->sale_price;
                        $stock+=$return_items;
                        $stock-=$preturn_items;
                        $tempObj->stock=$stock;
                        $tempObj->opening_date=$item->opening_date;
                        $tempObj->opening_quantity=$item->opening_quantity;
                        $total+=$stock;
                        $tempObj->return_items=$return_items;
                        $tempObj->mytotal=$tempObj->stock * $tempObj->purchase_price;
                        $total_price+=$tempObj->mytotal;
                        array_push($item_reports,$tempObj);*/

                        $obj = $this->evaluatedItem($item,Carbon::today()->toDateString());

                        $total+=$obj->total;
                        $total_price+=$obj->total_price;
                        array_push($item_reports,$obj->tempObj);
                    }
                
            }
            else{
                foreach($ITEMS as $item){
                    /*$selectedDate=Carbon::parse($request['on_date']);
                    $stock=$this->evaluateQuantityBeforeDate($item,$selectedDate);

                    $si=$item->dateSales($selectedDate);
                    $return_items=0;

                    $on_date=Carbon::parse($request['on_date']);

                    foreach($si as $SI){

                        $stock-=$SI->quantity;
                    }



                    $pi=$item->datePurchases($selectedDate);

                    foreach($pi as $SI){

                        $stock+=$SI->quantity;
                    }




                    $sr=$item->dateSalesReturns($selectedDate);
                    foreach($sr as $SR){
                        $return_items+=$SR->quantity;
                    }

                    $preturn_items=0;
                    $pr=$item->datePurchaseReturns($selectedDate);
                    foreach($pr as $SR){


                        $preturn_items+=$SR->quantity;

                    }
                
                    $tempObj=new \stdClass();
                    $tempObj->name=$item->name;
                    $tempObj->quality=$item->quality->name;
                    $tempObj->id=$item->id;
                    $tempObj->covered=$item->covered;
                    $tempObj->discount=0;
                    if($tempObj->covered==1)
                        $tempObj->discount=$item->CDiscount();
                    else $tempObj->discount=$item->UDiscount();
                    
                    $tempObj->purchase_price=$item->purchase_price;
                    $tempObj->sale_price=$item->sale_price;
                    $stock+=$return_items;
                    $stock-=$preturn_items;
                    $tempObj->stock=$stock;
                    $tempObj->opening_date=$item->opening_date;
                    $tempObj->opening_quantity=$item->opening_quantity;
                    $total+=$stock;

                    $tempObj->return_items=$return_items;
                    $tempObj->mytotal=$tempObj->stock * $tempObj->purchase_price;
                    $total_price+=$tempObj->mytotal;
                    array_push($item_reports,$tempObj);*/
                    $obj = $this->evaluatedItem($item,$request['on_date']);
                    $total+=$obj->total;
                    $total_price+=$obj->total_price;
                    array_push($item_reports,$obj->tempObj);
                }
            }

        }
        return view('admin.reports.stock.companyValuation',compact(['noDisplay','companies','item_reports','on_date','total','total_price']));
    }




    public function evaluatedItem(Item $item,$date){
        
        $this->authorize('owner', User::class);

        $selectedDate=Carbon::parse($date);
        $stock=$this->evaluateQuantityBeforeDate($item,$selectedDate);

        $si=$item->dateSales($selectedDate);
        $return_items=0;
        foreach($si as $SI){

            $stock-=$SI->quantity;
        }
        $pi=$item->datePurchases($selectedDate);
        foreach($pi as $SI){

            $stock+=$SI->quantity;
        }
        $sr=$item->dateSalesReturns($selectedDate);
        foreach($sr as $SR){
            $return_items+=$SR->quantity;
        }

        $preturn_items=0;
        $pr=$item->datePurchaseReturns($selectedDate);
        foreach($pr as $SR){


            $preturn_items+=$SR->quantity;

        }

        $tempObj=new \stdClass();
        $tempObj->name=$item->name;
        $tempObj->quality=$item->quality->name;
        $tempObj->id=$item->id;
        $tempObj->covered=$item->covered;
        $tempObj->discount=0;
        if($tempObj->covered==1)
            $tempObj->discount=$item->CDiscount();
        else $tempObj->discount=$item->UDiscount();


        $tempObj->purchase_price=$item->purchase_price;

        $tempObj->sale_price=$item->sale_price;
        $stock+=$return_items;
        $stock-=$preturn_items;
        $tempObj->stock=$stock;
        $tempObj->opening_date=$item->opening_date;
        $tempObj->opening_quantity=$item->opening_quantity;

        $tempObj->return_items=$return_items;
        $tempObj->mytotal=$tempObj->stock * $item->purchase_price * ((1-($tempObj->discount/100)));
        //dd($tempObj);
        $returnObj = new \stdClass();
        $returnObj->tempObj=$tempObj;
        $returnObj->total=$stock;
        $returnObj->total_price=$tempObj->mytotal;
        return $returnObj;
    }
}
