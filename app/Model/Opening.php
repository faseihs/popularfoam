<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Opening extends Model
{
    //

    protected $guarded=[];


    public function transaction(){
        return $this->morphOne('App\Model\Transaction','related')
            ->where('type','account-opening');
    }
}
