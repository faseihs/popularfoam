<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseReturnDetails extends Model
{
    //
    protected $guarded=[];

    public function purchaseReturn(){
        return $this->belongsTo('App\PurchaseReturn');
    }

    public function item(){
        return $this->belongsTo('App\Item');
    }

    public function GetDateAttribute(){

        $si=$this->purchaseReturn;
        return $si?$si->date:null;
    }


    public function GetDatAttribute(){
        return $this->attributes['dat'] =$this->date;
    }

    public function getSuperidAttribute(){
        return $this->attributes['superid'] =$this->purchaseReturn->id;
    }
    protected $appends = ['dat','superid'];
}

