<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesReturn extends Model
{
    //
    protected $guarded = [];
    public function payment(){
        return $this->morphOne('App\Payments','sales');
    }

    public function items(){
        return $this->hasMany('App\SalesReturnDetails');
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }
    public function salesInvoice(){
        return $this->belongsTo('App\SalesInvoice');
    }

    public function GetCustAttribute(){
        if($this->customer)
            return $this->customer->name;
        else{
            if($this->name)
                return $this->name;
            else return 'Walking Customer';
        }
    }

    public function getCustomerName(){
        $customer = $this->customer;
        if($customer){
            if($customer->name == "Cash Sale"){
                if($this->name)
                    return $this->name;
                else return "Walking Customer"; 
            }
            else return $customer->name;
        }
        elseif($this->name)
            return $this->name;
        else return "Walking Customer";
    }

    public function creditPayment(){
        return $this->morphOne('App\Payments','sales')->where('type','credit')->get()->first();
    }
    public function debitPayment(){
        return $this->morphOne('App\Payments','sales')->where('type','debit')->get()->first();    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
