<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseReturn extends Model
{
    //

    protected $guarded=[];

    public function company(){
        return $this->belongsTo('App\Company');
    }

    public function items(){
        return $this->HasMany('App\PurchaseReturnDetails');
    }
    public function payment(){
        return $this->morphOne('App\Payments','sales');
    }

}
