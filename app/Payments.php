<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    //
    protected $guarded = [];

    public function owner(){
        return $this->morphTo();
    }
    public function sales(){
        return $this->morphTo();
    }

    public function account(){
        return $this->belongsTo('App\Account');
    }

    public function GetCustAttribute(){
        if($this->owner)
            return $this->owner->name;
      else{
          if($this->sales)
              return $this->sales->getCustomerName();
      }
    }

    public function photo(){
        return $this->morphOne('App\Photo','imageable');
    }
}
