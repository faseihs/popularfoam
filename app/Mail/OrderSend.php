<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderSend extends Mailable
{
    use Queueable, SerializesModels;
    public $purchaseOrder;
    public $attachment;
   
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($purchaseOrder,$attachment)
    {
        //
        $this->purchaseOrder=$purchaseOrder;
        $this->attachment=$attachment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.ordersend')
        ->from('popularfoam56@gmail.com','Popular Foam')
        ->attachData($this->attachment->output(),'Purchase Order #'.$this->purchaseOrder->id.'.pdf');
    }
}
