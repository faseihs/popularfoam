<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesReturnDetails extends Model
{
    //
    protected $guarded = [];
    public function salesReturn(){
        return $this->belongsTo('App\SalesReturn');
    }

    public function item(){
        return $this->belongsTo('App\Item');
    }


    public function GetDateAttribute(){

        $si=$this->salesReturn;
        return $si?$si->date:null;
    }

    public function getSuperidAttribute(){
        return $this->attributes['superid'] =$this->salesReturn?$this->salesReturn->id:null;
    }

    public function GetDatAttribute(){
        return $this->attributes['dat'] =$this->date;
    }
    protected $appends = ['dat','superid'];

    
}
