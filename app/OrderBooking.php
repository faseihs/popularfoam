<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderBooking extends Model
{
    //
    protected $guarded = [];



    public function items(){
        return $this->hasMany('App\OrderBookingDetail');
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }

    public function GetCustAttribute(){
        if($this->customer)
            return $this->customer->name;
      else{
          if($this->name)
              return $this->name;
            else return 'Walking Customer';
      }
    }

    public function getCustomerName(){
        $customer = $this->customer;
        if($customer){
            if($customer->name == "Cash Sale"){
                if($this->name)
                    return $this->name;
                else return "Walking Customer"; 
            }
            else return $customer->name;
        }
        elseif($this->name)
            return $this->name;
        else return "Walking Customer";
    }
    public function salesInvoice(){
        return $this->hasOne('App\SalesInvoice');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
