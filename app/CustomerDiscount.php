<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerDiscount extends Model
{
    //
    protected $guarded = [];

    public function company(){
        return $this->belongsTo('App\Company');
    }


    public function item(){
        return $this->belongsTo('App\Item');
    }

    public function quality(){
        return $this->belongsTo('App\Quality');
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }
}
