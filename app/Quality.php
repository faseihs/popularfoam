<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quality extends Model
{
    //
    protected $fillable = [
        'name','company_id','comments'
    ];
    public function company(){
        return $this->belongsTo('App\Company');
    }

    public function discount(){
        return $this->hasOne('App\Discount');
    }

    public function discounts(){
        return $this->hasMany('App\Discount');
    }

    public function items(){
        return $this->hasMany('App\Item');
    }

    public function customer_discounts(){
        return $this->hasMany('App\CustomerDiscount');
    }
}
