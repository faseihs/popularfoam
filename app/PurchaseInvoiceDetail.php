<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseInvoiceDetail extends Model
{
    //
    protected $guarded = [];

    public function purchaseInvoice(){
        return $this->belongsTo('App\PurchaseInvoice');
    }

    public function item(){
        return $this->belongsTo('App\Item');
    }

    public function GetDateAttribute(){

        $si=$this->purchaseInvoice;
        return $si?$si->date:null;
    }

    public function discountedPrice(){

        return $this->discount==0?$this->total_price:$this->total_price*(1-$this->discount/100);
    }

    public function GetDatAttribute(){
        return $this->attributes['dat'] =$this->date;
    }

    public function getSuperidAttribute(){
        return $this->attributes['superid'] =$this->purchaseInvoice?$this->purchaseInvoice->id:null;
    }
    protected $appends = ['dat','superid'];
}
