<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */

    // public function before (User $user)
    // {
    //     if($user->role_id === 1)
    //         return true;
    // }
    public function sales(User $user)
    {
        return in_array($user->role_id,[
            1,2,3
        ]);
    }
    public function admin(User $user)
    {
        return in_array($user->role_id,[
            1,2
        ]);
    }
   
    public function owner(User $user)
    {
        if($user->role_id === 1)
            return true;
    }
   
}
