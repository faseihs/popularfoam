<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PurchaseOrderPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function before (User $user)
    {
        if($user->role_id === 1)
            return true;
    }

    public function view(User $user)
    {
        return in_array($user->role_id,[1,2]);
    }
}
