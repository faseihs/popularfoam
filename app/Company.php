<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $guarded=[];
    public function quality(){
        return $this->hasMany('App\Quality');
    }

    public function items(){
        return $this->hasManyThrough('App\Item','App\Quality');
    }
    public function account(){
        return $this->hasOne('App\Account');
    }

    public function debitNonCash(){
        return $this->hasMany('App\NonCashTransaction','debit_id');
    }

    public function creditNonCash(){
        return $this->hasMany('App\NonCashTransaction','credit_id');
    }

    public function discounts(){
        return $this->hasMany('App\Discount');
    }

    public function cashRegister(){
        return $this->morphOne('App\CashRegister','owner');
    }
    public function payments(){
        return $this->morphMany('App\Payments','owner');
    }

    public function purchaseInvoices(){
        return $this->hasMany('App\PurchaseInvoice');
    }

    public function purchaseOrders(){
        return $this->hasMany('App\PurchaseOrder');
    }
}
