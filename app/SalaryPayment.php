<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryPayment extends Model
{
    //
    protected $guarded = [];

    public function worker(){
        return $this->belongsTo('App\Worker');
    }
}
