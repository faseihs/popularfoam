<?php
/**
 * Created by PhpStorm.
 * User: Grace
 * Date: 09/09/2018
 * Time: 12:08 PM
 */

namespace App\Transformer;
use Carbon\Carbon;
use League\Fractal;
use App\Item;
use App\SalesInvoice;

class SalesInvoiceApiTransformer extends Fractal\TransformerAbstract
{

    public function transform(SalesInvoice $item)
    {
        $array = $item->toArray();
        $array['allPaid']=$item->allPaid();
        $array['date']=Carbon::parse($item->date)->format('d-m-Y');
        $array['Balance']=$item->Balance();
        $array['cust']=$item->cust;
        $array["user"]=$item->user?$item->user->name:'-';
        return $array;
    }
}