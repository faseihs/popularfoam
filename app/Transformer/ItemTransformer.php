<?php
/**
 * Created by PhpStorm.
 * User: Grace
 * Date: 09/09/2018
 * Time: 12:08 PM
 */

namespace App\Transformer;
use League\Fractal;
use App\Item;

class ItemTransformer extends Fractal\TransformerAbstract
{
    public function __construct($id) {
        $this->id = $id;
    }
    public function transform(Item $item)
    {
        return [
            'id'=>$item->id,
            'name'=>$item->name,
            'sale_price'=>$item->discountedPrice($this->id),
            'quality'=>$item->quality->name,
            'size_t'=>$item->size_t,
            'covered'=>$item->covered,
            'covered_discount'=>$item->UCDiscount($this->id),
            'uncovered_discount'=>$item->CCDiscount($this->id),
            'package_unit'=>$item->package_unit,
            //"stock"=>$item->getStock()
        ];
    }
}