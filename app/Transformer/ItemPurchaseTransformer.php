<?php


namespace App\Transformer;


use League\Fractal;
use App\Item;

class ItemPurchaseTransformer extends Fractal\TransformerAbstract
{
    public function __construct() {
    }
    public function transform(Item $item)
    {
        return [
            'id'=>$item->id,
            'name'=>$item->name,
            'purchase_price'=>$item->purchase_price,
            'quality'=>$item->quality->name,
            'size_t'=>$item->size_t,
            'covered'=>$item->covered,
            'covered_discount'=>$item->UDiscount(),
            'uncovered_discount'=>$item->CDiscount(),
            'package_unit'=>$item->package_unit,
            'autoQuantity'=>$item->autoQuantity(),
            "company_name"=>$item->company()?$item->company()->name:"-",
            //"stock"=>$item->getStock()
        ];
    }
}