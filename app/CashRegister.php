<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashRegister extends Model
{
    //
    protected $guarded = [];

    public function owner(){
        return $this->morphTo();
    }
}
