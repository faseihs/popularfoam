<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class Item extends Model
{
    //
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function quality()
    {
        return $this->belongsTo('App\Quality');
    }

    public function photo()
    {
        return $this->morphOne('App\Photo', 'imageable');
    }

    public function purchaseOrders()
    {
        return $this->hasManyThrough('App\PurchaseOrder', 'App\PurchaseOrderDetail');
    }

    public function discount()
    {
        return $this->hasOne('App\Discount');
    }

    public function UDiscount()
    {
        //dd($this->discount);
        if ($this->discount) {
            return $this->discount->uncovered;
        } else {
            if ($this->quality->discounts) {
                foreach ($this->quality->discounts as $d) {
                    if ($d->item_id == 0) {
                        return $d->uncovered;
                        break;
                    }

                }
            } else return 0;
        }

        return 0;
    }

    public function CDiscount()
    {
        if ($this->discount) {
            return $this->discount->covered;
        } else {
            if ($this->quality->discounts) {
                foreach ($this->quality->discounts as $d) {
                    if ($d->item_id == 0) {
                        return $d->covered;
                        break;
                    }

                }
            } else return 0;
        }
        return 0;
    }

    public function company()
    {
        return $this->quality->company;
    }

    public function customer_discounts(){
        return $this->hasMany('App\CustomerDiscount');
    }

    public function UCDiscount($customer_id){
        /*foreach($this->quality->customer_discounts as $d) {
            if($d->customer_id==$customer_id && $d->item_id==$this->id){
                return $d->uncovered;
            }
        }
        foreach($this->quality->customer_discounts as $d) {
            if($d->customer_id==$customer_id && $d->item_id==$this->id){
                return $d->uncovered;
            }
        }
        foreach($this->quality->customer_discounts as $d) {
            if($d->customer_id==0){
                return $d->uncovered;
            }
        }*/
        $candi=false;
        $candi_d=0;
        $canda=false;
        $canda_d=0;
        $ianda=false;
        $ianda_d=0;
        $aanda=false;
        $aanda_d=0;
        foreach($this->quality->customer_discounts()->where('type',1)->get() as $d) {
            if($d->customer_id==$customer_id && $d->item_id==$this->id){
                $candi=true;
                $candi_d=$d->uncovered;
            }
            elseif($d->customer_id==$customer_id && $d->item_id==0){
                $canda=true;
                $canda_d=$d->uncovered;
            }
           /* elseif ($d->customer_id==0 && $d->item_id==$this->id){
                $ianda=true;
                $ianda_d=$d->uncovered;
            }
            elseif($d->customer_id==0 && $d->item_id==0){
                $aanda=true;
                $aanda_d=$d->uncovered;
            }*/
        }

        if($candi){
            return $candi_d;
        }
        elseif($canda)
            return $canda_d;
        else if($ianda)
            return $ianda_d;
        elseif($aanda)
            return $aanda_d;
        return 0;

    }

    public function CCDiscount($customer_id){
        /*foreach($this->quality->customer_discounts as $d) {
            if($d->customer_id==$customer_id && $d->item_id==$this->id){
                return $d->covered;
            }
        }
        foreach($this->quality->customer_discounts as $d) {
            if($d->customer_id==0){
                return $d->covered;
            }
        }*/
        $candi=false;
        $candi_d=0;
        $canda=false;
        $canda_d=0;
        $ianda=false;
        $ianda_d=0;
        $aanda=false;
        $aanda_d=0;
        foreach($this->quality->customer_discounts()->where('type',1)->get() as $d) {
            if($d->customer_id==$customer_id && $d->item_id==$this->id){
                $candi=true;
                $candi_d=$d->covered;
            }
            elseif($d->customer_id==$customer_id && $d->item_id==0){
                $canda=true;
                $canda_d=$d->covered;
            }
            /*elseif ($d->customer_id==0 && $d->item_id==$this->id){
                $ianda=true;
                $ianda_d=$d->covered;
            }
            elseif($d->customer_id==0 && $d->item_id==0){
                $aanda=true;
                $aanda_d=$d->covered;
            }*/
        }

        if($candi){
            return $candi_d;
        }
        elseif($canda)
            return $canda_d;
        else if($ianda)
            return $ianda_d;
        elseif($aanda)
            return $aanda_d;
        return 0;
        return 0;

    }

    //Discounted Prices
    public function discountedPrice($customer_id){
        $candi=false;
        $candi_d=0;
        $canda=false;
        $canda_d=0;
        $ianda=false;
        $ianda_d=0;
        $aanda=false;
        $aanda_d=0;
        foreach($this->quality->customer_discounts()->where('type',2)->get() as $d) {
            if($d->customer_id==$customer_id && $d->item_id==$this->id){
                $candi=true;
                $candi_d=$d->price;
            }
            elseif($d->customer_id==$customer_id && $d->item_id==0){
                $canda=true;
                $canda_d=$d->price;
            }
           /* elseif ($d->customer_id==0 && $d->item_id==$this->id){
                $ianda=true;
                $ianda_d=$d->price;
            }*/
            /*elseif($d->customer_id==0 && $d->item_id==0){
                $aanda=true;
                $aanda_d=$d->price;
            }*/
        }

        if($candi){
            return $candi_d;
        }
        elseif($canda)
            return $canda_d;
        else if($ianda)
            return $ianda_d;
        elseif($aanda)
            return $aanda_d;
        return $this->sale_price;

    }



    public function autoQuantity(){
        return ($this->default_quantity -($this->current_quantity + $this->store_quantity));
    }   



    public function todaysSales(){
       
        return $this->hasMany('App\SalesInvoiceDetails');
    }

    public function todaysPurchases(){
        return $this->hasMany('App\PurchaseInvoiceDetail');
    }


    public function dateSales(Carbon $date){
        $sales=SalesInvoice::where('date',$date->toDateTimeString('Y-m-d'))->get();
        $rtArray=[];
        foreach ($sales as $si){
            foreach ($si->items as $sid){
                if($sid->item_id==$this->id)
                    array_push($rtArray,$sid);
            }
        }

        return $rtArray;
    }
    public function datePurchases(Carbon $date){
        $sales=PurchaseInvoice::where('date',$date->toDateTimeString('Y-m-d'))->get();
        $rtArray=[];
        foreach ($sales as $si){
            foreach ($si->items as $sid){
                if($sid->item_id==$this->id)
                array_push($rtArray,$sid);
            }
        }

        return $rtArray;
    }
    public function dateSalesReturns(Carbon $date){
        $sales=SalesReturn::where('date',$date->toDateTimeString('Y-m-d'))->get();
        $rtArray=[];
        foreach ($sales as $si){
            foreach ($si->items as $sid){
                if($sid->item_id==$this->id)
                array_push($rtArray,$sid);
            }
        }

        return $rtArray;
    }
    public function datePurchaseReturns(Carbon $date){
        $sales=PurchaseReturn::where('date',$date->toDateTimeString('Y-m-d'))->get();
        $rtArray=[];
        foreach ($sales as $si){
            foreach ($si->items as $sid){
                if($sid->item_id==$this->id)
                array_push($rtArray,$sid);
            }
        }

        return $rtArray;
    }


    

    public function onDateSales($date){
        return $this->hasMany('App\SalesInvoiceDetails')->where('date','=',$date->toDateString());

    }

    public function onDatePurchase($date){
        return $this->hasMany('App\PurchaseInvoiceDetail')->where('date','=',$date->toDateString());

    }


    public function fromDateSales($from,$to){
        return $this->hasMany('App\SalesInvoiceDetails')->where('date','>=',$from->toDateString())->where('date','<=',$to->toDateString());
    }

    public function fromDatePurchases($from,$to){
        return $this->hasMany('App\PurchaseInvoiceDetail')
            ->select(['purchase_invoice_details.*'])
            ->join('purchase_invoices','purchase_invoice_details.purchase_invoice_id','=','purchase_invoices.id')
            ->where('purchase_invoices.date','>=',$from->toDateString())
            ->where('date','<=',$to->toDateString());

    }

    public function purchases(){
        
        $purchases= $this->hasMany('App\PurchaseInvoiceDetail','item_id');
        $purchases=$purchases->get()->all();
        usort($purchases,function($a,$b){
            return Carbon::parse($a->purchaseInvoice->date)>Carbon::parse($b->purchaseInvoice->date);
        });
        return $purchases;
    }

    public function sales(){
       
        $purchases= $this->hasMany('App\SalesInvoiceDetails','item_id');
        $purchases=$purchases->get()->all();
        usort($purchases,function($a,$b){
            return Carbon::parse($a->salesInvoice->date)>Carbon::parse($b->salesInvoice->date);
        });
        return $purchases;
    }

    public function firstPurchases($lastNum){
        $purchases=$this->purchases();
        $lp=[];
        $count=0;
        foreach($purchases as $p){
            if($count==$lastNum)
                break;

            for($i=1;$i<=$p->quantity;$i++){
                if($count==$lastNum)
                    break;
                $tempObj=new \stdClass;
                $tempObj->item_id=$p->item_id;
                $tempObj->quantity=1;
                $tempObj->price=$this->getDiscountedPrice($p->price);
                $tempObj->invoiceDiscountedPrice=$p->price*(1-($p->discount/100));
                $tempObj->price=$tempObj->invoiceDiscountedPrice;
                $tempObj->date=$p->purchaseInvoice->date;
                $count++;
                array_push($lp,$tempObj);
            }
            
            
        }

        return $lp;
    }

    public function firstSales($lastNum,$from=null,$to=null){
        if(!$from && !$to){
        $from=Carbon::parse('2018-06-01');
        $to=Carbon::now();
        }
        $sales=$this->sales();
        $lp=[];
        $count=0;
        foreach($sales as $p){
            if($count==$lastNum)
                break;

            for($i=1;$i<=$p->quantity;$i++){
                if($count==$lastNum)
                    break;
                $tempObj=new \stdClass;
                $tempObj->item_id=$p->item_id;
                $tempObj->quantity=1;
                $tempObj->price=$p->price;
                $tempObj->discount=$p->discount;
                $tempObj->date=$p->salesInvoice->date;
                $count++;
                array_push($lp,$tempObj);
            }
            /*if(floor($p->quantity)!=$p->quantity){
                $rem_quantity=$p->quantity-floor($p->quantity);

            }*/
            
            
        }

        return $lp;
    }

    public function countPurchases(){
        $purchases= $this->hasMany('App\PurchaseInvoiceDetail','item_id')->get()->all();
        $count=0;
        foreach($purchases as $p){
           

            for($i=1;$i<=$p->quantity;$i++){
                    $count++;
            }
        }
        return $count;

    }

    public function countSales(){
        $purchases= $this->hasMany('App\SalesInvoiceDetails','item_id')->get()->all();
        $count=0;
        foreach($purchases as $p){
           

            for($i=1;$i<=$p->quantity;$i++){
                    $count++;
            }
        }
        return $count;
    }



    public function getNumPurchases($number){
        $count=0;
        $returnArray=[];

        if($this->opening_quantity>=$number){
            for($i=1;$i<=$number;$i++){
                $tempObj = new \stdClass;
                $tempObj->item_id=$this->id;
                if($this->covered==1)
                $discount=$this->CDiscount();
                else  $discount=$this->UDiscount();
                if($discount==0)
                    $tempObj->price=$this->opening_price;
                else  $tempObj->price=$this->opening_price*(1-$discount/100);
                $tempObj->discount=$discount;
                array_push($returnArray,$tempObj);
            }
        }

        else{
            for($i=1;$i<=$this->opening_quantity;$i++){
                $tempObj = new \stdClass;
                $tempObj->item_id=$this->id;
                if($this->covered==1)
                $discount=$this->CDiscount();
                else  $discount=$this->UDiscount();
                if($discount==0)
                    $tempObj->price=$this->opening_price;
                else  $tempObj->price=$this->opening_price*(1-$discount/100);
                $tempObj->discount=$discount;
                array_push($returnArray,$tempObj);
            }
            $diff=$number-$this->opening_quantity;
            $purchases=$this->firstPurchases($diff);
            foreach($purchases as $p){
                array_push($returnArray,$p);
            }
        }
        // if($this->id==1)
        // dd($returnArray);
        return $returnArray;

    }

    public function getOpeningDiscount(){
        if($this->covered==1)
            $discount=$this->CDiscount();
        else  $discount=$this->UDiscount();
        if($discount==0)
                $price=$this->opening_price;
        else  $price=$this->opening_price*(1-$discount/100);

        return ['price'=>$price,'discount'=>$discount];
    }

    public function getDiscountedPrice($Price){
        if($this->covered==1)
            $discount=$this->CDiscount();
        else  $discount=$this->UDiscount();
        if($discount==0)
                $price=$Price;
        else  $price=$Price*(1-$discount/100);
        return $price;
    }


   
    public function getSalesIndividual(){
        $purchases= $this->hasMany('App\SalesInvoiceDetails','item_id');
        $purchases=$purchases->get()->all();
        usort($purchases,function($a,$b){
            return Carbon::parse($a->salesInvoice->date)>Carbon::parse($b->salesInvoice->date);
        });
        $returnArray=[];
        foreach($purchases as $p){
            $count=0;
            for($i=1;$i<=$p->quantity;$i++){
                $tempObj= new \stdClass;
                $tempObj->date=$p->date;
                //echo $tempObj->date;
                $tempObj->price=$p->price;
                $tempObj->discount=$p->discount;
                $tempObj->quantity=1;
                array_push($returnArray,$tempObj);

            }
            if(floor($p->quantity)!=$p->quantity){
                $rem=$p->quantity-floor($p->quantity);
                //dd($rem);
                $tempObj= new \stdClass;
                $tempObj->date=$p->date;
                //echo $tempObj->date;
                $tempObj->price=$p->price*$rem;
                $tempObj->discount=$p->discount;
                $tempObj->quantity=$rem;
                array_push($returnArray,$tempObj);
            }
        }
        return $returnArray;

    }
    public function getTotalSales($from,$to){
        // echo "<br>".$from;
        // echo "<br>".$to;
        $sales=$this->getSalesIndividual();
        $mainSales=[];
        $previousSales=[];
        foreach($sales as $s){
           
            $date=Carbon::parse($s->date);
            //echo "<br>".$date;
           
           
            if($date>=$from && $date<=$to)
                array_push($mainSales,$s);
            if($date<$from)
                array_push($previousSales,$s);
        }

        return ['mainSales'=>$mainSales,'prevSales'=>$previousSales];
    }

    public function  getTotalBefore($from,$to){
        $arr=$this->getTotalSales($from,$to);
        //dd($arr);
        return [
            'countSales'=>count($arr['mainSales']),
            'sales'=>$arr['mainSales'],
            'purchases'=>$this->getPurchasesfromOffset(count($arr['prevSales']),count($arr['mainSales']))
        ];


        
    }

    public function getPurchasesfromOffset($offset,$number){
        $count=0;
        $returnArray=[];
        $offsetcount=1;
        //echo "<br>$offset , $number";
        if($this->opening_quantity>=$number+$offset){
            //echo "<br>In first loop";
            for($i=1;$i<=$number;$i++){
                $tempObj = new \stdClass;
                $tempObj->item_id=$this->id;
                if($this->covered==1)
                    $discount=$this->CDiscount();
                else  $discount=$this->UDiscount();
                if($discount==0)
                    $tempObj->price=$this->opening_price;
                else  $tempObj->price=$this->opening_price*(1-$discount/100);
                $tempObj->discount=$discount;
                
                if($offsetcount > $offset)
                    array_push($returnArray,$tempObj);

                $offsetcount++;
            }
        }

        else{
            //echo "<br> IN second Loop";
            for($i=1;$i<=$this->opening_quantity;$i++){
                $tempObj = new \stdClass;
                $tempObj->item_id=$this->id;
                if($this->covered==1)
                $discount=$this->CDiscount();
                else  $discount=$this->UDiscount();
                if($discount==0)
                    $tempObj->price=$this->opening_price;
                else  $tempObj->price=$this->opening_price*(1-$discount/100);
                $tempObj->discount=$discount;
                $tempObj->date=$this->opening_date;
                //echo "<br>In 2nd first loop $offsetcount";
                if($offsetcount > $offset)
                    array_push($returnArray,$tempObj);
                $offsetcount++;
            }
            $diff=($offset+$number)-$this->opening_quantity;
            
            $purchases=$this->firstPurchases($diff);
            foreach($purchases as $p){
                //echo "<br>In 2nd 2nd loop $offsetcount";
                if($offsetcount > $offset)
                    array_push($returnArray,$p);
                $offsetcount++;
            }
        }

        return $returnArray;
    }



    public function salesReturns(){
        return $this->hasMany('App\SalesReturnDetails');
    }

    public function purchaseReturns(){
        return $this->hasMany('App\PurchaseReturnDetails');
    }


    public function salesInvoiceDetails(){
        return $this->hasMany('App\SalesInvoiceDetails');
    }

    public function purchaseInvoiceDetails(){
        return $this->hasMany('App\PurchaseInvoiceDetail');
    }


    public function getStock(){
        $stock = DB::select("
            SELECT SUM(quantity)as stock,item_id FROM
            (SELECT SUM(quantity) AS quantity,item_id FROM
                (
                    SELECT SUM(opening_quantity) quantity,id item_id from items WHERE id in ($this->id) GROUP BY id
                    UNION ALL
                    SELECT SUM(quantity)*-1 quantity,item_id FROM sales_invoice_details WHERE item_id in ($this->id) GROUP BY item_id
                    UNION ALL
                    SELECT SUM(quantity)*-1 quantity,item_id FROM purchase_return_details WHERE item_id in ($this->id) GROUP BY item_id
                    UNION  ALL
                    SELECT SUM(quantity) quantity,item_id FROM sales_return_details WHERE item_id in ($this->id) GROUP BY item_id
                    UNION ALL
                    SELECT SUM(quantity) quantity,item_id FROM purchase_invoice_details WHERE item_id in ($this->id) GROUP BY item_id
                ) t1 GROUP BY item_id
            )  t1 GROUP BY item_id
        ");
        return (int)$stock[0]->stock;
    }
}
