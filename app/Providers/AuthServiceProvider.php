<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        'App\User' => 'App\Policies\UserPolicy',
        'App\SalesInvoice' => 'App\Policies\SalesInvoicePoicy',
        'App\SalesReturn' => 'App\Policies\SalesReturnPoicy',
        'App\orderBooking' => 'App\Policies\orderBookingPolicy',
        'App\PurchaseOrder' => 'App\Policies\PurchaseOrderPolicy',
        'App\Category' => 'App\Policies\CategoryPolicy',
        'App\Company' => 'App\Policies\CompanyPolicy',
        'App\PurchaseIvoice' => 'App\Policies\PurchaseIvoicePolicy',
        'App\Item' => 'App\Policies\ItemPolicy',
        'App\Quality' => 'App\Policies\QualityPolicy',
        'App\Vehicle' => 'App\Policies\VehiclePolicy',


    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
