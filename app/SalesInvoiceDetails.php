<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesInvoiceDetails extends Model
{
    //
    protected $guarded = [];
    public function salesInvoice(){
        return $this->belongsTo('App\SalesInvoice');
    }

    public function item(){
        return $this->belongsTo('App\Item');
    }


    public function GetDateAttribute(){

        $si=$this->salesInvoice;
        return $si->date;
    }


    public function GetDatAttribute(){
        return $this->attributes['dat'] =$this->date;
    }
    public function getSuperidAttribute(){
        return $this->attributes['superid'] =$this->salesInvoice->id;
    }
    protected $appends = ['dat','superid'];
}
