<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Worker extends Model
{
    //
    protected $guarded = [];
    public function photo(){
        return $this->morphOne('App\Photo','imageable');
    }

    public function attendances(){
        return $this->hasMany('App\Attendance');
    }

    public function todaysAttenance(){
        return $this->hasMany('App\Attendance')->where('date','=',Carbon::now()->setTimezone('Asia/Karachi')->toDateString());
    }

    public function monthsAttendance($startOfMonth,$endOfMonth){
        return $this->hasMany('App\Attendance')->where('date','>=',$startOfMonth)->where('date','<=',$endOfMonth);
    }

    public function datesAttendence($date){
        return $this->hasMany('App\Attendance')->where('date','=',$date);

    }

    public function DateAttendence($date){
        return $this->hasOne('App\Attendance')->where('date','=',$date)->get()->all();

    }

    public function salaryPayments(){
        return $this->hasMany('App\SalaryPayment');
    }

    public function cashRegister(){
        return $this->morphOne('App\CashRegister','owner');
    }


    public function PresentThisMonth(){
        $at=$this->monthsAttendance(Carbon::now()->startOfMonth()->toDateString(),Carbon::now()->endOfMonth()->toDateString())->get();
        //dd($at);
        $atCount=0;
        $abCount=0;
        foreach ($at as $a){
            if($a->status==1)
                $atCount++;
            else $abCount++;
        }

        $nmCount=Carbon::now()->daysInMonth-($abCount +$atCount);
        return [$atCount,$abCount,$nmCount];
    }

    public function PresentOnMonth($month){
        $at=$this->monthsAttendance($month->startOfMonth()->toDateString(),$month::now()->endOfMonth()->toDateString())->get();
        //dd($at);
        $atCount=0;
        $abCount=0;
        foreach ($at as $a){
            if($a->status==1)
                $atCount++;
            else $abCount++;
        }

        $nmCount=Carbon::now()->daysInMonth-($abCount +$atCount);
        return [$atCount,$abCount,$nmCount];
    }

    public function GetNumberAttribute($value){
        if($value)
        return '0'.$value;
        else return '';
    }
}
