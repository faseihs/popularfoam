<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesInvoice extends Model
{
    //
    protected $guarded = [];



    public function items(){
        return $this->hasMany('App\SalesInvoiceDetails');
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }
    public function salesReturn(){
        return $this->hasOne('App\SalesReturn');
    }

    public function payment(){
        return $this->morphOne('App\Payments','sales');
    }

    public function GetCustAttribute(){
        if($this->customer)
            return $this->customer->name;
      else{
          if($this->name)
              return $this->name;
            else return 'Walking Customer';
      }
    }


    public function getCustomerName(){
        $customer = $this->customer;
        if($customer){
            if($customer->name == "Cash Sale"){
                if($this->name)
                    return $this->name;
                else return "Walking Customer"; 
            }
            else return $customer->name;
        }
        elseif($this->name)
            return $this->name;
        else return "Walking Customer";
    }

    public  function orderBooking(){
        return $this->belongsTo('App\OrderBooking');
    }


    public function payLaterPayment(){
        return $this->morphOne('App\Payments','sales')->where('pay_later','1')->get()->first();
    }
    public function payNowPayment(){
        return $this->morphOne('App\Payments','sales')->where('pay_later','0')->get()->first();
    }


    public function extraPayment(){
        return $this->hasOne('App\Payments','sales_invoice_id');
    }

    public function extraPayments(){
        return $this->hasMany('App\Payments','sales_invoice_id');
    }


    public function user(){
        return $this->belongsTo('App\User');
    }




    public function allPaid(){
        if($this->order_booking_id)
        return true;
        $paid= $this->paid_amount;
        if($paid>=$this->total_price)
            return true;
        if( $this->extraPayments){
            $payment = 0;
            foreach($this->extraPayments as $paym){
                $payment += $paym->amount;
            }
            if($paid+$payment>=$this->total_price)
                return true;
        }

        return false;
    }

    public function order_booking(){
        return $this->belongsTo('App\OrderBooking');
    }

    public function Balance(){
        $total=$this->total_price;
        $paid=$this->paid_amount;
        if($paid > $total)
            $total=$paid;
        $extra=0;

        foreach ($this->extraPayments as $payment){
            $extra+=$payment->amount;
        }
        return $total-$paid-$extra;
        return number_format(
            $total-$paid-$extra,
            '0',
            '.',
            ','
        );
    }

}
