
function setItems(items){
    finalStr=``;
    for (let i =0;i<items.length;i++){
        let item = items[i];
        finalStr+=`<tr  tabindex="${item.id}">
                                    <!--<td><button class="btn btn-sm btn-primary">Select</button></td>-->
                                    <td>${item.id}</td>
                                    <td>${item.name}</td>
                                    <td>${item.sale_price}</td>
                                    <td>${item.quality}</td>
                                    <td>${item.size_t}</td>
                                    <td>${item.Covered}</td>

                                    <td>
                                        ${item.covered_discount}
                    </td>
                    <td>
${item.uncovered_discount}
                    </td>
                    <td style="display:none">${item.package_unit}</td>
                                    <td style="display:none">${item.current_quantity}</td>
                                </tr>`
        finalStr+=`</tr>`;

    }
    $('#itemTbody').append(finalStr);
    bindItemSelectionEvents();

}

function bindItemSelectionEvents(){
    $('#scrollmodal,.selected').on('keydown',function (e) {
        let key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        if(key==40) {
            //$('#scrollmodal').focus();
            //e.preventDefault();
            $('#searchIn').blur();
            //console.log('Key Down Pressed');
            if($('.selected:not(:hidden)').length==0){

                $('#tableID tbody tr').not(':hidden').first().addClass('selected').focus();
            }
            else{
                let current=$('.selected:not(:hidden)');
                if(current.nextAll().not(':hidden').length){
                    current.nextAll().not(':hidden').first().addClass('selected').focus();
                    current.removeClass('selected');
                    //current.next().focus();
                }
                else{
                    current.removeClass('selected');
                    $('#searchIn').focus();
                }
            }

        }

        //Up Key
        if(key==38){
            //e.preventDefault();
            //$('#scrollmodal').focus();
            console.log('Dabb gaya');
            $('#searchIn').blur();
            if($('.selected:not(:hidden)').length==0){

                $('#tableID tbody tr').not(':hidden').last().addClass('selected').focus();
                console.log('yahan 1');
            }
            else{
                let current=$('.selected:not(:hidden)');
                if(current.prevAll().not(':hidden').length){
                    current.prevAll().not(':hidden').first().addClass('selected').focus();
                    current.removeClass('selected');
                }
                else{
                    current.removeClass('selected');
                    $('#searchIn').focus();
                    console.log('yahan 3');
                }
            }

        }

        if(key==13){
            e.preventDefault();
            $('#scrollmodal').focus();
            //console.log('Enter Pressed');
            if($('.selected:not(:hidden)').length>0) {
                let selected=$('.selected');
                //$('.card-body').append(selected.html());
                $('#scrollmodal').modal('toggle');
                getItemFromTable(selected);
                calcTotal();


            }

        }
    });
    $('#table_ID tbody tr').on('click',function () {
        $('#scrollmodal').modal('toggle');
        getItemFromTable($(this));
        calcTotal();

    });
}

$(document).ready(function () {
    $.ajax({
        url: "/api/get-items-edit/"+customerId,
        method:"GET"
    }).done(function(r) {
        $('#loading').remove();
        setItems(r.data);
    });

    /* Date.prototype.toDateInputValue = (function() {
         var local = new Date(this);
         local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
         return local.toJSON().slice(0,10);
     });*/
    //$('#date').val(new Date().toDateInputValue());
    Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
        if (e.preventDefault) {
            e.preventDefault();
            //$('#search').click();
            $('#scrollmodal').modal('toggle');

        } else {
            // internet explorer
            e.returnValue = false;
        }

    });

    Mousetrap.bind(['ctrl+q', 'meta+s'], function(e) {
        if (e.preventDefault) {
            e.preventDefault();
            $('#paid_amount').focus();

        } else {
            // internet explorer
            e.returnValue = false;
        }

    });

    Mousetrap.bind(['ctrl+s', 'meta+s'], function(e) {
        if (e.preventDefault) {
            e.preventDefault();
            $('#submitID').click();

        } else {
            // internet explorer
            e.returnValue = false;
        }

    });

    /*$('#pay_later select').on('change',function () {
       if($(this).val()==1) {
           $('.card-footer').append(udhar);
           if((!isNaN(parseInt($('#total span').html())) && parseInt($('#total span').html())>0) && !isNaN(parseInt($('#paid_amount').val()))){
               var ret=$('#paid_amount').val() - parseInt($('#total span').html());
               $('#to_pay').val(abs(parseInt(ret)));
           }
       }
       else $('#udhar').remove();
    });*/


    $('#scrollmodal').on('shown.bs.modal', function () {
        $('#searchIn').focus();
    });
    $('#scrollmodal').on('hidden.bs.modal', function () {
        $('#tableID tr').each(function () {
            $(this).removeClass('selected');
            $('#searchIn').val('');
            $('#table_ID tr').each(function () {
                $(this).css('display','');
            });
        });
        $('#search').blur();
    });

    $('#company').on('change', function() {
        let comp=$('#company option:selected').html();
        if(comp!=='Company')
            $('#companyID span').html(comp);
        else $('#companyID span').html('');
    });

    $('#vehicle').on('change', function() {
        let comp=$('#vehicle option:selected').html();
        if(comp!=='Vehicle' || $('#total-coverage').hasClass('alert-danger')) {
            $('#vehicleID span').html(comp);
            setVehicleSize();

            $('#submitID').removeAttr('disabled');

        }
        else {$('#vehicleID span').html('');setVehicleSize();$('#submitID').attr('disabled','true');}
    });

    $('.discountIN').on('keyup',function () {

        calcTotal();
        console.log('Yahoo');
    });
    $('.quantityIN').on('keyup',function () {
        calcTotal();

    });

    $('.bonus input').on('change',function () {

    });



    $('.priceIN').on('keyup',function(){
        calcTotal();
    });

    $('tr').on('focus',function(){
        console.log('FOvussed');
    });

    $('#given').on('keyup',function () {
        if(!isNaN(parseInt($('#total span').html()))) {
            $('#returned').val($(this).val() - parseInt($('#total span').html()));
            $('#to_pay').val(abs($(this).val() - parseInt($('#total span').html())));
        }
    });
    $('.deleteBtn').bind('click',function () {
        $(this).parent().parent().remove();
        calcTotal();
    });

    $('#Mdiscount').on('keyup',function () {
        calcTotal();
    });


});
function search() {
    $('#table_ID tbody tr').each(function () {
        $(this).removeClass('selected');
    });
    var input, filter, table, tr, td, i;
    input = document.getElementById("searchIn");
    filter = input.value.toUpperCase();
    table = document.getElementById("table_ID");
    //console.log(table);
    var tb = table.getElementsByTagName("tbody");
    tr=tb[0].getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";

            } else {
                tr[i].style.display = "none";

            }
        }
    }
}

function getItemFromTable(object){
    var item={id:-1, name:"", price:-1,quality:"" ,company:"",covered:'',udiscount:-1,cdiscount:-1};
    var counter=1;
    object.children().each(function () {
        if(counter==1)
            item.id=$(this).html();
        else if(counter==2)
            item.name=$(this).html();
        else if(counter==3)
            item.price=$(this).html();
        else if(counter==4)
            item.quality=$(this).html();
        else if(counter==5)
            item.size=$(this).html();
        else if(counter==6)
            item.covered=$(this).html();
        else if(counter==7)
            item.udiscount=$(this).html();
        else if(counter==8)
            item.cdiscount=$(this).html();
        else if(counter==9)
            item.package_unit=$(this).html();
        else if(counter==10)
            item.current_quantity=$(this).html();
        counter++;
    });

    //console.log(item);
    addItemToTable(item);
}

function addItemToTable(item){
    var selected;
    var selected_;
    var unselected;
    var unselected_;
    var dvalue;
    if(item.covered=='No'){
        selected=0;
        selected_='No';
        unselected=1;
        unselected_='Yes';
        dvalue=item.udiscount;
    }
    else{
        selected=1;
        selected_='Yes';
        unselected=0;
        unselected_='No';
        dvalue=item.cdiscount;
    }


    var row=`    <tr id='`+item.id+`'>
                                <td class="text-center">`+item.quality+`</td>
                                <td class="text-center">`+item.name+`</td>
                                <td class="text-center covered">
                                <select class="form-control-sm mousetrap">
                                <option selected value='`+selected+`'>`+selected_+`</option>
                                <option value='`+unselected+`'>`+unselected_+`</option>
                                </select>
                                 </td>
                                <td style="display:none" class="text-center size">`+item.size+`</td>
                                <td class="text-center cdiscount" style="display: none">`+item.cdiscount+`</td>
                                <td class="text-center udiscount" style="display: none">`+item.udiscount+`</td>
                                <td class="text-center">`+ item.current_quantity+`</td>
                                <td class="text-center quantity"><input step="any"  min="0" class="form-control-sm sm quantityIN mousetrap" type="number" value="1"/></td>
                                <td class="text-center">`+item.package_unit+`</td>
                                <td class="text-center bonus"><input min="0" class="form-control-sm sm mousetrap" type="number" value="0"/></td>
                                <td class="text-center price"><input step="any" min="0" class="form-control-sm priceIN mousetrap" type="number" value="`+item.price+`"/></td>
                                <td class="text-center discount"><input step="any" min="0" max="100" class="form-control-sm sm discountIN mousetrap" type="number" value=`+dvalue+`/> %</td>

                                <td class="text-center total">`+item.price+`</td>
                                <td style="display: none"><input class="obj" type="checkbox" name="checkBoxArray[]"  value=""></td>
                                <td><button class="btn btn-danger btn-sm deleteBtn" type="button">Delete</button></td>



                       </tr>`;
    $('#itemTableID tbody').append(row);
    $('#itemTableID tbody tr').last().find('.quantity input').focus();
    $('.quantityIN').bind('keyup', function() {
        calcTotal();
        console.log('Yahoo');
    });
    $('.discountIN').bind('change', function() {
        calcTotal();
        console.log('Yahoo');
    });


    $('.deleteBtn').bind('click',function () {
        $(this).parent().parent().remove();
        calcTotal();
    });

    $('.covered select').on('change',function () {
        var val =$(this).val();
        var p=$(this).parent().parent();

        //console.log(parseInt(p.find('.cdiscount').html()));
        //console.log(parseInt(p.find('.udiscount').html()));
        if(val==1)
            p.find('.discountIN').val(parseInt(p.find('.cdiscount').html()));
        else{
            p.find('.discountIN').val(parseInt(p.find('.udiscount').html()));
        }
        calcTotal();
    });

    $('.priceIN').on('keyup',function(){
        calcTotal();
    });



}

function calcTotal(){
    var sum=0;
    $('#itemTableID tbody').children().each(function () {

        var individualSum=parseFloat($(this).find('.price input').val()) *parseFloat($(this).find('.quantity input').val());
        console.log('Ind '+individualSum);
        var discount=parseFloat($(this).find('.discount input').val());
        console.log(discount);
        if(discount>0){
            individualSum=individualSum*(1-(discount/100));
        }
        individualSum=Number((individualSum).toFixed(1));
        $(this).children('.total').html(individualSum);
        sum+=individualSum;
    });

    sum=Number((sum).toFixed(1));
    console.log("Total" +sum);

    if(!isNaN($('#Mdiscount').val())){
        sum-=parseInt($('#Mdiscount').val());
    }
    $('#total span').html(sum);

    $('#totalPriceID').val(sum);
    if((!isNaN(parseInt($('#total span').html())) && parseInt($('#total span').html())>0) && !isNaN(parseInt($('#given').val()))) {
        //var ret=$('#paid_amount').val() - parseInt($('#total span').html());
        var ret=parseInt($('#given').val())- parseInt($('#total span').html());

        $('#returned').val(ret);
        //$('#to_pay').val(abs(ret));

    }
    else {
        $('#returned').val(0);
        //$('#to_pay').val(0);
    }
}

function submitFunc(){
    if($('#itemTableID tbody tr').children().length<1){
        alert("No Items Selected");
        return false;
    }

    if((($('#paid_amount').val()+$('#Mdiscount').val())-parseInt($('#total span').html())<0) && $('#pay_later select').val()==0){
        alert('Paid Amount is less than total amount ');
        return false;
    }

    if(($('#paid_amount').val()-parseInt($('#total span').html())>0) && $('#pay_later select').val()==1){
        alert('Paid Amount is greater so Pay Later (Yes) Option cannot be used ');
        return false;
    }
    /* if($('#customer_id').val()==0){
         if($('#pay_later select').val()==1)
         {
             alert('Walking Customer has to pay the whole amount now');
             return false;
         }
     }*/
    var item_array=Array();
    $('#itemTableID tbody tr').each(function () {

        var id=$(this).attr('id');
        var item={item_id:-1,quantity:-1,total_price:-1,bonus:-1,discount:-1,covered:-1};
        item.item_id=id;
        var $this=$(this);
        var count=1;
        item.price=$(this).children('.price').find(':nth-child(1)').val();
        //console.log($(this).find(':nth-child(5)').find(':nth-child(1)').val());
        item.quantity=$(this).children('.quantity').find(':nth-child(1)').val();
        item.bonus=$(this).children('.bonus').find(':nth-child(1)').val();
        item.discount=$(this).children('.discount').find(':nth-child(1)').val();
        item.covered=$(this).children('.covered').find(':nth-child(1)').val();
        item.total_price=$(this).children('.total').html();
        $(this).find('input.obj').val(item);
        //console.log($(this).find('input.obj').val());
        item_array.push(item);
    });
    var items=JSON.stringify(item_array);
    $('#formID').append("<textarea id='items' name='items' style='display:none'>"+items+"</textarea> ");
    //console.log($('#formID #items'));
    return true;
}



sortTable();
function sortTable() {
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("table_ID");
    switching = true;
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.getElementsByTagName("TR");
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[3];
            y = rows[i + 1].getElementsByTagName("TD")[3];
            // Check if the two rows should switch place:
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}
