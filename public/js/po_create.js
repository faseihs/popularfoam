$(document).ready(function () {
    $('#submitID').attr('disabled','true');
    Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
        if (e.preventDefault) {
            e.preventDefault();
            //$('#search').click();
            $('#scrollmodal').modal('toggle');

        } else {
            // internet explorer
            e.returnValue = false;
        }

    });



    $('#scrollmodal').on('shown.bs.modal', function () {
        $('#searchIn').focus();
    });
    $('#scrollmodal').on('hidden.bs.modal', function () {
        $('#tableID tr').each(function () {
            $(this).removeClass('selected');
            $('#searchIn').val('');
            $('#table_ID tr').each(function () {
                $(this).css('display','');
            });
        });
        $('#search').blur();
    });
    $('#scrollmodal,.selected').on('keydown',function (e) {
        let key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        if(key==40) {
            //$('#scrollmodal').focus();
            //e.preventDefault();
            /*$('#searchIn').blur();
            //console.log('Key Down Pressed');
            if($('.selected:not(:hidden)').length==0){

                $('#tableID tbody tr').not(':hidden').first().addClass('selected').focus();
            }
            else{
                let current=$('.selected:not(:hidden)');
                if(current.next().not(':hidden').length){
                    current.next().addClass('selected');
                    current.removeClass('selected');
                    current.next().focus();
                }
                else{
                    current.removeClass('selected');
                    $('#searchIn').focus();
                }
            }*/

        }

        if(key==38){
            //e.preventDefault();
            /*$('#scrollmodal').focus();
            $('#searchIn').blur();
            if($('.selected:not(:hidden)').length==0){

                $('#tableID tbody tr').not('hidden').last().addClass('selected');
            }
            else{
                let current=$('.selected:not(:hidden)');
                if(current.prev().not(':hidden').length){
                    current.prev().addClass('selected');
                    current.removeClass('selected');
                }
                else{
                    current.removeClass('selected');
                    $('#searchIn').focus();
                }
            }*/
        }

        if(key==13){
            /*e.preventDefault();
            $('#scrollmodal').focus();
            //console.log('Enter Pressed');
            if($('.selected:not(:hidden)').length>0) {
                let selected=$('.selected');
                //$('.card-body').append(selected.html());
                $('#scrollmodal').modal('toggle');
                getItemFromTable(selected);
                calcTotal();
                calcSize();

            }*/

        }
    });
    $('#company').on('change', function() {
        let comp=$('#company option:selected').html();
        if(comp!=='Company')
            $('#companyID span').html(comp);
        else $('#companyID span').html('');
    });

    $('#vehicle').on('change', function() {
        let comp=$('#vehicle option:selected').html();
        if(comp!=='Vehicle' || $('#total-coverage').hasClass('alert-danger')) {
            $('#vehicleID span').html(comp);
            setVehicleSize();
            calcSize();
            $('#submitID').removeAttr('disabled');

        }
        else {$('#vehicleID span').html('');setVehicleSize();$('#submitID').attr('disabled','true');}
    });

    $('.discountIN').on('keyup',function () {
        calcTotal();
        console.log('Yahoo');
    });
    $('.quantityIN').on('keyup',function () {
        calcTotal();
        calcSize();
    });

    $('.bonus input').on('change',function () {
        calcSize();
    });


    $('#table_ID tbody tr').on('click',function () {
        $('#scrollmodal').modal('toggle');
        getItemFromTable($(this));
        calcTotal();
        calcSize();
    });

    $('tr').on('focus',function(){
        console.log('FOvussed');
    });



});
function search() {
    $('#tableID tr').each(function () {
        $(this).removeClass('selected');
    });
    var input, filter, table, tr, td, i;
    input = document.getElementById("searchIn");
    filter = input.value.toUpperCase();
    table = document.getElementById("table_ID");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";

            } else {
                tr[i].style.display = "none";

            }
        }
    }
}

function getItemFromTable(object){
    var item={id:-1, name:"", price:-1,quality:"" ,company:"",covered:''};
    var counter=1;
    object.children().each(function () {
        if(counter==1)
            item.id=$(this).html();
        else if(counter==2)
            item.name=$(this).html();
        else if(counter==3)
            item.price=$(this).html();
        else if(counter==4)
            item.quality=$(this).html();
        else if(counter==5)
            item.size=$(this).html();
        else if(counter==6)
            item.covered=$(this).html();
        counter++;
    });

    //console.log(item);
    addItemToTable(item);
}

function addItemToTable(item){
    var selected;
    var selected_;
    var unselected;
    var unselected_;
    if(item.covered=='No'){
        selected=0;
        selected_='No';
        unselected=1;
        unselected_='Yes';
    }
    else{
        selected=1;
        selected_='Yes';
        unselected=0;
        unselected_='No';
    }


    var row=`    <tr id='`+item.id+`'>
                                <td>`+item.name+`</td>
                                <td>`+item.quality+`</td>
                                <td class="size">`+item.size+`</td>
                                <td class="text-right price">`+item.price+`</td>
                                <td class="text-right quantity"><input  min="1" class="form-control-sm sm quantityIN mousetrap" type="number" value="1"/></td>
                                <td class="text-right bonus"><input min="0" class="form-control-sm sm mousetrap" type="number" value="0"/></td>
                                <td class="text-right discount"><input min="0" max="100" class="form-control-sm sm discountIN mousetrap" type="number" value="0"/> %</td>
                                <td class="text-right covered">
                                <select class="form-control-sm mousetrap">
                                <option selected value='`+selected+`'>`+selected_+`</option>
                                <option value='`+unselected+`'>`+unselected_+`</option>
                                </select>
                                 </td>
                                <td class="text-right total">`+item.price+`</td>
                                <td style="display: none"><input class="obj" type="checkbox" name="checkBoxArray[]"  value=""></td>
                                <td><button class="btn btn-danger btn-sm deleteBtn" type="button">Delete</button></td>

                       </tr>`;
    $('#itemTableID tbody').append(row);
    $('.quantityIN').bind('change', function() {
        calcTotal();
        calcSize();
        console.log('Yahoo');
    });
    $('.discountIN').bind('change', function() {
        calcTotal();
        console.log('Yahoo');
    });

    $('.bonus').bind('change', function() {
        calcSize();
        console.log('Yahoo');
    });
    $('.deleteBtn').bind('click',function () {
       $(this).parent().parent().remove();
       calcSize();
       calcTotal();
    });

    
    
    
}

function calcTotal(){
    var sum=0;
    $('#itemTableID tbody').children().each(function () {
        //console.log($(this).children('.price').html());
        //console.log($(this));
        //console.log($(this).children('.price').html());
        //console.log($(this).find('.quantity input').val());
        var individualSum=parseInt($(this).children('.price').html()) *parseInt($(this).find('.quantity input').val());
        console.log(individualSum);
        var discount=parseInt($(this).find('.discount input').val());
        console.log(discount);
        if(discount!==0){
            individualSum=individualSum*(1-(discount/100));
        }
        $(this).children('.total').html(individualSum);
        sum+=individualSum;
    });
    sum=Number((sum).toFixed(1));
    $('#total span').html(sum);
    $('#totalPriceID').val(sum);
}

function setVehicleSize(){
    var v_id=$('#vehicle option:selected').val();

    if(v_id) {

        var size = $('#v' + v_id).html();
        $('#vehicle-size span').html(size);
    }
    else {
        $('#vehicle-size span').html('0');
        console.log(v_id+'ss');
    }
}

function calcSize(){
    var sum=0;
    $('#itemTableID tbody').children().each(function () {
        var individualSize;
        if(parseInt($(this).find('.bonus input').val())>0)
            individualSize=(parseInt($(this).find('.quantity input').val()) +parseInt($(this).find('.bonus input').val()) )*parseInt($(this).children('.size').html());
        else individualSize=parseInt($(this).find('.quantity input').val())*parseInt($(this).children('.size').html());
        sum+=individualSize;
    });
    var percentage=(sum*100)/parseInt($('#vehicle-size span').html());
    console.log('Size is '+sum);
    percentage=Number((percentage).toFixed(1));
    if(percentage>=100 || $('#vehicle option:selected').html()==='Vehicle'){
        $('#total-coverage').addClass('alert-danger');
        $('#submitID').attr('disabled','true');

    }
    else { $('#total-coverage').removeClass('alert-danger');$('#submitID').removeAttr('disabled');}
    $('#total-coverage span').html(percentage);
}

function submitFunc(){
    var item_array=Array();
    $('#itemTableID tbody tr').each(function () {

        var id=$(this).attr('id');
        var item={item_id:-1,quantity:-1,total_price:-1,bonus:-1,discount:-1,covered:-1};
        item.item_id=id;
        var $this=$(this);
        var count=1;
        item.price=$(this).find(':nth-child(4)').html();
        //console.log($(this).find(':nth-child(5)').find(':nth-child(1)').val());
        item.quantity=$(this).find(':nth-child(5)').find(':nth-child(1)').val();
        item.bonus=$(this).find(':nth-child(6)').find(':nth-child(1)').val();
        item.discount=$(this).find(':nth-child(7)').find(':nth-child(1)').val();
        item.covered=$(this).find(':nth-child(8)').find(':nth-child(1)').val();
        item.total_price=$(this).find(':nth-child(9)').html();
        /*$(this).children().each(function () {
            if(count==4)
                item.price=$(this).children('.price').html();
            else if(count==5)
                item.quantity=$(this).find('input.quantity').val();
            else if(count==6)
                item.bonus=$(this).find('input.bonus').val();
            else if(count==7)
                item.discount=$(this).find('input.discount').val();
            count++;
        });*/
        //console.log(item);
        $(this).find('input.obj').val(item);
        //console.log($(this).find('input.obj').val());
        item_array.push(item);
    });
    var items=JSON.stringify(item_array);
    $('#formID').append("<textarea id='items' name='items' style='display:none'>"+items+"</textarea> ");
    //console.log($('#formID #items'));
    return true;
}
