
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import moment from 'moment'

Vue.prototype.moment = moment;

import Multiselect from 'vue-multiselect'

// register globally
Vue.component('multiselect', Multiselect);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('purchase-report-item-summary',require('./components/Pruchase/PurchaseReportItemSummary.vue'));
const app = new Vue({
    el: '#app'
});
