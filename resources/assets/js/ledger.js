
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('vue-table',require('./components/ledgerTable.vue'));
Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
})
const ledger_app = new Vue({
    el: '#ledger-app',
    data(){
        return{
            customers:[],
            companies:[]
        }
    },
    beforeCreate(){
        axios.get('/admin/getCust')
            .then(response=>{
                this.customers=response.data;

            })
            .catch(e=>console.log(e));
        axios.get('/admin/getComp')
            .then(response=>{
                this.companies=response.data;
            })
            .catch(e=>console.log(e));
    }
});
