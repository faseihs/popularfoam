
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('vue-table',require('./components/CustTable.vue'));
Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
})
const ledger_app = new Vue({
    el: '#ledger-app',
    data(){
        return{
            columns:['Sr.No','ID','Date','Type','Debit Amount','Credit Amount','Balance'],
            dataArray:[]
        }
    },
    beforeCreate(){
        axios.get('/admin/getCul')
            .then(response=>{
                this.dataArray=response.data;
            })
            .catch(e=>console.log(e));
    }
});
