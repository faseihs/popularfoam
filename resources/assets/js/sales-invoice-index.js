
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('today',require('./components/SalesInvoiceIndex/Today.vue'));
Vue.component('old',require('./components/SalesInvoiceIndex/Old.vue'));
Vue.component('payment-pending',require('./components/SalesInvoiceIndex/PaymentPending.vue'));
Vue.component('order-booking',require('./components/SalesInvoiceIndex/OrderBooking.vue'));

Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
});


import moment from 'moment'

Vue.prototype.moment = moment


const SalesInvoiceToday = new Vue({

    el: '#sales-invoice-today'
});

/*
const SalesInvoiceOld = new Vue({

    el: '#sales-invoice-old'
});


const Orderbookings = new Vue({

    el: '#order-booking'
});

const PaymentPending = new Vue({

    el: '#payment-pending'
});
*/
