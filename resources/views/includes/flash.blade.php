@if (session('success'))
    <div class="alert alert-primary" role="alert">
        {{session('success')}}
    </div>
    <script>
        setTimeout(function(){ $('.alert.alert-primary').hide(500) }, 3000);
    </script>
@endif

@if (session('deleted'))
    <div class="alert alert-danger">
        {{session('deleted')}}
    </div>
    <script>
        setTimeout(function(){ $('.alert.alert-danger').hide(500) }, 3000);
    </script>
@endif

