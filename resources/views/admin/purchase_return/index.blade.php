@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">Purchase Returns</h3>
                </div>
                <div class="col-md-5">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search...">
                </div>
                <div class="table-data__tool-right col-md-4">
                    <button id="add" type="button" class="btn btn-primary mb-1 float-right" data-toggle="modal" data-target="#scrollmodal">
                        Add Purchase Returns
                    </button>
                </div>
            </div>

        </div>

        @if(count($purchase_returns)==0)
            <h1 class="text-md-center">No Purchase Returns Found</h1>
        @endif
        @if(count($purchase_returns)>0)

            <div id="tableID" class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Created Date</th>
                        <th>Invoice Date</th>
                        <th>Company</th>
                        <th class="text-right">Total Bill</th>

                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($purchase_returns as $purchase_return)
                        <tr class="shadow">
                            <td>{{$purchase_return->id}}</td>
                            <td>{{Carbon::parse($purchase_return->created_at)->format('d-m-Y')}}</td>
                            <td>{{Carbon::parse($purchase_return->date)->format('d-m-Y')}}</td>
                            <td>{{$purchase_return->company->name}}</td>
                            <td class="text-right">{{number_format($purchase_return->total_price,0,'.',',')}}</td>

                            <td>                                <a class="btn btn-primary btn-sm" href="/admin/purchase_return/{{$purchase_return->id}}">View</a>
                            </td>
                            <td>
                                <div class="table-data-feature">
                                    <form id="del{{$purchase_return->id}}" action="/admin/purchase_return/{{$purchase_return->id}}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        <a href="#" onclick="clicked({{$purchase_return->id}})"  class="purchase_return" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                    </form>
                                    <a href="/admin/purchase_return/{{$purchase_return->id}}/edit" class="purchase_return" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>

                                </div>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        @endif
        <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        Select Company
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['method'=>'POST','action'=>'AdminPurchaseReturnController@createC']) !!}
                        <div class="form-group">

                            {!! Form::select('company_id',$companies,null,['class'=>'form-control','placeholder'=>'Select Company'])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Submit',['class'=>'btn btn-default'])!!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <div class="d-flex justify-content-around">
                            <a href="/admin/purchase_return/{id}" class="p2"></a>
                            <button type="button" class="btn btn-secondary p2" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function clicked(id){
                if(confirm("Are You Sure ?")){
                    document.getElementById('del'+id).submit();
                }
                else{
                }
            }
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#add').click();

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>

    </div>       <!-- END DATA TABLE-->
    <style>
        .img{
            padding: 22px 40px !important;
            padding-left: 10px !important;
        }
    </style>
    @include('includes.search')
@endsection