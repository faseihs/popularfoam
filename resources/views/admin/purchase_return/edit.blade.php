@extends('layouts.simple')
@section('content')
    <div id="hidden" style="display:none"></div>
    <div class="col-12">
        <div class="card">
            {!! Form::model($purchaseReturn,['method'=>'PATCH','action'=>['AdminPurchaseReturnController@update',$purchaseReturn->id],'onsubmit'=>'return submitFunc()','id'=>'formID','enctype' => 'multipart/form-data']) !!}
            <div class="card-header">
                <input name="company_id" type="hidden" value="{{$purchaseReturn->company->id}}">
                <input id="totalPriceID" name="total_price" value="{{$purchaseReturn->total_price}}" type="hidden">

                <strong style="padding-right: 10px;">Purchase Return</strong>
                <button id="search" type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#scrollmodal">
                    Add Item
                </button>
            </div>
            <div class="card-body card-block">
                <div class="d-flex justify-content-around">
                    <div id="companyID" class="p-2"><strong>Company: </strong>{{$purchaseReturn->company->name}}<span></span></div>
                    <div class="p2">
                        {!! Form::label('invoice_id','Return of Invoice:',['class'=>'font-weight-bold'])!!}
                        {!! Form::text('invoice_id',null,['pattern'=>'[0-9]{1,}','class'=>'form-control-sm','placeholder'=>'Optional'])!!}
                        {!! Form::label('date','Invoice Date :',['class'=>'font-weight-bold'])!!}
                        {!! Form::date('date',null,['class'=>'form-control-sm','required'])!!}
                    </div>
                </div>
                <div style="overflow-y: scroll" id="itemDivID" class="table-responsive table--no-card m-b-30">
                    <table id="itemTableID" class="table table-borderless  table-earning">
                        <thead>
                        <tr>
                            <th>Quality</th>
                            <th>Item</th>
                            <th class="text-right">Covered</th>
                            <th class="text-right">Quantity</th>
                            <th class="text-right">Bonus</th>
                            <th class="text-right">Price</th>
                            <th class="text-right">Discount</th>
                            <th class="text-right">Total</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr id="{{$item->item->id}}">
                                <td>{{$item->item->quality->name}}</td>
                                <td>{{$item->item->name}}</td>
                                <td class="text-right covered">
                                    <select class="form-control-sm mousetrap">
                                        <option selected value={{$item->covered}}>{{$item->covered==0?'No':'Yes'}}</option>
                                        <option value="{{$item->covered==0?1:0}}">{{$item->covered==0?'Yes':'No'}}</option>
                                    </select>
                                </td>
                                <td style="display: none" class="size">{{$item->item->size_t}}</td>
                                <td class="udiscount" style="display: none">{{$item->item->UDiscount()}}</td>
                                <td class="cdiscount" style="display: none">{{$item->item->CDiscount()}}</td>

                                <td class="text-right quantity"><input step="any"  min="0" class="form-control-sm sm quantityIN mousetrap" type="number" value="{{$item->quantity}}"/></td>
                                <td class="text-right bonus"><input min="0" class="form-control-sm sm mousetrap" type="number" value="{{$item->bonus}}"/></td>
                                <td class="text-right price">{{$item->price}}</td>

                                <td class="text-right discount"><input min="0" max="100" class="form-control-sm sm discountIN mousetrap" type="number" value="{{$item->discount}}"/> %</td>

                                <td class="text-right total">{{$item->total_price}}</td>
                                <td style="display: none"><input class="obj" type="checkbox" name="checkBoxArray[]"  value=""></td>
                                <td><button class="btn btn-danger btn-sm deleteBtn" type="button">Delete</button></td>



                            </tr>
                        @endforeach

                        </tbody>

                    </table>
                </div>
            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-between">
                    <div id="total" class="p2"><strong>Total : </strong><span>{{$purchaseReturn->total_price}}</span></div>
                    {!! Form::submit('Submit',['class'=>'btn btn-primary p2','id'=>'submitID'])!!}
                </div>
            </div>

        </div>
        {!! Form::close() !!}
    </div>

    @if ($purchaseReturn->pic)

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Invoice Image</h4>
                        <button id="delImg" style="float: right" type="button" class="btn btn-danger btn-sm">
                            <i class="fa fa-eraser"></i> Delete Photo
                        </button>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                    </div>
                    <div class="modal-body">
                        <img id="invoiceImg" src="{{$purchaseReturn->pic}}" alt="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

    @endif


    <!-- modal scroll -->
    <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <input onkeyup="search()"  autofocus name="searchItem" id="searchIn" type="text" class="form-control mousetrap" />
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="tableID" class="table-responsive table--no-card m-b-30">
                        <table id="table_ID" class="table table-borderless  table-earning">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Item</th>
                                <th>Price</th>
                                <th>Quality</th>
                                <th>Size</th>
                                <th>Covered</th>
                                <th>UDiscount</th>
                                <th>CDiscount</th>
                            </tr>
                            </thead>

                            <tbody id="itemTbody">
                            <tr id="loading"><td class="text-center" colspan="8"><h3>Loading....</h3></td></tr>

                            {{-- @php($i=1)
                             @foreach($Items as $item)
                                 <tr  tabindex="{{$i}}">
                                     <!--<td><button class="btn btn-sm btn-primary">Select</button></td>-->
                                     <td>{{$item->id}}</td>
                                     <td>{{$item->name}}</td>
                                     <td>{{$item->purchase_price}}</td>
                                     <td>{{$item->quality->name}}</td>
                                     <td>{{$item->size_t}}</td>
                                     <td>{{$item->covered==0?'No':'Yes'}}</td>

                                     <td>--}}{{--@php
                                             if($item->discount){
                                                 echo $item->discount->uncovered;
                                             }
                                             else{
                                                 if($item->company()->discounts){
                                                     foreach ($item->company()->discounts as $d){
                                                         if($d->item_id==0){
                                                             echo $d->uncovered;
                                                             break;
                                                         }

                                                     }
                                                 }
                                                 else echo '0';
                                             }
                                         @endphp--}}{{--
                                         {{$item->UDiscount()}}
                                     </td>
                                     <td>
                                         --}}{{--@php
                                             if($item->discount){
                                                 echo $item->discount->covered;
                                             }
                                             else{
                                                 if($item->company()->discounts){
                                                 foreach ($item->company()->discounts as $d){
                                                     if($d->item_id==0){
                                                         echo $d->covered;
                                                         break;
                                                     }

                                                 }
                                             }
                                             else echo '0';

                                             }
                                         @endphp--}}{{--
                                         {{$item->CDiscount()}}
                                     </td>
                                 </tr>
                                 @php($i++)
                             @endforeach--}}

                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

                </div>
            </div>
        </div>
    </div>

    @include('includes.errors')
    <style>
        .card-body{
            min-height: 83vh;
            max-height: 83vh !important;
        }
        tr:focus{
            background: #b2b2b2;
        }
        tr:focus{
            background: #b2b2b2;
        }

        .selected{
            background: #b2b2b2 !important;
            color: white !important;
        }
        .selected td{
            color:white !important;
        }
        .form-control-sm{
            border:1px solid #ced4da !important;
        }
        .sm{
            width: 50px !important;
        }
        #itemDivID{
            max-height: 70vh;
        }
        #table_ID td,th{
            padding: 12px 10px !important;
        }
        #itemTableID td,th{
            padding: 12px 10px !important;
            font-size: 12px;
        }

        .card-header{
            padding: 0;
            font-size: 16px !important;
        }
        .card-footer{
            padding: 0;
            padding-top: 1px;
            padding-left: 5px;
            padding-right: 5px;
            font-size: 12px !important;
        }
        .form-control-sm{
            font-size: 10px !important;
        }
        select.form-control-sm{
            height: 1.5rem !important;
        }
        .p2{
            font-size: 12px !important;
        }
        .btn-sm{
            font-size: .5rem !important;
        }
    </style>

    {{--<script src="{{asset('js/po_create.js')}}"></script>--}}
    <script>
        var delImg='<input type="hidden" name="delPic" value="1" />'

        var companyId={!! $purchaseReturn->company->id !!}
        function setItems(items){
            finalStr=``;
            for (let i =0;i<items.length;i++){
                let item = items[i];
                finalStr+=`<tr  tabindex="${item.id}">
                                    <!--<td><button class="btn btn-sm btn-primary">Select</button></td>-->
                                    <td>${item.id}</td>
                                    <td>${item.name}</td>
                                    <td>${item.purchase_price}</td>
                                    <td>${item.quality}</td>
                                    <td>${item.size_t}</td>
                                    <td>${item.covered==1?'Yes':'-'}</td>

                                    <td>
                                        ${item.covered_discount}
                    </td>
                    <td>
${item.uncovered_discount}
                    </td>
                  </tr>`
                finalStr+=`</tr>`;

            }
            $('#itemTbody').append(finalStr);
            bindEvents();
        };

        $.ajax({
            url: "/api/get-items-company/"+companyId,
            method:"GET"
        }).done(function(r) {
            $('#loading').remove();
            setItems(r.data);
        });



        function bindEvents(){
            $('#scrollmodal').on('shown.bs.modal', function () {
                $('#searchIn').focus();
            });
            $('#scrollmodal').on('hidden.bs.modal', function () {
                $('#tableID tr').each(function () {
                    $(this).removeClass('selected');
                    $('#searchIn').val('');
                    $('#table_ID tr').each(function () {
                        $(this).css('display','');
                    });
                });
                $('#search').blur();
            });
            $('#scrollmodal,.selected').on('keydown',function (e) {
                let key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
                if(key==40) {
                    //$('#scrollmodal').focus();
                    //e.preventDefault();
                    $('#searchIn').blur();
                    //console.log('Key Down Pressed');
                    if($('.selected:not(:hidden)').length==0){

                        $('#tableID tbody tr').not(':hidden').first().addClass('selected').focus();
                    }
                    else{
                        let current=$('.selected:not(:hidden)');
                        if(current.nextAll().not(':hidden').length){
                            current.nextAll().not(':hidden').first().addClass('selected').focus();
                            current.removeClass('selected');
                            //current.next().focus();
                        }
                        else{
                            current.removeClass('selected');
                            $('#searchIn').focus();
                        }
                    }

                }

                //Up Key
                if(key==38){
                    //e.preventDefault();
                    //$('#scrollmodal').focus();
                    console.log('Dabb gaya');
                    $('#searchIn').blur();
                    if($('.selected:not(:hidden)').length==0){

                        $('#tableID tbody tr').not(':hidden').last().addClass('selected').focus();
                        console.log('yahan 1');
                    }
                    else{
                        let current=$('.selected:not(:hidden)');
                        if(current.prevAll().not(':hidden').length){
                            current.prevAll().not(':hidden').first().addClass('selected').focus();
                            current.removeClass('selected');
                        }
                        else{
                            current.removeClass('selected');
                            $('#searchIn').focus();
                            console.log('yahan 3');
                        }
                    }

                }

                if(key==13){
                    e.preventDefault();
                    $('#scrollmodal').focus();
                    //console.log('Enter Pressed');
                    if($('.selected:not(:hidden)').length>0) {
                        let selected=$('.selected');
                        //$('.card-body').append(selected.html());
                        $('#scrollmodal').modal('toggle');
                        getItemFromTable(selected);
                        calcTotal();
                        calcSize();

                    }

                }
            });
            $('#company').on('change', function() {
                let comp=$('#company option:selected').html();
                if(comp!=='Company')
                    $('#companyID span').html(comp);
                else $('#companyID span').html('');
            });

            $('#vehicle').on('change', function() {
                let comp=$('#vehicle option:selected').html();
                if(comp!=='Vehicle' || $('#total-coverage').hasClass('alert-danger')) {
                    $('#vehicleID span').html(comp);
                    setVehicleSize();
                    calcSize();
                    $('#submitID').removeAttr('disabled');

                }
                else {$('#vehicleID span').html('');setVehicleSize();$('#submitID').attr('disabled','true');}
            });

            $('#table_ID tbody tr').on('click',function () {
                $('#scrollmodal').modal('toggle');
                getItemFromTable($(this));
                calcTotal();
                calcSize();
            });

            $('tr').on('focus',function(){
                console.log('FOvussed');
            });


            //Binding

            $('.quantityIN').bind('change', function() {
                calcTotal();
                console.log('Yahoo');
            });
            $('.discountIN').bind('change', function() {
                calcTotal();
                console.log('Yahoo');
            });


            $('.deleteBtn').bind('click',function () {
                $(this).parent().parent().remove();
                calcTotal();
            });

            $('.covered select').on('change',function () {
                var val =$(this).val();
                var p=$(this).parent().parent();

                //console.log(parseInt(p.find('.cdiscount').html()));
                //console.log(parseInt(p.find('.udiscount').html()));
                if(val==1)
                    p.find('.discountIN').val(parseInt(p.find('.cdiscount').html()));
                else{
                    p.find('.discountIN').val(parseInt(p.find('.udiscount').html()));
                }
                calcTotal();
            });
        }
        $(document).ready(function () {
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    //$('#search').click();
                    $('#scrollmodal').modal('toggle');

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            $('#delImg').click(function(){
                $('#invoiceImg').remove();
                $('#formID').append(delImg);
                $('#myModal').modal('toggle');
            });





        });
        function search() {
            $('#tableID tr').each(function () {
                $(this).removeClass('selected');
            });
            var input, filter, table, tr, td, i;
            input = document.getElementById("searchIn");
            filter = input.value.toUpperCase();
            table = document.getElementById("table_ID");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";

                    } else {
                        tr[i].style.display = "none";

                    }
                }
            }
        }

        function getItemFromTable(object){
            var item={id:-1, name:"", price:-1,quality:"" ,company:"",covered:'',udiscount:-1,cdiscount:-1};
            var counter=1;
            object.children().each(function () {
                if(counter==1)
                    item.id=$(this).html();
                else if(counter==2)
                    item.name=$(this).html();
                else if(counter==3)
                    item.price=$(this).html();
                else if(counter==4)
                    item.quality=$(this).html();
                else if(counter==5)
                    item.size=$(this).html();
                else if(counter==6)
                    item.covered=$(this).html();
                else if(counter==7)
                    item.udiscount=$(this).html();
                else if(counter==8)
                    item.cdiscount=$(this).html();
                counter++;
            });

            //console.log(item);
            addItemToTable(item);
        }

        function addItemToTable(item){
            var selected;
            var selected_;
            var unselected;
            var unselected_;
            var dvalue;
            if(item.covered=='No'){
                selected=0;
                selected_='No';
                unselected=1;
                unselected_='Yes';
                dvalue=item.udiscount;
            }
            else{
                selected=1;
                selected_='Yes';
                unselected=0;
                unselected_='No';
                dvalue=item.cdiscount;
            }


            var row=`    <tr id='`+item.id+`'>
                                <td>`+item.quality+`</td>
                                <td>`+item.name+`</td>
                                <td class="text-right covered">
                                <select class="form-control-sm mousetrap">
                                <option selected value='`+selected+`'>`+selected_+`</option>
                                <option value='`+unselected+`'>`+unselected_+`</option>
                                </select>
                                 </td>
                                <td style="display:none" class="size">`+item.size+`</td>
                                <td class="cdiscount" style="display: none">`+item.cdiscount+`</td>
                                <td class="udiscount" style="display: none">`+item.udiscount+`</td>

                                <td class="text-right quantity"><input  min="1" class="form-control-sm sm quantityIN mousetrap" type="number" value="1"/></td>
                                <td class="text-right bonus"><input min="0" class="form-control-sm sm mousetrap" type="number" value="0"/></td>
                                 <td class="text-right price">`+item.price+`</td>
                                <td class="text-right discount"><input min="0" max="100" class="form-control-sm sm discountIN mousetrap" type="number" value=`+dvalue+`/> %</td>

                                <td class="text-right total">`+item.price+`</td>
                                <td style="display: none"><input class="obj" type="checkbox" name="checkBoxArray[]"  value=""></td>
                                <td><button class="btn btn-danger btn-sm deleteBtn" type="button">Delete</button></td>



                       </tr>`;
            $('#itemTableID tbody').append(row);
            $('.quantityIN').bind('change', function() {
                calcTotal();
                console.log('Yahoo');
            });
            $('.discountIN').bind('change', function() {
                calcTotal();
                console.log('Yahoo');
            });


            $('.deleteBtn').bind('click',function () {
                $(this).parent().parent().remove();
                calcTotal();
            });

            $('.covered select').on('change',function () {
                var val =$(this).val();
                var p=$(this).parent().parent();

                //console.log(parseInt(p.find('.cdiscount').html()));
                //console.log(parseInt(p.find('.udiscount').html()));
                if(val==1)
                    p.find('.discountIN').val(parseInt(p.find('.cdiscount').html()));
                else{
                    p.find('.discountIN').val(parseInt(p.find('.udiscount').html()));
                }
                calcTotal();
            });




        }

        function calcTotal(){
            var sum=0;
            $('#itemTableID tbody').children().each(function () {

                var individualSum=parseFloat($(this).children('.price').html()) *parseFloat($(this).find('.quantity input').val());
                console.log(individualSum);
                var discount=parseInt($(this).find('.discount input').val());
                console.log(discount);
                if(discount>0){
                    individualSum=individualSum*(1-(discount/100));
                }
                individualSum=Number((individualSum).toFixed(1));
                $(this).children('.total').html(individualSum);
                sum+=individualSum;
            });
            sum=Number((sum).toFixed(1));
            $('#total span').html(sum);
            $('#totalPriceID').val(sum);
        }

        function submitFunc(){
            if($('#itemTableID tbody tr').children().length<1){
                alert("No Items Selected");
                return false;
            }
            var item_array=Array();
            $('#itemTableID tbody tr').each(function () {

                var id=$(this).attr('id');
                var item={item_id:-1,quantity:-1,total_price:-1,bonus:-1,discount:-1,covered:-1};
                item.item_id=id;
                var $this=$(this);
                var count=1;
                item.price=$(this).children('.price').html();
                //console.log($(this).find(':nth-child(5)').find(':nth-child(1)').val());
                item.quantity=$(this).children('.quantity').find(':nth-child(1)').val();
                item.bonus=$(this).children('.bonus').find(':nth-child(1)').val();
                item.discount=$(this).children('.discount').find(':nth-child(1)').val();
                item.covered=$(this).children('.covered').find(':nth-child(1)').val();
                item.total_price=$(this).children('.total').html();
                $(this).find('input.obj').val(item);
                //console.log($(this).find('input.obj').val());
                item_array.push(item);
            });
            var items=JSON.stringify(item_array);
            $('#formID').append("<textarea id='items' name='items' style='display:none'>"+items+"</textarea> ");
            //console.log($('#formID #items'));
            return true;
        }

    </script>
@endsection