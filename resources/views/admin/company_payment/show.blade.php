@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            <div class="card-body card-block">
                <div class="row">
                    <div class="col-md-6">
                        <img  class="img img-responsive" src="{{$payment->photo?$payment->photo->getImage():'https://placehold.co/100x100'}}" >
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-2 font-weight-bold">Company : </div>
                    <div class="col-md-3">{{$payment->owner->name}}</div>
                </div>
                <div class="row">
                    <div class="col-md-2 font-weight-bold">Amount : </div>
                    <div class="col-md-3">{{number_format($payment->amount,0,'.',',')}} /-</div>
                </div>
                <div class="row">
                    <div class="col-md-2 font-weight-bold">Date : </div>
                    <div class="col-md-3">{{Carbon::parse($payment->date)->format('d-m-Y')}}</div>
                </div>
                <div class="row">
                    <div class="col-md-2 font-weight-bold">Account : </div>
                    <div class="col-md-3">{{$payment->account->name}}</div>
                </div>
                <div class="row">
                    <div class="col-md-2 font-weight-bold">Pay Later : </div>
                    <div class="col-md-3">{{$payment->pay_later=='1'?'Yes':'No'}}</div>
                </div>
                
                <div class="row">
                    <div class="col-md-2 font-weight-bold">Comments : </div>
                    <div class="col-md-3">{{$payment->comments?$payment->comments:'NA'}}</div>
                </div>
            </div>
            <div class="card-footer">
                <a href="/admin/company_payment/{{$payment->id}}/edit" class="btn btn-dark btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Edit
                </a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <script>
        var custForm=`   <div id="cust">
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="name">Name:</label>
                        </div>
                        <div class="col-3 col-md-3">
                            <input class="form-control" required placeholder="Company Name" name="name" type="text" id="name">

                        </div>
                    </div>
                </div>`;
        $(document).ready(function () {
            $('#owner_id').on('change',function () {
                if($('#owner_id option:selected').html()=='New'){
                    $('.card-body').append(custForm);
                }
                else{
                    $('#cust').remove();
                }
            });
        });
    </script>
    @include('includes.errors')
@endsection