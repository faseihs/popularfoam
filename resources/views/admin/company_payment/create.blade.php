@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::open(['method'=>'POST','action'=>'AdminCompanyPaymentController@store','class'=>'form-horizontal','enctype' => 'multipart/form-data','id'=>'FORM']) !!}
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('owner_id','Company:')!!}
                    </div>
                    <div class="col-md-3 col-sm-12">
                        {!! Form::select('owner_id',$companies,null,['class'=>'form-control','placeholder'=>'New','id'=>'owner_id'])!!}
                    </div>

                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('amount','Amount:')!!}
                    </div>
                    <div class="col-md-3 col-sm-12">
                        {!! Form::text('amount',null,['class'=>'form-control','placeholder'=>'Enter Amount','required'])!!}
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('date','Date:')!!}
                    </div>
                    <div class="col-md-3 col-sm-12">
                        {!! Form::date('date',null,['class'=>'form-control','required'])!!}
                    </div>
                </div>

                
                <div class="row form-group">
                    <div class="col-md-3">
                        {!! Form::label('pay_later','Paid Later :')!!}
                    </div>
                    <div class="col-md-3 col-sm-12">
                        {!! Form::select('pay_later',[0=>"No",1=>"Yes"],null,['class'=>'form-control','required'])!!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 ">
                        {!! Form::label('account_id','Account :')!!}
                    </div>
                    <div class="col-md-3 col-sm-12">
                        {!! Form::select('account_id',$accounts,null,['class'=>'form-control','required','placeholder'=>'Select Account'])!!}
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="col-md-3">
                        {!! Form::label('comments','Comments :')!!}
                    </div>
                    <div class="col-md-3 col-sm-12">
                        {!! Form::textarea('comments',null,['class'=>'form-control','placeholder'=>'Any Comments (Optional)','rows'=>3])!!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        {!! Form::label('photo','Photo :')!!}
                    </div>
                    <div class="col-md-3 col-sm-12">
                        {!! Form::file('photo',['class'=>'form-control'])!!}
                    </div>
                </div>

                <div id="cust">
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="name">Name:</label>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <input class="form-control" required placeholder="Company Name" name="name" type="text" id="name">

                        </div>
                    </div>
                </div>


            </div>
            <div class="card-footer">

                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <script>
        var custForm=`   <div id="cust">
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="name">Name:</label>
                        </div>
                        <div class="col-3 col-md-3">
                            <input class="form-control" required placeholder="Company Name" name="name" type="text" id="name">

                        </div>
                    </div>
                </div>`;
        $(document).ready(function () {
            $('#owner_id').on('change',function () {
                if($('#owner_id option:selected').html()=='New'){
                    $('.card-body').append(custForm);
                }
                else{
                    $('#cust').remove();
                }
            });
        });
    </script>
    @include('includes.errors')
@endsection