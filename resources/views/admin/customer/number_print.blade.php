<head>    <link href="{{ asset('vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">
    <title> Customer Numbers</title>
</head>
<div class="col-12">


    <div class="row">
        <div class="col-12 text-center">
            <strong style="font-size:20px;padding-left: 20px;padding-right: 10px;">Customer Numbers</strong>
            <p style="font-size:6px;">Powered by SAB-Solutions</p>
        </div>
    </div>
</div>
<div class="col-12">


    <div class="row">
        <div class="col-12 text-center">
            <strong style="font-size:12px;padding-left: 20px;padding-right: 10px;">Permanent Customers</strong>

        </div>
    </div>
</div>
<div  id="itemDivID" >
    <table id="itemTableID" class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Number</th>
        </tr>
        </thead>
        <tbody>
            @foreach($customers as $customer)
                <tr>
                    <td>{{$customer->name}}</td>
                    <td>{{$customer->number}}</td>
                </tr>


                @endforeach
        </tbody>
    </table>
</div>


<!-- Walking Customers Table and Heading -->

<div class="col-12">


    <div class="row">
        <div class="col-12 text-center">
            <strong style="font-size:12px;padding-left: 20px;padding-right: 10px;">Walking Customers</strong>

        </div>
    </div>
</div>
<div  id="itemDivID" >
    <table id="itemTableID" class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Number</th>
        </tr>
        </thead>
        <tbody>
        @foreach($walking as $customer)
            <tr>
                <td>{{$customer->name}}</td>
                <td>{{$customer->number}}</td>
            </tr>


        @endforeach
        </tbody>
    </table>
</div>



<style>
    body { border:1px solid black }
    .table{
        font-size:12px;
    }
</style>