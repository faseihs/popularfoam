@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::open(['method'=>'POST','action'=>'AdminCustomerController@store','class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('name','Name:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Name'])!!}

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('number','Number:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('number',null,['class'=>'form-control','placeholder'=>'Enter Number'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('address','Address:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('address',null,['class'=>'form-control','placeholder'=>'Enter Address (Optional)'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('type','Customer Type:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('type',['temp'=>'Temporary','perm'=>'Permanent'],null,['class'=>'form-control','placeholder'=>'Select Type'])!!}

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('balance_type','Balance Type:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('balance_type',[0=>'Customer has to Pay',1=>'Customer has to receive'],null,['class'=>'form-control'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('balance_amount','Balance Amount:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::number('balance_amount',null,['class'=>'form-control','placeholder'=>'Enter Amount'])!!}
                    </div>
                </div>

            </div>
            <div class="card-footer">

                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('includes.errors')
@endsection