@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">Customers</h3>
                </div>
                <div class="col-md-6">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search customer...">
                </div>
                <div class="table-data__tool-right col-md-4">
                    <a id="add" href="/admin/customer/create" class="au-btn au-btn-icon au-btn--green au-btn--small float-right">
                        <i class="zmdi zmdi-plus"></i>add customer</a>
                </div>
            </div>
        </div>

    </div>

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Permanent</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Walking</a>
            </li>
            
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    @if(count($customers)==0)
                    <h1 class="text-md-center">No customers Found</h1>
                @endif
                @if(count($customers)>0)
                    <div class="table-responsive table-responsive-data2">
                        <table id="tableID" class="table table-data2">
                            <thead>
                            <tr>
                                <th>.</th>
                                <th>name</th>
                                <th>Type</th>
                                <th>Number</th>
                                <th>Address</th>
                                <th>Op.Balance Type</th>
                                <th>Op.Balance Amount</th>
                                <th>created</th>
                                <th>updated</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($customers as $customer)
                                <tr class="tr-shadow">
                                    <td>.</td>
                                    <td>{{$customer->name}}</td>
                                    <td>{{$customer->type=='temp'?'Temporary':'Permanent'}}</td>
                                    <td>{{$customer->number}}</td>
                                    <td><a data-toggle="tooltip" title="{{$customer->address}}" href="#">{{$customer->address?substr($customer->address,0,10):'NA'}}</a></td>
                                    <td>{{$customer->balance_type==0?'Customer to Pay':'Customer to Receive'}}</td>
                                    <td>{{$customer->balance_amount}}</td>
                                    <td>{{Carbon::parse($customer->created_at)->format('d-m-Y')}}</td>
                                    <td>
                                        {{Carbon::parse($customer->updated_at)->format('d-m-Y')}}
                                    </td>
                                    <td>
                                        <div class="table-data-feature">
                                            <form id="del{{$customer->id}}" action="/admin/customer/{{$customer->id}}" method="POST">
                                                <input name="_method" type="hidden" value="DELETE">
                                                <input name="_token" type="hidden" value="{{csrf_token()}}">
                                                <a href="#" onclick="clicked({{$customer->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </a>
                                            </form>
                                            <a href="/admin/customer/{{$customer->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                <i class="zmdi zmdi-edit"></i>
                                                </button>
            
                                        </div>
                                    </td>
                                </tr>
                                <tr class="spacer"></tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @endif
                
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                    <div class="table-responsive table-responsive-data2">
                        <table  class="table table-data2">
                            <thead class="thead-dark">
                                <tr>
                                    <td>Name</td>
                                    <td>Number</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($walking as $item)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>0{{$item->number}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
            </div>
          </div>
        </div>
        </div>
        <script>
            function clicked(id){
                if(confirm("Are You Sure ?")){
                    document.getElementById('del'+id).submit();
                }
                else{
                }
            }
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#add').click();
                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <!-- END DATA TABLE-->
        @include('includes.search');
@endsection