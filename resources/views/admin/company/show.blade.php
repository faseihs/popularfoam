@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="default-tab">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home"
                                                     aria-selected="true">Company</a>
                     <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-item" role="tab" aria-controls="nav-home"
                                                     aria-selected="true">Items</a>
                     <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-pi" role="tab" aria-controls="nav-home"
                                                     aria-selected="true">Purchase Invoice</a>
                     <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-po" role="tab" aria-controls="nav-home"
													 aria-selected="true">Purchase Order</a>

                </div>
            </nav>
            <div class="tab-content pl-3 pt-2" id="nav-tabContent">
				<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
					<div class="row">
                        <div class="col-md-6 text-center">
                             <p>ID : <strong>{{$company->id}}</strong></p>
                        </div>
                        <div class="col-md-6 text-center">
                             <p>Name : <strong>{{$company->name}}</strong></p>
                        </div>

                          

                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <p>Party Name : <strong>{{$company->party_name}}</strong></p>
                        </div>
                        <div class="col-md-6 text-center">
                            <p>Party ID : <strong>{{$company->party_id}}</strong></p>
                        </div>
                    </div>


                    <hr/>
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <p>Created  : <strong>{{Carbon::parse($company->created_at)->format('d-m-Y')}}</strong></p>  
                        </div>
                           <div class="col-md-6 text-center">
                            <p>Updated  : <strong>{{Carbon::parse($company->updated_at)->format('d-m-Y')}}</strong></p>  
                        </div> 
                    </div>  
                    <hr/>
                    <div class="row">
                        <div class="col-md-6 text-center">
                             <p>Opening Balance Type: <strong>{{$company->balance_type==0?'Company Pays':'Company Receeives'}}</strong></p>
                        </div>
                        <div class="col-md-6 text-center">
                             <p>Opening Balance Amount : <strong>{{$company->balance_amount}}</strong></p>
                        </div>
                          
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <p>Total Purchase Invoices  : <strong>{{$pi_count}}</strong></p>  
                        </div>
                           <div class="col-md-6 text-center">
                            <p>Total Purchase Invoices Cost (Rs)  : <strong>{{$pi_bill}}/-</strong></p>  
                        </div>  
                    </div>

                    <hr/>
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <p>Total Purchase Orders  : <strong>{{$po_count}}</strong></p>  
                        </div>
                           <div class="col-md-6 text-center">
                            <p>Total Purchase Orders Cost (Rs)  : <strong>{{$po_bill}}/-</strong></p>  
                        </div>  
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-3 text-center">
                            <p>Total Items  : <strong>{{$item_count}}</strong></p>  
                        </div>
                           <div class="col-md-4 text-center">
                            <p>Items : Total Purchase Cost (Rs)  : <strong>{{$item_pcost}}/-</strong></p>  
                        </div> 

                        <div class="col-md-4 text-center">
                            <p>Items : Total Selling Cost (Rs)  : <strong>{{$item_scost}}/-</strong></p>  
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <p>Total Paid by Company (Rs) : <strong>{{$company->cashRegister->paid}}</strong></p>  
                        </div>
                           <div class="col-md-4 text-center">
                            <p>Company has to Pay (Rs) : <strong>{{$company->cashRegister->to_pay}}/-</strong></p>  
                        </div> 

                        <div class="col-md-4 text-center">
                            <p>Total Receieved  by Company (Rs) : <strong>{{$company->cashRegister->received}}/-</strong></p>  
                        </div>
                    </div>

                </div>

                <div class="tab-pane fade" id="nav-item" role="tabpanel" aria-labelledby="nav-home-tab">
					<div class="col-12">
                         <div class="table-responsive table--no-card m-b-30">
                            <table class="table table-borderless table-striped table-earning">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Item</th>
                                        <th>Covered</th>
                                        <th>Purchase Price</th>
                                        <th>Sale Price</th>
                                        <th>Current Qt</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     @if(count($items)==0)
                                    <tr>
                                        <td colspan="6">
                                        <p class="text-center">No Items</p>
                                        </td>
                                    </tr>
                                    @endif
                                    @foreach($items as $i)
                                        <tr>
                                            <td>{{$i->id}}</td>
                                            <td><a href="/admin/item/{{$i->id}}" >{{$i->name}}</a></td>
                                            <td>{{$i->covered==0?'No':'Yes'}}</td>
                                            <td>{{$i->purchase_price}}</td>
                                            <td>{{$i->sale_price}}</td>
                                            <td>{{$i->current_quantity}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>					
                </div>
                <div class="tab-pane fade" id="nav-pi" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="col-12">
                         <div class="table-responsive table--no-card m-b-30">
                            <table class="table table-borderless table-striped table-earning">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Date</th>
                                        <th>Total Price</th>
                                        <th>Item Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($pi)==0)
                                    <tr>
                                        <td colspan="4">
                                        <p class="text-center">No Purchase Invoices</p>
                                        </td>
                                    </tr>
                                    @endif
                                    @foreach($pi as $i)
                                        <tr>
                                            <td>{{$i->id}}</td>
                                            <td><a href="/admin/purchase_invoice/{{$i->id}}" >{{$i->date}}</a></td>
                                            <td>{{$i->total_price}}</td>
                                            <td>{{$i->items->count()}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                         </div>
                    </div>
                </div>  
                <div class="tab-pane fade" id="nav-po" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="col-12">
                         <div class="table-responsive table--no-card m-b-30">
                            <table class="table table-borderless table-striped table-earning">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Date</th>
                                        <th>Total Price</th>
                                        <th>Item Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($po)==0)
                                    <tr>
                                        <td colspan="4">
                                        <p class="text-center">No Purchase Orders</p>
                                        </td>
                                    </tr>
                                    @endif
                                    @foreach($po as $i)
                                        <tr>
                                            <td>{{$i->id}}</td>
                                            <td><a href="/admin/purchase_order/{{$i->id}}" >{{$i->date}}</a></td>
                                            <td>{{$i->total_price}}</td>
                                            <td>{{$i->items->count()}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                         </div>
                    </div>
                </div>   
            </div>
        </div>
    @include('includes.errors')
    </div>

    <style>
        .au-breadcrumb2 {
             padding-top: 10px;
            padding-bottom: 10px;
           
        }
    </style>
@endsection