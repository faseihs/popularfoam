@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::open(['method'=>'POST','action'=>'AdminCompanyController@store','class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
            <div class="card-body card-block">
                    <div class="row form-group">
                        <div class="col col-md-3">
                            {!! Form::label('name','Name:')!!}
                        </div>
                        <div class="col-3 col-md-3">
                            {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Name'])!!}

                        </div>
                    </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('party_id','Party ID:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('party_id',null,['class'=>'form-control','placeholder'=>'Enter Party ID'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('party_name','Party Name :')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('party_name',null,['class'=>'form-control','placeholder'=>'Enter Party Name'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('balance_type','Balance Type:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('balance_type',[0=>'Company has to Pay',1=>'Company has Paid',2=>'Company to Receive',3=>'Company has Received'],null,['class'=>'form-control'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('balance_amount','Balance Amount:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::number('balance_amount',null,['class'=>'form-control','placeholder'=>'Enter Amount'])!!}
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('balance_date','Balance Date:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::date('balance_date',null,['class'=>'form-control'])!!}
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('balance_limit','Balance Limit:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::number('balance_limit',null,['class'=>'form-control','placeholder'=>'Enter Amount'])!!}
                    </div>
                </div>
            </div>
            <div class="card-footer">

                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@include('includes.errors')
@endsection