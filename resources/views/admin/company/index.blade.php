@extends('layouts.myapp')


@section("styles")

<link rel="stylesheet" href="/css/datatables.min.css">
{{-- <link rel="stylesheet" href="/css/dataTables.bootstrap.min.css"> --}}


<style>
    tr{
        box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;
    }
    .dataTables_processing.card{
        visibility: hidden !important;
    }

  
</style>

@endsection

@section('content')
    <!-- DATA TABLE-->

            <div class="row">
                @include('includes.flash')
                <div class="col-md-12">
                        <div class="row">
                                <div class="col-md-2">
                                    <h3 class="title-5 m-b-35">Companies</h3>
                                </div>
                               
                                <div class="table-data__tool-right col-md-4">
                                    <a id="add" href="/admin/company/create" class="au-btn au-btn-icon au-btn--green au-btn--small float-right">
                                        <i class="zmdi zmdi-plus"></i>add company</a>
                                </div>
                            </div>
                    </div>

                    </div>
                    @if(count($companies)==0)
                        <h1 class="text-md-center">No Companies Found</h1>
                    @endif
                    @if(count($companies)>0)
                    <div class="table-responsive table-responsive-data2">
                        <table id="tableID" class="table table-data2">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>name</th>
                                <th>Op.Balance Type</th>
                                <th>Op.Balance Amount</th>
                                <th>created</th>
                                <th>updated</th>
                                <th></th>
                            </tr>
                            </thead>
                            {{-- <tbody>
                            @foreach($companies as $company)
                            <tr class="tr-shadow">

                                <td>{{$company->id}}</td>
                                <td class="desc"><a href="/admin/company/{{$company->id}}">{{$company->name}}</a></td>
                                <td>{{$company->balance_type==0?'Company to Pay':'Company to Receive'}}</td>
                                <td>{{$company->balance_amount}}</td>
                                <td>{{Carbon::parse($company->created_at)->format('d-m-Y')}}</td>
                                <td>
                                    {{Carbon::parse($company->updated_at)->format('d-m-Y')}}
                                </td>
                                <td>
                                    <div class="table-data-feature">
                                        <form id="del{{$company->id}}" action="/admin/company/{{$company->id}}" method="POST">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <input name="_token" type="hidden" value="{{csrf_token()}}">
                                            <a href="#" onclick="clicked({{$company->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                        </form>
                                        <a href="/admin/company/{{$company->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>

                                    </div>
                                </td>
                            </tr>
                            <tr class="spacer"></tr>
                            @endforeach 
                            </tbody>
                            --}}
                        </table>
                    </div>
                    @endif
                </div>
            </div>
<script>

    
    function clicked(id){
        if(confirm("Are You Sure ?")){
            document.getElementById('del'+id).submit();
        }
        else{
        }
    }
    Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
        if (e.preventDefault) {
            e.preventDefault();
            $('#add').click();
        } else {
            // internet explorer
            e.returnValue = false;
        }

    });
</script>
    <!-- END DATA TABLE-->
    @include('includes.search');
@endsection


@section("scripts")
<script src="/js/datatables.min.js"></script>

<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var $datatable = $('#tableID');
        var table = $datatable.DataTable({
            scrollResize: true,
            scrollY: window.innerHeight-400,
            scrollCollapse: true,
            pageResize: true,
            scrollX:true,
            "autoWidth": false,
            "columns": [
                {"data": "id"},
                {"data": "name"},
                {"data": "balance_type",render:function(data){
                    return data==0?'Company to Pay':'Company to Receive';
                }},
                {"data": "balance_amount"},
                {"data": "updated_at"},
                {"data": "created_at"},
                {
                    "data": "id",
                    "className": "center",
                    "render": function(id){
                        let s =``;
                        s+=`<div class="table-data-feature">
                                <form id="del${id}" action="/admin/company/${id}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                    <a href="#" onclick="clicked(${id})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                    <i class="zmdi zmdi-delete"></i>
                                </a>
                                </form>
                                <a href="/admin/company/${id}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>

                            </div>`;
                        return s;
                    }
                }
            ],
            'processing': true,
            "stateSave": false,
            'serverSide': true,
            'ajax': {
                'url': '{{ route("company-data") }}',
                'type': 'POST'
            },
            'order': [[ 1, 'asc' ]],
            'columnDefs': [
                { "orderable": false, "targets": [-1] },
                { "searchable": false, "targets": [-1] }
            ],
            "dom":"Bfltip"
        });
        $( table.table().container() ).removeClass( 'form-inline' );
    });
</script>

@endsection