@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-12">
            <div class="row">

                    <div class="col-md-3">
                        <h3 class="title-5 m-b-35">Salary Payments</h3>
                    </div>
                    {!! Form::open(['method'=>'POST','action'=>'AdminSalaryPaymentController@query','class'=>'col-md-7','id'=>'formRow']) !!}

                        <div class="row">
                            {!! Form::select('today',[1=>'This Month',2=>'Select Month'],isset($on_date)?2:1
                               ,['class'=>'form-control-sm col-md-4','id'=>'selectID'])!!}
                            @if(isset($on_date))
                                <span style="padding-right: 5px;padding-left: 5px;">{{Carbon::parse($on_date)->format('F-Y')}}</span>
                                <input id='date'  type='date' class='form-control-sm col-md-2' name='date' required />
                            @endif
                            <button class="btn btn-dark btn-sm col-md-1" type="submit">Go</button>
                        </div>

                    {!! Form::close() !!}

                    <div class="col-md-2 float-right">
                        <a id="add" href="/admin/salary_payment/create" class="au-btn au-btn-icon au-btn--green au-btn--small float-right">
                            <i class="zmdi zmdi-plus"></i>add</a>
                    </div>

            </div>

        </div>
        @if(count($salary_payments)==0)
            <h1 class="col-md-12 text-center" >No salary_payments Found</h1>
        @endif
        @if(count($salary_payments)>0)
            <div class="table-responsive table-responsive-data2">
                <table id="tableID" class="table table-data2">
                    <thead>
                    <tr>

                        <th>id</th>
                        <th>worker</th>
                        <th>Amount</th>
                        <th>type</th>
                        <th>month</th>
                        <th>date</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($salary_payments as $salary_payment)
                        <tr class="tr-shadow">

                            <td>{{$salary_payment->id}}</td>
                            <td class="desc"><a href="/admin/worker/{{$salary_payment->worker?$salary_payment->worker->id:'0'}}">{{$salary_payment->worker?$salary_payment->worker->name:'Deleted / NA'}}</a></td>
                            <td>{{number_format($salary_payment->amount,0,'.',',')}}</td>
                            <td>{{$salary_payment->type==0? 'Loan' :'Direct'}}</td>
                            <td>{{Carbon::parse($salary_payment->month)->format('F Y')}}</td>
                            <td>
                                {{Carbon::parse($salary_payment->date)->format('d-m-Y')}}
                            </td>
                            <td>
                                <div class="table-data-feature">
                                    <form id="del{{$salary_payment->id}}" action="/admin/salary_payment/{{$salary_payment->id}}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        <a href="#" onclick="clicked({{$salary_payment->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                    </form>
                                    <a href="/admin/salary_payment/{{$salary_payment->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>

                                    </a>
                                </div>
                            </td>
                        </tr>
                        <tr class="spacer"></tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>

    <script>
        Date.prototype.toDateInputValue = (function() {
            var local = new Date(this);
            local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
            return local.toJSON().slice(0,10);
        });
        var d= "<input id='date' type='month' class='form-control-sm' name='month' required />";
        function clicked(id){
            if(confirm("Are You Sure ?")){
                document.getElementById('del'+id).submit();
            }
            else{
            }
        }
        Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
            if (e.preventDefault) {
                e.preventDefault();
                $('#add').click();


            } else {
                // internet explorer
                e.returnValue = false;
            }

        });

        $(document).ready(function () {
            $('#selectID').on('change',function () {
                if ($(this).val()==1){
                    $('#date').remove();
                }
                else{
                    $('#formRow').append(d);
                }
            });
            $('[data-toggle="tooltip"]').tooltip();
            
        });

        
        function sortTable() {
            var table, rows, switching, i, x, y, shouldSwitch;
            table = document.getElementById("tableID");
            console.log(table);
            switching = true;
            /* Make a loop that will continue until
            no switching has been done: */
            while (switching) {
                // Start by saying: no switching is done:
                switching = false;
                rows = table.getElementsByTagName("TR");
                /* Loop through all table rows (except the
                first, which contains table headers): */
                for (i = 1; i < (rows.length - 1); i++) {
                    // Start by saying there should be no switching:
                    shouldSwitch = false;
                    /* Get the two elements you want to compare,
                    one from current row and one from the next: */
                    x = rows[i].getElementsByTagName("TD")[1];
                    y = rows[i + 1].getElementsByTagName("TD")[1];
                    // Check if the two rows should switch place:
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
                if (shouldSwitch) {
                    /* If a switch has been marked, make the switch
                    and mark that a switch has been done: */
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                }
            }
        }
        sortTable();


    </script>
    <style>
        .au-input--w300{
            width: 100% !important;
        }

    </style>
    <!-- END DATA TABLE-->
    @include('includes.search')
@endsection