@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::open(['method'=>'POST','action'=>'AdminSalaryPaymentController@store','class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('worker_id','Worker:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('worker_id',$workers,null,['class'=>'form-control','placeholder'=>'Select Worker'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('amount','Amount (Rs) :')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('amount',null,['class'=>'form-control'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('month','Month:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        <input type="month" name="month" />

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('date','Date:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::date('date',Carbon::now()->toDateString(),['class'=>'form-control'])!!}

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('type','Type:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('type',[0=>'Loan',1=>'Direct'],null,['class'=>'form-control','placeholder'=>'Select Type'])!!}

                    </div>
                </div>

            </div>



        </div>
        <div class="card-footer">

            {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
        </div>
        {!! Form::close() !!}
    </div>
    </div>
    @include('includes.errors')
@endsection