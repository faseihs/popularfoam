@extends('layouts.myapp')
@section("styles")
<link rel="stylesheet" href="/css/datatables.min.css">
@endsection
@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">Purchase Invoice</h3>
                </div>
                <div class="table-data__tool-right col-md-4">
                    <button id="add" type="button" class="btn btn-primary mb-1 float-right" data-toggle="modal" data-target="#scrollmodal">
                        Add Purchase Invoice
                    </button>
                </div>
            </div>

        </div>

        @if(count($purchase_invoices)==0)
            <h1 class="text-md-center">No Purchase Invoices Found</h1>
        @endif
        @if(count($purchase_invoices)>0)

            <div id="tableID" class="table-responsive table-responsive-data2">
                <table id="tableData" class="table table-data2">
                    <thead>
                    <tr>
                        <th>Invoice Date</th>
                        <th>Receive Date</th>
                        <th>Invoice ID</th>
                        <th>Photo</th>
                        <th>Company</th>
                        <th class="text-right">Total Bill</th>
                        <th>Created Date</th>
                       
                        <th style="display:inline-flex"></th>
                    </tr>
                    </thead>
                  
                    <!-- @foreach($purchase_invoices as $purchase_invoice)
                        <tr class="shadow">
                            <td>{{Carbon::parse($purchase_invoice->date)->format('d-m-Y')}}</td>
                            <td>{{$purchase_invoice->receive_date?Carbon::parse($purchase_invoice->receive_date)->format('d-m-Y'):'-'}}</td>
                            <td>{{$purchase_invoice->invoice_id?$purchase_invoice->invoice_id:'-'}}</td>
                            <td class="text-right img"><img height="50px"  width="50px" class="img-circle img-responsive" src="{{$purchase_invoice->pic? $purchase_invoice->pic: 'https://placehold.co/50x50'}}" alt=""></td>
                            <td>{{$purchase_invoice->company?$purchase_invoice->company->name:$purchase_invoice->name." | ".$purchase_invoice->number}}</td>
                            <td class="text-right">{{number_format($purchase_invoice->total_price,0,'.',',')}} /-</td>
                            <td>{{Carbon::parse($purchase_invoice->created_at)->format('d-m-Y')}}</td>
                            <td>                                <a class="btn btn-primary btn-sm" href="/admin/purchase_invoice/{{$purchase_invoice->id}}">View</a>
                            </td>
                            <td>
                                <div class="table-data-feature">
                                    <form id="del{{$purchase_invoice->id}}" action="/admin/purchase_invoice/{{$purchase_invoice->id}}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        <a href="#" onclick="clicked({{$purchase_invoice->id}})"  class="purchase_invoice" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                    </form>
                                    <a href="/admin/purchase_invoice/{{$purchase_invoice->id}}/edit" class="purchase_invoice" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>

                                </div>

                            </td>
                        </tr>
                    @endforeach -->
                   
                </table>

            </div>
        @endif
        <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        Select Company
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['method'=>'POST','action'=>'AdminPurchaseInvoiceController@createC']) !!}
                        <div class="form-group">

                            {!! Form::select('company_id',$companies,null,['class'=>'form-control','placeholder'=>'Select Company'])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Submit',['class'=>'btn btn-default'])!!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <div class="d-flex justify-content-around">
                            <a href="/admin/purchase_invoice/{id}" class="p2"></a>
                            <button type="button" class="btn btn-secondary p2" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function clicked(id){
                if(confirm("Are You Sure ?")){
                    document.getElementById('del'+id).submit();
                }
                else{
                }
            }
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#add').click();

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>

    </div>       <!-- END DATA TABLE-->
    <style>
        .img{
            padding: 22px 40px !important;
            padding-left: 10px !important;
        }
    </style>
    @include('includes.search')


    <script src="/js/datatables.min.js"></script>

    <script>
         $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var $datatable = $('#tableData');
        var table = $datatable.DataTable({
            scrollResize: true,
            scrollY: window.innerHeight-400,
            scrollCollapse: true,
            pageResize: true,
            scrollX:true,
            "autoWidth": false,
            "columns": [
                {"data": "date"},
                {"data": "receive_date"},
                {"data": "invoice_id"},
                {"data":"id",render:function(id,type,row){
                    let data =row.pic;
                    return `<img height="50px"  width="50px" class="img-circle img-responsive" src="${data?'images/'+data.path: 'https://placehold.co/50x50'}" alt="">`
                }},
               {"data":"company",name:"company.name"},
               
                {"data": "total_price"},
                {"data": "created_at"},
                {
                    "data": "id",
                    "className": "center",
                    "render": function(id){
                        let s =``;
                        s+=`  <a class="purchase_invoice" href="/admin/purchase_invoice/${id}"><i class="zmdi zmdi-eye"></i></a>
                        <a href="/admin/purchase_invoice/${id}/edit" class="purchase_invoice" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                            </a> 
                        <form style="display:inline" id="del${id}" action="/admin/purchase_invoice/${id}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        <a href="#" onclick="clicked('${id}')"  class="purchase_invoice" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                    </form>
                                   `;
                        return s;
                    }
                }
            ],
            'processing': true,
            "stateSave": false,
            'serverSide': true,
            'ajax': {
                'url': '{{ route("purchase-invoice.data") }}',
                'type': 'POST'
            },
            'order': [[ 0, 'asc' ]],
            'columnDefs': [
                { "orderable": false, "targets": [-1] },
                { "searchable": false, "targets": [-1,1,2,6] }
            ],
            "dom":"Bfltip"
        });
        $( table.table().container() ).removeClass( 'form-inline' );
    });
    </script>
@endsection