@extends('layouts.simple')
@section('content')
    <div id="hidden" style="display:none"></div>
    <div class="col-12">
        <div class="card">
            {!! Form::open(['method'=>'POST','action'=>'AdminPurchaseInvoiceController@store','onsubmit'=>'return submitFunc()','id'=>'formID','enctype' => 'multipart/form-data']) !!}
            <div class="card-header">
                <input name="company_id" type="hidden" value="{{$company->id}}">
                <input id="totalPriceID" name="total_price" value="" type="hidden">

                <strong style="padding-right: 10px;">Purhcase Invoice</strong>
                <button id="search" type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#scrollmodal">
                    Add Item
                </button>


            </div>
            <div class="card-body card-block">
                <div class="d-flex justify-content-between">
                    <div id="companyID" class="p-2"><strong>Company: </strong>{{$company->name}}<span></span></div>
                    <div class="p2">
                        {!! Form::label('invoice_id','Company Invoice :',['class'=>'font-weight-bold'])!!}
                        {!! Form::text('invoice_id',null,['class'=>'form-control-sm mousetrap font-weight-bold','required','placeholder'=>'Invoice/Chalan #'])!!}
                        {!! Form::label('date','Invoice Date :',['class'=>'font-weight-bold'])!!}
                        {!! Form::date('date',Carbon::now()->format('Y-m-d'),['class'=>'form-control-sm mousetrap','required'])!!}
                        {!! Form::label('receive_date','Receive Date :',['class'=>'font-weight-bold'])!!}
                        {!! Form::date('receive_date',null,['class'=>'form-control-sm mousetrap','required'])!!}
                    </div>
                </div>
                <div style="overflow-y: scroll" id="itemDivID" class="table-responsive table--no-card m-b-30">
                    <table id="itemTableID" class="table table-borderless  table-earning">
                        <thead>
                        <tr>

                            <th>Quality</th>
                            <th>Item</th>
                            <th class="text-right">Covered</th>
                            <th class="text-right">Quantity</th>
                            <th class="text-right">Bonus</th>
                            <th class="text-right">Price</th>
                            <th class="text-right">Discount</th>
                            <th class="text-right">Total</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>


                        </tbody>

                    </table>
                </div>
            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-between">
                    <div id="total" class="p2"><strong>Total : </strong><span></span></div>
                    {!! Form::file('photo',null,['class'=>'form-control p2'])!!}
                    <button type="button" class="btn btn-sm btn-dark" data-toggle="modal" data-target="#quantityModal">Total Quantity</button>
                    {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm p2','id'=>'submitID'])!!}

                </div>
            </div>

        </div>
        {!! Form::close() !!}
    </div>


    <div id="quantityModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Quantity</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                   <div class="row">
                       <div class="col-md-3">
                           Pieces :
                       </div>
                       <div id="pieces" class="col-md-5">

                       </div>
                   </div>
                    <div class="row">
                       <div class="col-md-3">
                           Metres :
                       </div>
                       <div id="metres" class="col-md-5"></div>
                       </div>
                    <div class="row">
                       <div class="col-md-3">
                           Kilograms :
                       </div>
                       <div id="kilograms" class="col-md-5">

                       </div>
                    </div>
                    <div class="row">
                       <div class="col-md-3">
                           Grams :
                       </div>
                       <div id="grams" class="col-md-5">

                       </div>
                    </div>
                       <div class="row">
                           <div class="col-md-3">
                               Dozen :
                           </div>
                           <div id="dozen" class="col-md-5">

                           </div>
                       </div>

                    <div class="row">
                        <div class="col-md-3">
                            Total :
                        </div>
                        <div id="tq" class="col-md-5">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- modal scroll -->
    <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <input onkeyup="getData(this.value)" placeholder="Type To Search" autofocus name="searchItem" id="searchIn" type="text" class="form-control mousetrap" />
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="tableID" class="table-responsive table--no-card m-b-30">
                        <table id="table_ID" class="table table-borderless  table-earning">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Item</th>
                                <th>Price</th>
                                <th>Quality</th>
                                <th>Size</th>
                                <th>Covered</th>
                                <th>UDiscount</th>
                                <th>CDiscount</th>
                                <th>Company</th>
                            </tr>
                            </thead>

                            <tbody id="itemTbody">
                            {{--@php($i=1)
                            @foreach($items as $item)
                                <tr  tabindex="{{$i}}">
                                    <!--<td><button class="btn btn-sm btn-primary">Select</button></td>-->
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->purchase_price}}</td>
                                    <td>{{$item->quality->name}}</td>
                                    <td>{{$item->size_t}}</td>
                                    <td>{{$item->covered==0?'No':'Yes'}}</td>

                                    <td>
                                        {{$item->UDiscount()}}
                                    </td>
                                    <td>
                                        {{$item->CDiscount()}}
                                    </td>
                                    <td style="display: none">{{$item->package_unit}}</td>
                                </tr>
                                @php($i++)
                            @endforeach--}}

                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

                </div>
            </div>
        </div>
    </div>

    @include('includes.errors')
    <style>
        .modal-lg{
            max-width:100% !important;
        }
        .card-body{
            min-height: 83vh;
            max-height: 83vh !important;
        }
        tr:focus{
            background: #b2b2b2;
        }
        tr:focus{
            background: #b2b2b2;
        }

        .selected{
            background: #b2b2b2 !important;
            color: white !important;
        }
        .selected td{
            color:white !important;
        }
        .form-control-sm{
            border:1px solid #ced4da !important;
        }
        .sm{
            width: 50px !important;
        }
        #itemDivID{
            max-height: 70vh;
        }
        #table_ID td,th{
            padding: 12px 10px !important;
        }
        #itemTableID td,th{
            padding: 12px 10px !important;
            font-size: 12px;
        }

        .card-header{
            padding: 0;
            font-size: 16px !important;
        }
        .card-footer{
            padding: 0;
            padding-top: 1px;
            padding-left: 5px;
            padding-right: 5px;
            font-size: 12px !important;
        }
        .form-control-sm{
            font-size: 10px !important;
        }
        select.form-control-sm{
            height: 1.5rem !important;
        }
        .p2{
            font-size: 12px !important;
        }
        .btn-sm{
            font-size: .5rem !important;
        }
    </style>

    {{--<script src="{{asset('js/po_create.js')}}"></script>--}}
    <script>

        var companyId={!! $company->id !!}
        function setItems(items){
            finalStr=``;
            for (let i =0;i<items.length;i++){
                let item = items[i];
                finalStr+=`<tr  tabindex="${item.id}">
                                    <!--<td><button class="btn btn-sm btn-primary">Select</button></td>-->
                                    <td>${item.id}</td>
                                    <td>${item.name}</td>
                                    <td>${item.purchase_price}</td>
                                    <td>${item.quality}</td>
                                    <td>${item.size_t}</td>
                                    <td>${item.covered==1?'Yes':'-'}</td>

                                    <td>
                                        ${item.covered_discount}
                    </td>
                    <td>
${item.uncovered_discount}
                    </td>
                    <td style="display:none">${item.package_unit}</td>
                                    <td style="display:none">${item.current_quantity}</td>
                                    <td>${item.company_name}</td>
                                </tr>`
                finalStr+=`</tr>`;

            }
            $('#itemTbody').html(finalStr);
            bindEvents();
        };
        function showLoading(){
            $('#itemTbody').html(` <tr id="loading"><td class="text-center" colspan="8"><h3>Loading....</h3></td></tr>`);
        }

        function removeLoading(){
            $('#loading').remove();
        }

        function clearItemTableData(){
            $('#itemTbody').html("");
        }


        function getData(search = ``){
            clearItemTableData();
            if($("#loading").length <1)
                showLoading();
            let url = `/api/get-items-company/0`;
            if(search.length > 0)
                url+=`?search=${search}`;
            $.ajax({
            url,
            method:"GET"
            }).done(function(r) {
                removeLoading();
                setItems(r.data);

            });
        }


        function bindEvents(){
            $('#scrollmodal').on('shown.bs.modal', function () {
                $('#searchIn').focus();
            });
            $('#scrollmodal').on('hidden.bs.modal', function () {
                 // $('#tableID tr').each(function () {
                //     $(this).removeClass('selected');
                //     $('#searchIn').val('');
                //     $('#table_ID tr').each(function () {
                //         $(this).css('display','');
                //     });
                // });
                $('#search').blur();
                $('#searchIn').val('');
                clearItemTableData();
            });
            $('#scrollmodal,.selected').on('keydown',function (e) {
                let key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
                if(key==40) {
                    //$('#scrollmodal').focus();
                    //e.preventDefault();
                    $('#searchIn').blur();
                    //console.log('Key Down Pressed');
                    if($('.selected:not(:hidden)').length==0){

                        $('#tableID tbody tr').not(':hidden').first().addClass('selected').focus();
                    }
                    else{
                        let current=$('.selected:not(:hidden)');
                        if(current.nextAll().not(':hidden').length){
                            current.nextAll().not(':hidden').first().addClass('selected').focus();
                            current.removeClass('selected');
                            //current.next().focus();
                        }
                        else{
                            current.removeClass('selected');
                            $('#searchIn').focus();
                        }
                    }

                }

                //Up Key
                if(key==38){
                    //e.preventDefault();
                    //$('#scrollmodal').focus();
                    console.log('Dabb gaya');
                    $('#searchIn').blur();
                    if($('.selected:not(:hidden)').length==0){

                        $('#tableID tbody tr').not(':hidden').last().addClass('selected').focus();
                        console.log('yahan 1');
                    }
                    else{
                        let current=$('.selected:not(:hidden)');
                        if(current.prevAll().not(':hidden').length){
                            current.prevAll().not(':hidden').first().addClass('selected').focus();
                            current.removeClass('selected');
                        }
                        else{
                            current.removeClass('selected');
                            $('#searchIn').focus();
                            console.log('yahan 3');
                        }
                    }

                }

                if(key==13){
                    e.preventDefault();
                    $('#scrollmodal').focus();
                    //console.log('Enter Pressed');
                    if($('.selected:not(:hidden)').length>0) {
                        let selected=$('.selected');
                        //$('.card-body').append(selected.html());
                        $('#scrollmodal').modal('toggle');
                        getItemFromTable(selected);
                        calcTotal();


                    }

                }
            });
            $('#company').on('change', function() {
                let comp=$('#company option:selected').html();
                if(comp!=='Company')
                    $('#companyID span').html(comp);
                else $('#companyID span').html('');
            });

            $('#vehicle').on('change', function() {
                let comp=$('#vehicle option:selected').html();
                if(comp!=='Vehicle' || $('#total-coverage').hasClass('alert-danger')) {
                    $('#vehicleID span').html(comp);
                    setVehicleSize();

                    $('#submitID').removeAttr('disabled');

                }
                else {$('#vehicleID span').html('');setVehicleSize();$('#submitID').attr('disabled','true');}
            });

            $('.discountIN').on('keyup',function () {

                calcTotal();
                console.log('Yahoo');
            });
            $('.quantityIN').on('keyup',function () {
                calcTotal();

            });

            $('.bonus input').on('change',function () {

            });


            $('#table_ID tbody tr').on('click',function () {
                $('#scrollmodal').modal('toggle');
                getItemFromTable($(this));
                calcTotal();

            });

            $('tr').on('focus',function(){
                console.log('FOvussed');
            });
        }

        $(document).ready(function () {
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    //$('#search').click();
                    $('#scrollmodal').modal('toggle');

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });








        });
        function search() {
            $('#tableID tr').each(function () {
                $(this).removeClass('selected');
            });
            var input, filter, table, tr, td, i;
            input = document.getElementById("searchIn");
            filter = input.value.toUpperCase();
            table = document.getElementById("table_ID");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";

                    } else {
                        tr[i].style.display = "none";

                    }
                }
            }
        }

        function getItemFromTable(object){
            var item={id:-1, name:"", price:-1,quality:"" ,company:"",covered:'',udiscount:-1,cdiscount:-1};
            var counter=1;
            object.children().each(function () {
                if(counter==1)
                    item.id=$(this).html();
                else if(counter==2)
                    item.name=$(this).html();
                else if(counter==3)
                    item.price=$(this).html();
                else if(counter==4)
                    item.quality=$(this).html();
                else if(counter==5)
                    item.size=$(this).html();
                else if(counter==6)
                    item.covered=$(this).html();
                else if(counter==7)
                    item.udiscount=$(this).html();
                else if(counter==8)
                    item.cdiscount=$(this).html();
                else if(counter==9)
                    item.package_unit=$(this).html();
                counter++;
            });

            //console.log(item);
            addItemToTable(item);
        }

        function addItemToTable(item){
            var selected;
            var selected_;
            var unselected;
            var unselected_;
            var dvalue;
            if(item.covered=='No'){
                selected=0;
                selected_='No';
                unselected=1;
                unselected_='Yes';
                dvalue=item.udiscount;
            }
            else{
                selected=1;
                selected_='Yes';
                unselected=0;
                unselected_='No';
                dvalue=item.cdiscount;
            }


            var row=`    <tr id='`+item.id+`'>
                                <td>`+item.quality+`</td>
                                <td>`+item.name+`</td>
                                <td class="text-right covered">
                                <select class="form-control-sm mousetrap">
                                <option selected value='`+selected+`'>`+selected_+`</option>
                                <option value='`+unselected+`'>`+unselected_+`</option>
                                </select>
                                 </td>
                                <td style="display:none" class="size">`+item.size+`</td>
                                <td class="cdiscount" style="display: none">`+item.cdiscount+`</td>
                                <td class="udiscount" style="display: none">`+item.udiscount+`</td>

                                <td class="text-right quantity"><input step="any" min="0" class="form-control-sm sm quantityIN mousetrap" type="number" value="1"/><span class="stock"></span></td>
                                <td class="text-right bonus"><input min="0" class="form-control-sm sm mousetrap" type="number" value="0"/></td>
                                 <td class="text-right price"><input step="any" min="0" class="form-control-sm sm priceIN mousetrap" type="number" value="`+item.price+`"/></td>
                                <td class="text-right discount"><input step="any" min="0" max="100" class="form-control-sm sm discountIN mousetrap" type="number" value=`+dvalue+`/> %</td>

                                <td class="text-right total">`+item.price+`</td>
                                <td style="display: none"><input class="obj" type="checkbox" name="checkBoxArray[]"  value=""></td>
                                <td style="display:none" class="package_unit">` + item.package_unit +`</td>
                                <td><button class="btn btn-danger btn-sm deleteBtn" type="button">Delete</button></td>



                       </tr>`;
            $('#itemTableID tbody').append(row);
            $('.quantityIN').bind('change', function() {
                calcTotal();
                console.log('Yahoo');
            });
            $('.discountIN').bind('change', function() {
                calcTotal();
                
            });

            $('.priceIN').bind('change', function() {
                calcTotal();
            });


            $('.deleteBtn').bind('click',function () {
                $(this).parent().parent().remove();
                calcTotal();
            });

            $('.covered select').on('change',function () {
                var val =$(this).val();
                var p=$(this).parent().parent();

                //console.log(parseInt(p.find('.cdiscount').html()));
                //console.log(parseInt(p.find('.udiscount').html()));
                if(val==1)
                    p.find('.discountIN').val(parseInt(p.find('.cdiscount').html()));
                else{
                    p.find('.discountIN').val(parseInt(p.find('.udiscount').html()));
                }
                calcTotal();
            });

            let url =`/api/get-item-stock/${item.id}`;
            $.ajax({
                method:"GET",
                url,
                success:function(response){
                    $(`tr#${item.id}`).find(".stock").append(`(${response.stock})`);
                }
            })




        }

        function calcTotal(){
            var sum=0;
            var piecesTotal=0;
            var kilogramsTotal=0;
            var metresTotal=0;
            var gramsTotal=0;
            var dozenTotal=0;
            $('#itemTableID tbody').children().each(function () {
                var unit =$(this).find('.package_unit').html();
                var qt=parseFloat($(this).find('.quantity input').val());

                if(unit==='Pcs'){
                    piecesTotal+=qt;
                }
                else if(unit==='Ms')
                    metresTotal+=qt;
                else if(unit==='Kgs')
                    kilogramsTotal+=qt;
                else if (unit==='Gms')
                    gramsTotal+=qt;
                else if(unit==='Doz')
                    dozenTotal+=qt;

                var individualSum=parseFloat($(this).find('.price input').val()) *parseFloat($(this).find('.quantity input').val());
                console.log(individualSum);
                var discount=parseFloat($(this).find('.discount input').val());
                console.log(discount);
                if(discount>0){
                    individualSum=individualSum*(1-(discount/100));
                }
                individualSum=Number((individualSum).toFixed(1));
                $(this).children('.total').html(individualSum);
                sum+=individualSum;
            });
            sum=Number((sum).toFixed(1));
            $('#total span').html(sum);
            $('#totalPriceID').val(sum);
            $('#pieces').html(piecesTotal);
            $('#kilograms').html(kilogramsTotal);
            $('#grams').html(gramsTotal);
            $('#metres').html(metresTotal);
            $('#dozen').html(dozenTotal);
            $('#tq').html(piecesTotal+kilogramsTotal+gramsTotal+metresTotal+dozenTotal);
        }

        function submitFunc(){
            if($('#itemTableID tbody tr').children().length<1){
                alert("No Items Selected");
                return false;
            }
            var item_array=Array();
            $('#itemTableID tbody tr').each(function () {

                var id=$(this).attr('id');
                var item={item_id:-1,quantity:-1,total_price:-1,bonus:-1,discount:-1,covered:-1};
                item.item_id=id;
                var $this=$(this);
                var count=1;
                //item.price=$(this).children('.price').html();
                item.price=$(this).children('.price').find(':nth-child(1)').val();
                //console.log($(this).find(':nth-child(5)').find(':nth-child(1)').val());
                item.quantity=$(this).children('.quantity').find(':nth-child(1)').val();
                item.bonus=$(this).children('.bonus').find(':nth-child(1)').val();
                item.discount=$(this).children('.discount').find(':nth-child(1)').val();
                item.covered=$(this).children('.covered').find(':nth-child(1)').val();
                item.total_price=$(this).children('.total').html();
                $(this).find('input.obj').val(item);
                //console.log($(this).find('input.obj').val());
                item_array.push(item);
            });
            var items=JSON.stringify(item_array);
            $('#formID').append("<textarea id='items' name='items' style='display:none'>"+items+"</textarea> ");
            //console.log($('#formID #items'));
            return true;
        }

    </script>
@endsection