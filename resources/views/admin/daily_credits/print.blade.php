<head>    <link href="{{ asset('vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">
    <title>Daily Credits #{{$transaction->id}}</title>
</head>
<div class="col-12">


    <div class="row">
        <div class="col-12 text-center">
            <strong style="font-size:20px;padding-left: 20px;padding-right: 10px;">Payment Receipt #{{$transaction->id}}</strong>
            <p style="font-size:6px;">Powered by SAB-Solutions</p>
        </div>
    </div>
</div>

<div class="col-12">


    <div class="row">
        <div class="col-12 text-center">
           {{-- <strong style="font-size:20px;padding-left: 20px;padding-right: 10px;">Payment Receipt</strong>
            <p style="font-size:6px;">Powered by SAB-Solutions</p>--}}
            <p>Amount : <strong>{{$transaction->amount}} </strong>/-</p>
            <p>Date : <strong>{{Carbon::parse($transaction->date)->format('d-m-Y')}} /-</strong></p>
            <p>Print Time : <strong>{{Carbon::now()->format('g:i A')}} /-</strong></p>
            <p>Description : <strong>{{$transaction->comments?$transaction->comments:'NA'}} </strong></p>
        </div>
    </div>
</div>

<style type="text/css" media="all">
    html{
        margin-top: 1.5in !important;
        font-size: 12px;
        margin-bottom: 1.5in !important;
        margin-right:0.75in !important;
        margin-left: 0.75in !important;
    }


    @page{
        height: 9.5in !important;
        width: 7in !important;
    }
    </style>