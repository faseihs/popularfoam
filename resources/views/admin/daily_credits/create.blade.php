@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::open(['method'=>'POST','action'=>'AdminDailyCreditsController@store','class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
            <div class="card-body card-block">
                <div style="display:none" class="row form-group">
                    <input type="hidden" name="debit_id" value="-1">
                </div>

                <div style="display:none" class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('credit_id','Credit Company:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('credit_id',[0=>'Popular Foam']+$companies,null,['class'=>'form-control','required'])!!}

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('amount','Amount:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::number('amount',null,['class'=>'form-control','placeholder'=>'Enter Amount'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('date','Date:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::date('date',null,['class'=>'form-control','required'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('comments','Comments:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::textarea('comments',null,['class'=>'form-control','rows'=>'3','required'])!!}

                    </div>
                </div>

            </div>
            <div class="card-footer">

                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('includes.errors')
@endsection