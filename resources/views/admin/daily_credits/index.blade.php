@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">Daily Credits</h3>
                </div>
                <div class="col-md-6">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search credits...">
                </div>
                <div class="table-data__tool-right col-md-4">
                    <a id="add" href="/admin/daily_credits/create" class="au-btn au-btn-icon au-btn--green au-btn--small float-right">
                        <i class="zmdi zmdi-plus"></i>add credits</a>
                </div>
            </div>
        </div>

    </div>
    @if(count($transactions)==0)
        <h1 class="text-md-center">No Credits Found</h1>
    @endif
    @if(count($transactions)>0)
        <div class="table-responsive table-responsive-data2">
            <table id="tableID" class="table table-data2">
                <thead>
                <tr>

                    <th>id</th>
                    <th>date</th>
                    <th>Amount</th>
                    <th>Comments</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($transactions as $transaction)
                    <tr class="tr-shadow">
                        <td>{{$transaction->id}}</td>
                        <td class="desc"><a href="/admin/daily_credits/{{$transaction->id}}">{{Carbon::parse($transaction->date)->format('d-m-Y')}}</a></td>
                        <td>{{number_format($transaction->amount,0,'.',',')}}</td>
                        <td><a data-toggle="tooltip" title="{{$transaction->comments}}" href="#">{{$transaction->comments?substr($transaction->comments,0,20).' ...':'NA'}}</a></td>
                        <td><a target="_blank" href="/admin/print_daily_credits/{{$transaction->id}}" class="btn btn-secondary">Print</a></td>
                        <td>
                            <div class="table-data-feature">
                                <form id="del{{$transaction->id}}" action="/admin/daily_credits/{{$transaction->id}}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                    <a href="#" onclick="clicked({{$transaction->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </a>
                                </form>
                                <a href="/admin/daily_credits/{{$transaction->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </a>

                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif

    <script>
        function clicked(id){
            if(confirm("Are You Sure ?")){
                document.getElementById('del'+id).submit();
            }
            else{
            }
        }
        Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
            if (e.preventDefault) {
                e.preventDefault();
                $('#add').click();
            } else {
                // internet explorer
                e.returnValue = false;
            }

        });
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <!-- END DATA TABLE-->
    @include('includes.search')
@endsection