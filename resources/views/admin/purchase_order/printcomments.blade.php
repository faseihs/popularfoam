@extends('layouts.myapp')

@section('content')
@include('includes.errors')
    <div class="col-12">
       
        <form id="formID" method="POST" action="/admin/purchase_order/finalPrint" onsubmit="return submitFunc()">
            {{csrf_field()}}
                <input name="purchase_order_id" type="hidden" value="{{$purchaseOrder->id}}">
            <div class="row">
                     <div class="col-md-3">
                        <select class="form-control" name="type" required>
                            <option selected value="1">With Valuation</option>
                            <option value="2">Without Valuation</option>    
                        </select>     
                    </div>   

                    <div class="col-md-3">
                        <select class="form-control" name="query" required>
                            <option selected value="1">Print</option>
                            <option value="2">Email</option>    
                        </select>     
                    </div>  
                    <div class="col-md-3">
                        <input class="form-control" type="email" name="email" placeholder="Email to send to">
                    </div>
                    <div class="row col-md-1"><button id="submitID" class="btn btn-dark" type="submit">Go</button></div>
            </div>
         <div class="table-responsive table-responsive-data2">
                 <table id="tableID" class="table table-data2">
                     <thead>
                         <tr>
                             <th>ID</th>
                             <th>Quality</th>
                             <th>Item</th>
                             <th>Quantity</th>
                             <th>Total Price</th>
                             <th>Comments</th>
                         </tr>

                         <tbody>
                             @foreach ($items as $item)
                                 <tr class="shadow">
                                     <td class="id">{{$item->item->id}}</td>
                                     <td class="quality">{{$item->item->quality->name}}</td>
                                     <td class="item">{{$item->item->name}}</td>
                                     <td style="display:none" class="price">{{$item->price}}</td>
                                     <td style="display:none" class="covered">{{$item->item->covered}}</td>
                                     <td class="quantity">{{$item->quantity}}</td>
                                     <td class="total_price">{{$item->total_price}}</td>
                                     <td class="comments"><input  class="form-control mousetrap" type="text"></td>
                                 </tr>
                             @endforeach
                         </tbody>
                     </thead>
                 </table>
         </div>
        </form>
    </div>
    <script>
        function submitFunc(){
            
           var items=[];
            $('#tableID tbody tr').each(function(){
                tempItem={};
                tempItem.id=$(this).find('.id').html();
                tempItem.quality=$(this).find('.quality').html();
                tempItem.item=$(this).find('.item').html();
                tempItem.quantity=$(this).find('.quantity').html();
                tempItem.total_price=$(this).find('.total_price').html();
                tempItem.price=$(this).find('.price').html();
                tempItem.covered=$(this).find('.covered').html();
                tempItem.comments=$(this).find('.comments input').val();
                items.push(tempItem);
            });
            
            var itemS=JSON.stringify(items);
            console.log(itemS);
           
            $('#formID').append('<textarea style="display:none" name="items">'+itemS+'</textarea>');
            return true;
           
        }

         Mousetrap.bind(['ctrl+s', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#formID').submit();

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });
    </script>
    @include('includes.errors')
@endsection