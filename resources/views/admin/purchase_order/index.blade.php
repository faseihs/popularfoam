@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">Purchase Orders</h3>
                </div>
                <div class="col-md-5">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search purchase orders...">
                </div>
                <div class="table-data__tool-right col-md-4">
                    <button id="add" type="button" class="btn btn-primary mb-1 float-right" data-toggle="modal" data-target="#scrollmodal">
                        Add Purchase Order
                    </button>
                </div>
            </div>

        </div>

        @if(count($purchase_orders)==0)
            <h1 class="text-md-center">No Purchase Orders Found</h1>
        @endif
        @if(count($purchase_orders)>0)

            <div id="tableID" class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead>
                    <tr>
                        <th>Creation Date</th>
                        <th>Order Date</th>
                        <th>Company</th>
                        <th>Vehicle</th>
                        <th>Coverage</th>
                        <th class="text-right">Total Bill</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($purchase_orders as $purchase_order)
                        <tr class="shadow">
                            <td>{{Carbon::parse($purchase_order->created_at)->format('d-m-Y')}}</td>
                            <td>{{$purchase_order->date?Carbon::parse($purchase_order->date)->format('d-m-Y'):'NA'}}</td>
                            <td>{{$purchase_order->company?$purchase_order->company->name:'Deleted / NA'}}</td>
                            <td>{{$purchase_order->vehicle->name}}</td>
                            <td>{{number_format($purchase_order->coverage(),1,'.',',')}}%</td>
                            <td class="text-right">{{number_format($purchase_order->total_price,0,'.',',')}}</td>
                            <td><a class="btn btn-info btn-sm" href="/admin/purchase_order/{{$purchase_order->id}}">View</a> 
                                <a target="_blank" class="btn btn-dark btn-sm" href="/admin/print/purchaseOrder/{{$purchase_order->id}}">Print</a></td>
                            <td>
                                <div class="table-data-feature">
                                    <form id="del{{$purchase_order->id}}" action="/admin/purchase_order/{{$purchase_order->id}}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        <a href="#" onclick="clicked({{$purchase_order->id}})"  class="purchase_order" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                    </form>
                                    <a href="/admin/purchase_order/{{$purchase_order->id}}/edit" class="purchase_order" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>

                                </div>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        @endif
            <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                           Select Company
                        </div>
                        <div class="modal-body">
                            {!! Form::open(['method'=>'POST','action'=>'AdminPurchaseOrderController@createC']) !!}
                                <div class="form-group">

                                    {!! Form::select('company_id',$companies,null,['class'=>'form-control','placeholder'=>'Select Company'])!!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Submit',['class'=>'btn btn-default'])!!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="modal-footer">
                            <div class="d-flex justify-content-around">
                                <a href="/admin/purchase_order/{id}" class="p2"></a>
                            <button type="button" class="btn btn-secondary p2" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function clicked(id){
                    if(confirm("Are You Sure ?")){
                        document.getElementById('del'+id).submit();
                    }
                    else{
                    }
                }
                Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                    if (e.preventDefault) {
                        e.preventDefault();
                        $('#add').click();

                    } else {
                        // internet explorer
                        e.returnValue = false;
                    }

                });
            </script>

    </div>       <!-- END DATA TABLE-->
    @include('includes.search')
@endsection