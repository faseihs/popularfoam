<head>    <link href="{{ asset('vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">
    <title> Purchase Order #{{$purchaseOrder->id}}</title>
</head>
    
    <div class="col-12">


        <div class="row">
            <div class="col-12 text-center">
                <strong style="font-size:20px;padding-left: 20px;padding-right: 10px;">{{$purchaseOrder->company->party_name? $purchaseOrder->company->party_name.' : ' :''}} Purchase Order</strong>
                <p style="font-size:6px;">Powered by SAB-Solutions</p>
            </div>
        </div>
    </div>
            <p class="margin">

                    <span>Company : <strong>{{$purchaseOrder->company->name}}</strong></span>
                    @if($purchaseOrder->company->party_id)


                    @endif
                    <span style="float:right">Vehicle : <strong>{{$vehicle->name}}</strong></span>
            </p>

                <p class="margin">
                        <span> Party ID : <strong>{{$purchaseOrder->company->party_id}}</strong></span>

                        <span style="float:right">Date : <strong>{{$purchaseOrder->date?Carbon::parse($purchaseOrder->date)->format('d-m-Y'):Carbon::parse($purchaseOrder->created_at)->format('d-m-Y')}}</strong></span>
                </p>

            <p class="margin">
                <span>Total Items : <strong>{{$covered+$uncovered}}</strong></span>
                <span>, Uncovered Items : <strong>{{$uncovered}}</strong></span>
                <span>, Covered Items : <strong>{{$covered}}</strong></span>
                <span style="float: right">Time : <strong>{{Carbon::now()->setTimezone('Asia/Karachi')->format('g:i A')}} </strong></span>

            </p>
                <div  id="itemDivID" >
                    @if ($type==1)
                    <?php
                    $sub_total = 0;
                    ?>
                    @foreach($items as $item) 
                        <?php  $sub_total += $item->total_price; ?>
                    @endforeach
                    @endif
                    <table id="itemTableID" class="table">
                        <thead>
                        <tr>
                            <th>Quality</th>
                            <th>Item</th>
                            <th class="text-right">Covered</th>
                            <th class="text-right">Quantity</th>
                            @if ($type==1)
                            <th class="text-right">Price</th>
                            
                            <th class="text-right">Total</th>
                            @endif
                            
                            @if ($type==2)
                            <th class="text-right">Comments</th>
                            @endif
                            
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($items as $item)
                            <tr id="{{$item->id}}">
                                <td>{{$item->quality}}</td>
                                <td>{{$item->item}}</td>
                                <td class="text-right covered">
                                        {{$item->covered==0?'':'Yes'}}
                                </td>
                                
                                

                                <td class="text-right quantity">{{$item->quantity}}</td>
                                @if ($type==1)
                                <td class="text-right price">{{$item->price}} \-</td>


                                <td class="text-right total">{{$item->total_price}} \-</td>
                                @endif
                               
                                @if ($type==2)
                                <td class="text-right comments">{{$item->comments==''?'NA':$item->comments}}</td>

                                @endif


                            </tr>
                        @endforeach
                        @if ($type==1)
                            <tr >
                                <td></td>
                                <td></td>
                                <td ></td>
                                <td class="text-right quantity"></td>
                                <td class="text-right "><strong>Sub Total</strong></td>
                                <td class="text-right total">
                                    <strong>{{$sub_total}} \-</strong>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                    {{--@if ($type==1)
                        <p class="col-md-12">
                            Total Price : <strong>{{$purchaseOrder->total_price}} \-</strong>
                        </p>
                    @endif
                    <p class="main">
                        Uncovered Items : <strong>{{$uncovered}}</strong>
                    </p>
                    <p class="main">
                        Covered Items : <strong>{{$covered}}</strong>
                    </p>
                    <p class="main">
                        Total Items : <strong>{{$covered+$uncovered}}</strong>
                    </p>--}}

        
    


    <!-- modal scroll -->
    

   
    <style type="text/css" media="all">
       .margin{
           margin-left: 15px;
           margin-right: 15px;
       }
        html{
            font-size: 12px;
        }
        body { border:1px solid black }
    </style>



    


    

    
    
