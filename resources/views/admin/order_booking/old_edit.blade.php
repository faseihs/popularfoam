@extends('layouts.simple')
@section('content')
    <div id="hidden" style="display:none"></div>
    <div class="col-12">
        <div class="card">
            {!! Form::model($order,['method'=>'PATCH','action'=>['AdminOrderBookingController@update',$order->id],'onsubmit'=>'return submitFunc()','id'=>'formID']) !!}
            <div class="card-header">

                <input id="customer_id" name="customer_id" type="hidden" value="{{$customer?$customer->id:0}}">
                <input id="totalPriceID" name="total_price" value="{{$order->total_price}}" type="hidden">
                <div class="d-flex justify-content-around">
                    <strong class="p2">Order Booking</strong>
                    <div id="companyID" class="p2"><strong>Customer: </strong>{{$customer?$customer->name:'Waking Customer'}}<span></span></div>
                    @if (!$customer)
                        <input required value="{{$order->name}}" placeholder="Customer Name" class="form-control-sm p2" type="text" name="name">
                        <input required value="{{$order->number}}" placeholder="Customer Number" class="form-control-sm p2" type="number" name="number">

                    @endif
                    <a target="_blank" class="btn btn-info btn-sm p2" href="/admin/item/create">Create Item</a>
                    <div class="p2">
                        Date :
                        <input id="date" value="{{Carbon::parse($order->date)->format('Y-m-d')}}"   type="date"  class="form-control-sm" name="date"/>
                    </div>
                    <div class="p2">
                        Delivery :
                        <input id="date" type="date" value="{{$order->delivery?Carbon::parse($order->delivery)->format('Y-m-d'):''}}"   class="form-control-sm" name="delivery"/>
                    </div>
                    <button id="search" type="button" class="btn btn-primary btn-sm p2" data-toggle="modal" data-target="#scrollmodal">
                        Add Item
                    </button>
                </div>


            </div>
            <div class="card-body card-block">

                <div style="overflow-y: scroll" id="itemDivID" class="table-responsive table--no-card m-b-30">
                    <table id="itemTableID" class="table table-borderless  table-earning">
                        <thead>
                        <tr>

                            <th>Quality</th>
                            <th>Item</th>
                            <th class="text-right">Covered</th>
                            <th class="text-right">Quantity</th>
                            <th style="display:none" class="text-right">Bonus</th>
                            <th class="text-right">Price</th>
                            <th class="text-right">Discount</th>
                            <th class="text-right">Total</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr id="{{$item->item->id}}">
                                <td>{{$item->item->quality->name}}</td>
                                <td>{{$item->item->name}}</td>
                                <td class="text-right covered">
                                    <select class="form-control-sm mousetrap">
                                        <option selected value={{$item->covered}}>{{$item->covered==0?'No':'Yes'}}</option>
                                        <option value="{{$item->covered==0?1:0}}">{{$item->covered==0?'Yes':'No'}}</option>
                                    </select>
                                </td>
                                <td style="display: none" class="size">{{$item->item->size_t}}</td>
                                <td class="udiscount" style="display: none">{{$item->item->UDiscount()}}</td>
                                <td class="cdiscount" style="display: none">{{$item->item->CDiscount()}}</td>

                                <td class="text-right quantity"><input step="any" min="0" class="form-control-sm sm quantityIN mousetrap" type="number" value="{{$item->quantity}}"/></td>
                                <td style="display: none" class="text-right bonus"><input min="0" class="form-control-sm sm mousetrap" type="number" /></td>
                                <td class="text-right price"><input step="any"  min="0" class="form-control-sm priceIN mousetrap" type="number" value="{{$item->price}}"/></td>

                                <td class="text-right discount"><input min="0" step="any" max="100" class="form-control-sm sm discountIN mousetrap" type="number" value="{{$item->discount}}"/> %</td>

                                <td class="text-right total">{{$item->total_price}}</td>
                                <td style="display: none"><input class="obj" type="checkbox" name="checkBoxArray[]"  value=""></td>
                                <td><button class="btn btn-danger btn-sm deleteBtn" type="button">Delete</button></td>



                            </tr>
                        @endforeach

                        </tbody>

                    </table>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div id="total" style="font-size:18px" class="col-md-2"><strong>Total : </strong><span>{{$order->total_price}}</span>/-</div>
                    <div id="advance" class="col-md-2">Adv:
                        <input value="{{$order->advance}}" id="paid_amount" class="form-control-sm" type="number" name="advance">
                    </div>
                    <div id="rem" class="col-md-2">Rem:
                        <input value="{{$order->remaining}}" id="returned"  class="form-control-sm" type="number" name="remaining">
                    </div>
                    <div id="cleared" class="col-md-2">Cleared:
                        {!! Form::select('cleared',[0=>'No',1=>'Yes'],null,['class'=>'form-control-sm'])!!}
                    </div>
                    <div id="comments" class="col-md-3">Comments:
                        <input  value="{{$order->comments}}" class="form-control-sm" type="text" name="comments">
                    </div>                    {{-- <div class="col-md-3">
                        {!! Form::label('paid_amount','Paid Amount') !!}
                        {!! Form::number('paid_amount',null,['class'=>'form-control-sm col-md-4 mousetrap','min'=>0,'id'=>'paid_amount']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('returned','Returned') !!}
                        {!! Form::text('returned',null,['class'=>'form-control-sm col-md-4','readonly','id'=>'returned']) !!}
                    </div>
                    <div class="col-md-2" id="pay_later">
                        {!! Form::label('pay_later','Pay Later') !!}
                        {!! Form::select('pay_later',[0=>'No',1=>'Yes'],null,['class'=>'form-control-sm','min'=>0,'id'=>'pay_later']) !!}
                    </div> --}}
                    <div class="col-md-1 text-right">
                        {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm','id'=>'submitID'])!!}
                    </div>

                </div>




            </div>

        </div>
        {!! Form::close() !!}
    </div>


    <!-- modal scroll -->
    <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <input onkeyup="search()"  autofocus name="searchItem" id="searchIn" type="text" class="form-control mousetrap" />
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="tableID" class="table-responsive table--no-card m-b-30">
                        <table id="table_ID" class="table table-borderless  table-earning">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Item</th>
                                <th>Price</th>
                                <th>Quality</th>
                                <th>Size</th>
                                <th>Covered</th>
                                <th>UDiscount</th>
                                <th>CDiscount</th>
                            </tr>
                            </thead>

                            <tbody>
                            @php($i=1)
                            @foreach($Items as $item)
                                <tr  tabindex="{{$i}}">
                                    <!--<td><button class="btn btn-sm btn-primary">Select</button></td>-->
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->discountedPrice($customer?$customer->id:0)}}</td>
                                    <td>{{$item->quality->name}}</td>
                                    <td>{{$item->size_t}}</td>
                                    <td>{{$item->covered==0?'No':'Yes'}}</td>

                                    <td>
                                        {{$item->UCDiscount($customer?$customer->id:0)}}
                                    </td>
                                    <td>
                                        {{$item->CCDiscount($customer?$customer->id:0)}}
                                    </td>
                                </tr>
                                @php($i++)
                            @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

                </div>
            </div>
        </div>
    </div>

    @include('includes.errors')
    <style>
        .card-body{
            min-height: 83vh;
            max-height: 83vh !important;
        }
        tr:focus{
            background: #b2b2b2;
        }
        tr:focus{
            background: #b2b2b2;
        }

        .selected{
            background: #b2b2b2 !important;
            color: white !important;
        }
        .selected td{
            color:white !important;
        }
        .form-control-sm{
            border:1px solid #ced4da !important;
        }
        .sm{
            width: 50px !important;
        }
        #itemDivID{
            max-height: 70vh;
        }
        #table_ID td,th{
            padding: 12px 10px !important;
        }
        #itemTableID td,th{
            padding: 12px 10px !important;
            font-size: 12px;
        }

        .card-header{
            padding: 0;
            font-size: 16px !important;
        }
        .card-footer{
            padding: 0;
            padding-top: 1px;
            padding-left: 5px;
            padding-right: 5px;
            font-size: 12px !important;
        }
        .form-control-sm{
            font-size: 10px !important;
        }
        select.form-control-sm{
            height: 1.5rem !important;
        }
        .p2{
            font-size: 12px !important;
        }
        .btn-sm{
            font-size: .5rem !important;
        }
    </style>

    {{--<script src="{{asset('js/po_create.js')}}"></script>--}}
    <script>

        $(document).ready(function () {

            Date.prototype.toDateInputValue = (function() {
                var local = new Date(this);
                local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
                return local.toJSON().slice(0,10);
            });
            //$('#date').val(new Date().toDateInputValue());
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    //$('#search').click();
                    $('#scrollmodal').modal('toggle');

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            Mousetrap.bind(['ctrl+q', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    console.log('CTRL + q pressed');
                    $('#paid_amount').focus();

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            Mousetrap.bind(['ctrl+s', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#submitID').click();

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            /*$('#pay_later select').on('change',function () {
               if($(this).val()==1) {
                   $('.card-footer').append(udhar);
                   if((!isNaN(parseInt($('#total span').html())) && parseInt($('#total span').html())>0) && !isNaN(parseInt($('#paid_amount').val()))){
                       var ret=$('#paid_amount').val() - parseInt($('#total span').html());
                       $('#to_pay').val(abs(parseInt(ret)));
                   }
               }
               else $('#udhar').remove();
            });*/


            $('#scrollmodal').on('shown.bs.modal', function () {
                $('#searchIn').focus();
            });
            $('#scrollmodal').on('hidden.bs.modal', function () {
                $('#tableID tr').each(function () {
                    $(this).removeClass('selected');
                    $('#searchIn').val('');
                    $('#table_ID tr').each(function () {
                        $(this).css('display','');
                    });
                });
                $('#search').blur();
            });
            $('#scrollmodal,.selected').on('keydown',function (e) {
                let key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
                if(key==40) {
                    //$('#scrollmodal').focus();
                    //e.preventDefault();
                    $('#searchIn').blur();
                    //console.log('Key Down Pressed');
                    if($('.selected:not(:hidden)').length==0){

                        $('#tableID tbody tr').not(':hidden').first().addClass('selected').focus();
                    }
                    else{
                        let current=$('.selected:not(:hidden)');
                        if(current.nextAll().not(':hidden').length){
                            current.nextAll().not(':hidden').first().addClass('selected').focus();
                            current.removeClass('selected');
                            //current.next().focus();
                        }
                        else{
                            current.removeClass('selected');
                            $('#searchIn').focus();
                        }
                    }

                }

                //Up Key
                if(key==38){
                    //e.preventDefault();
                    //$('#scrollmodal').focus();
                    console.log('Dabb gaya');
                    $('#searchIn').blur();
                    if($('.selected:not(:hidden)').length==0){

                        $('#tableID tbody tr').not(':hidden').last().addClass('selected').focus();
                        console.log('yahan 1');
                    }
                    else{
                        let current=$('.selected:not(:hidden)');
                        if(current.prevAll().not(':hidden').length){
                            current.prevAll().not(':hidden').first().addClass('selected').focus();
                            current.removeClass('selected');
                        }
                        else{
                            current.removeClass('selected');
                            $('#searchIn').focus();
                            console.log('yahan 3');
                        }
                    }

                }

                if(key==13){
                    e.preventDefault();
                    $('#scrollmodal').focus();
                    //console.log('Enter Pressed');
                    if($('.selected:not(:hidden)').length>0) {
                        let selected=$('.selected');
                        //$('.card-body').append(selected.html());
                        $('#scrollmodal').modal('toggle');
                        getItemFromTable(selected);
                        calcTotal();


                    }

                }
            });
            $('#company').on('change', function() {
                let comp=$('#company option:selected').html();
                if(comp!=='Company')
                    $('#companyID span').html(comp);
                else $('#companyID span').html('');
            });

            $('#vehicle').on('change', function() {
                let comp=$('#vehicle option:selected').html();
                if(comp!=='Vehicle' || $('#total-coverage').hasClass('alert-danger')) {
                    $('#vehicleID span').html(comp);
                    setVehicleSize();

                    $('#submitID').removeAttr('disabled');

                }
                else {$('#vehicleID span').html('');setVehicleSize();$('#submitID').attr('disabled','true');}
            });

            $('.discountIN').on('keyup',function () {

                calcTotal();
                console.log('Yahoo');
            });
            $('.quantityIN').on('keyup',function () {
                calcTotal();

            });

            $('.bonus input').on('change',function () {

            });
            $('#paid_amount').on('keyup',function () {
                console.log('Pressed');
                if(!isNaN(parseInt($('#total span').html()))) {
                    $('#returned').val(parseInt($('#total span').html()-$(this).val()));

                }
            });

            $('.inputIN').on('keyup',function(){
                calcTotal();
            });
            $('#table_ID tbody tr').on('click',function () {
                $('#scrollmodal').modal('toggle');
                getItemFromTable($(this));
                calcTotal();

            });
            $('.deleteBtn').on('click',function(){
                $(this).parent().parent().remove();
                calcTotal();
            });

            $('tr').on('focus',function(){
                console.log('FOvussed');
            });

            $('#paid_amount').on('keyup',function () {
                if(!isNaN(parseInt($('#total span').html()))) {
                    $('#returned').val($(this).val() - parseInt($('#total span').html()));
                    $('#to_pay').val(abs($(this).val() - parseInt($('#total span').html())));
                }
            });


        });
        function search() {
            $('#table_ID tbody tr').each(function () {
                $(this).removeClass('selected');
            });
            var input, filter, table, tr, td, i;
            input = document.getElementById("searchIn");
            filter = input.value.toUpperCase();
            table = document.getElementById("table_ID");
            //console.log(table);
            var tb = table.getElementsByTagName("tbody");
            tr=tb[0].getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";

                    } else {
                        tr[i].style.display = "none";

                    }
                }
            }
        }

        function getItemFromTable(object){
            var item={id:-1, name:"", price:-1,quality:"" ,company:"",covered:'',udiscount:-1,cdiscount:-1};
            var counter=1;
            object.children().each(function () {
                if(counter==1)
                    item.id=$(this).html();
                else if(counter==2)
                    item.name=$(this).html();
                else if(counter==3)
                    item.price=$(this).html();
                else if(counter==4)
                    item.quality=$(this).html();
                else if(counter==5)
                    item.size=$(this).html();
                else if(counter==6)
                    item.covered=$(this).html();
                else if(counter==7)
                    item.udiscount=$(this).html();
                else if(counter==8)
                    item.cdiscount=$(this).html();
                counter++;
            });

            //console.log(item);
            addItemToTable(item);
        }

        function addItemToTable(item){
            var selected;
            var selected_;
            var unselected;
            var unselected_;
            var dvalue;
            if(item.covered=='No'){
                selected=0;
                selected_='No';
                unselected=1;
                unselected_='Yes';
                dvalue=item.udiscount;
            }
            else{
                selected=1;
                selected_='Yes';
                unselected=0;
                unselected_='No';
                dvalue=item.cdiscount;
            }


            var row=`    <tr id='`+item.id+`'>
                                <td>`+item.quality+`</td>
                                <td>`+item.name+`</td>
                                <td class="text-right covered">
                                <select class="form-control-sm mousetrap">
                                <option selected value='`+selected+`'>`+selected_+`</option>
                                <option value='`+unselected+`'>`+unselected_+`</option>
                                </select>
                                 </td>
                                <td style="display:none" class="size">`+item.size+`</td>
                                <td class="cdiscount" style="display: none">`+item.cdiscount+`</td>
                                <td class="udiscount" style="display: none">`+item.udiscount+`</td>

                                <td class="text-right quantity"><input step="any" min="0" class="form-control-sm sm quantityIN mousetrap" type="number" value="1"/></td>
                                <td style="display:none" class="text-right bonus"><input min="0" class="form-control-sm sm mousetrap" type="number" value="0"/></td>
                                 <td class="text-right price"><input step="any"  min="0" class="form-control-sm priceIN mousetrap" type="number" value="`+item.price+`"/></td>
                                <td class="text-right discount"><input step="any" min="0" max="100" class="form-control-sm sm discountIN mousetrap" type="number" value=`+dvalue+`/> %</td>

                                <td class="text-right total">`+item.price+`</td>
                                <td style="display: none"><input class="obj" type="checkbox" name="checkBoxArray[]"  value=""></td>
                                <td><button class="btn btn-danger btn-sm deleteBtn" type="button">Delete</button></td>



                       </tr>`;
            $('#itemTableID tbody').append(row);
            $('.quantityIN').bind('change', function() {
                calcTotal();
                console.log('Yahoo');
            });
            $('.discountIN').bind('change', function() {
                calcTotal();
                console.log('Yahoo');
            });


            $('.deleteBtn').bind('click',function () {
                $(this).parent().parent().remove();
                calcTotal();
            });

            $('.covered select').on('change',function () {
                var val =$(this).val();
                var p=$(this).parent().parent();

                //console.log(parseInt(p.find('.cdiscount').html()));
                //console.log(parseInt(p.find('.udiscount').html()));
                if(val==1)
                    p.find('.discountIN').val(parseInt(p.find('.cdiscount').html()));
                else{
                    p.find('.discountIN').val(parseInt(p.find('.udiscount').html()));
                }
                calcTotal();
            });

            $('.priceIN').on('keyup',function(){
                calcTotal();
            });




        }

        function calcTotal(){
            var sum=0;
            $('#itemTableID tbody').children().each(function () {

                var individualSum=parseFloat($(this).find('.price input').val()) *parseFloat($(this).find('.quantity input').val());
                console.log(individualSum);
                var discount=parseFloat($(this).find('.discount input').val());
                console.log(discount);
                if(discount>0){
                    individualSum=individualSum*(1-(discount/100));
                }
                individualSum=Number((individualSum).toFixed(1));
                $(this).children('.total').html(individualSum);
                sum+=individualSum;
            });
            sum=Number((sum).toFixed(0));
            $('#total span').html(sum);
            $('#totalPriceID').val(sum);
            if((!isNaN(parseInt($('#total span').html())) && parseInt($('#total span').html())>0) && !isNaN(parseInt($('#paid_amount').val()))) {
                var ret=$('#paid_amount').val() - parseInt($('#total span').html());
                $('#returned').val(abs(ret));
                $('#to_pay').val(abs(ret));

            }
            else {
                $('#returned').val(0);
                $('#to_pay').val(0);
            }
        }

        function submitFunc(){
            if($('#itemTableID tbody tr').children().length<1){
                alert("No Items Selected");
                return false;
            }


            var item_array=Array();
            $('#itemTableID tbody tr').each(function () {

                var id=$(this).attr('id');
                var item={item_id:-1,quantity:-1,total_price:-1,discount:-1,covered:-1};
                item.item_id=id;
                var $this=$(this);
                var count=1;
                item.price=$(this).children('.price').find(':nth-child(1)').val();
                //console.log($(this).find(':nth-child(5)').find(':nth-child(1)').val());
                item.quantity=$(this).children('.quantity').find(':nth-child(1)').val();
                //item.bonus=$(this).children('.bonus').find(':nth-child(1)').val();
                item.discount=$(this).children('.discount').find(':nth-child(1)').val();
                item.covered=$(this).children('.covered').find(':nth-child(1)').val();
                item.total_price=$(this).children('.total').html();
                $(this).find('input.obj').val(item);
                //console.log($(this).find('input.obj').val());
                item_array.push(item);
            });
            var items=JSON.stringify(item_array);
            $('#formID').append("<textarea id='items' name='items' style='display:none'>"+items+"</textarea> ");
            //console.log($('#formID #items'));
            return true;
        }



        sortTable();
        function sortTable() {
            var table, rows, switching, i, x, y, shouldSwitch;
            table = document.getElementById("table_ID");
            switching = true;
            /* Make a loop that will continue until
            no switching has been done: */
            while (switching) {
                // Start by saying: no switching is done:
                switching = false;
                rows = table.getElementsByTagName("TR");
                /* Loop through all table rows (except the
                first, which contains table headers): */
                for (i = 1; i < (rows.length - 1); i++) {
                    // Start by saying there should be no switching:
                    shouldSwitch = false;
                    /* Get the two elements you want to compare,
                    one from current row and one from the next: */
                    x = rows[i].getElementsByTagName("TD")[3];
                    y = rows[i + 1].getElementsByTagName("TD")[3];
                    // Check if the two rows should switch place:
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
                if (shouldSwitch) {
                    /* If a switch has been marked, make the switch
                    and mark that a switch has been done: */
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                }
            }
        }
    </script>
@endsection