@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">Order Bookings</h3>
                </div>
                <div class="col-md-5">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search...">
                </div>
                <div class="table-data__tool-right col-md-4">
                    <button id="add" type="button" class="btn btn-primary mb-1 float-right" data-toggle="modal" data-target="#scrollmodal">
                        Add Order Booking
                    </button>
                </div>
            </div>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#ucomp" role="tab" aria-controls="nav-home"
                       aria-selected="true">Not Completed</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#comp" role="tab" aria-controls="nav-profile"
                       aria-selected="false">Completed</a>

                </div>
            </nav>
            <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                <div class="tab-pane fade show active" id="ucomp" role="tabpanel" aria-labelledby="nav-home-tab">

                    @if(count($order_bookings)==0)
                        <h1  class="col-md-12 text-md-center">No Order Bookings Found</h1>
                    @endif
                    @if(count($order_bookings)>0)

                        <div id="tableID" class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Order Date</th>
                                    <th>Customer Category</th>
                                    <th>Customer</th>
                                    <th>Customer Number</th>
                                    <th class="text-right">Total Bill</th>
                                    <th>Paid</th>
                                    <th>Remaining</th>
                                    <th>Data Created</th>
                                    <th>Created By</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order_bookings as $order_booking)
                                    <tr>
                                        <td>{{$order_booking->id}}</td>
                                        <td>{{Carbon::parse($order_booking->date)->format('d-m-Y')}}</td>
                                        <td>{{$order_booking->cust}}</td>
                                        <td>{{$order_booking->name}}</td>
                                        <td>{{$order_booking->number}}</td>
                                        <td class="text-right">{{$order_booking->total_price}}</td>
                                        <td>{{$order_booking->salesInvoice?$order_booking->salesInvoice->paid_amount:$order_booking->advance}}</td>
                                        <td>{{$order_booking->salesInvoice?$order_booking->salesInvoice:$order_booking->remaining}}</td>
                                        <td>{{Carbon::parse($order_booking->created_at)->format('d-m-Y')}}</td>
                                        <td>{{$order_booking->user?$order_booking->user->name:'-'}}</td>
                                        <td><a class="btn btn-primary btn-sm" href="/admin/order_booking/{{$order_booking->id}}">View</a>
                                            <a  target="_blank" class="btn btn-dark btn-sm" href="/admin/print_option/salesInvoice/{{$order_booking->id}}?print_url={{urlencode("/admin/order_booking/print/$order_booking->id")}}">Print</a>
                                        </td>
                                        <td>
                                            <div class="table-data-feature">
                                                <form id="del{{$order_booking->id}}" action="/admin/order_booking/{{$order_booking->id}}" method="POST">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                                    <a href="#" onclick="clicked({{$order_booking->id}})"  class="order_booking" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </a>
                                                </form>
                                                <a  href="/admin/order_booking/{{$order_booking->id}}/edit" class="order_booking" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </a>

                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    @endif
                </div>

                <div class="tab-pane fade" id="comp" role="tabpanel" aria-labelledby="nav-home-tab">

                    @if(count($sorder_bookings)==0)
                        <h1  class="col-md-12 text-md-center">No Order Bookings Found</h1>
                    @endif
                    @if(count($sorder_bookings)>0)

                        <div id="tableID" class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Order Date</th>
                                    <th>Delivery Date</th>
                                    <th>Customer</th>
                                    <th class="text-right">Total Bill</th>

                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sorder_bookings as $order_booking)
                                    <tr>
                                        <td>{{$order_booking->order_booking->id}}</td>
                                        <td>{{Carbon::parse($order_booking->order_booking->date)->format('d-m-Y')}}</td>
                                        <td>{{Carbon::parse($order_booking->date)->format('d-m-Y')}}</td>
                                        <td>{{$order_booking->order_booking->cust}}</td>
                                        <td class="text-right">{{$order_booking->total_price}}</td>
                                        <td><a class="btn btn-primary btn-sm" href="/admin/sales_invoice/{{$order_booking->id}}">View Invoice</a>
                                            <a class="btn btn-secondary btn-sm" href="/admin/order_booking/{{$order_booking->order_booking_id}}">View Order</a>
                                            <a  target="_blank" class="btn btn-dark btn-sm" href="/admin/print/salesInvoice/{{$order_booking->id}}">Print</a>
                                        </td>
                                        <td>
                                            <div class="table-data-feature">
                                                <form id="del{{$order_booking->id}}" action="/admin/order_booking/{{$order_booking->id}}" method="POST">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                                    <a href="#" onclick="clicked({{$order_booking->id}})"  class="order_booking" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </a>
                                                </form>
                                                <a  href="/admin/order_booking/{{$order_booking->id}}/edit" class="order_booking" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </a>

                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    @endif
                </div>
            </div>

        </div>

        <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        Select Customer
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['method'=>'POST','action'=>'AdminOrderBookingController@createC']) !!}
                        <div class="form-group">

                            {!! Form::select('customer_id',[0=>'Walking Customer']+$customers,null,['class'=>'form-control','placeholder'=>'Select Customer'])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Submit',['class'=>'btn btn-default'])!!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <div class="d-flex justify-content-around">
                            <a href="/admin/order_booking/{id}" class="p2"></a>
                            <button type="button" class="btn btn-secondary p2" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function clicked(id){
                if(confirm("Are You Sure ?")){
                    document.getElementById('del'+id).submit();
                }
                else{
                }
            }
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#add').click();

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>

    </div>       <!-- END DATA TABLE-->
    
    @include('includes.search')
@endsection