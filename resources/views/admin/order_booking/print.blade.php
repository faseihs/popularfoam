<head>    <link href="{{ asset('vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">
    <title>Order Booking #{{$order_booking->id}}</title>
    </head>
        
        <div class="col-12">
            
                <p class="text-right">
                    <div  class="row">
                        <div class="col-12 col-md-12 text-center">
                            <strong style="font-size:18px;padding-left: 20px;padding-right: 10px;">Order Booking</strong>
                            <hr>
                        </div>
                    </div>
               
                     
                    <div class="col-md-12">
                            <strong> Customer : {{$order_booking->cust}}</strong>
                           
                            <span style="float:right;">Order ID :<strong>{{$order_booking->id}}</strong></span>

                        </div> 
                    <div style="margin-top:5px;" class="col-md-12">
                            <strong>Order Date : {{Carbon::parse($order_booking->date)->format('d-m-Y')}} </strong>
                            <span style="float:right;"> D.Date :  <strong>{{Carbon::parse($order_booking->delivery)->format('d-m-Y')}}</strong></span>
                    </div>
                                   
                         
                   
                    
                   
                    
    
                </p>
                
                
    <br>
                    <div style="border:none;"  id="itemDivID" >
                        <table style="border:none;" id="itemTableID" class="table">
                            <thead style="border:none;">
                            <tr style="border:none">
                                <th>Quality</th>
                                <th>Item</th>
                                
                                <th class="text-right">Qty</th>
                                
                                <th class="text-right">Price</th>
                                
                                <th class="text-right">Amount</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
    
                            @foreach($items as $item)
                                <tr id="{{$item->item->id}}">
                                    <td>{{$item->item->quality->name}}</td>
                                    <td>{{$item->item->name}}</td>
                                   
                                    
                                    
    
                                    <td class="text-right quantity">{{$item->quantity}}</td>
                                    <td class="text-right price">{{$item->price}}/-</td>
    
    
                                    <td class="text-right total">{{$item->total_price}}/-</td>
    
    
    
                                </tr>
                            @endforeach
                            <tr style="border-top:1px solid black;border-bottom:1px solid black;" class="lastRow">
                                <td colspan="3">Time : <strong>{{Carbon::now()->setTimezone('Asia/Karachi')->format('g:i A')}} </strong>
                                </td>
                                <td  style="font-size:16px;" class="text-right" colspan="2">Total : <strong>{{$order_booking->total_price}} /-</strong>
                                   
                                    <div style="display:block">
                                        Advance : <strong>{{$order_booking->advance}} /-</strong>
                                    </div>
                                    <div style="display:block">
                                         R.Balance : <strong>{{$order_booking->remaining}} /-</strong>
                                    </div>
                                  
                                
                                </td>
                               
                            </tr>
    
                            </tbody>
                        </table>
                    </div>
    
                    
                    
                </div>
                
                
    
            </div>
            
            
        
    
    
        <!-- modal scroll -->
        
    
       
        <style type="text/css" media="all">
            html{
                margin-top: 1.7in !important;
                font-size: 12px;
                margin-bottom: 4.0in !important;
                margin-right:1.0in !important;
                margin-left: 1.0in !important;
            }
            
    
            @page{
                height: 9.5in !important;
                width: 7in !important;
            }
             #itemDivID{
                padding: 0;
                margin: 0;
            } 
            table{
                border:none !important;
                
                padding: 0 !important;
                margin:0 !important;
            }
            tr{
                margin:0 !important;
                padding: 0 !important;
            }
    
            .amount{
                
                bottom:10px !important;
                left:70% !important;
                position: absolute;
               
               
            }
            .time{
                bottom:10px !important;
                
                position: absolute;
            }
    
            .lastRow{
                border:1px solid black !important;
            }
    
            
        </style>
        <script>
            sortTable();
            function sortTable() {
                var table, rows, switching, i, x, y, shouldSwitch;
                table = document.getElementById("itemTableID");
                switching = true;
                /* Make a loop that will continue until
                no switching has been done: */
                while (switching) {
                    // Start by saying: no switching is done:
                    switching = false;
                    rows = table.getElementsByTagName("TR");
                    /* Loop through all table rows (except the
                    first, which contains table headers): */
                    for (i = 1; i < (rows.length - 1); i++) {
                        // Start by saying there should be no switching:
                        shouldSwitch = false;
                        /* Get the two elements you want to compare,
                        one from current row and one from the next: */
                        x = rows[i].getElementsByTagName("TD")[0];
                        y = rows[i + 1].getElementsByTagName("TD")[0];
                        // Check if the two rows should switch place:
                       
                        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                            // If so, mark as a switch and break the loop:
                            
                            if()
                            shouldSwitch = true;
                            break;
                        }
                       
                    }
                    if (shouldSwitch) {
                        /* If a switch has been marked, make the switch
                        and mark that a switch has been done: */
                        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                        switching = true;
                    }
                }
            }
    
        </script>
    
        
        
    