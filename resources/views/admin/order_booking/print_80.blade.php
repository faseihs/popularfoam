<html>

<head>
    <title>Order Booking #{{$order_booking->id}}</title>
    <style>
        html,
        p {
            font-family: Helvetica, sans-serif padding: 0;
            margin: 0;
            font-size: 10px;
            letter-spacing: 1px;
        }

        table,
        .table {
            width: 99.5%;
            margin: 0;
            padding: 0;
            margin-right: 0px;
            margin-left: 0px;
        }

        /* p{
        font-size: 12px;
    } */

        .strong {
            font-weight: bold;
        }


        th {

            text-align: left;
        }

        td {

            text-align: left;
        }

        .text-right {
            text-align: right;
        }

        tr {
            margin-left: 12px;
            margon-right: 12px;
        }
    </style>
</head>

<body>

    <p>
        <center><img style="width:150px;height:75px;margin-top:8px;" src="{{public_path("images/pf.jpeg")}}" alt=""
                srcset=""></center>
    </p>
    <p class="strong">
        <center><strong  style="border-bottom: 1px solid black;padding-bottom:8px;">Order Booking</center> </strong>
    </p>
    <br>
    <p style="margin-left:8px; margin-right:8px;">
        <span> <strong> Date: </strong>{{Carbon::parse($order_booking->date)->format('d-m-Y')}} </span>


        <span style="float:right"><strong> Order: </strong>{{$order_booking->id}}</span>

    </p>

    <p style="margin-left:8px; margin-right:8px;">
        <span> <strong> Delivery: </strong>{{Carbon::parse($order_booking->delivery)->format('d-m-Y')}} </span>


    </p>
    <p style="margin-left:8px; margin-right:8px;">
        <span ><strong> Customer: </strong>{{$order_booking->getCustomerName()}}</span>

    </p>

    <br>
    <div style="border:none;margin-bottom:8px;" id="itemDivID">
        <table id="itemTableID" class="table">
            <thead style="border:1px solid black;">
                <tr style="border:none">
                    <th>Quality</th>
                    <th>Item</th>

                    <th class="text-right">Qty</th>

                    <th class="text-right">Price</th>

                    <th class="text-right">Amount</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>

                @foreach($items as $item)
                <tr id="{{$item->item->id}}">
                    <td>{{$item->item->quality->name}}</td>
                    <td>{{$item->item->name}}</td>




                    <td class="text-right quantity">{{$item->quantity}}</td>
                    <td class="text-right price">{{$item->price}}/-</td>


                    <td class="text-right total">{{$item->total_price}}/-</td>



                </tr>
                @endforeach
              
                <tr>
                    <td style="border:1px solid black" class="text-right" colspan="5">
                        <span style='float:left'>
                            <strong>Time: </strong>
                            {{Carbon::now()->setTimezone('Asia/Karachi')->format('g:i A')}}</span>
                        <strong>Total :</strong>
                        {{$order_booking->total_price}} /-



                    </td>

                </tr>

                <tr>
                    <td style="border:1px solid black" class="text-right" colspan="5">
                       
                        <div style="display:block">
                            <strong> Advance: </strong>{{$order_booking->advance}} /-
                         </div>
                        


                    </td>

                </tr>

                <tr>
                    <td style="border:1px solid black" class="text-right" colspan="5">
                        <div style="display:block">
                            <strong>R.Balance : </strong>{{$order_booking->remaining}} /-
                        </div>

                    </td>

                </tr>

            </tbody>
        </table>
    </div>
    <br>
    <p><center><strong>Address: </strong>{{env("SHOP_ADDRESS")}}</center></p>
    <br>
    <p><center><strong>Contact: </strong>{{env("SHOP_NUMBER")}}</center></p>
    <br>
    <p><center>Thank you for shopping at </center></p>
    <br>
    <p><center><strong>{{env("SHOP_NAME")}}</strong></center></p>
    <br>

</body>

</html>