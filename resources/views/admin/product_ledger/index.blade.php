@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div id="ledger-app">
        <vue-table  :columns="columns"></vue-table>
    </div>

    <script src="{{asset('js/productLedger.js')}}"></script>
    <style>
        .au-breadcrumb2 {
            padding-top: 10px;
            padding-bottom: 0px;
        }
    </style>
@endsection