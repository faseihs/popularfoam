<head>
    <link href="{{ asset('vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">
    <title>Sales Report</title>
</head>
<div class="col-12">


    <div class="row">
        <div class="col-12 text-center">
            <strong style="font-size:20px;padding-left: 20px;padding-right: 10px;">Sales Report</strong>
            <p style="font-size:6px;">Powered by SAB-Solutions</p>
        </div>
    </div>
</div>
@if($sOption==2)
<p >

    <strong>On Date : {{Carbon::parse($onDate)->format('d-m-Y')}} </strong>
</p>

@endif
@if($sOption==3)
    <p >

        <strong>From Date : {{Carbon::parse($fromDate)->format('d-m-Y')}} </strong>
    </p>

    <p >

        <strong>To Date : {{Carbon::parse($toDate)->format('d-m-Y')}} </strong>
    </p>

@endif


<br>
<div style="border:none;"  id="itemDivID" >
    <table style="font-size:12px;border:none;" id="itemTableID" class="table">
        <thead style="border:none;">
        <tr style="border:none">
            <th>ID</th>
            <th >Date</th>
            <th >Customer</th>
            <th class="text-right" >Total Items</th>
            <th class="text-right" >Total Bill</th>
        </tr>
        </thead>
        <tbody>
        @php($s_count=0)
        @foreach($sales_invoices as $s)
            <tr>
                <td><a target="_blank" href="/admin/sales_invoice/{{$s->id}}">{{$s->id}}</a></td>
                <td class="text-left">{{Carbon::parse($s->date)->format('d-m-Y')}}</td>
                <td class="text-left">{{$s->cust}}</td>
                <td class="text-right">{{count($s->items)}}</td>
                <td class="text-right">{{number_format($s->total_price,0,'.',',')}}</td>
            </tr>
            @php($s_count+=$s->total_price)
        @endforeach
        {{--<tr style="border-top:1px solid black;border-bottom:1px solid black;" class="lastRow">
            <td colspan="5" class="text-right" style="font-size:13px;"> Paid : <strong>{{number_format($sales_invoice->paid_amount,0,'.',',')}} /-</strong></td>
            <td  style="font-size:13px;"  class="text-right" colspan="2">Total : <strong>{{number_format($sales_invoice->total_price,0,'.',',')}} /-</strong>

                @if ($sales_invoice->returned>0)
                    <div style="display: block;">
                        Ret : <strong>{{$sales_invoice->returned}} /-</strong>
                    </div>
                @endif

                @if (($sales_invoice->paid_amount + $sales_invoice->discount)<$sales_invoice->total_price)

                    <div style="display:block">
                        Rem : <strong>{{$sales_invoice->total_price - $sales_invoice->paid_amount}} /-</strong>
                    </div>
                @endif
                @if ($sales_invoice->discount)
                    <div style="display:block">
                        Disc : <strong>{{$sales_invoice->discount}} /-</strong>
                    </div>

                @endif

            </td>

        </tr>--}}

        </tbody>
    </table>
</div>










<!-- modal scroll -->



<style type="text/css" media="all">
    /*html{
        margin-top: 1.5in !important;
        font-size: 12px;
        margin-bottom: 1.5in !important;
        margin-right:0.75in !important;
        margin-left: 0.75in !important;
    }*/


    /*@page{
        height: 9.5in !important;
        width: 7in !important;
    }*/
    #itemDivID{
        padding: 0;
        margin: 0;
    }
    table{
        border:none !important;

        padding: 0 !important;
        margin:0 !important;
    }
    tr{
        margin:0 !important;
        padding: 0 !important;
    }

    .amount{

        bottom:10px !important;
        left:70% !important;
        position: absolute;


    }
    .time{
        bottom:10px !important;

        position: absolute;
    }

    .lastRow{
        border:1px solid black !important;
    }


</style>
<script>
    sortTable();
    function sortTable() {
        var table, rows, switching, i, x, y, shouldSwitch;
        table = document.getElementById("itemTableID");
        switching = true;
        /* Make a loop that will continue until
        no switching has been done: */
        while (switching) {
            // Start by saying: no switching is done:
            switching = false;
            rows = table.getElementsByTagName("TR");
            /* Loop through all table rows (except the
            first, which contains table headers): */
            for (i = 1; i < (rows.length - 1); i++) {
                // Start by saying there should be no switching:
                shouldSwitch = false;
                /* Get the two elements you want to compare,
                one from current row and one from the next: */
                x = rows[i].getElementsByTagName("TD")[0];
                y = rows[i + 1].getElementsByTagName("TD")[0];
                // Check if the two rows should switch place:

                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    // If so, mark as a switch and break the loop:

                    if()
                        shouldSwitch = true;
                    break;
                }

            }
            if (shouldSwitch) {
                /* If a switch has been marked, make the switch
                and mark that a switch has been done: */
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
            }
        }
    }

</script>



