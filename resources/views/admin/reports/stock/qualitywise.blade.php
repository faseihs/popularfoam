@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-12">
            <div class="row">

                    <div class="col-md-3">
                        <h3 class="title-5 m-b-35">Sales Report : Quality Wise</h3>
                    </div>
                    {!! Form::open(['method'=>'POST','action'=>'AdminStockReportController@qualityQuery','class'=>'col-md-7','id'=>'formRow']) !!}

                        <div class="row">
                                {!! Form::select('quality_id',$qualities,null
                                ,['class'=>'form-control-sm col-md-3','placeholder'=>'Select Quality','required'])!!}
                            {!! Form::select('today',[1=>'Today',2=>'On Date'],isset($on_date)?2:1
                               ,['class'=>'form-control-sm col-md-3','id'=>'selectID'])!!}
                            @if(isset($on_date))
                                <input id='date'  type='date' class='form-control-sm col-md-3 on_date' value="{{Carbon::parse($on_date)->format('Y-m-d')}}" name='on_date' required />
                            @endif
                            

                            <button class="btn btn-dark btn-sm col-md-1" type="submit">Go</button>
                            @if(isset($from_date))
                                <input id='date'  type='date' class='form-control-sm col-md-2 duration_dates' value="{{Carbon::parse($from_date)->format('Y-m-d')}}" name='from_date' required />
                            @endif

                            @if(isset($to_date))
                                <input id='date'  type='date' class='form-control-sm col-md-2 duration_dates' name='to_date' value="{{Carbon::parse($to_date)->format('Y-m-d')}}" required />
                            @endif
                            
                        </div>

                    {!! Form::close() !!}



            </div>

        </div>
        @if(isset($item_reports) && count($item_reports)==0)
            <h1 class="col-md-12 text-center" >No Items Found</h1>
        @endif
        @if(isset($item_reports) && count($item_reports)>0)
            <div class="table-responsive table-responsive-data2">
                <table id="tableID" class="table table-data2">
                    <thead>
                    <tr>

                        <th>id</th>
                        <th>Item</th>
                        <th>Opening</th>
                        <th>Purchases</th>
                        <th>P.Return</th>
                        <th>Sales</th>
                        <th>S.Returns</th>
                        <th>Stock</th>
                       
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($item_reports as $item)
                            <tr class="shadow">
                                <td><a target="_blank" href="/admin/item/{{$item->id}}">{{$item->id}}</a></td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->opening}}</td>
                                <td>{{$item->purchase_items}}</td>
                                <td>{{$item->preturn_items}}</td>
                                <td>{{$item->sale_items}}</td>
                                <td>{{$item->return_items}}</td>
                                <td>{{$item->stock}}</td>
                            </tr>
                        @endforeach
                    
                    </tbody>
                </table>
            </div>
        @endif
    </div>

    <script>
        Date.prototype.toDateInputValue = (function() {
            var local = new Date(this);
            local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
            return local.toJSON().slice(0,10);
        });
        var on_date= "<input class='on_date' type='date' class='form-control-sm col-md-2' name='on_date' required />";
        var from_date="<input class='duration_dates' type='date' class='form-control-sm col-md-2' name='from_date' required /><input  class='duration_dates' type='date' class='form-control-sm' name='to_date' required />"
        Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
            if (e.preventDefault) {
                e.preventDefault();
                $('#add').click();


            } else {
                // internet explorer
                e.returnValue = false;
            }

        });

        $(document).ready(function () {
            $('#selectID').on('change',function () {
                if ($(this).val()==1){
                    $('.on_date').remove();
                    $('.duration_dates').remove();
                }
                else if($(this).val()==2){
                    $('#formRow').append(on_date);
                    $('.duration_dates').remove();
                }

                else{
                    $('#formRow').append(from_date);
                    $('.on_date').remove();
                }
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <style>
        .au-input--w300{
            width: 100% !important;
        }

    </style>
    <!-- END DATA TABLE-->
    @include('includes.search')
@endsection