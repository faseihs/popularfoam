
@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-12">
            <div class="row">

                    <div class="col-md-2">
                        <h3 class="title-5 m-b-35">Stock Valuation</h3>
                    </div>
                    {!! Form::open(['method'=>'POST','action'=>'AdminStockReportController@companyQuery','class'=>'col-md-10','id'=>'formRow']) !!}

                        <div class="row">
                                {!! Form::select('company_id',['all'=>'All']+$companies,'all'
                                ,['class'=>'form-control-sm col-md-4','required'])!!}
                            {!! Form::select('today',[1=>'Today',2=>'On Date'],isset($on_date)?2:1
                               ,['class'=>'form-control-sm col-md-2 noprint','id'=>'selectID'])!!}
                            @if(isset($on_date))
                                 <input id='date'  type='date' class='form-control-sm col-md-3 on_date' value="{{Carbon::parse($on_date)->format('Y-m-d')}}" name='on_date' required />
                            @endif

                            @if (!isset($on_date))
                            <input id='date' class="today" style="display:none;padding-left:5px;"  type='date' class='form-control-sm col-md-3 on_date' value="{{Carbon::now()->format('Y-m-d')}}" name='on_date' required />
       
                            @endif
                            
                            <button style="margin-left:5px;" onclick="window.print()" class="btn btn-info btn-sm noprint" type="button"><i class="fa fa-print"></i> Print</button>

                            <button style="margin-left:5px;" class="btn btn-dark btn-sm noprint" type="submit"><i class="fa fa-long-arrow-alt-up"></i> Go</button>
                            <span style="margin-left:5px; padding:5px ;border:1px dashed black;border-radius:10px;">  Total Stock : <strong>{{$total}}</strong></span>
                            <span style="margin-left:5px; padding:5px ;border:1px dashed black;border-radius:10px;">  Total St.Value : <strong>{{$total_price}}/-</strong></span>
                            
                        </div>

                    {!! Form::close() !!}

                    

            </div>

        </div>
        @if(isset($item_reports) && count($item_reports)==0)
            <h1 class="col-md-12 text-center" >No Items Found</h1>
        @endif
        @if(isset($item_reports) && count($item_reports)>0)
            <div class="table-responsive table-responsive-data2">
                <table id="tableID" class="table table-data2">
                    <thead>
                    <tr>

                        <th>id</th>
                        <th>Quality</th>
                        <th>Item</th>
                        <th>Cvrd</th>
                        <th class="noprint">Op.Date</th>
                        <th class="noprint">Op.Stock</th>
                        <th>Stock</th>
                        <th>Purchase Price</th>
                        <th>Dis(%)</th>
                        <th  class="text-center">Stock Val</th>
                        
                        
                       
                    </tr>
                    </thead>
                    <tbody>
                    @if(!$noDisplay)
                        @foreach ($item_reports as $item)
                            <tr class="shadow {{$item->stock==0?'noprint':''}}">
                                <td><a target="_blank" href="/admin/item/{{$item->id}}">{{$item->id}}</a></td>
                                <td>{{$item->quality}}</td>
                                <td>{{$item->name}}</td>
                                <td class="text-center">{{$item->covered==1?'Y':'-'}}</td>
                                <td class="noprint">{{$item->opening_date?Carbon::parse($item->opening_date)->format('d-m-Y'):'NA'}}</td>
                                <td class="noprint">{{$item->opening_quantity}}</td>
                                <td  class="text-center">{{$item->stock}}</td>
                                <td  class="text-center" >{{$item->purchase_price}}/-</td>
                                <td  class="text-center">{{$item->discount}}%</td>
                                <td class="text-right">{{$item->mytotal}}/-</td>
                                
                            </tr>
                        @endforeach
                    @endif
                    
                    </tbody>
                </table>
            </div>
        @endif
    </div>

    <script>
        Date.prototype.toDateInputValue = (function() {
            var local = new Date(this);
            local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
            return local.toJSON().slice(0,10);
        });
        var on_date= "<input class='on_date' type='date' class='form-control-sm col-md-2' name='on_date' required />";
        var from_date="<input class='duration_dates' type='date' class='form-control-sm col-md-2' name='from_date' required /><input  class='duration_dates' type='date' class='form-control-sm' name='to_date' required />"
        Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
            if (e.preventDefault) {
                e.preventDefault();
                $('#add').click();


            } else {
                // internet explorer
                e.returnValue = false;
            }

        });

        $(document).ready(function () {
            $('#selectID').on('change',function () {
                if ($(this).val()==1){
                    $('.on_date').remove();
                    $('.duration_dates').remove();
                }
                else if($(this).val()==2){
                    $('#formRow').append(on_date);
                    $('.duration_dates').remove();
                }

                else{
                    $('#formRow').append(from_date);
                    $('.on_date').remove();
                }
            });
            $('[data-toggle="tooltip"]').tooltip();

            sortTable();
        function sortTable() {
            var table, rows, switching, i, x, y, shouldSwitch;
            table = document.getElementById("tableID");
            switching = true;
            /* Make a loop that will continue until
            no switching has been done: */
            while (switching) {
                // Start by saying: no switching is done:
                switching = false;
                rows = table.getElementsByTagName("TR");
                /* Loop through all table rows (except the
                first, which contains table headers): */
                for (i = 1; i < (rows.length - 1); i++) {
                    // Start by saying there should be no switching:
                    shouldSwitch = false;
                    /* Get the two elements you want to compare,
                    one from current row and one from the next: */
                    x = rows[i].getElementsByTagName("TD")[1];
                    y = rows[i + 1].getElementsByTagName("TD")[1];
                    // Check if the two rows should switch place:
                    s=rows[i].getElementsByTagName("TD")[2];
                    t=rows[i + 1].getElementsByTagName("TD")[2];

                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        
                        
                        shouldSwitch = true;
                        break;
                    }
                    
                }
                if (shouldSwitch) {
                    /* If a switch has been marked, make the switch
                    and mark that a switch has been done: */
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                }
            }
        }

        });
    </script>
    <style>
        .au-input--w300{
            width: 100% !important;
        }
        .table-responsive::-webkit-scrollbar { width: 0 !important }


        @media print
        {
            .noprint {display:none !important}
            html{
                margin:0 !important;
            }

            thead tr{
                border:1px solid black;
                border-radius: 5px;
            }
            tbody {
                border:1px solid black;
                border-radius: 5px;
            }

            .today{
                display:block !important;
               
            }
            
        }

    </style>
    <!-- END DATA TABLE-->
    @include('includes.search')
@endsection