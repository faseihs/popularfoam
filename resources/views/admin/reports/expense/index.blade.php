@extends('layouts.myapp')

@section('content')
<div class="row">
    <h3 class="col-md-2">Expenses</h3>
    {!! Form::open(['method'=>'POST','action'=>'AdminCashFlowController@expenseQuery','class'=>'col-md-10','id'=>'formRow']) !!}
        <div id="fRow" class="row">
                {!! Form::select('today',['all'=>'All',1=>'Today',2=>'On Date','3'=>'From Date'],$default
                ,['class'=>'form-control-sm col-md-2 noprint','id'=>'selectID'])!!}
            <button style="margin-left:5px;" class="btn btn-primary btn-sm col-md-1 noprint" type="submit">Go</button>
            
            <span style="margin-left:5px; padding:5px ;border:1px dashed black;border-radius:10px;">  Total Expense : <strong>{{number_format($total_expense,0,'.',',')}} /-</strong></span>
            

                @if(isset($on_date))
                <input id='date'  type='date' class='form-control-sm col-md-3 on_date' value="{{Carbon::parse($on_date)->format('Y-m-d')}}" name='on_date' required />
                @endif

              @if (!isset($on_date) && !isset($from_date))
              <input id='date' style="display:none;padding-left:5px;"  type='date' class='form-control-sm col-md-3 today' value="{{Carbon::now()->format('Y-m-d')}}" name='on_date' required />
              @endif
              @if(isset($from_date))
                <input  type='date' class='form-control-sm col-md-2 duration_dates' value="{{Carbon::parse($from_date)->format('Y-m-d')}}" name='on_date' required />
                <input  type='date' class='form-control-sm col-md-2 duration_dates' value="{{Carbon::parse($to_date)->format('Y-m-d')}}" name='on_date' required />

                @endif
        </div>
    {!! Form::close() !!}

</div>
{{-- @if (count($purchase_invoices)>0)
<div class="row">

        
        <div class="col-md-12 text-left"><h3>Purchase Invoices</h3></div>
    
        <div class="col-12">
            <div class="table-responsive table-responsive-data2">
                    <table id="tableID" class="table table-data2">
                        <thead>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Company</th>
                            <th>Total Bill</th>
                            
                        </thead>
                        <tbody>
                            @foreach ($purchase_invoices as $item)
                                <tr class="shadow">
                                    <td>{{$item->id}}</td>
                                    <td>{{Carbon::parse($item->date)->format('d-m-Y')}}</td>
                                    <td>{{$item->company->name}}</td>
                                    <td >{{$item->total_price}} /-</td>
                                </tr>
                            @endforeach

                            <tr class="shadow">
                                <td colspan="3"></td>
                                <td colspan="1"> Total : <strong>{{$pi_total}} /-</strong></td>
                            </tr>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
@else
 <div class="row"><div class="col-md-12 text-left"><h3>No Purchase Invoices</h3></div></div>
@endif
<hr> --}}
@if (count($transactions)>0)
<div class="row">

       
        <div class="col-md-12 text-left"><h3>Other (Shop) Expenses</h3></div>
        <div class="col-12">
            <div class="table-responsive table-responsive-data2">
                    <table id="tableID" class="table table-data2">
                        <thead>
                            <th>Description</th>
                            <th>Date</th>
                            <th>Amount</th>
                             
                        </thead>
                        <tbody>
                                @foreach ($transactions as $item)
                                    <tr class="shadow">
                                        <td>{{$item->comments}}</td>
                                        <td>{{Carbon::parse($item->date)->format('d-m-Y')}}</td>
                                        <td >{{number_format($item->amount,0,'.',',')}} /-</td>
                                    </tr>
                                @endforeach
    
                                <tr class="shadow">
                                    <td colspan="2"></td>
                                    <td colspan="1"> Total : <strong>{{number_format($t_total,0,'.',',')}} /-</strong></td>
                                </tr>
                            </tbody>
                    </table>
            </div>
        </div>
    </div>
@else
    <div class="row"><div class="col-md-12 text-left"><h3>No Shop Expenses</h3></div></div>
@endif

<hr>

@if (count($salary_payments)>0)
    <div class="row">
        <div class="col-md-12 text-left"><h3>Salary Payments</h3></div>           
            <div class="col-12">
                <div class="table-responsive table-responsive-data2">
                        <table id="tableID" class="table table-data2">
                            <thead>
                                <th>Date</th>
                                <th>Worker</th>
                                <th>Amount</th>
                            </thead>
                            <tbody>
                                    @foreach ($salary_payments as $item)
                                        <tr class="shadow">
                                           
                                            <td>{{Carbon::parse($item->date)->format('d-m-Y')}}</td>
                                            <td>{{$item->worker->name}}</td>
                                            <td >{{number_format($item->amount,0,'.',',')}} /-</td>
                                        </tr>
                                    @endforeach
        
                                    <tr class="shadow">
                                        <td colspan="2"></td>
                                        <td colspan="1"> Total : <strong>{{number_format($sp_total,0,'.',',')}} /-</strong></td>
                                    </tr>
                                </tbody>
                        </table>
                </div>
            </div>
        </div>
        @else
        <div class="row"><div class="col-md-12 text-left"><h3>No Salary Payments</h3></div></div>
@endif

@if (count($sales_returns)>0)
    <div class="row">
        <div class="col-md-12 text-left"><h3>Sales Returns</h3></div>           
            <div class="col-12">
                <div class="table-responsive table-responsive-data2">
                        <table id="tableID" class="table table-data2">
                            <thead>
                                <th>ID</th>
                                <th>Date</th>
                                <th>Customer</th>
                                <th>Amount</th>
                            </thead>
                            <tbody>
                                    @foreach ($sales_returns as $item)
                                        <tr class="shadow">
                                            <td><a target="_blank" href="/admin/sales_return/{{$item->id}}">{{$item->id}}</a></td>
                                            <td>{{Carbon::parse($item->date)->format('d-m-Y')}}</td>
                                            <td>{{$item->cust}}</td>
                                            <td >{{number_format($item->total_price,0,'.',',')}} /-</td>
                                        </tr>
                                    @endforeach
        
                                    <tr class="shadow">
                                        <td colspan="2"></td>
                                        <td colspan="1"> Total : <strong>{{number_format($sr_total,0,'.',',')}} /-</strong></td>
                                    </tr>
                                </tbody>
                        </table>
                </div>
            </div>
        </div>
        @else
        <div class="row"><div class="col-md-12 text-left"><h3>No Sales Returns</h3></div></div>
@endif


@if (count($payments)>0)
<div class="row">

       
        <div class="col-md-12 text-left"><h3>Payments</h3></div>
        <div class="col-12">
            <div class="table-responsive table-responsive-data2">
                    <table id="tableID" class="table table-data2">
                        <thead>
                            <th>ID</th>
                            <th>Company/Customer</th>
                            <th>Date</th>
                            <th>Amount</th>
                             
                        </thead>
                        <tbody>
                                @foreach ($payments as $item)
                                    <tr class="shadow">
                                        <td><a target="_blank" href="/admin/@php 
                                        if($item->owner_type=='App\Company')
                                        echo 'company_payment';
                                        else echo 'customer_payment';
                                        @endphp/{{$item->id}}">{{$item->id}}</a></td>
                                        <td>{{$item->owner->name}}</td>
                                        <td>{{Carbon::parse($item->date)->format('d-m-Y')}}</td>
                                        <td >{{number_format($item->amount,0,'.',',')}} /-</td>
                                    </tr>
                                @endforeach
    
                                <tr class="shadow">
                                    <td colspan="2"></td>
                                    <td colspan="1"> Total : <strong>{{number_format($p_total,0,'.',',')}} /-</strong></td>
                                </tr>
                            </tbody>
                    </table>
            </div>
        </div>
    </div>
@else
    <div class="row"><div class="col-md-12 text-left"><h3>No Payments</h3></div></div>
@endif

<style>
        .au-input--w300{
            width: 100% !important;
        }
        @media print
        {
            .noprint {display:none !important}
            .today{
                display: block !important;
            }
        }
</style>
<script>
var on_date= "<input style='margin-left:5px;' class='on_date' type='date' class='form-control-sm col-md-2' name='on_date' required />";
var from_date="<input style='margin-left:5px;' class='duration_dates' type='date' class='form-control-sm col-md-2' name='from_date' required /><input  class='duration_dates' type='date' class='form-control-sm' name='to_date' required />"
 
    $(document).ready(function(){
        $('#selectID').on('change',function(){

            if($(this).val()==1 || $(this).val()=='all'){
                $('.on_date').remove();
                $('.duration_dates').remove();
            }
            else if($(this).val()==2){
                $('#fRow').append(on_date);
                $('.duration_dates').remove();
            }

            else if($(this).val()==3){
                $('#fRow').append(from_date);
                $('.on_date').remove();
            }


        });
    });
</script>
@endsection