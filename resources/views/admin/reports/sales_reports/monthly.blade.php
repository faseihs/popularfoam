@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="default-tab">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link @php if(isset($active)){if($active==1) echo 'active'; } else echo 'active'; @endphp" id="nav-home-tab" href="#nav-home" data-toggle="tab"  role="tab" aria-controls="nav-home"
                       aria-selected="true">Sales Invoice</a>
                    <a class="nav-item nav-link @php if(isset($active)){if($active==2) echo 'active'; } @endphp" id="nav-profile-tab" data-toggle="tab"  href="#nav-profile" role="tab" aria-controls="nav-profile"
                       aria-selected="false">Sales Returns</a>
                    <a class="nav-item nav-link @php if(isset($active)){if($active==3) echo 'active'; } @endphp" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact"
                       aria-selected="false">Purchase Invoice</a>
                </div>
            </nav>
            <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                <div class="tab-pane fade show @php if(isset($active)){if($active==1) echo 'active'; } else echo 'active'; @endphp" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">


                    {!! Form::open(['method'=>'POST','action'=>'AdminMonthlySales@store','id'=>'invoice_form','class'=>'noprint']) !!}
                    <div id="invoiceformID" class="row">
                        <div class="form-group col-md-4">
                            <select id="sel" class="form-control-sm" name="s_option">
                                <option value="1" selected>All Months</option>
                                <option value="2">On Date</option>
                                <option value="3">During Dates</option>
                            </select>
                            <input type="submit" class="btn btn-primary btn-sm" value="Go">
                            <button id="genPdf" type="button" class="btn btn-dark btn-sm">Excel</button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                    <div class="table-responsive">
                        <table class="table table-top-campaign">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th >Date</th>
                                <th >Customer</th>
                                <th class="text-right" >Total Items</th>
                                <th class="text-right" >Total Bill</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($s_count=0)
                            @foreach($sales_invoices as $s)
                                <tr>
                                    <td><a target="_blank" href="/admin/sales_invoice/{{$s->id}}">{{$s->id}}</a></td>
                                    <td class="text-left">{{Carbon::parse($s->date)->format('d-m-Y')}}</td>
                                    <td class="text-left">{{$s->cust}}</td>
                                    <td class="text-right">{{count($s->items)}}</td>
                                    <td class="text-right">{{number_format($s->total_price,0,'.',',')}}</td>
                                </tr>
                                @php($s_count+=$s->total_price)
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div  class="col-md-2">
                            <h5 id="s_count">Total : <span>{{$s_count}}</span></h5>
                        </div>
                        <div class="col-md-6">
                            <h6 id="s_in_words">In Words : <span></span></h6>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade show @php if(isset($active)){if($active==2) echo 'active'; } @endphp" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    {!! Form::open(['method'=>'POST','action'=>'AdminMonthlySales@store','id'=>'return_form']) !!}
                    <div id="returnformID" class="row">
                        <div class="form-group col-md-3">
                            <select id="r_sel" class="form-control-sm" name="r_option">
                                <option value="1" selected>All Months</option>
                                <option value="2">On Date</option>
                                <option value="3">During Dates</option>
                            </select>
                            <input type="submit" class="btn btn-primary btn-sm" value="Go">
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="table-responsive">
                        <table class="table table-top-campaign">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Invoice ID</th>
                                <th >Date</th>
                                <th >Customer</th>
                                <th class="text-right" >Total Items</th>
                                <th class="text-right" >Total Bill</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($r_count=0)
                            @foreach($sales_returns as $s)
                                <tr>
                                    <td><a target="_blank" href="/admin/sales_return/{{$s->id}}">{{$s->id}}</a></td>
                                    <td>{{$s->sales_invoice_id}}</td>
                                    <td class="text-left">{{Carbon::parse($s->date)->format('d-m-Y')}}</td>
                                    <td class="text-left">{{$s->cust}}</td>
                                    <td class="text-right">{{count($s->items)}}</td>
                                    <td class="text-right">{{number_format($s->total_price,0,'.',',')}}</td>
                                </tr>
                                @php($r_count+=$s->total_price)
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div  class="col-md-2">
                            <h5 id="r_count">Total : <span>{{$r_count}}</span></h5>
                        </div>
                        <div class="col-md-6">
                            <h6 id="r_in_words">In Words : <span></span></h6>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade show @php if(isset($active)){if($active==3) echo 'active'; } else echo 'active'; @endphp" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                    {!! Form::open(['method'=>'POST','action'=>'AdminMonthlySales@store','id'=>'purchase_form']) !!}
                    <div id="purchaseformID" class="row">
                        <div class="form-group col-md-3">
                            <select id="p_inv" class="form-control-sm" name="p_option">
                                <option value="1" selected>All Months</option>
                                <option value="2">On Date</option>
                                <option value="3">During Dates</option>
                            </select>
                            <input type="submit" class="btn btn-primary btn-sm" value="Go">
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="table-responsive">
                        <table class="table table-top-campaign">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th >Date</th>
                                <th >Company</th>
                                <th class="text-right" >Total Items</th>
                                <th class="text-right" >Total Bill</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($p_count=0)
                            @foreach($purchase_invoices as $s)
                                <tr>
                                    <td><a target="_blank" href="/admin/purchase_invoice/{{$s->id}}">{{$s->id}}</a></td>
                                   
                                    <td class="text-left">{{Carbon::parse($s->date)->format('d-m-Y')}}</td>
                                    <td class="text-left">{{$s->company->name}}</td>
                                    <td class="text-right">{{count($s->items)}}</td>
                                    <td class="text-right">{{number_format($s->total_price,0,'.',',')}}</td>
                                </tr>
                                @php($p_count+=$s->total_price)
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div  class="col-md-2">
                            <h5 id="p_count">Total : <span>{{$p_count}}</span></h5>
                        </div>
                        <div class="col-md-6">
                            <h6 id="p_in_words">In Words : <span></span></h6>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <style>
        .au-breadcrumb2 {
             padding-top: 10px;
            padding-bottom: 10px;
            min-height: 86vh;
        }
        .table-responsive{
            min-height: 60vh !important;
            max-height: 60vh !important;
            overflow-y: auto;
        }
        @media print{
            .noprint {display:none !important}
        }
    </style>
    <script src="{{asset('js/numbers.js')}}"></script>
    <script>
        var on_date='<div id="on_date" class="form-group col-md-3">\n' +
            '                            <label style="padding-right: 5px;" class="control-label" for="on_date">On</label><input required name="on_date" class="form-control-sm" type="date"/>\n' +
            '                        </div>';
        var during_dates='<div id="during_dates" class="form-group col-md-6">\n' +
            '                           <label style="padding-right: 5px;" class="control-label" for="from_date">From</label>  <input required name="from_date" class="form-control-sm" type="date"/>\n' +
            '                                    \                 <label style="padding-right: 5px;" class="control-label" for="to_date">To</label>            <input required name="to_date" class="form-control-sm" type="date"/></div>';

        var r_on_date='<div id="r_on_date" class="form-group col-md-3">\n' +
            '                            <label style="padding-right: 5px;" class="control-label" for="on_date">On</label><input required name="on_date" class="form-control-sm" type="date"/>\n' +
            '                        </div>';
        var r_during_dates='<div id="r_during_dates" class="form-group col-md-6">\n' +
            '                           <label style="padding-right: 5px;" class="control-label" for="from_date">From</label>  <input required name="from_date" class="form-control-sm" type="date"/>\n' +
            '                                    \                 <label style="padding-right: 5px;" class="control-label" for="to_date">To</label><input required name="to_date" class="form-control-sm" type="date"/></div>';
        
        
        var p_on_date='<div id="p_on_date" class="form-group col-md-3">\n' +
            '                            <label style="padding-right: 5px;" class="control-label" for="on_date">On</label><input required name="on_date" class="form-control-sm" type="date"/>\n' +
            '                        </div>';
        var p_during_dates='<div id="p_during_dates" class="form-group col-md-6">\n' +
            '                           <label style="padding-right: 5px;" class="control-label" for="from_date">From</label>  <input required name="from_date" class="form-control-sm" type="date"/>\n' +
            '                                    \                 <label style="padding-right: 5px;" class="control-label" for="to_date">To</label><input required name="to_date" class="form-control-sm" type="date"/></div>';
        $('#sel').on('change',function(){
            if($(this).val()==1){
                $('#on_date').remove();
                $('#during_dates').remove();
            }
            else if($(this).val()==2){
                $('#invoiceformID').append(on_date);
                $('#during_dates').remove();
            }

            else if($(this).val()==3){
                $('#invoiceformID').append(during_dates);
                $('#on_date').remove();
            }

        });

        $('#r_sel').on('change',function(){
            if($(this).val()==1){
                $('#r_on_date').remove();
                $('#r_during_dates').remove();
            }
            else if($(this).val()==2){
                $('#returnformID').append(r_on_date);
                $('#r_during_dates').remove();
            }

            else if($(this).val()==3){
                $('#returnformID').append(r_during_dates);
                $('#r_on_date').remove();
            }

        });
        
        $('#p_inv').on('change',function(){
            if($(this).val()==1){
                $('#p_on_date').remove();
                $('#p_during_dates').remove();
                console.log('1');
            }
            else if($(this).val()==2){
                $('#purchaseformID').append(p_on_date);
                $('#p_during_dates').remove();
                console.log('1');
            }

            else if($(this).val()==3){
                $('#purchaseformID').append(p_during_dates);
                $('#p_on_date').remove();
                console.log('1');
            }

        });
        $(document).ready(function () {
           $('#s_in_words span').html(toWords(parseInt($('#s_count span').html())).toUpperCase());
            $('#r_in_words span').html(toWords(parseInt($('#r_count span').html())).toUpperCase());
             $('#p_in_words span').html(toWords(parseInt($('#p_count span').html())).toUpperCase());

             $('#genPdf').click(function () {
                //e.preventDefault();
                 $('#invoice_form').attr('action', "/admin/print-sales-report").submit();
             });
        });



    </script>
    @include('includes.errors')
@endsection