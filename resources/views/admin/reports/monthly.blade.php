@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        {{-- {!! Form::open(['method'=>'POST','action'=>'AdminMonthlyOverall@store','id'=>'invoice_form']) !!} --}}

        <div class="card">
            <div class="card-header">
                <strong>Overall Report</strong>
            </div>

            <div class="card-body card-block">
                <div class="row text-right">
                    <div class="col-md-4">
                        <p>Total Workers : <strong>{{$worker_count}}</strong></p>
                    </div>
                    <div class="col-md-4">
                        <p>Total Worker Salaries : <strong>{{$ws}}</strong></p>
                    </div>
                    
                </div>
                <hr/>
                <div class="row text-right">
                    <div class="col-md-4">
                        <p>Total Companies : <strong>{{$company_count}}</strong></p>
                    </div>
                    <div class="col-md-4">
                        <p>Total Items : <strong>{{$item_count}}</strong></p>
                    </div>
                    
                </div>
                <hr/>
                <div class="row text-right">
                    <div class="col-md-4">
                        <p>Total Categories : <strong>{{$category_count}}</strong></p>
                    </div>
                    <div class="col-md-4">
                        <p>Total Vehicles : <strong>{{$vehicle_count}}</strong></p>
                    </div>
                    
                </div>
                <hr/>
                <div class="row text-right">
                    <div class="col-md-4">
                        <p>Total Sales Invoices : <strong>{{$si_count}}</strong></p>
                    </div>
                    <div class="col-md-4">
                        <p>Total Sales Invoices Bill (Rs) : <strong>{{$si_bill}}/-</strong></p>
                    </div>
                    
                </div>
                <hr/>
                <div class="row text-right">
                    <div class="col-md-4">
                        <p>Total Sales Returns : <strong>{{$sr_count}}</strong></p>
                    </div>
                    <div class="col-md-4">
                        <p>Total Sales Return Bill (Rs) : <strong>{{$sr_bill}}/-</strong></p>
                    </div>
                    
                </div>

                <hr/>
                <div class="row text-right">
                    <div class="col-md-4">
                        <p>Total Purchase Invoices : <strong>{{$pi_count}}</strong></p>
                    </div>
                    <div class="col-md-4">
                        <p>Total Purchase Invoices Bill (Rs) : <strong>{{$pi_bill}}/-</strong></p>
                    </div>
                    
                </div>
                <hr/>
                <div class="row text-right">
                    <div class="col-md-4">
                        <p>Total Purchase Orders : <strong>{{$po_count}}</strong></p>
                    </div>
                    <div class="col-md-4">
                        <p>Total Purchase Orders Bill (Rs) : <strong>{{$po_bill}}/-</strong></p>
                    </div>
                    
                </div>



            </div>


            <div class="card-footer">

            {{-- {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!} --}}
            </div>
        </div>
        
        {!! Form::close() !!}
    </div>
    @include('includes.errors')
@endsection