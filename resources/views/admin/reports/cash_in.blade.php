@extends('layouts.myapp')

@section('content')
<div class="row">
    <h3 class="col-md-2">Cash In</h3>
    {!! Form::open(['method'=>'POST','action'=>'AdminCashFlowController@cashQuery','class'=>'col-md-10','id'=>'formRow']) !!}
        <div id="fRow" class="row">
                {!! Form::select('today',['all'=>'All',1=>'Today',2=>'On Date','3'=>'From Date'],$default
                ,['class'=>'form-control-sm col-md-2 noprint','id'=>'selectID'])!!}
            <button style="margin-left:5px;" class="btn btn-primary btn-sm col-md-1 noprint" type="submit">Go</button>
            
            <span style="margin-left:5px; padding:5px ;border:1px dashed black;border-radius:10px;">  Total Cash In : <strong>{{number_format($total,0,'.',',')}} /-</strong></span>
            

                @if(isset($on_date))
                <input id='date'  type='date' class='form-control-sm col-md-3 on_date' value="{{Carbon::parse($on_date)->format('Y-m-d')}}" name='on_date' required />
                @endif

              @if (!isset($on_date) && !isset($from_date))
              <input id='date' style="display:none;padding-left:5px;"  type='date' class='form-control-sm col-md-3 today' value="{{Carbon::now()->format('Y-m-d')}}" name='on_date' required />
              @endif
              @if(isset($from_date))
                <input style="margin-left:15px;" type='date' class='form-control-sm col-md-2 duration_dates' value="{{Carbon::parse($from_date)->format('Y-m-d')}}" name='on_date' required />
                <input style="margin-left:15px;"  type='date' class='form-control-sm col-md-2 duration_dates' value="{{Carbon::parse($to_date)->format('Y-m-d')}}" name='on_date' required />

                @endif
        </div>
    {!! Form::close() !!}

</div>
@if (count($sales_invoices)>0)
<div class="row">

        
        <div class="col-md-12 text-left"><h3>Sales Invoices</h3></div>
    
        <div class="col-12">
            <div class="table-responsive table-responsive-data2">
                    <table id="tableID" class="table table-data2">
                        <thead>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Total Bill</th>
                            <th>Paid Amount</th>
                            
                        </thead>
                        <tbody>
                            @foreach ($sales_invoices as $item)
                                <tr class="shadow">
                                    <td><a target="_blank" href="/admin/sales_invoice/{{$item->id}}">{{$item->id}}</a></td>
                                    <td>{{Carbon::parse($item->date)->format('d-m-Y')}}</td>
                                   
                                    <td>{{$item->cust}}</td>
                                    <td>{{number_format($item->total_price,0,'.',',')}}</td>
                                    <td >{{number_format($item->remaining?$item->remaining:($item->paid_amount?$item->paid_amount:'0'),0,'.',',')}} /-</td>
                                </tr>
                            @endforeach

                            <tr class="shadow">
                                <td colspan="4"></td>
                                <td colspan="1"> Total : <strong>{{number_format($si_total,0,'.',',')}} /-</strong></td>
                            </tr>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
@else
 <div class="row"><div class="col-md-12 text-left"><h3>No Sales Invoices</h3></div></div>
@endif


<hr>
@if (count($pr)>0)
<div class="row">

        
        <div class="col-md-12 text-left"><h3>Purchase Return</h3></div>
    
        <div class="col-12">
            <div class="table-responsive table-responsive-data2">
                    <table id="tableID" class="table table-data2">
                        <thead>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Total Bill</th>
                            
                        </thead>
                        <tbody>
                            @foreach ($pr as $item)
                                <tr class="shadow">
                                    <td><a target="_blank" href="/admin/purchase_return/{{$item->id}}">{{$item->id}}</a></td>
                                    <td>{{Carbon::parse($item->date)->format('d-m-Y')}}</td>
                                    
                                    <td>{{number_format($item->total_price,0,'.',',')}} /-</td>
                                   
                                </tr>
                            @endforeach

                            <tr class="shadow">
                                <td colspan="2"></td>
                                <td colspan="1"> Total : <strong>{{number_format($pr_total,0,'.',',')}} /-</strong></td>
                            </tr>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
@else
 <div class="row"><div class="col-md-12 text-left"><h3>No Purchase Returns</h3></div></div>
@endif




<hr>
@if (count($orders)>0)
<div class="row">

        
        <div class="col-md-12 text-left"><h3>Order Bookings</h3></div>
    
        <div class="col-12">
            <div class="table-responsive table-responsive-data2">
                    <table id="tableID" class="table table-data2">
                        <thead>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Total Bill</th>
                            <th>Advance</th>
                            
                        </thead>
                        <tbody>
                            @foreach ($orders as $item)
                                <tr class="shadow">
                                    <td><a target="_blank" href="/admin/order_booking/{{$item->id}}">{{$item->id}}</a></td>
                                    <td>{{Carbon::parse($item->date)->format('d-m-Y')}}</td>
                                    <td>{{$item->cust}}
                                    </td>
                                    <td>{{number_format($item->total_price,0,'.',',')}}</td>
                                    <td >{{number_format($item->advance,0,'.',',')}} /-</td>
                                </tr>
                            @endforeach

                            <tr class="shadow">
                                <td colspan="4"></td>
                                <td colspan="1"> Total : <strong>{{number_format($o_total,0,'.',',')}} /-</strong></td>
                            </tr>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
@else
 <div class="row"><div class="col-md-12 text-left"><h3>No Order Bookings</h3></div></div>
@endif


<hr>
@if (count($daily_credits)>0)
<div class="row">

        
        <div class="col-md-12 text-left"><h3>Daily Credits</h3></div>
    
        <div class="col-12">
            <div class="table-responsive table-responsive-data2">
                    <table id="tableID" class="table table-data2">
                        <thead>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Description</th>
                            <th>Amount</th>
                            
                            
                        </thead>
                        <tbody>
                            @foreach ($daily_credits as $item)
                                <tr class="shadow">
                                    <td>{{$item->id}}</td>
                                    <td>{{Carbon::parse($item->date)->format('d-m-Y')}}</td>
                                    <td>{{$item->comments}}
                                    </td>
                                    <td>{{number_format($item->amount,0,'.',',')}}</td>
                                </tr>
                            @endforeach

                            <tr class="shadow">
                                <td colspan="3"></td>
                                <td colspan="1"> Total : <strong>{{number_format($dc_total,0,'.',',')}} /-</strong></td>
                            </tr>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
@else
 <div class="row"><div class="col-md-12 text-left"><h3>No Daily Credits</h3></div></div>
@endif

<hr>
@if (count($customer_payments)>0)
<div class="row">

        
        <div class="col-md-12 text-left"><h3>Customer Payments</h3></div>
    
        <div class="col-12">
            <div class="table-responsive table-responsive-data2">
                    <table id="tableID" class="table table-data2">
                        <thead>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Description</th>
                            <th>Amount</th>
                            
                            
                        </thead>
                        <tbody>
                            @foreach ($customer_payments as $item)
                                <tr class="shadow">
                                    <td>{{$item->id}}</td>
                                    <td>{{Carbon::parse($item->date)->format('d-m-Y')}}</td>
                                    <td>{{$item->cust}}</td>
                                    <td>{{$item->comments}}
                                    </td>
                                    <td>{{number_format($item->amount,0,'.',',')}}</td>
                                </tr>
                            @endforeach

                            <tr class="shadow">
                                <td colspan="4"></td>
                                <td colspan="1"> Total : <strong>{{number_format($cp_total,0,'.',',')}} /-</strong></td>
                            </tr>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
@else
 <div class="row"><div class="col-md-12 text-left"><h3>No Daily Credits</h3></div></div>
@endif

<style>
        .au-input--w300{
            width: 100% !important;
        }
        @media print
        {
            .noprint {display:none !important}
            .today{
                display: block !important;
            }
        }
</style>
<script>
var on_date= "<input style='margin-left:5px;' class='on_date' type='date' class='form-control-sm col-md-2' name='on_date' required />";
var from_date="<input style='margin-left:15px;' class='duration_dates' type='date' class='form-control-sm col-md-2' name='from_date' required /><input style='margin-left:15px;' class='duration_dates' type='date' class='form-control-sm' name='to_date' required />"
 
    $(document).ready(function(){
        $('#selectID').on('change',function(){

            if($(this).val()==1 || $(this).val()=='all'){
                $('.on_date').remove();
                $('.duration_dates').remove();
            }
            else if($(this).val()==2){
                $('#fRow').append(on_date);
                $('.duration_dates').remove();
            }

            else if($(this).val()==3){
                $('#fRow').append(from_date);
                $('.on_date').remove();
            }


        });
    });
</script>
@endsection