@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div id="app">
        <purchase-report-item-summary></purchase-report-item-summary>
    </div>

    <script src="{{asset('js/app.js?v=1')}}"></script>
    <style>
        .au-breadcrumb2 {
            padding-top: 10px;
            padding-bottom: 0px;
        }
    </style>
@endsection