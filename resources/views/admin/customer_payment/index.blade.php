@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <h3 class="title-5 m-b-35">Customer payments</h3>
                </div>
                <div class="col-md-5">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search customer_payment...">
                </div>
                <div class="table-data__tool-right col-md-4">
                    <a id="add" href="/admin/customer_payment/create" class="au-btn au-btn-icon au-btn--green au-btn--small float-right">
                        <i class="zmdi zmdi-plus"></i>add Customer payment</a>
                </div>
            </div>
        </div>

    </div>
    @if(count($customer_payments)==0)
        <h1 class="text-md-center">No customer_payments Found</h1>
    @endif
    @if(count($customer_payments)>0)
        <div class="table-responsive table-responsive-data2">
            <table id="tableID" class="table table-data2">
                <thead style="text-align: center!important">
                <tr>
                    <th>ID</th>
                    <th>name</th>
                    <th>Paid Later</th>
                    <th>Amount</th>
                    <th>Type</th>
                    <th>updated</th>
                    <th>created</th>
                    <th></th>
                </tr>
                </thead>
                <tbody style="text-align: center!important">
                @foreach($customer_payments as $customer_payment)
                    <tr class="tr-shadow">
                        <td style="vertical-align: middle"><a target="_blank" href="/admin/customer_payment/{{$customer_payment->id}}">{{$customer_payment->id}}</a></td>
                        <td>{{$customer_payment->owner?$customer_payment->owner->name:'Walking Customer'}}</td>
                        <td class="desc">{{$customer_payment->pay_later==0?'No':'Yes'}}</td>
                        <td>{{$customer_payment->amount}}</td>
                        <td>{{$customer_payment->type}}</td>
                        <td>
                            {{($customer_payment->updated_at)->format('d-m-Y')}}
                        </td>
                        <td>
                            {{($customer_payment->created_at)->format('d-m-Y')}}
                        </td>
                        <td>
                            <div class="table-data-feature">
                                <form id="del{{$customer_payment->id}}" action="/admin/customer_payment/{{$customer_payment->id}}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                    <a href="#" onclick="clicked({{$customer_payment->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </a>
                                </form>
                                <a href="/admin/customer_payment/{{$customer_payment->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                    </button>

                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @endif
        </div>
        </div>
        <script>
            function clicked(id){
                if(confirm("Are You Sure ?")){
                    document.getElementById('del'+id).submit();
                }
                else{
                }
            }
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#add').click();
                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });
        </script>
        <!-- END DATA TABLE-->
        @include('includes.search');
@endsection