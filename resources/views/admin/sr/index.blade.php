@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">Sales Returns</h3>
                </div>
                <div class="col-md-5">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search...">
                </div>
                <div class="table-data__tool-right col-md-4">
                    <button id="add" type="button" class="btn btn-primary mb-1 float-right" data-toggle="modal" data-target="#scrollmodal">
                        Add Sales Return
                    </button>
                </div>
            </div>

        </div>

        @if(count($sales_returns)==0)
            <h1 class=" col-md-12 text-md-center">No Sales Returns Found</h1>
        @endif
        @if(count($sales_returns)>0)

            <div id="tableID" class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead>
                    <tr>
                        <th>Return Date</th>
                        <th>ID</th>
                        <th>Customer Category</th>
                        <th>Customer</th>
                        <th>Customer Number</th>
                        <th class="text-right">Total Bill</th>
                        <th>Created Date</th>
                        <th>Created By</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sales_returns as $sales_return)
                        <tr class="shadow">
                            <td>{{Carbon::parse($sales_return->date)->format('d-m-Y')}}</td>
                            <td>{{$sales_return->id}}</td>
                            <td>{{$sales_return->cust}}</td>
                            <td>{{$sales_return->name}}</td>
                            <td>{{$sales_return->number}}</td>
                            <td class="text-right">{{$sales_return->total_price}}</td>

                            <td>{{$sales_return->created_at}}</td>
                            <td>{{$sales_return->user?$sales_return->user->name:'-'}}</td>
                            <td>
                                <a class="btn btn-info btn-sm" href="/admin/sales_return/{{$sales_return->id}}">View</a>
                                <a target="_blank" class="btn btn-dark btn-sm"  
                                    
                                    
                                    href="/admin/print_option/salesInvoice/{{$sales_return->id}}?print_url={{urlencode("/admin/sales_return/print/$sales_return->id")}}"
                                    >Print</a>

                            </td>
                            <td>
                                <div class="table-data-feature">
                                    <form id="del{{$sales_return->id}}" action="/admin/sales_return/{{$sales_return->id}}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        <a href="#" onclick="clicked({{$sales_return->id}})"  class="sales_return" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                    </form>
                                    <a href="/admin/sales_return/{{$sales_return->id}}/edit" class="sales_return" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>

                                </div>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        @endif

        <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        Select Customer
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['method'=>'POST','action'=>'AdminSalesReturnController@createC']) !!}
                        <div class="form-group">

                            {!! Form::select('customer_id',[0=>'Walking Customer']+$customers,null,['class'=>'form-control','required'])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Submit',['class'=>'btn btn-default'])!!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <div class="d-flex justify-content-around">
                            <a href="/admin/sales_invoice/{id}" class="p2"></a>
                            <button type="button" class="btn btn-secondary p2" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            function clicked(id){
                if(confirm("Are You Sure ?")){
                    document.getElementById('del'+id).submit();
                }
                else{
                }
            }
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#add').click();

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });
        </script>

    </div>       <!-- END DATA TABLE-->
    @include('includes.search')
@endsection