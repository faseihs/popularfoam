@extends('layouts.myapp')

@section('content')
    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">Accounts</h3>
                </div>
                <div class="col-md-6">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search account...">
                </div>
                <div class="table-data__tool-right col-md-4">
                    <a id="add" href="/admin/account/create" class="au-btn au-btn-icon au-btn--green au-btn--small float-right">
                        <i class="zmdi zmdi-plus"></i>add account</a>
                </div>
            </div>
        </div>
    </div>
    <h3>Head Accounts</h3>
    @if(count($accounts)==0)
        <h1 class="text-md-center">No Head Accounts Found</h1>
    @endif
    @if(count($headAccounts)>0)
        <div class="table-responsive table-responsive-data2">
            <table id="tableID" class="table table-data2">
                <thead>
                <tr>

                    <th>id</th>
                    <th>name</th>
                    <th>Type</th>
                    <th>Debit</th>
                    <th>Credit</th>
                    <th>Head Account</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($headAccounts as $account)
                    <tr class="tr-shadow">

                        <td><a target="_blank" href="/admin/account/{{$account->id}}">{{$account->id}}</a></td>
                        <td class="desc"><a target="_blank" href="/admin/account/{{$account->id}}">{{$account->name}}</a></td>
                        <td>{{ucfirst($account->type)}}</td>
                        <td>{{ucfirst($account->debit)}}</td>
                        <td>{{ucfirst($account->credit)}}</td>
                      <td>{{$account->head?$account->head->name:'-'}}</td>
                        <td>
                            <div class="table-data-feature">
                                @if($account->can_delete)


                                <form id="del{{$account->id}}" action="/admin/account/{{$account->id}}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                    <a href="#" onclick="clicked({{$account->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </a>
                                </form>
                                @endif
                                <a href="/admin/account/{{$account->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                    </a>

                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @endif

    <h3>Sub Accounts</h3>
    @if(count($accounts)==0)
        <h1 class="text-md-center">No Sub-Accounts Found</h1>
    @endif
    @if(count($accounts)>0)
        <div class="table-responsive table-responsive-data2">
            <table id="tableID" class="table table-data2">
                <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>Type</th>
                    <th>Debit</th>
                    <th>Credit</th>
                    <th>Head Account</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($accounts as $account)
                    <tr class="tr-shadow">

                        <td><a target="_blank" href="/admin/account/{{$account->id}}">{{$account->id}}</a></td>
                        <td class="desc"><a target="_blank" href="/admin/account/{{$account->id}}">{{$account->name}}</a></td>
                        <td>{{ucfirst($account->type)}}</td>
                       <td>
                           @if($account->head)
                               {{ucfirst($account->head->debit)}}
                           @endif
                       </td>
                        <td>
                            @if($account->head)
                                {{ucfirst($account->head->credit)}}
                            @endif
                        </td>
                        <td>{{$account->head?$account->head->name:'-'}}</td>

                        <td>
                            <div class="table-data-feature">
                                <form id="del{{$account->id}}" action="/admin/account/{{$account->id}}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                    <a href="#" onclick="clicked({{$account->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </a>
                                </form>
                                <a href="/admin/account/{{$account->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                    </a>

                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @endif
        </div>

        <script>
            function clicked(id){
                if(confirm("Are You Sure ?")){
                    document.getElementById('del'+id).submit();
                }
                else{
                }
            }
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#add').click();
                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });
        </script>
        <!-- END DATA TABLE-->
        @include('includes.search');
@endsection