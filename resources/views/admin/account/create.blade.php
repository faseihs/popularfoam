@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::open(['method'=>'POST','action'=>'AdminAccountController@store','class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('name','Title:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Title'])!!}

                    </div>
                </div>
                {{--<div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('type','Account Type:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('type',['debit'=>'Debit','credit'=>'Credit'],null,['class'=>'form-control','placeholder'=>'Select Type'])!!}

                    </div>
                </div>--}}
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('head_id','Head Account:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('head_id',$accounts,null,['class'=>'form-control select2','placeholder'=>'Select Account','required'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('opening','Opening:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::number('opening',null,['class'=>'form-control','placeholder'=>'Enter Opening Amount'])!!}

                    </div>
                </div>
               {{-- <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('openingDate','Opening Date:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::date('openingDate',null,['class'=>'form-control'])!!}

                    </div>
                </div>--}}


                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('comments','Comments:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::textarea('comments',null,['class'=>'form-control','placeholder'=>'Any Comments(Optional)','rows'=>5])!!}

                    </div>
                </div>
            </div>
            <div class="card-footer">

                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    @include('includes.select2')
    @include('includes.errors')
@endsection