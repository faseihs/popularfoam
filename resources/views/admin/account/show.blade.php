@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Account Details</strong>
            </div>
           
            <div class="card-body card-block">
                    <div class="row">
                            <div class="col-md-2">
                                Title :
                            </div>
                            <div class="col-md-5 font-weight-bold">
                                {{$account->name}}
                            </div>
                        <div class="col-md-5">
                            {!! Form::open(['method'=>'POST','action'=>['AdminAccountController@query',$account->id]]) !!}
                             {!! Form::date('date',isset($date)?$date->format('Y-m-d'):null,['class'=>'form-control-sm']) !!}
                                <button type="submit" class="btn btn-secondary btn-sm">Go</button>
                            {!! Form::close() !!}
                        </div>
                        </div>
                        <div class="row">
                                <div class="col-md-2">
                                    Type :
                                </div>
                                <div class="col-md-2 font-weight-bold">
                                    {{$account->type}}
                                </div>
                                
                            </div>
                <hr>
                @if (count($account->payment)>0)
                    
                
                <table class="table">
                    
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Type</th>
                            <th>Customer/Company</th>
                            <th>Amount</th>    
                            <th>Pay Later</th>
                        </tr>    
                    </thead>  
                    <tbody>
                            @php($total=0)
                        @foreach ($payments as $item)
                        @php($total+=$item->amount)
                        <tr>
                            <td><a target="_blank" href="/admin/@php 
                                if($item->owner_type=='App\Company')
                                echo 'company_payment';
                                else echo 'customer_payment';
                                @endphp/{{$item->id}}">{{$item->id}}</a></td>
                                
                                <td>{{Carbon::parse($item->date)->format('d-m-Y')}}</td>
                                <td>{{$item->owner_type=='App\Company'?'Company':'Customer'}}</td>
                                <td>{{$item->owner?$item->owner->name:'NA'}}</td>
                                <td>{{number_format($item->amount,0,'.',',')}}/-</td>
                                <td>{{$item->pay_later==1?'Yes':'No'}}</td>
                        </tr>
                            
                        @endforeach    
                        <tr>
                            <td colspan="4"></td>
                            <td class="font-weight-bold">Total : {{number_format($total,0,'.',',')}} /-</td>    
                        </tr>
                    </tbody>  
                </table>
                @else
                    <h3 class="text-center">No Payments</h3>
                @endif    
            </div>
            <div class="card-footer">

            </div>
            
        </div>
    </div>
    @include('includes.errors')
@endsection