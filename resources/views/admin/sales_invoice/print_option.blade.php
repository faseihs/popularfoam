@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
           <form method="GET" action='{{$url}}'>
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('print_size','Print Size:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                    {!! Form::select('print_size',["a4"=>'A4','80'=>"80mm"],'a4',['class'=>'form-control','required','id'=>'print_size'])!!}

                    </div>
                </div>

            </div>
            <div class="card-footer">

                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
            </div>
           </form>
        </div>
    </div>
    @include('includes.errors')
@endsection