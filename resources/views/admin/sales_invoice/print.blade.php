<head>    <link href="{{ asset('vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">
<title>Sales Invoice #{{$sales_invoice->id}}</title>
</head>
    <div class="col-12">
                <div class="row">
                    <div class="col-12 text-left">
                        <h3>{{env("SHOP_NAME")}}</h3>
                        <h6>{{env("SHOP_ADDRESS")}}</h6>
                        <h6>Phone: {{env("SHOP_NUMBER")}}</h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-center">
                    {{-- <p style="margin-bottom: 0;padding-bottom: 0"><strong style="font-size:20px;padding-left: 20px;padding-right: 10px;margin-bottom: 0;padding-bottom: 0">Popular Foam</strong>
                        </p> --}}
                        <strong style="font-size:20px;padding-left: 20px;padding-right: 10px;">Sales Invoice</strong>
                    <p style="font-size:6px;">Powered by Codiea.pk</p>
                    </div>
                </div>
    </div>
            <p >
                
                        <strong>Date :  </strong>{{Carbon::parse($sales_invoice->date)->format('d-m-Y')}}
                        <span style="float:right">Invoice ID :<strong>{{$sales_invoice->id}}</strong></span>
                
        </p>
        <p>
            {{-- @if(!$sales_invoice->name)
                 <strong> Customer Category : </strong>{{$sales_invoice->cust}}
            @endif --}}
            <strong>Customer: </strong>{{$sales_invoice->getCustomerName()}}</span>
           


            <span style="float:right">Time :<strong>{{Carbon::now()->setTimezone('Asia/Karachi')->format('g:i A')}}</strong></span>
        </p>
{{-- @if($sales_invoice->name)
    <p>
    <strong> Customer Name : </strong>{{$sales_invoice->name}}
    </p>
@endif --}}
            
            
<br>
                <div style="border:none;"  id="itemDivID" >
                    <table style="border:none;" id="itemTableID" class="table table-sm table-striped">
                        <thead style="border:none;">
                        <tr style="border:none">
                            <th>Sr.</th>
                            <th>Quality</th>
                            <th>Item</th>
                            
                            <th class="text-right">Qty</th>
                            <th class="text-right">Unit</th>
                            
                            <th class="text-right">Price</th>
                            
                            <th class="text-right">Amount</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            @php($count=0)
                        @foreach($items as $item)
                            <tr id="{{$item->item->id}}">
                                <td>@php($count++){{$count}}</td>
                                <td>{{$item->item->quality->name}}</td>
                                <td>{{$item->item->name}}</td>
                                <td class="text-right quantity">{{$item->quantity}}</td>
                                <td class="text-right quantity">{{$item->item->package_unit}}</td>
                                <td class="text-right price">{{$item->price}}/-</td>


                                <td class="text-right total">{{$item->total_price}}/-</td>



                            </tr>
                        @endforeach
                        <tr style="border-top:1px solid black;border-bottom:1px solid black;" class="lastRow">
                            <td colspan="5" class="text-right" style="font-size:13px;"> Paid : <strong>{{number_format($sales_invoice->paid_amount,0,'.',',')}} /-</strong></td>
                            <td  style="font-size:13px;"  class="text-right" colspan="2">Total : <strong>{{number_format($sales_invoice->total_price,0,'.',',')}} /-</strong>
                                
                                @if ($sales_invoice->returned>0)
                                <div style="display: block;">
                                    Ret : <strong>{{$sales_invoice->returned}} /-</strong>
                                </div>
                                @endif
                                
                                @if (($sales_invoice->paid_amount + $sales_invoice->discount)<$sales_invoice->total_price)

                                <div style="display:block">
                                    Rem : <strong>{{$sales_invoice->total_price - $sales_invoice->paid_amount}} /-</strong>
                                </div>
                                @endif
                                @if ($sales_invoice->discount)
                                    <div style="display:block">
                                        Disc : <strong>{{$sales_invoice->discount}} /-</strong>
                                    </div>
                                    
                                @endif
                            
                            </td>
                           
                        </tr>

                        </tbody>
                    </table>
                </div>

            
            


        
        
    


    <!-- modal scroll -->
    

   
    <style type="text/css" media="all">
        html{
           /* margin-top: 1.5in !important;
            font-size: 12px;
            margin-bottom: 1.5in !important;
            margin-right:0.75in !important;
            margin-left: 0.75in !important;*/
        }
        

        @page{
           /* height: 9.5in !important;
            width: 7in !important;*/
        }
         #itemDivID{
            padding: 0;
            margin: 0;
        } 
        table{
            border:none !important;
            
            padding: 0 !important;
            margin:0 !important;
        }
        tr{
            margin:0 !important;
            padding: 0 !important;
        }
        table td,th{
            padding: 0;
        }

        .amount{
            
            bottom:10px !important;
            left:70% !important;
            position: absolute;
           
           
        }
        .time{
            bottom:10px !important;
            
            position: absolute;
        }

        .lastRow{
            border:1px solid black !important;
        }

        
    </style>


    
    
