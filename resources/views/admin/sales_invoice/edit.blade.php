@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->
    <div id="point-of-sales">
        <vue-app id="{{$sales_invoice->id}}" Thistype="si" ></vue-app>
    </div>

    <script src="{{asset('js/point-of-sales.js?v=9')}}"></script>
    <style>
        .au-breadcrumb2 {
            padding-top: 10px;
            padding-bottom: 0px;
        }
        .page-content--bgf7{
            background: white !important;
        }
    </style>
@endsection