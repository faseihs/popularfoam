@extends('layouts.myapp')
{{--@section('styles')

    <link rel="stylesheet" type="text/css" href="{{asset('js/DataTables/datatables.min.css')}}">

    <script type="text/javascript" charset="utf8" src="{{asset('js/DataTables/datatables.min.js')}}"></script>

@endsection--}}

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">Sales Invoices</h3>
                </div>
                <div class="table-data__tool-right col-md-4">
                    <button id="add" type="button" class="btn btn-primary mb-1 float-right" data-toggle="modal" data-target="#scrollmodal">
                        Add Sales Invoice
                    </button>
                </div>
            </div>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link {{$defaultView=='today'?'active':''}}" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home"
                       aria-selected="true">Today</a>
                    <a class="nav-item nav-link {{$defaultView=='previous'?'show active':''}}" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile"
                       aria-selected="false">Previous</a>
                       <a class="nav-item nav-link" id="nav-ob-tab" data-toggle="tab" href="#ob" role="tab" aria-controls="nav-profile"
                       aria-selected="false">Order Bookings</a>
                    <a class="nav-item nav-link" id="nav-ob-tab" data-toggle="tab" href="#not-paid" role="tab" aria-controls="nav-not-paid"
                       aria-selected="false">Payment Pending</a>

                </div>
            </nav>
            <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                <div class="tab-pane fade {{$defaultView=='today'?'show active':''}}" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    @if(count($sales_invoices_today)==0)
                        <h1 class="text-md-center">No Sales Invoices Found</h1>
                    @endif
                    @if(count($sales_invoices_today)>0)

                        <div id="tableID" class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Invoice Date</th>

                                    <th>Customer</th>
                                    <th class="text-right">Total Bill</th>
                                    <th class="text-right">Paid</th>
                                    <th class="text-right">Balance</th>
                                    <th ><span data-toggle="tooltip" data-placement="top" title="Simple/Order Booking Invoice">Type <i class="fa fa-question"></i></span></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php($counter1=0)
                                    @php($counter2=0)
                                @foreach($sales_invoices_today as $sales_invoice)
                                @php($counter1+=$sales_invoice->total_price)
                                @php($counter2+=$sales_invoice->paid_amount)
                                    <tr class="shadow {{$sales_invoice->allPaid()?'':'red'}}">
                                        <td>{{$sales_invoice->id}}</td>
                                        <td>{{Carbon::parse($sales_invoice->date)->format('d-m-Y')}}</td>
                                        <td>{{$sales_invoice->cust}}</td>
                                        <td class="text-right">{{number_format($sales_invoice->total_price,0,'.',',')}} /-</td>
                                        <td class="text-right">{{number_format($sales_invoice->paid_amount,0,'.',',')}} /-</td>
                                        <td class="text-right">{{number_format($sales_invoice->total_price-$sales_invoice->paid_amount,0,'.',',')}} /-</td>

                                        <td>{{$sales_invoice->order_booking_id?'Order Booking':'Simple'}}</td>
                                        <td><a class="btn btn-info btn-sm" href="/admin/sales_invoice/{{$sales_invoice->id}}">View</a>
                                            <button onclick="openPrintModal('{{$sales_invoice->id}}')" class="btn btn-dark btn-sm" href="/admin/print/salesInvoice/{{$sales_invoice->id}}">Print</button></td>


                                        <td>
                                            <div class="table-data-feature">
                                                <form id="del{{$sales_invoice->id}}" action="/admin/sales_invoice/{{$sales_invoice->id}}" method="POST">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                                    <a href="#" onclick="clicked({{$sales_invoice->id}})"  class="sales_invoice" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </a>
                                                </form>
                                                <a href="/admin/sales_invoice/{{$sales_invoice->id}}/edit" class="sales_invoice" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </a>

                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                <tr class="blue">
                                    <td colspan="3" class="text-right"><strong>Total :</strong></td>
                                    <td class="text-right">{{number_format($counter1,0,'.',',')}} /-</td>
                                    <td class="text-right">{{number_format($counter2,0,'.',',')}} /-</td>
                                    <td colspan="3"></td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    @endif
                </div>
                <div class="tab-pane fade {{$defaultView=='previous'?'show active':''}}" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    @if(count($sales_invoices_previous)==0)
                        <h1 class="text-md-center">No Sales Invoices Found</h1>
                    @endif
                    @if(count($sales_invoices_previous)>0)

                        <div id="tableID" class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Invoice Date</th>

                                    <th>Customer</th>
                                    <th class="text-right">Total Bill</th>
                                    <th class="text-right">Paid</th>
                                    <th class="text-right">Balance</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sales_invoices_previous as $sales_invoice)
                                    <tr class="shadow {{$sales_invoice->allPaid()?'':'red'}}">
                                        <td>{{$sales_invoice->id}}</td>
                                        <td>{{Carbon::parse($sales_invoice->date)->format('d-m-Y')}}</td>
                                        <td>{{$sales_invoice->cust}}</td>
                                        <td class="text-right">{{$sales_invoice->total_price}} /-</td>
                                        <td class="text-right">{{$sales_invoice->paid_amount}} /-</td>
                                        <td class="text-right">{{$sales_invoice->Balance()}} /-</td>

                                        <td><a class="btn btn-info btn-sm" href="/admin/sales_invoice/{{$sales_invoice->id}}">View</a>
                                            <button onclick="openPrintModal('{{$sales_invoice->id}}')" class="btn btn-dark btn-sm" href="/admin/print/salesInvoice/{{$sales_invoice->id}}">Print</button></td>


                                        <td>
                                            <div class="table-data-feature">
                                                <form id="del{{$sales_invoice->id}}" action="/admin/sales_invoice/{{$sales_invoice->id}}" method="POST">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                                    <a href="#" onclick="clicked({{$sales_invoice->id}})"  class="sales_invoice" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </a>
                                                </form>
                                                <a href="/admin/sales_invoice/{{$sales_invoice->id}}/edit" class="sales_invoice" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </a>

                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <hr>
                            {{-- {{
                            $sales_invoices_previous->links( "pagination::bootstrap-4")
                            }} --}}

                        </div>
                    @endif
                </div>
                <div class="tab-pane fade" id="ob" role="tabpanel" aria-labelledby="nav-profile-tab">
                    @if(count($order_bookings)==0)
                        <h1 class="text-md-center">No Order Bookings Found</h1>
                    @endif
                    @if(count($order_bookings)>0)

                        <div id="tableID" class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Order ID</th>
                                    <th>Invoice Date</th>

                                    <th>Customer</th>
                                    <th class="text-right">Total Bill</th>
                                    <th class="text-right">Paid</th>
                                    <th class="text-right">Balance</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($order_bookings as $sales_invoice)
                                    <tr class="shadow {{$sales_invoice->allPaid()?'':'red'}}">
                                        <td>{{$sales_invoice->id}}</td>
                                        <td>{{$sales_invoice->order_booking_id}}</td>
                                        <td>{{Carbon::parse($sales_invoice->date)->format('d-m-Y')}}</td>
                                        <td>{{$sales_invoice->cust}}</td>
                                        <td class="text-right">{{$sales_invoice->total_price}} /-</td>
                                        <td class="text-right">{{$sales_invoice->paid_amount}} /-</td>
                                        <td class="text-right">{{$sales_invoice->Balance()}} /-</td>

                                        <td><a class="btn btn-info btn-sm" href="/admin/sales_invoice/{{$sales_invoice->id}}">View</a>
                                            <button onclick="openPrintModal('{{$sales_invoice->id}}')"  class="btn btn-dark btn-sm" href="/admin/print/salesInvoice/{{$sales_invoice->id}}">Print</button></td>


                                        <td>
                                            <div class="table-data-feature">
                                                <form id="del{{$sales_invoice->id}}" action="/admin/sales_invoice/{{$sales_invoice->id}}" method="POST">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                                    <a href="#" onclick="clicked({{$sales_invoice->id}})"  class="sales_invoice" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </a>
                                                </form>
                                                <a href="/admin/sales_invoice/{{$sales_invoice->id}}/edit" class="sales_invoice" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </a>

                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    @endif
                </div>
                <div class="tab-pane fade " id="not-paid" role="tabpanel" aria-labelledby="nav-not-paid">
                        <div id="tableID" class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Invoice Date</th>

                                    <th>Customer</th>
                                    <th class="text-right">Total Bill</th>
                                    <th class="text-right">Paid</th>
                                    <th class="text-right">Balance</th>
                                    <th ><span data-toggle="tooltip" data-placement="top" title="Simple/Order Booking Invoice">Type <i class="fa fa-question"></i></span></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($total=0)
                                @php($paid=0)
                                @php($balance=0)
                                @foreach($salesInvoicesAll as $sales_invoice)
                                    @if(!$sales_invoice->allPaid() && $sales_invoice->customer_id==0)
                                        @php($total+=$sales_invoice->total_price)
                                        @php($paid+=$sales_invoice->paid_amount)
                                        @php($balance+=$sales_invoice->Balance())
                                        <tr class="shadow">
                                            <td>{{$sales_invoice->id}}</td>
                                            <td>{{Carbon::parse($sales_invoice->date)->format('d-m-Y')}}</td>
                                            <td>{{$sales_invoice->cust}}</td>
                                            <td class="text-right">{{number_format($sales_invoice->total_price,0,'.',',')}} /-</td>
                                            <td class="text-right">{{number_format($sales_invoice->paid_amount,0,'.',',')}} /-</td>
                                            <td class="text-right">{{number_format($sales_invoice->Balance(),0,'.',',')}} /-</td>

                                            <td>{{$sales_invoice->order_booking_id?'Order Booking':'Simple'}}</td>
                                            <td><a class="btn btn-info btn-sm" href="/admin/sales_invoice/{{$sales_invoice->id}}">View</a>
                                                <button onclick="openPrintModal('{{$sales_invoice->id}}')"  class="btn btn-dark btn-sm" href="/admin/print/salesInvoice/{{$sales_invoice->id}}">Print</button></td>


                                            <td>
                                                <div class="table-data-feature">
                                                    <form id="del{{$sales_invoice->id}}" action="/admin/sales_invoice/{{$sales_invoice->id}}" method="POST">
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                                        <a href="#" onclick="clicked({{$sales_invoice->id}})"  class="sales_invoice" data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </a>
                                                    </form>
                                                    <a href="/admin/sales_invoice/{{$sales_invoice->id}}/edit" class="sales_invoice" data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </a>

                                                </div>

                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                <tr class="blue">
                                    <td colspan="3" class="text-right"><strong>Total :</strong></td>
                                    <td class="text-right">{{number_format($total,0,'.',',')}} /-</td>
                                    <td class="text-right">{{number_format($paid,0,'.',',')}} /-</td>
                                    <td class="text-right">{{number_format($balance,0,'.',',')}} /-</td>
                                    <td colspan="2"></td>
                                </tr>
                                </tbody>
                            </table>

                        </div>

                </div>

            </div>



        </div>

        {{--@if(count($sales_invoices)==0)
            <h1 class="text-md-center">No Sales Return Found</h1>
        @endif
        @if(count($sales_invoices)>0)

            <div id="tableID" class="table-responsive table-responsive-data2">
                <table class="table table-data2">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Invoice Date</th>

                        <th>Customer</th>
                        <th class="text-right">Total Bill</th>
                       <th ><span data-toggle="tooltip" data-placement="top" title="Simple/Order Booking Invoice">Type <i class="fa fa-question"></i></span></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sales_invoices as $sales_invoice)
                        <tr class="shadow">
                            <td>{{$sales_invoice->id}}</td>
                            <td>{{Carbon::parse($sales_invoice->date)->format('d-m-Y')}}</td>
                            <td>{{$sales_invoice->cust}}</td>
                            <td class="text-right">{{$sales_invoice->total_price}}</td>

                           <td>{{$sales_invoice->order_booking_id?'Order Booking':'Simple'}}</td>
                            <td><a class="btn btn-info btn-sm" href="/admin/sales_invoice/{{$sales_invoice->id}}">View</a>
                            <button onclick="openPrintModal('{{$sales_invoice->id}}')" class="btn btn-dark btn-sm" href="/admin/print/salesInvoice/{{$sales_invoice->id}}">Print</button></td>


                            <td>
                                <div class="table-data-feature">
                                    <form id="del{{$sales_invoice->id}}" action="/admin/sales_invoice/{{$sales_invoice->id}}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        <a href="#" onclick="clicked({{$sales_invoice->id}})"  class="sales_invoice" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                    </form>
                                    <a href="/admin/sales_invoice/{{$sales_invoice->id}}/edit" class="sales_invoice" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>

                                </div>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        @endif--}}
        <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        Select Customer
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['method'=>'POST','action'=>'AdminSalesInvoiceController@createC']) !!}
                        <div class="form-group">

                            {!! Form::select('customer_id',[0=>'Walking Customer']+$customers,null,['class'=>'form-control','required'])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Submit',['class'=>'btn btn-default'])!!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <div class="d-flex justify-content-around">
                            <a href="/admin/sales_invoice/{id}" class="p2"></a>
                            <button type="button" class="btn btn-secondary p2" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        Select Print Type
                    </div>
                    <div class="modal-body">
                       <form id="printForm" type="GET"  action="/admin/print/salesInvoice/">
                        <div class="form-group">
                            {!! Form::select('print_size',["a4"=>'A4','80mm'=>"80"],'a4',['class'=>'form-control','required','id'=>'print_size'])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Submit',['class'=>'btn btn-default'])!!}
                        </div>
                    </form>
                    </div>
                    <div class="modal-footer">
                        <div class="d-flex justify-content-around">
                            <a href="/admin/sales_invoice/{id}" class="p2"></a>
                            <button type="button" class="btn btn-secondary p2" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script>
            function clicked(id){
                if(confirm("Are You Sure ?")){
                    document.getElementById('del'+id).submit();
                }
                else{
                }
            }
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#add').click();

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            function openPrintModal(id){
                $("#printForm").attr("action",`/admin/print/salesInvoice/${id}`)
                $('#printModal').modal('toggle');
            }
        </script>

    </div>       <!-- END DATA TABLE-->
    <style>
            .blue{
                background: #0056b3;
            }
            .blue td{
                color:white !important;
            }

        .red{
            background: #ff3232;
        }
            .red td{
                color:white !important;
            }

        .red .zmdi{
            color:white !important;
        }
            
            
            </style>
    @include('includes.search')
@endsection