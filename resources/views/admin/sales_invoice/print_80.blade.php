<html>
<head>
<title>Sales Invoice #{{$sales_invoice->id}}</title>
<style>
    html,p{
        font-family: Helvetica, sans-serif
        padding: 0;
        margin: 0;
        font-size: 10px;
        letter-spacing:1px;
    }
    table,.table{
        width: 99.5%;
        margin: 0;
        padding: 0;
        margin-right: 0px;
        margin-left: 0px;
    }
    /* p{
        font-size: 12px;
    } */

    .strong{
        font-weight: bold;
    }
    

    th{
       
        text-align: left;
    }

    td{
        
        text-align: left;
    }

    .text-right{
        text-align: right;
    }
    tr{
        margin-left: 12px;
        margon-right:12px;
    }
</style>
</head>
<body>
    {{-- <p class="strong"><center>{{env("SHOP_NAME")}}</center></p> --}}
    <p ><center><img  style="width:150px;height:75px;margin-top:8px;" src="{{public_path("images/pf.jpeg")}}" alt="" srcset=""></center></p>
    
    <p class="strong"><center><strong style="border-bottom: 1px solid black;padding-bottom:8px;"> Sales Invoice</strong></center> </p>
    <br>
    <br>
    {{-- <p><center style="font-size:6px;">Powered by Codiea.pk</center></p>  --}}
    <p style="margin-left:8px; margin-right:8px;">
        <span>  <strong> Date : </strong>{{Carbon::parse($sales_invoice->date)->format('d-m-Y')}} </span>
        {{-- |  --}}

        <span style="float:right"><strong> Invoice: </strong>{{$sales_invoice->id}}</span>
        
    </p>    
    <br>
    <p style="margin-left:8px; margin-right:8px;"><span><strong>Customer: </strong>{{$sales_invoice->getCustomerName()}}</span></p>        

            
    <br>
    <div style="border:none;margin-bottom:8px;"  id="itemDivID" >
        <table  id="itemTableID" class="table table-sm table-striped">
            <thead style="border:1px solid">
            <tr style="border:none">
                <th>Sr.</th>
                <th>Quality</th>
                <th>Item</th>
                <th class="text-right" >Qty</th>
           
                <th class="text-right">Price</th>
                <th class="text-right">Amount</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                @php($count=0)
            @foreach($items as $item)
                <tr id="{{$item->item->id}}">
                     
                    <td>@php($count++){{$count}}</td>
                    <td>{{$item->item->quality->name}}</td>
                    <td>{{$item->item->name}}</td>
                    <td class="text-right quantity">{{$item->quantity}}</td>
                   
                    <td class="text-right price">{{$item->price}}/-</td>


                    <td class="text-right total">{{$item->total_price}}/-</td>



                </tr>
            @endforeach
         
            <tr>
               
                <td  style="border:1px solid black;"  class="text-right" colspan=7>
                    <span style="float:left"><strong> Time : </strong>{{Carbon::now()->setTimezone('Asia/Karachi')->format('g:i A')}}</span>
                    
                   

                    <strong>Total : </strong>{{number_format($sales_invoice->total_price,0,'.',',')}} /-

                
                </td>
                
            </tr>

            <tr style="" class="lastRow">
                
                <td colspan="7" class="text-right" style="border:1px solid black;">

                    @if ($sales_invoice->returned>0)
                
                    <span style="float:left">
                            <strong> Ret: </strong>{{$sales_invoice->returned}} /-
                        </span>
                      
                
                    @endif
                    
                   
                    @if ($sales_invoice->discount)
                    <span style="float:left">
                            <strong>Disc : </strong>{{$sales_invoice->discount}} /-
                    </span>
                        
                    @endif
                    
                    <strong> Paid : </strong>{{number_format($sales_invoice->paid_amount,0,'.',',')}} /-
                
                
                
                </td>
            </tr>
            @php($rem = $sales_invoice->total_price - $sales_invoice->paid_amount)
            {{-- @if (($sales_invoice->paid_amount + $sales_invoice->discount)<$sales_invoice->total_price) --}}
            @if($rem > 0)
            <tr>
                <td colspan="7"  class="text-right" style="border:1px solid black;">
                    <strong>Rem : </strong>{{$sales_invoice->total_price - $sales_invoice->paid_amount}} /-
                </td>
            </tr>
           
            @endif
           

            </tbody>
        </table>
        <br>
        <p><center><strong>Address: </strong>{{env("SHOP_ADDRESS")}}</center></p>
        <br>
        <p><center><strong>Contact: </strong>{{env("SHOP_NUMBER")}}</center></p>
        <br>
        <p><center>Thank you for shopping at </center></p>
        <br>
        <p><center><strong>{{env("SHOP_NAME")}}</strong></center></p>
        <br>


    </div>
</body>

</html>

    
    
