@extends('layouts.simple')
@section('content')
    <div id="hidden" style="display:none"></div>
    <div class="col-12">
        <div class="card">
            @include('includes.flash')
            {!! Form::model($sales_invoice,['method'=>'PATCH','action'=>['AdminSalesInvoiceController@update',$sales_invoice->id],'onsubmit'=>'return submitFunc()','id'=>'formID']) !!}
            <div class="card-header">

                <input id="customer_id" name="customer_id" type="hidden" value="{{$sales_invoice->customer?$sales_invoice->customer->id:0}}">
                <input id="totalPriceID" name="total_price" value="{{$sales_invoice->total_price}}" type="hidden">
                <div class="d-flex justify-content-around">
                    <strong class="p2">Sales Invoice</strong>
                    <div id="companyID" class="p2"><strong></strong>{{$sales_invoice->customer?$sales_invoice->customer->name:'Waking Customer'}}<span></span></div>
                    @if ($sales_invoice->customer)
                        
                    
                    <div class="p2">
                        Previous Balance: <strong><span>{{$cr?$cr->to_pay:'No Previous Balance'}}</span></strong>
                    </div>
                    <div class="p2">
                        Last Payment : <strong><span>{{$payment?$payment->amount:'No Previous Sale'}}</span></strong>
                    </div>
                    @endif

                  
                    <input value="{{$sales_invoice->name}}" style="font-size:16px;" placeholder="Customer Name" class="form-control-sm p2" type="text" name="name">
                    <input  value="{{$sales_invoice->number}}" style="font-size:16px;" placeholder="Customer Number" class="form-control-sm p2" type="number" name="number">
                        
                    
                    <div class="p2">
                        Date :
                        <input value="{{Carbon::parse($sales_invoice->date)->format('Y-m-d')}}" id="date" type="date"  class="form-control-sm" name="date"/>
                    </div>
                    <button id="search" type="button" class="btn btn-primary btn-sm p2" data-toggle="modal" data-target="#scrollmodal">
                        Add Item
                    </button>
                </div>


            </div>
            <div class="card-body card-block">

                <div style="overflow-y: scroll" id="itemDivID" class="table-responsive table-responsive-data2">
                    <table id="itemTableID" class="table table-data2">
                        <thead>
                        <tr>

                            <th class="text-center"><span class="my-thead">Quality</span></th>
                            <th class="text-center"><span class="my-thead">Item</span></th>
                            <th class="text-center"><span class="my-thead">Cvrd</span></th>
                            <th class="text-center"><span class="my-thead">C.Stock</span></th>
                            <th class="text-center"><span class="my-thead">Qty</span></th>
                            <th class="text-center"><span class="my-thead">Pack</span></th>
                            <th class="text-center"><span class="my-thead">Bon</span></th>
                            <th class="text-center"><span class="my-thead">Price</span></th>
                            <th class="text-center"><span class="my-thead">Dis (%)</span></th>
                            <th class="text-center"><span class="my-thead">Total</span></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr id="{{$item->item->id}}">
                                <td class="text-center">{{$item->item->quality->name}}</td>
                                <td class="text-center">{{$item->item->name}}</td>
                                <td class="text-center covered">
                                    <select class="form-control-sm mousetrap">
                                        <option selected value={{$item->covered}}>{{$item->covered==0?'No':'Yes'}}</option>
                                        <option value="{{$item->covered==0?1:0}}">{{$item->covered==0?'Yes':'No'}}</option>
                                    </select>
                                </td>
                                <td style="display: none" class="text-center size">{{$item->item->size_t}}</td>
                                <td class="text-center udiscount" style="display: none">{{$item->item->UDiscount()}}</td>
                                <td class="text-center cdiscount" style="display: none">{{$item->item->CDiscount()}}</td>
                                <td class="text-center">{{$item->item->current_quantity}}</td>
                                <td class="text-center quantity"><input step="any"  min="0" class="form-control-sm sm quantityIN mousetrap" type="number" value="{{$item->quantity}}"/></td>
                                <td class="text-center">{{$item->item->package_unit}}</td>
                                <td class="text-center bonus"><input min="0" class="form-control-sm sm mousetrap" type="number" value="{{$item->bonus}}"/></td>
                                <td class="text-center price"><input  step="any" min="0" class="form-control-sm priceIN mousetrap" type="number" value="{{$item->price}}"/></td>

                                <td class="text-center discount"><input step="any" min="0" max="100" class="form-control-sm sm discountIN mousetrap" type="number" value="{{$item->discount}}"/> %</td>

                                <td class="text-center total">{{$item->total_price}}</td>
                                <td style="display: none"><input class="obj" type="checkbox" name="checkBoxArray[]"  value=""></td>
                                <td><button class="btn btn-danger btn-sm deleteBtn" type="button">Delete</button></td>



                            </tr>
                            @endforeach

                        </tbody>

                    </table>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div id="total" class="col-md-2"><strong>Total : </strong><span>{{$sales_invoice->total_price}}</span></div>
                    <div class="col-md-1">
                        <strong>
                            {!! Form::label('paid_amount','Paid:') !!}
                        </strong>
                        {!! Form::number('paid_amount',null,['class'=>'form-control-sm col-md-4 mousetrap','min'=>0]) !!}
                    </div>
                    <div class="col-md-1">
                        <strong>
                            {!! Form::label('given','Given:') !!}
                        </strong>
                        {!! Form::number('given',null,['class'=>'form-control-sm col-md-4 mousetrap','min'=>0,'id'=>'given']) !!}
                    </div>
                    <div class="col-md-1">
                        <strong>
                            {!! Form::label('returned','Ret:') !!}
                        </strong>
                        {!! Form::text('returned',null,['class'=>'form-control-sm col-md-4','readonly','id'=>'returned']) !!}
                    </div>
                    <div class="col-md-1" id="pay_later">
                        <strong>
                            {!! Form::label('pay_later','Pay Later') !!}
                        </strong>
                        {!! Form::select('pay_later',[0=>'No',1=>'Yes'],null,['class'=>'form-control-sm','min'=>0,'id'=>'pay_later']) !!}
                    </div>
                    <div class="col-md-1">
                        <strong>
                        {!! Form::label('remaining','Order Rem :') !!}
                    </strong>
                        <input class="form-control-sm mousetrap" placeholder="Rem" name="remaining" type="number">
                    </div>
                    <div class="col-md-1">
                        <strong>
                        {!! Form::label('discount','Disc :') !!}
                    </strong>
                        <input id="Mdiscount" value="0" class="form-control-sm mousetrap" min="0" name="discount" type="number">
                    </div>
                    <div class="col-md-1" style="vertical-align:middle">
                        {!! Form::submit('Submit',['class'=>'btn btn-primary','id'=>'submitID','style'=>'color:white'])!!}
                    </div>

                </div>




            </div>

        </div>
        {!! Form::close() !!}
    </div>


    <!-- modal scroll -->
    <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <input onkeyup="search()"  autofocus name="searchItem" id="searchIn" type="text" class="form-control mousetrap" />
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="tableID" class="table-responsive table--no-card m-b-30">
                        <table id="table_ID" class="table table-borderless  table-earning">
                            <thead>
                            <tr>
                                <th class="my-thead">ID</th>
                                <th>Item</th>
                                <th>Price</th>
                                <th>Quality</th>
                                <th>Size</th>
                                <th>Cvrd</th>
                                <th>UDiscount</th>
                                <th>CDiscount</th>
                            </tr>
                            </thead>

                            <tbody id="itemTbody">
                            <tr id="loading"><td class="text-center" colspan="8"><h3>Loading....</h3></td></tr>
                            {{--@php($i=1)
                            @foreach($Items as $item)
                                <tr  tabindex="{{$i}}">
                                    <!--<td><button class="btn btn-sm btn-primary">Select</button></td>-->
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->discountedPrice($customer?$customer->id:0)}}</td>
                                    <td>{{$item->quality->name}}</td>
                                    <td>{{$item->size_t}}</td>
                                    <td>{{$item->covered==0?'No':'Yes'}}</td>

                                    <td>
                                        {{$item->UCDiscount($customer?$customer->id:0)}}
                                    </td>
                                    <td>
                                        {{$item->CCDiscount($customer?$customer->id:0)}}
                                    </td>
                                    <td style="display:none">{{$item->package_unit}}</td>
                                    <td style="display:none">{{$item->current_quantity}}</td>
                                </tr>
                                @php($i++)
                            @endforeach--}}

                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

                </div>
            </div>
        </div>
    </div>

    @include('includes.errors')
    <style>
        .card-body{
            min-height: 78vh;
            max-height: 78vh !important;
        }
        tr:focus{
            background: #b2b2b2;
        }
        tr:focus{
            background: #b2b2b2;
        }
        #itemTableID tbody tr td:first-child{
            vertical-align: middle !important;
        }

        .selected{
            background: #b2b2b2 !important;
            color: white !important;
        }
        .selected td{
            color:white !important;
        }
        .form-control-sm{
            border:1px solid #ced4da !important;
        }
        .sm{
            width: 50px !important;
        }
        #itemDivID{
            max-height: 70vh;
        }
        #table_ID td,th{
            padding: 12px 10px !important;
        }
        #itemTableID td,th{
            padding: 12px 10px !important;
            font-size: 13px;
        }

        .card-header{

            font-size: 16px !important;
        }
        .card-footer{
            padding: 0;
            padding-top: 1px;
            padding-left: 5px;
            padding-right: 5px;
            font-size: 12px !important;
        }
        /* .form-control-sm{
            font-size: 14px !important;
        } */
        /* select.form-control-sm{
            height: 1.5rem !important;
        } */
        .p2{
            font-size: 14px !important;
        }
        .btn-sm{
            font-size: .5rem !important;
        }
        .price input{
            max-width: 75px;
        }
        .my-thead{
            padding: 5px;
            background: #0069d9;
            color:white;
            border-radius: 5px;
        }

        input{
            font-size:13px !important;
            color: #808080;
        }

        input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input[type="number"] {
            -moz-appearance: textfield;
        }

        .card-footer .form-control-sm{
            max-width: 100px !important;
        }
        .modal-dialog {
            max-width: 100%;
            width:100%;
        }
    </style>
    <script>
        let customerId={{$sales_invoice->customer_id}};
    </script>

    {{--<script src="{{asset('js/po_create.js')}}"></script>--}}
    <script src="{{asset('js/sales-invoice-edit.js')}}"></script>

@endsection