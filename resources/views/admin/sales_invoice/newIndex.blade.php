@extends('layouts.myapp')


@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">Sales Invoices</h3>
                </div>
                <div class="table-data__tool-right col-md-4">
                    <button id="add" type="button" class="btn btn-primary mb-1 float-right" data-toggle="modal" data-target="#scrollmodal">
                        Add Sales Invoice
                    </button>
                </div>
            </div>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link {{$defaultView=='today'?'active':''}}" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home"
                       aria-selected="true">Today</a>
                    <a class="nav-item nav-link {{$defaultView=='previous'?'show active':''}}" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile"
                       aria-selected="false">Previous</a>
                    <a class="nav-item nav-link" id="nav-ob-tab" data-toggle="tab" href="#ob" role="tab" aria-controls="nav-profile"
                       aria-selected="false">Completed Order Bookings</a>
                    <a class="nav-item nav-link" id="nav-ob-tab" data-toggle="tab" href="#not-paid" role="tab" aria-controls="nav-not-paid"
                       aria-selected="false">Payment Pending</a>

                </div>
            </nav>
            <div class="tab-content pl-3 pt-2" id="sales-invoice-today">
                <div class="tab-pane fade {{$defaultView=='today'?'show active':''}}" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

                        <today></today>

                </div>
                <div class="tab-pane fade {{$defaultView=='previous'?'show active':''}}" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <old></old>
                </div>
                <div class="tab-pane fade" id="ob" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <order-booking></order-booking>
                </div>
                <div class="tab-pane fade " id="not-paid" role="tabpanel" aria-labelledby="nav-not-paid">
                    <payment-pending></payment-pending>
                </div>

            </div>



        </div>


        <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        Select Customer
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['method'=>'POST','action'=>'AdminSalesInvoiceController@createC']) !!}
                        <div class="form-group">
                            {!! Form::select('customer_id',[0=>'Walking Customer']+$customers,null,['class'=>'form-control','required'])!!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Submit',['class'=>'btn btn-default'])!!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <div class="d-flex justify-content-around">
                            <a href="/admin/sales_invoice/{id}" class="p2"></a>
                            <button type="button" class="btn btn-secondary p2" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{asset('js/sales-invoice-index.js?v=6')}}"></script>
        <script>
            function clicked(id){
                if(confirm("Are You Sure ?")){
                    document.getElementById('del'+id).submit();
                }
                else{
                }
            }
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#add').click();

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });
        </script>

    </div>       <!-- END DATA TABLE-->
    <style>
        .blue{
            background: #0056b3;
        }
        .blue td{
            color:white !important;
        }

        .red{
            background: #ff3232;
        }
        .red .my-link{
            color:white;
            font-weight: bold;
        }
        .red td{
            color:white !important;
        }

        .red .zmdi{
            color:white !important;
        }


    </style>
    @include('includes.search')
@endsection