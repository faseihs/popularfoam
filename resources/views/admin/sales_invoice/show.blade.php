@extends('layouts.simple')
@section('content')
    <div id="hidden" style="display:none"></div>
    <div class="col-12">
        <div class="card">
            {!! Form::model($sales_invoice,['method'=>'PATCH','action'=>['AdminSalesInvoiceController@update',$sales_invoice->id],'onsubmit'=>'return submitFunc()','id'=>'formID']) !!}
            <div class="card-header">

                <input id="customer_id" name="customer_id" type="hidden" value="{{$sales_invoice->customer?$sales_invoice->customer->id:0}}">
                <input id="totalPriceID" name="total_price" value="{{$sales_invoice->total_price}}" type="hidden">
                <div class="d-flex justify-content-around">
                    <strong class="p2">Sales Invoice</strong>
                    <div id="companyID" class="p2"><strong>Customer: </strong>{{$sales_invoice->customer?$sales_invoice->customer->name:'Waking Customer'}}<span></span></div>
                    @if (!$customer)
                    <input readonly value="{{$sales_invoice->name}}" placeholder="Customer Name" class="form-control-sm p2" type="text" name="name">
                    <input readonly value="{{$sales_invoice->number}}" placeholder="Customer Number" class="form-control-sm p2" type="number" name="number">
                        
                    @endif
                    <div class="p2">
                        Date :
                        <input value="{{Carbon::parse($sales_invoice->date)->format('Y-m-d')}}" id="date" type="date" readonly  class="form-control-sm" name="date"/>
                    </div>

                </div>


            </div>
            <div class="card-body card-block">

                <div style="overflow-y: scroll" id="itemDivID" class="table-responsive table-responsive-data2">
                    <table id="itemTableID" class="table table-data2">
                        <thead>
                        <tr>

                            <th class="text-center"><span class="my-thead">Quality</span></th>
                            <th class="text-center"><span class="my-thead">Item</span></th>
                            <th class="text-center"><span class="my-thead">Cvrd</span></th>

                            <th class="text-center"><span class="my-thead">Qty</span></th>
                            <th class="text-center"><span class="my-thead">Pack</span></th>
                            <th class="text-center"><span class="my-thead">Bon</span></th>
                            <th class="text-center"><span class="my-thead">Price</span></th>
                            <th class="text-center"><span class="my-thead">Dis (%)</span></th>
                            <th class="text-center"><span class="my-thead">Total</span></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr id="{{$item->item->id}}">
                                <td class="text-center">{{$item->item->quality->name}}</td>
                                <td class="text-center">{{$item->item->name}}</td>
                                <td class="text-center covered">
                                    <select disabled class="form-control-sm mousetrap">
                                        <option selected value={{$item->covered}}>{{$item->covered==0?'No':'Yes'}}</option>
                                        <option value="{{$item->covered==0?1:0}}">{{$item->covered==0?'Yes':'No'}}</option>
                                    </select>
                                </td>
                                <td style="display: none" class="text-center size">{{$item->item->size_t}}</td>
                                <td class="text-center udiscount" style="display: none">{{$item->item->UDiscount()}}</td>
                                <td class="text-center cdiscount" style="display: none">{{$item->item->CDiscount()}}</td>

                                <td class="text-center quantity"><input  min="1" class="form-control-sm sm quantityIN mousetrap" type="number" value="{{$item->quantity}}"/></td>
                                <td class="text-center">{{$item->item->package_unit}}</td>
                                <td class="text-center bonus"><input min="0" class="form-control-sm sm mousetrap" type="number" value="{{$item->bonus}}"/></td>
                                <td class="text-center price">{{$item->price}}</td>

                                <td class="text-center discount"><input min="0" max="100" class="form-control-sm sm discountIN mousetrap" type="number" value="{{$item->discount}}"/> %</td>

                                <td class="text-center total">{{$item->total_price}}</td>
                                <td style="display: none"><input class="obj" type="checkbox" name="checkBoxArray[]"  value=""></td>



                            </tr>
                        @endforeach

                        </tbody>

                    </table>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div  id="total" class="col-md-2"><strong>Total : </strong><span class="totalSpan" >{{$sales_invoice->total_price}}</span></div>
                    <div class="col-md-1">
                        <strong>
                            {!! Form::label('paid_amount','Paid:') !!}
                        </strong>
                        {!! Form::number('paid_amount',null,['class'=>'form-control-sm col-md-4 mousetrap','min'=>0]) !!}
                    </div>
                    <div class="col-md-1">
                        <strong>
                            {!! Form::label('given','Given:') !!}
                        </strong>
                        {!! Form::number('given',null,['class'=>'form-control-sm col-md-4 mousetrap','min'=>0,'id'=>'given']) !!}
                    </div>
                    <div class="col-md-1">
                        <strong>
                            {!! Form::label('returned','Ret:') !!}
                        </strong>
                        {!! Form::text('returned',null,['class'=>'form-control-sm col-md-4','readonly','id'=>'returned']) !!}
                    </div>
                    <div class="col-md-1" id="pay_later">
                        <strong>
                            {!! Form::label('pay_later','Pay Later') !!}
                        </strong>
                        {!! Form::select('pay_later',[0=>'No',1=>'Yes'],null,['class'=>'form-control-sm','min'=>0,'id'=>'pay_later']) !!}
                    </div>
                    <div class="col-md-1">
                        {!! Form::label('remaining','Order Rem :') !!}
                        <input class="form-control-sm mousetrap" placeholder="Rem" name="remaining" type="number">
                    </div>
                    <div class="col-md-1">
                        {!! Form::label('discount','Disc :') !!}
                        <input id="Mdiscount" value="0" class="form-control-sm mousetrap" min="0" name="discount" type="number">
                    </div>

                </div>




            </div>

        </div>
        {!! Form::close() !!}
    </div>

    <div class="col-12 mt-4">
        <div class="card">
            <div class="card-header">
                Customer Payments
            </div>
            <div style="height:auto;min-height:auto;" class="card-body">
                @if ($extra_payments->count()>0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($extra_payments as $item)
                            <tr>
                                <td>{{Carbon::parse($item->date)->format('d-m-Y')}}</td>
                                <td>{{$item->cust}}</td>
                                <td>{{$item->amount}} /-</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <div class="alert alert-warning">No Additonal Payments...</div>
                @endif
            </div>
        </div>
    </div>


    <!-- modal scroll -->


    @include('includes.errors')
    <style>
        .card-body{
            min-height: 78vh;
            max-height: 78vh !important;
        }
        tr:focus{
            background: #b2b2b2;
        }
        tr:focus{
            background: #b2b2b2;
        }
        #itemTableID tbody tr td:first-child{
            vertical-align: middle !important;
        }

        .selected{
            background: #b2b2b2 !important;
            color: white !important;
        }
        .selected td{
            color:white !important;
        }
        .form-control-sm{
            border:1px solid #ced4da !important;
        }
        .sm{
            width: 50px !important;
        }
        #itemDivID{
            max-height: 70vh;
        }
        #table_ID td,th{
            padding: 12px 10px !important;
        }
        #itemTableID td,th{
            padding: 12px 10px !important;
            font-size: 13px;
        }

        .card-header{

            font-size: 16px !important;
        }
        .card-footer{
            padding: 0;
            padding-top: 1px;
            padding-left: 5px;
            padding-right: 5px;
            font-size: 12px !important;
        }
        /* .form-control-sm{
            font-size: 14px !important;
        } */
        /* select.form-control-sm{
            height: 1.5rem !important;
        } */
        .p2{
            font-size: 14px !important;
        }
        .btn-sm{
            font-size: .5rem !important;
        }
        .price input{
            max-width: 75px;
        }
        .my-thead{
            padding: 5px;
            background: #0069d9;
            color:white;
            border-radius: 10px;
        }
        .totalSpan{
            padding: 5px;
            background: #0069d9;
            color:white;
            border-radius: 5px;
        }

        input{
            font-size:13px !important;
            color: #808080;
        }

        input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input[type="number"] {
            -moz-appearance: textfield;
        }

        .card-footer .form-control-sm{
            max-width: 100px !important;
        }
    </style>

    {{--<script src="{{asset('js/po_create.js')}}"></script>--}}
    <script>

        $(document).ready(function () {

            /*Date.prototype.toDateInputValue = (function() {
                var local = new Date(this);
                local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
                return local.toJSON().slice(0,10);
            });*/
            $('#date').val(new Date().toDateInputValue());
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    //$('#search').click();
                    $('#scrollmodal').modal('toggle');

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            Mousetrap.bind(['ctrl+q', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#paid_amount').focus(function () {
                        $(this).select();
                    });

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            Mousetrap.bind(['ctrl+s', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#submitID').click();

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            /*$('#pay_later select').on('change',function () {
               if($(this).val()==1) {
                   $('.card-footer').append(udhar);
                   if((!isNaN(parseInt($('#total span').html())) && parseInt($('#total span').html())>0) && !isNaN(parseInt($('#paid_amount').val()))){
                       var ret=$('#paid_amount').val() - parseInt($('#total span').html());
                       $('#to_pay').val(abs(parseInt(ret)));
                   }
               }
               else $('#udhar').remove();
            });*/


            $('#scrollmodal').on('shown.bs.modal', function () {
                $('#searchIn').focus();
            });
            $('#scrollmodal').on('hidden.bs.modal', function () {
                $('#tableID tr').each(function () {
                    $(this).removeClass('selected');
                    $('#searchIn').val('');
                    $('#table_ID tr').each(function () {
                        $(this).css('display','');
                    });
                });
                $('#search').blur();
            });
            $('#scrollmodal,.selected').on('keydown',function (e) {
                let key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
                if(key==40) {
                    //$('#scrollmodal').focus();
                    //e.preventDefault();
                    $('#searchIn').blur();
                    //console.log('Key Down Pressed');
                    if($('.selected:not(:hidden)').length==0){

                        $('#tableID tbody tr').not(':hidden').first().addClass('selected').focus();
                    }
                    else{
                        let current=$('.selected:not(:hidden)');
                        if(current.nextAll().not(':hidden').length){
                            current.nextAll().not(':hidden').first().addClass('selected').focus();
                            current.removeClass('selected');
                            //current.next().focus();
                        }
                        else{
                            current.removeClass('selected');
                            $('#searchIn').focus();
                        }
                    }

                }

                //Up Key
                if(key==38){
                    //e.preventDefault();
                    //$('#scrollmodal').focus();
                    console.log('Dabb gaya');
                    $('#searchIn').blur();
                    if($('.selected:not(:hidden)').length==0){

                        $('#tableID tbody tr').not(':hidden').last().addClass('selected').focus();
                        console.log('yahan 1');
                    }
                    else{
                        let current=$('.selected:not(:hidden)');
                        if(current.prevAll().not(':hidden').length){
                            current.prevAll().not(':hidden').first().addClass('selected').focus();
                            current.removeClass('selected');
                        }
                        else{
                            current.removeClass('selected');
                            $('#searchIn').focus();
                            console.log('yahan 3');
                        }
                    }

                }

                if(key==13){
                    e.preventDefault();
                    $('#scrollmodal').focus();
                    //console.log('Enter Pressed');
                    if($('.selected:not(:hidden)').length>0) {
                        let selected=$('.selected');
                        //$('.card-body').append(selected.html());
                        $('#scrollmodal').modal('toggle');
                        getItemFromTable(selected);
                        calcTotal();
                        calcSize();

                    }

                }
            });
            $('#company').on('change', function() {
                let comp=$('#company option:selected').html();
                if(comp!=='Company')
                    $('#companyID span').html(comp);
                else $('#companyID span').html('');
            });

            $('#vehicle').on('change', function() {
                let comp=$('#vehicle option:selected').html();
                if(comp!=='Vehicle' || $('#total-coverage').hasClass('alert-danger')) {
                    $('#vehicleID span').html(comp);
                    setVehicleSize();
                    calcSize();
                    $('#submitID').removeAttr('disabled');

                }
                else {$('#vehicleID span').html('');setVehicleSize();$('#submitID').attr('disabled','true');}
            });

            $('.discountIN').on('keyup',function () {

                calcTotal();
                console.log('Yahoo');
            });
            $('.quantityIN').on('keyup',function () {
                calcTotal();
                calcSize();
            });

            $('.bonus input').on('change',function () {
                calcSize();
            });


            $('#table_ID tbody tr').on('click',function () {
                $('#scrollmodal').modal('toggle');
                getItemFromTable($(this));
                calcTotal();
                calcSize();
            });

            $('tr').on('focus',function(){
                console.log('FOvussed');
            });

            $('#paid_amount').on('keyup',function () {
                if(!isNaN(parseInt($('#total span').html()))) {
                    $('#returned').val($(this).val() - parseInt($('#total span').html()));
                    $('#to_pay').val(abs($(this).val() - parseInt($('#total span').html())));
                }
            });


        });
        function search() {
            $('#table_ID tbody tr').each(function () {
                $(this).removeClass('selected');
            });
            var input, filter, table, tr, td, i;
            input = document.getElementById("searchIn");
            filter = input.value.toUpperCase();
            table = document.getElementById("table_ID");
            //console.log(table);
            var tb = table.getElementsByTagName("tbody");
            tr=tb[0].getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";

                    } else {
                        tr[i].style.display = "none";

                    }
                }
            }
        }

        function getItemFromTable(object){
            var item={id:-1, name:"", price:-1,quality:"" ,company:"",covered:'',udiscount:-1,cdiscount:-1};
            var counter=1;
            object.children().each(function () {
                if(counter==1)
                    item.id=$(this).html();
                else if(counter==2)
                    item.name=$(this).html();
                else if(counter==3)
                    item.price=$(this).html();
                else if(counter==4)
                    item.quality=$(this).html();
                else if(counter==5)
                    item.size=$(this).html();
                else if(counter==6)
                    item.covered=$(this).html();
                else if(counter==7)
                    item.udiscount=$(this).html();
                else if(counter==8)
                    item.cdiscount=$(this).html();
                counter++;
            });

            //console.log(item);
            addItemToTable(item);
        }

        function addItemToTable(item){
            var selected;
            var selected_;
            var unselected;
            var unselected_;
            var dvalue;
            if(item.covered=='No'){
                selected=0;
                selected_='No';
                unselected=1;
                unselected_='Yes';
                dvalue=item.udiscount;
            }
            else{
                selected=1;
                selected_='Yes';
                unselected=0;
                unselected_='No';
                dvalue=item.cdiscount;
            }


            var row=`    <tr id='`+item.id+`'>
                                <td>`+item.quality+`</td>
                                <td>`+item.name+`</td>
                                <td class="text-right covered">
                                <select class="form-control-sm mousetrap">
                                <option selected value='`+selected+`'>`+selected_+`</option>
                                <option value='`+unselected+`'>`+unselected_+`</option>
                                </select>
                                 </td>
                                <td style="display:none" class="size">`+item.size+`</td>
                                <td class="cdiscount" style="display: none">`+item.cdiscount+`</td>
                                <td class="udiscount" style="display: none">`+item.udiscount+`</td>

                                <td class="text-right quantity"><input  min="1" class="form-control-sm sm quantityIN mousetrap" type="number" value="1"/></td>
                                <td class="text-right bonus"><input min="0" class="form-control-sm sm mousetrap" type="number" value="0"/></td>
                                 <td class="text-right price">`+item.price+`</td>
                                <td class="text-right discount"><input min="0" max="100" class="form-control-sm sm discountIN mousetrap" type="number" value=`+dvalue+`/> %</td>

                                <td class="text-right total">`+item.price+`</td>
                                <td style="display: none"><input class="obj" type="checkbox" name="checkBoxArray[]"  value=""></td>
                                <td><button class="btn btn-danger btn-sm deleteBtn" type="button">Delete</button></td>



                       </tr>`;
            $('#itemTableID tbody').append(row);
            $('.quantityIN').bind('change', function() {
                calcTotal();
                console.log('Yahoo');
            });
            $('.discountIN').bind('change', function() {
                calcTotal();
                console.log('Yahoo');
            });


            $('.deleteBtn').bind('click',function () {
                $(this).parent().parent().remove();
                calcTotal();
            });

            $('.covered select').on('change',function () {
                var val =$(this).val();
                var p=$(this).parent().parent();

                //console.log(parseInt(p.find('.cdiscount').html()));
                //console.log(parseInt(p.find('.udiscount').html()));
                if(val==1)
                    p.find('.discountIN').val(parseInt(p.find('.cdiscount').html()));
                else{
                    p.find('.discountIN').val(parseInt(p.find('.udiscount').html()));
                }
                calcTotal();
            });




        }

        function calcTotal(){
            var sum=0;
            $('#itemTableID tbody').children().each(function () {

                var individualSum=parseInt($(this).children('.price').html()) *parseInt($(this).find('.quantity input').val());
                console.log(individualSum);
                var discount=parseInt($(this).find('.discount input').val());
                console.log(discount);
                if(discount>0){
                    individualSum=individualSum*(1-(discount/100));
                }
                individualSum=Number((individualSum).toFixed(1));
                $(this).children('.total').html(individualSum);
                sum+=individualSum;
            });
            sum=Number((sum).toFixed(1));
            $('#total span').html(sum);
            $('#totalPriceID').val(sum);
            if((!isNaN(parseInt($('#total span').html())) && parseInt($('#total span').html())>0) && !isNaN(parseInt($('#paid_amount').val()))) {
                var ret=$('#paid_amount').val() - parseInt($('#total span').html());
                $('#returned').val(ret);
                $('#to_pay').val(abs(ret));

            }
            else {
                $('#returned').val(0);
                $('#to_pay').val(0);
            }
        }

        function submitFunc(){
            return false;
            if($('#itemTableID tbody tr').children().length<1){
                alert("No Items Selected");
                return false;
            }

            if(($('#paid_amount').val()-parseInt($('#total span').html())<0) && $('#pay_later select').val()==0){
                alert('Paid Amount is less than total amount ');
                return false;
            }

            if(($('#paid_amount').val()-parseInt($('#total span').html())>0) && $('#pay_later select').val()==1){
                alert('Paid Amount is greater so Pay Later (Yes) Option cannot be used ');
                return false;
            }
            if($('#customer_id').val()==0){
                if($('#pay_later select').val()==1)
                {
                    alert('Walking Customer has to pay the whole amount now');
                    return false;
                }
            }
            var item_array=Array();
            $('#itemTableID tbody tr').each(function () {

                var id=$(this).attr('id');
                var item={item_id:-1,quantity:-1,total_price:-1,bonus:-1,discount:-1,covered:-1};
                item.item_id=id;
                var $this=$(this);
                var count=1;
                item.price=$(this).children('.price').html();
                //console.log($(this).find(':nth-child(5)').find(':nth-child(1)').val());
                item.quantity=$(this).children('.quantity').find(':nth-child(1)').val();
                item.bonus=$(this).children('.bonus').find(':nth-child(1)').val();
                item.discount=$(this).children('.discount').find(':nth-child(1)').val();
                item.covered=$(this).children('.covered').find(':nth-child(1)').val();
                item.total_price=$(this).children('.total').html();
                $(this).find('input.obj').val(item);
                //console.log($(this).find('input.obj').val());
                item_array.push(item);
            });
            var items=JSON.stringify(item_array);
            $('#formID').append("<textarea id='items' name='items' style='display:none'>"+items+"</textarea> ");
            //console.log($('#formID #items'));
            return true;
        }



        sortTable();
        function sortTable() {
            var table, rows, switching, i, x, y, shouldSwitch;
            table = document.getElementById("table_ID");
            switching = true;
            /* Make a loop that will continue until
            no switching has been done: */
            while (switching) {
                // Start by saying: no switching is done:
                switching = false;
                rows = table.getElementsByTagName("TR");
                /* Loop through all table rows (except the
                first, which contains table headers): */
                for (i = 1; i < (rows.length - 1); i++) {
                    // Start by saying there should be no switching:
                    shouldSwitch = false;
                    /* Get the two elements you want to compare,
                    one from current row and one from the next: */
                    x = rows[i].getElementsByTagName("TD")[3];
                    y = rows[i + 1].getElementsByTagName("TD")[3];
                    // Check if the two rows should switch place:
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
                if (shouldSwitch) {
                    /* If a switch has been marked, make the switch
                    and mark that a switch has been done: */
                    rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                    switching = true;
                }
            }
        }
    </script>
@endsection