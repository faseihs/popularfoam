@extends('layouts.simple')
@section('content')
    <div id="hidden" style="display:none"></div>
    <div class="col-12">
        <div class="card">
            {!! Form::open(['method'=>'POST','action'=>'AdminSalesInvoiceController@store','onsubmit'=>'return submitFunc()','id'=>'formID']) !!}
            <div class="card-header">

                <input id="customer_id" name="customer_id" type="hidden" value="{{$customer?$customer->id:0}}">
                <input id="totalPriceID" name="total_price" value="" type="hidden">
                <div class="d-flex justify-content-around">
                    <strong class="p2">Sales Invoice</strong>
                    <div id="companyID" class="p2"><strong></strong>{{$customer?$customer->name:'Waking Customer'}}<span></span></div>
                    @if ($customer)
                        
                    
                    <div class="p2">
                        Previous Balance: <strong><span>{{$cr?$cr->to_pay:'No Previous Balance'}}</span></strong>
                    </div>
                    <div class="p2">
                        Last Payment : <strong><span>{{$payment?$payment->amount:'No Previous Sale'}}</span></strong>
                    </div>
                    @endif

                    @if (!$customer)
                    <input  placeholder="Customer Name" style="font-size:16px;" class="form-control-sm p2 cust" type="text" name="name">
                    <input  placeholder="Customer Number" style="font-size:16px;" class="form-control-sm p2 cust" type="number" name="number">
                        
                    @endif
                    <div class="p2">
                        Date :
                        <input id="date" type="date"  class="form-control-sm" name="date"/>
                    </div>
                    <button id="search" type="button" class="btn btn-primary p2" data-toggle="modal" data-target="#scrollmodal">
                        Add Item
                    </button>
                </div>


            </div>
            <div class="card-body card-block">

                <div style="overflow-y: scroll" id="itemDivID" class="table-responsive table-responsive-data2">
                    <table id="itemTableID" class="table table-data2">
                        <thead >
                        <tr>

                            <th class="text-center"><span class="my-thead">Quality</span></th>
                            <th class="text-center"><span class="my-thead">Item</span></th>
                            <th class="text-center"><span class="my-thead">Cvrd</span></th>
                            <th class="text-center"><span class="my-thead">C.Stock</span></th>
                            <th class="text-center"><span class="my-thead">Qty</span></th>
                            <th class="text-center"><span class="my-thead">Pack</span></th>
                            <th class="text-center"><span class="my-thead">Bon</span></th>
                            <th class="text-center"><span class="my-thead">Price</span></th>
                            <th class="text-center"><span class="my-thead">Dis (%)</span></th>
                            <th class="text-center"><span class="my-thead">Total</span></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>


                        </tbody>

                    </table>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div id="total" class="col-md-2"><strong>Total : </strong><span class="totalSpan" ></span></div>
                    <div class="col-md-1">
                        <strong>
                        {!! Form::label('paid_amount','Paid:') !!}
                    </strong>
                        {!! Form::number('paid_amount',null,['class'=>'form-control-sm col-md-4 mousetrap','min'=>0]) !!}
                    </div>
                    <div class="col-md-1">
                        <strong>
                            {!! Form::label('given','Given:') !!}
                        </strong>
                        {!! Form::number('given',null,['class'=>'form-control-sm col-md-4 mousetrap','min'=>0,'id'=>'given']) !!}
                    </div>
                    <div class="col-md-1">
                        <strong>
                        {!! Form::label('returned','Ret:') !!}
                    </strong>
                        {!! Form::text('returned',null,['class'=>'form-control-sm col-md-4','readonly','id'=>'returned']) !!}
                    </div>
                    <div class="col-md-1" id="pay_later">
                        <strong>
                        {!! Form::label('pay_later','Pay Later') !!}
                    </strong>
                        {!! Form::select('pay_later',[0=>'No',1=>'Yes'],null,['class'=>'form-control-sm','min'=>0,'id'=>'pay_later']) !!}
                    </div>
                    <div class="col-md-1">
                        <strong>
                            {!! Form::label('remaining','Order Rem :') !!}
                        </strong>
                        <input class="form-control-sm mousetrap" placeholder="Rem" name="remaining" type="number">
                    </div>
                    <div class="col-md-1">
                        <strong>
                            {!! Form::label('discount','Disc :') !!}
                        </strong>
                        <input id="Mdiscount" value="0" class="form-control-sm mousetrap" min="0" name="discount" type="number">
                    </div>
                    <div class="col-md-1" style="vertical-align:middle">
                        {!! Form::submit('Submit',['class'=>'btn btn-primary','id'=>'submitID','style'=>'color:white'])!!}
                    </div>

                </div>
            </div>

        </div>
        {!! Form::close() !!}
    </div>


    <!-- modal scroll -->
    <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <input onkeyup="search()"  autofocus name="searchItem" id="searchIn" type="text" class="form-control mousetrap" />
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="tableID" class="table-responsive table--no-card m-b-30">
                        <table id="table_ID" class="table table-borderless  table-earning">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Item</th>
                                <th>Price</th>
                                <th>Quality</th>
                                <th>Size</th>
                                <th>Cvrd</th>
                                <th>UDiscount</th>
                                <th>CDiscount</th>
                            </tr>
                            </thead>

                            <tbody>
                            @php($i=1)
                            @foreach($items as $item)
                                <tr  tabindex="{{$i}}">
                                    <!--<td><button class="btn btn-sm btn-primary">Select</button></td>-->
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->discountedPrice($customer?$customer->id:0)}}</td>
                                    <td>{{$item->quality->name}}</td>
                                    <td>{{$item->size_t}}</td>
                                    <td>{{$item->covered==0?'No':'Yes'}}</td>

                                    <td>
                                        {{$item->UCDiscount($customer?$customer->id:0)}}
                                    </td>
                                    <td>
                                        {{$item->CCDiscount($customer?$customer->id:0)}}
                                    </td>
                                    <td style="display:none">{{$item->package_unit}}</td>
                                    <td style="display:none">{{$item->current_quantity}}</td>
                                </tr>
                                @php($i++)
                            @endforeach

                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

                </div>
            </div>
        </div>
    </div>

    @include('includes.errors')
    <style>
        .card-body{
            min-height: 78vh;
            max-height: 78vh !important;
        }
        tr:focus{
            background: #b2b2b2;
        }
        tr:focus{
            background: #b2b2b2;
        }
        #itemTableID tbody tr td:first-child{
            vertical-align: middle !important;
        }

        .selected{
            background: #b2b2b2 !important;
            color: white !important;
        }
        .selected td{
            color:white !important;
        }
        .form-control-sm{
            border:1px solid #ced4da !important;
        }
        .sm{
            width: 50px !important;
        }
        #itemDivID{
            max-height: 70vh;
        }
        #table_ID td,th{
            padding: 12px 10px !important;
        }
        #itemTableID td,th{
            padding: 12px 10px !important;
            font-size: 13px;
        }

        .card-header{
            
            font-size: 16px !important;
        }
        .card-footer{
            padding: 0;
            padding-top: 1px;
            padding-left: 5px;
            padding-right: 5px;
            font-size: 12px !important;
        }
        /* .form-control-sm{
            font-size: 14px !important;
        } */
        /* select.form-control-sm{
            height: 1.5rem !important;
        } */
        .p2{
            font-size: 14px !important;
        }
        .btn-sm{
            font-size: .5rem !important;
        }
        .price input{
            max-width: 75px;
        }
        .my-thead{
            padding: 5px;
            background: #0069d9;
            color:white;
            border-radius: 5px;
        }

        input{
            font-size:13px !important;
            color: #808080;
        }

        input[type="number"]::-webkit-outer-spin-button, input[type="number"]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input[type="number"] {
            -moz-appearance: textfield;
        }

        .card-footer .form-control-sm{
            max-width: 100px !important;
        }
        
        
    </style>

    <script src="{{asset('js/sales_invoice_create.js')}}"></script>
@endsection