@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div id="ledger-app">
        <vue-table :dataarray="dataArray" :columns="columns"></vue-table>
    </div>

    <script src="{{asset('js/coL.js')}}"></script>
    <style>
        .au-breadcrumb2 {
             padding-top: 10px;
             padding-bottom: 0px;
        }
    </style>
@endsection