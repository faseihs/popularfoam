@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">workers</h3>
                </div>
                <div class="col-md-6">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search workers...">
                </div>
                <div class="table-data__tool-right col-md-4">
                    <a id="add" href="/admin/worker/create" class="au-btn au-btn-icon au-btn--green au-btn--small float-right">
                        <i class="zmdi zmdi-plus"></i>add workers</a>
                </div>
            </div>

        </div>
        @if(count($workers)==0)
            <h1 class="col-12 text-center">No Workers Found</h1>
        @endif
        @if(count($workers)>0)
            <div class="table-responsive table-responsive-data2">
                <table id="tableID" class="table table-data2">
                    <thead>
                    <tr>

                        <th>photo</th>
                        <th>name</th>
                        <th>number</th>
                        <th>address</th>
                        <th>joining</th>
                        <th>leaving</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($workers as $worker)
                        <tr class="tr-shadow">
                            <td><img height="50px" width="50px"  class="img-circle img-responsive" src="{{$worker->photo? asset('images/'.$worker->photo->path): 'https://placehold.co/50x50'}}"></td>

                            <td class="desc"><a href="/admin/worker/{{$worker->id}}">{{$worker->name}}</a></td>
                            <td>{{$worker->number}}</td>
                            <td><a data-toggle="tooltip" title="{{$worker->address}}" href="#">{{$worker->address?substr($worker->address,0,10):'NA'}}</a></td>
                            <td>{{Carbon::parse($worker->joining)->format('d-m-Y')}}</td>
                            <td>
                                {{$worker->leaving?$worker->leaving:'Present'}}
                            </td>
                            <td>
                                <div class="table-data-feature">
                                    @if(!($worker->role_id==1))
                                    <form id="del{{$worker->id}}" action="/admin/worker/{{$worker->id}}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        <a href="#" onclick="clicked({{$worker->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                    </form>
                                    @endif
                                    <a href="/admin/worker/{{$worker->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>

                                </div>
                            </td>
                        </tr>
                        <tr class="spacer"></tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
    </div>
    <script>
        function clicked(id){
            if(confirm("Are You Sure ?")){
                document.getElementById('del'+id).submit();
            }
            else{
            }
        }
        Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
            if (e.preventDefault) {
                e.preventDefault();
                $('#add').click();


            } else {
                // internet explorer
                e.returnValue = false;
            }

        });
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <!-- END DATA TABLE-->
    @include('includes.search');
@endsection