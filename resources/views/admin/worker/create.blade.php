@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::open(['method'=>'POST','action'=>'AdminWorkerController@store','class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('name','Name:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Name'])!!}

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('number','Number:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('number',null,['class'=>'form-control','placeholder'=>'Enter Number'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('address','Address:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('address',null,['class'=>'form-control','placeholder'=>'Enter Address'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('salary','Salary (Rs) :')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::number('salary',null,['class'=>'form-control','placeholder'=>'Enter Salary'])!!}

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('joining','Joining Date:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::date('joining',null,['class'=>'form-control'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('leaving','Leaving Date (Optional) :')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::date('leaving',null,['class'=>'form-control'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('active','Status:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('active',[1=>'Working',2=>'Not Working'],null,['class'=>'form-control','placeholder'=>'Select Status'])!!}

                    </div>
                </div>
                <div class="row form-group">
                <div class="col col-md-3">
                    {!! Form::label('photo','Photo:')!!}
                </div>
                <div class="col-3 col-md-3">
                    {!! Form::file('photo',null,['class'=>'form-control','placeholder'=>'Password'])!!}

                </div>
                </div>

            </div>
            <div class="card-footer">

                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('includes.errors')
@endsection