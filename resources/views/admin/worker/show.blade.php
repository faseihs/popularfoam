@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="default-tab">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link {{(isset($query) || isset($payment))?'':'active'}}" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home"
                       aria-selected="true">Overview</a>
                    <a class="nav-item nav-link {{isset($query)?'active':''}}" id="nav-at-tab" data-toggle="tab" href="#nav-at" role="tab" aria-controls="nav-home"
                       aria-selected="true">Attendance</a>
                       <a class="nav-item nav-link {{isset($payment)?'active':''}}" id="nav-p-tab" data-toggle="tab" href="#navPayment" role="tab" aria-controls="nav-home"
                       aria-selected="true">Payment</a>
                </div>
            </nav>
        </div>
        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
            <div class="tab-pane fade {{(isset($query) || isset($payment))?'':'show active'}}" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="col-10">
                    <div class="row">
                        <div class="col-md-2">
                            Photo :
                        </div>
                        <div class="col-md-3">
                            <img class="img-responsive" height="50px" src="{{$worker->photo? asset('images/'.$worker->photo->path): 'https://placehold.co/400x400'}}" alt="">
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-4">
                                    ID :
                                </div>
                                <div class="col-md-3">
                                    <strong>{{$worker->id}}</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    Name :
                                </div>
                                <div class="col-md-3">
                                    <strong>{{$worker->name}}</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    Number :
                                </div>
                                <div class="col-md-3">
                                    <strong>{{$worker->number}}</strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    Address :
                                </div>
                                <div class="col-md-8">
                                    <strong>{{$worker->address}}</strong>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    Salary :
                                </div>
                                <div class="col-md-3">
                                    <strong>{{$worker->salary}}</strong>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    Joining Date :
                                </div>
                                <div class="col-md-8">
                                    <strong>{{$worker->joining}}</strong>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    Leaving Date :
                                </div>
                                <div class="col-md-8">
                                    <strong>{{$worker->leaving?$worker->leaving:'Present'}}</strong>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    Data Created :
                                </div>
                                <div class="col-md-8">
                                    <strong>{{Carbon::parse($worker->created_at)->format('d-m-Y')}}</strong>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    Data Updated :
                                </div>
                                <div class="col-md-8">
                                    <strong>{{Carbon::parse($worker->updated_at)->format('d-m-Y')}}</strong>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>
            <div class="tab-pane fade {{isset($query)?'show active':''}}" id="nav-at" role="tabpanel" aria-labelledby="nav-home-tab">

                    {!! Form::open(['method'=>'POST','action'=>'AdminWorkerController@query','class'=>'row','id'=>'formRow']) !!}
                        <input type="hidden" name="worker_id" value="{{$worker->id}}">
                        <div class="form-group col-md-2">

                            {!! Form::select('today',[1=>'This Month',2=>'Select Month'],isset($on_date)?2:1
                            ,['class'=>'form-control-sm','id'=>'selectID'])!!}
                        </div>
                        <div class="form-group col-md-3">
                            {!! Form::submit('Go',['class'=>'btn btn-primary btn-sm'])!!}
                        </div>

                        @if(isset($on_date))
                            Month : {{$on_date->format('F-Y')}}
                            <input id='date' value="{!! Carbon::parse($on_date)->format('F-Y') !!}" type='month' class='form-control-sm' name='month' required />
                        @endif

                    {!! Form::close() !!}
                    <div class="row">
                        <div class="col-md-4">
                           Total Working Days :
                        </div>
                        <div class="col-md-2">
                            {{-- {{$at[0]}} --}}
                            {{$totalDays}}
                        </div>
                        <div class="col-md-6">
                            <span class="alert-success">Green</span> : Present
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-4">
                        Days Present :
                    </div>
                    <div class="col-md-2">
                        {{-- {{$at[0]}} --}}
                        {{$totalPresent}}
                    </div>
                    <div class="col-md-6">
                        <span class="alert-danger">Red</span> : Friday
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        Days Absent :
                    </div>
                    <div class="col-md-2">
                        {{-- {{$at[1]}} --}}
                        {{$totalAbsent}}
                    </div>
                    <div class="col-md-6">
                        <span class="alert-warning">Yellow</span> : Absent
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        Not Marked :
                    </div>
                    <div class="col-md-2">
                        {{-- {{$at[2]}} --}}
                        {{$totalNotMarked}}
                    </div>
                    <div class="col-md-6">
                        <span class="alert-primary">Blue</span> : Not Marked
                    </div>
                </div>
                <hr>

                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th class="text-center">Date</th>
                            <th class="text-center">Day</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Arrival</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($att as $item)
                        <tr class="@php 
                        if(Carbon::parse($item->date)->dayOfWeek==5)
                        echo "alert-danger";
                        else {
                            if($item->status==1)
                            echo 'alert-success';
                            else if($item->status==2)
                            echo 'alert-primary';
                            else echo 'alert-warning';
                        }
                        @endphp
                        " class="{{Carbon::parse($item->date)->dayOfWeek==5?'alert-danger':''}}">
                                <td class="text-center">{{Carbon::parse($item->date)->format('d-m-Y')}}</td>
                                <td class="text-center">{{Carbon::parse($item->date)->format('l')}}</td>
                                <td class="text-center">@php
                                    if($item->status==2) echo "Not Marked";
                                    else if($item->status==1)
                                        echo "Present";
                                    else if($item->status==0)
                                        echo "Absent";
                                @endphp</td>
                                <td class="text-center">{{$item->arrival?$item->arrival:'NA'}}</td>
                            </tr>
                            
                        @endforeach
                    </tbody>
                </table>

            </div>
            <div class="tab-pane fade {{isset($payment)?'show active':''}}" id="navPayment" role="tabpanel" aria-labelledby="nav-p-tab">
               <div class="row">

                    <div class="col-md-4">
                        Select Month (chose any date of month)
                    </div>
                    <div class="col-md-4">
                        Selected Month : {{isset($selDate)?$selDate->format('F Y'):''}}    
                    </div>
                    <div class="col-md-3">
                        <form action="/admin/worker/queryPayment" method="post">
                            {{csrf_field()}}
                            <input name="worker_id" value="{{$worker->id}}" type="hidden">
                        <input required name="on_date" type="date"> 
                        <button class="btn btn-primary btn-sm" type="submit">Go</button>
                        </form>   
                    </div>
                </div>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Type</th>
                            <th>Amount</th>    
                        </tr>    
                    </thead>
                    <tbody>
                        @foreach($payments as $p)
                        <tr>
                        <td>{{$p->id}}</td>
                        <td>{{Carbon::parse($p->date)->format('d-m-Y')}}</td>
                        <td>{{$p->type=='0'?'Loan':'Direct'}}</td>
                        <td>{{number_format($p->amount,0,'.',',')}}</td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    @include('includes.errors')
    <style>
        .au-breadcrumb2 {
            padding-top: 10px;
            padding-bottom: 10px;

        }
    </style>

    <script>
        Date.prototype.toDateInputValue = (function() {
            var local = new Date(this);
            local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
            return local.toJSON().slice(0,10);
        });
        var d= "<input id='date' type='month' class='form-control-sm' name='month' required />";
        $(document).ready(function () {

            $('#selectID').on('change',function () {
                if ($(this).val()==1){
                    $('#date').remove();
                }
                else{
                    $('#formRow').append(d);
                }
            });
        });
    </script>
@endsection