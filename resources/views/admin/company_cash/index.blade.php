@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <h3 class="title-5 m-b-35">cash registers : company</h3>
                </div>
                <div class="col-md-5">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search customers...">
                </div>
                {{--<div class="table-data__tool-right col-md-4">
                    <a id="add" href="/admin/company_cash/create" class="au-btn au-btn-icon au-btn--green au-btn--small float-right">
                        <i class="zmdi zmdi-plus"></i>add company_cash</a>
                </div>--}}
            </div>
        </div>

    </div>
    @if(count($cash_registers)==0)
        <h1 class="text-md-center">No cash_registers Found</h1>
    @endif
    @if(count($cash_registers)>0)
        <div class="table-responsive table-responsive-data2">
            <table id="tableID" class="table table-data2">
                <thead>
                <tr>

                    <th>id</th>
                    <th>name</th>
                    <th>Paid</th>
                    <th>To Pay</th>
                    <th>Received</th>
                    <th>To Receive</th>
                    <th>created</th>
                    <th>updated</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($cash_registers as $company_cash)
                    <tr class="tr-shadow">

                        <td>{{$company_cash->id}}</td>
                        <td class="desc">{{$company_cash->owner?$company_cash->owner->name:'Deleted / NA'}}</td>
                        <td>{{number_format($company_cash->paid,0,'.',',')}}</td>
                        <td>{{number_format($company_cash->to_pay,0,'.',',')}}</td>
                        <td>{{number_format($company_cash->received,0,'.',',')}}</td>
                        <td>{{number_format($company_cash->to_receive,0,'.',',')}}</td>
                        <td>{{Carbon::parse($company_cash->created_at)->format('d-m-Y')}}</td>
                        <td>
                            {{Carbon::parse($company_cash->updated_at)->format('d-m-Y')}}
                        </td>
                        <td>
                            <div class="table-data-feature">
                                <form id="del{{$company_cash->id}}" action="/admin/company_cash/{{$company_cash->id}}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                    <a href="#" onclick="clicked({{$company_cash->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </a>
                                </form>
                                <a href="/admin/company_cash/{{$company_cash->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                    </button>

                            </div>
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @endif
        </div>
        </div>
        <script>
            function clicked(id){
                if(confirm("Are You Sure ?")){
                    document.getElementById('del'+id).submit();
                }
                else{
                }
            }
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#add').click();
                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });
        </script>
        <!-- END DATA TABLE-->
        @include('includes.search');
@endsection