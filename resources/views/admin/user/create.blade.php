@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::open(['method'=>'POST','action'=>'AdminUserController@store','class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('name','Name:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Name'])!!}

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('email','Email:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Enter Email'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('role_id','Role:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('role_id',$roles,null,['class'=>'form-control','placeholder'=>'Select Role'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('password','Password:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password'])!!}

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('photo','Photo:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::file('photo',null,['class'=>'form-control','placeholder'=>'Password'])!!}

                    </div>
                </div>
            </div>
            <div class="card-footer">

                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('includes.errors')
@endsection