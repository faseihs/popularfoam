@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">Users</h3>
                </div>
                <div class="col-md-6">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search Users...">
                </div>
                <div class="table-data__tool-right col-md-4">
                    <a id="add" href="/admin/user/create" class="au-btn au-btn-icon au-btn--green au-btn--small float-right">
                        <i class="zmdi zmdi-plus"></i>add users</a>
                </div>
            </div>

        </div>
        @if(count($users)==0)
            <h1 class="text-md-center">No Users Found</h1>
        @endif
        @if(count($users)>0)
            <div class="table-responsive table-responsive-data2">
                <table id="tableID" class="table table-data2">
                    <thead>
                    <tr>

                        <th>photo</th>
                        <th>name</th>
                        <th>email</th>
                        <th>role</th>
                        <th>created</th>
                        <th>updated</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr class="tr-shadow">
                            <td><img height="50px" width="50px"  class="img-circle img-responsive" src="{{$user->photo? asset('images/'.$user->photo->path): 'https://placehold.co/50x50'}}"></td>

                            <td class="desc">{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->role? $user->role->name :'NA'}}</td>
                            <td>{{Carbon::parse($user->created_at)->format('d-m-Y')}}</td>
                            <td>
                                {{Carbon::parse($user->updated_at)->format('d-m-Y')}}
                            </td>
                            <td>
                                <div class="table-data-feature">
                                    @if(!($user->role_id==1))
                                    <form id="del{{$user->id}}" action="/admin/user/{{$user->id}}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        <a href="#" onclick="clicked({{$user->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                    </form>
                                    @endif
                                    <a href="/admin/user/{{$user->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>

                                </div>
                            </td>
                        </tr>
                        <tr class="spacer"></tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
    </div>
    <script>
        function clicked(id){
            if(confirm("Are You Sure ?")){
                document.getElementById('del'+id).submit();
            }
            else{
            }
        }
        Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
            if (e.preventDefault) {
                e.preventDefault();
                $('#add').click();


            } else {
                // internet explorer
                e.returnValue = false;
            }

        });
    </script>
    <!-- END DATA TABLE-->
    @include('includes.search');
@endsection