@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::model($user,['method'=>'PATCH','action'=>['AdminUserController@update',$user->id],'class'=>'form-horizontal','enctype' => 'multipart/form-data','id'=>'formID']) !!}
            <div class="card-body card-block">
                <div id="imgDiv" class="row">
                    <div class="col-md-4"><img class="img-responsive" src="{{$user->photo? asset('images/'.$user->photo->path): 'https://placehold.co/400x400'}}" alt="">
                    </div>
                    @if($user->photo)
                        <div class="col-md-8">
                            <button id="delBtn" type="button" class="btn btn-primary btn-sm">
                                <i class="fa fa-eraser"></i> Delete Photo
                            </button>
                        </div>
                    @endif
                </div>
                <hr/>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('name','Name:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Name'])!!}

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('email','Email:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Enter Email'])!!}

                    </div>
                </div>
                @if($user->role_id==1)
                {!! Form::text('role_id',1,['class'=>'form-control','style'=>'display:none'])!!}

                @endif
                @if($user->role_id!=1)
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('role_id','Role:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('role_id',$roles,null,['class'=>'form-control','placeholder'=>'Select Role'])!!}

                    </div>
                </div>
                @endif
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('password','Password:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('photo','Photo:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::file('photo',null,['class'=>'form-control','placeholder'=>'Password'])!!}

                    </div>
                </div>
            </div>
            <div class="card-footer">

                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <script>
        $('#delBtn').click(function () {
            $('#imgDiv').remove();
            $('#formID').append("<input type='hidden' name='delPic' value='1' />")
        });
    </script>
    @include('includes.errors')
@endsection