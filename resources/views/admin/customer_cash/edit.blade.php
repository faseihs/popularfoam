@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::model($customer_cash,['method'=>'PATCH','action'=>['AdminCustomerCashController@update',$customer_cash->id],'class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('customer_name','Name:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('customer_name',$customer_cash->owner->name,['class'=>'form-control','readonly'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('paid','Paid Amount:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::number('paid',null,['class'=>'form-control','required'])!!}

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('to_pay','Amount to Pay:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::number('to_pay',null,['class'=>'form-control','required'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('received','Amount Given:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::number('received',null,['class'=>'form-control','required'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('to_receive','Amount to Receive:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::number('to_receive',null,['class'=>'form-control','required'])!!}

                    </div>
                </div>
            </div>
            <div class="card-footer">

                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('includes.errors')
@endsection