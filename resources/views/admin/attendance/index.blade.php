@extends('layouts.myapp')

@section('content')
<!-- DATA TABLE-->

<div class="row">
    @include('includes.flash')
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2">
                <h3 class="title-5 m-b-35">attendance</h3>
            </div>
            {{--<div class="col-md-3">
                <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search attendance...">
            </div>--}}
            <div class="col-md-8">
                {!! Form::open(['method'=>'POST','action'=>'AdminAttendanceController@store']) !!}
                    <div id="formRow" class="row">
                            <div class="form-group col-md-2">

                            {!! Form::select('today',[1=>'Today',2=>'On Date'],isset($on_date)?2:1
                            ,['class'=>'form-control-sm','id'=>'selectID'])!!}
                            </div>
                            <div class="form-group col-md-3">
                                {!! Form::submit('Go',['class'=>'btn btn-primary btn-sm'])!!}
                            </div>

                        @if(isset($on_date))
                            <input id='date' value="{!! Carbon::parse($on_date)->format('F-Y') !!}" type='date' class='form-control-sm' name='date' required />
                        @endif
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

</div>
<style>
    .au-input--w300{
        max-width: 100%;
        min-width: 100%;
    }
</style>
@if(count($attendances)==0)
<h1 class="text-md-center">No attendances Found</h1>
@endif
@if(count($attendances)>0)
<div class="table-responsive table-responsive-data2">
    <table id="tableID" class="table table-data2">
        <thead>
        <tr>
            <th>Date</th>
            <th>Worker</th>
            <th>Status</th>
            <th>Arrival Time</th>
            <th>updated</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($attendances as $attendance)
        <tr class="tr-shadow">

            <td>{{Carbon::parse($attendance->date)->format('d-m-Y')}}</td>
            <td class="desc"><a href="{{$attendance->worker?'/admin/worker/'.$attendance->worker->id:'Deleted / NA'}}">{{$attendance->worker?$attendance->worker->name:'Deleted / NA'}}</a></td>
            <td>{{$attendance->status==0?'Absent':'Present'}}</td>
            <td>{{$attendance->arrival}}</td>
            <td>
                {{Carbon::parse($attendance->updated_at)->format('d-m-Y')}}
            </td>
            <td>
                <div class="table-data-feature">
                    <form id="del{{$attendance->id}}" action="/admin/attendance/{{$attendance->id}}" method="POST">
                        <input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                        <a href="#" onclick="clicked({{$attendance->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                            <i class="zmdi zmdi-delete"></i>
                        </a>
                    </form>
                    <a href="/admin/attendance/{{$attendance->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                        <i class="zmdi zmdi-edit"></i>
                        </a>

                </div>
            </td>
        </tr>
        <tr class="spacer"></tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif
<script>
    Date.prototype.toDateInputValue = (function() {
        var local = new Date(this);
        local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
        return local.toJSON().slice(0,10);
    });
    var d= "<input id='date' type='date' class='form-control-sm' name='date' required />";
    function clicked(id){
        if(confirm("Are You Sure ?")){
            document.getElementById('del'+id).submit();
        }
        else{
        }
    }
    Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
        if (e.preventDefault) {
            e.preventDefault();
            $('#add').click();
        } else {
            // internet explorer
            e.returnValue = false;
        }


    });

    $(document).ready(function () {

        $('#selectID').on('change',function () {
            if ($(this).val()==1){
                $('#date').remove();
            }
            else{
                $('#formRow').append(d);
            }
        });
    });
</script>
<!-- END DATA TABLE-->
@include('includes.search')
@endsection