@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::model($at,['method'=>'PATCH','action'=>['AdminAttendanceController@update',$at->id],'class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
            <div class="card-body card-block">
                <div class="row form-group">
                    Worker : <strong>{{$at->worker->name}}</strong>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('status','Status:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('status',[0=>'Absent',1=>'Present'],null,['class'=>'form-control'])!!}

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('arrival','Arrival Time:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::time('arrival',null,['class'=>'form-control'])!!}

                    </div>
                </div>
            </div>
            <div class="card-footer">

                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('includes.errors')
@endsection