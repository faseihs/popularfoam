@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::model($discount,['method'=>'PATCH','action'=>['AdminCDiscountController@update',$discount->id],'class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
            {!! Form::text('quality_id',null,['class'=>'form-control','style'=>'display:none'])!!}
            {!! Form::text('customer_id',null,['class'=>'form-control','style'=>'display:none'])!!}
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        Customer :
                    </div>
                    <div class="col-3 col-md-3">
                        <strong>{{$discount->customer?$discount->customer->name:'All'}}</strong>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('item_id','Item:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('item_id',[0=>'All Items']+$items,null,['class'=>'form-control'])!!}

                    </div>
                </div>
                <div @if($discount->type==2) style="display: none;" @endif class="row form-group discounts">
                    <div class="col col-md-3">
                        {!! Form::label('Covered Discount','Covered Discount')!!}
                    </div>
                    <div class="col-3 col-md-2">
                        {!! Form::number('covered',0,['class'=>'form-control','min'=>0,'max'=>'100','step'=>'any'])!!}
                    </div>
                </div>
                <div @if($discount->type==2) style="display: none;" @endif class="row form-group discounts">
                    <div class="col col-md-3">
                        {!! Form::label('Uncovered Discount','Uncovered Discount')!!}
                    </div>
                    <div class="col-3 col-md-2">
                        {!! Form::number('uncovered',0,['class'=>'form-control','min'=>0,'max'=>'100','step'=>'any'])!!}
                    </div>
                </div>
                <div @if($discount->type==1) style="display: none;" @endif class="row form-group price">
                    <div class="col col-md-3">
                        {!! Form::label('price','Price')!!}
                    </div>
                    <div class="col-3 col-md-2">
                        {!! Form::number('price',null,['class'=>'form-control','step'=>'any'])!!}
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('type','Type:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('type',[1=>'Discount',2=>'Price'],null,['class'=>'form-control','id'=>'type'])!!}

                    </div>
                </div>

            </div>



        </div>
        <div class="card-footer">

            {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
        </div>
        {!! Form::close() !!}
    </div>
    <script>
        $(document).ready(function () {
            $('#type').change(function () {
                var value = $(this).val();
                if(value==1){
                    $('.discounts').show(500);
                    $('.price').hide(500);
                }
                else {
                    $('.price').show(500);
                    $('.discounts').hide(500);
                }
            }) ;
        });
    </script>
    @include('includes.errors')
@endsection