@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">Discounts</h3>
                </div>
                <div class="col-md-5">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2 mousetrap" type="text" placeholder="Search purchase orders...">
                </div>
                <div class="table-data__tool-right col-md-4">
                    <button id="add" type="button" class="btn btn-primary mb-1 float-right" data-toggle="modal" data-target="#scrollmodal">
                        Add Discount
                    </button>
                </div>
            </div>

        </div>

        @if(count($discounts)==0)
            <h1 class="text-md-center" >No discounts Found</h1>
        @endif
        @if(count($discounts)>0)
            <div class="table-responsive table-responsive-data2">
                <table id="tableID" class="table table-data2">
                    <thead>
                    <tr>
                        <th>Company</th>
                        <th>Quality</th>
                        <th>Customer</th>
                        <th>item</th>
                        <th>Covered</th>
                        <th>Uncovered</th>
                        <th>Price</th>
                        <th>Type</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($discounts as $discount)
                        <tr class="tr-shadow">
                            <td>{{$discount->quality?$discount->quality->company->name:'Deleted / NA'}}</td>
                            <td>{{$discount->quality?$discount->quality->name:'Deleted / NA'}}</td>
                            <td>{{$discount->customer?$discount->customer->name:'All'}}</td>
                            <td>{{$discount->item_id==0? 'All' :$discount->item->name}}</td>
                            <td>{{$discount->covered}}</td>

                            <td>
                                {{$discount->uncovered}}
                            </td>
                            <td>{{$discount->price?$discount->price:'NA'}}</td>
                            <td>{{$discount->type==1?'Discount':'Price'}}</td>
                            <td>
                                <div class="table-data-feature">
                                    <form id="del{{$discount->id}}" action="/admin/customer_discount/{{$discount->id}}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        <a href="#" onclick="clicked({{$discount->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                    </form>
                                    <a href="/admin/customer_discount/{{$discount->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>

                                    </a>
                                </div>
                            </td>
                        </tr>
                        <tr class="spacer"></tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>

    <div class="modal fade" id="scrollmodal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    Select Quality
                </div>
                <div class="modal-body">
                    {!! Form::open(['method'=>'POST','action'=>'AdminCDiscountController@createC']) !!}
                    <div class="form-group">

                        {!! Form::select('quality_id',$qualities,null,['class'=>'form-control','placeholder'=>'Select Quality','required'])!!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('customer_id','Select Customer')!!}
                        {!! Form::select('customer_id',[0=>'Walking Customer']+$customers,null,['class'=>'form-control'])!!}
                    </div>
                    <div class="form-group">
                        {!! Form::submit('Submit',['class'=>'btn btn-default'])!!}
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <div class="d-flex justify-content-around">
                        <a href="/admin/customer_discount/{id}" class="p2"></a>
                        <button type="button" class="btn btn-secondary p2" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function clicked(id){
            if(confirm("Are You Sure ?")){
                document.getElementById('del'+id).submit();
            }
            else{
            }
        }
        Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
            if (e.preventDefault) {
                e.preventDefault();
                $('#add').click();


            } else {
                // internet explorer
                e.returnValue = false;
            }

        });
    </script>
    <!-- END DATA TABLE-->
    <script>
        function search() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("searchID");
            filter = input.value.toUpperCase();
            table = document.getElementById("tableID");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
@endsection