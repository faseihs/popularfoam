@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
                <div class="row">
                        <div class="col-md-1">
                            <h3 class="title-5 m-b-35">Vehicles</h3>
                        </div>
                        <div class="col-md-7">
                            <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search Vehicles...">
                        </div>
                        <div class="table-data__tool-right col-md-4">
                            <a id="add" href="/admin/vehicle/create" class="au-btn au-btn-icon au-btn--green au-btn--small float-right">
                                <i class="zmdi zmdi-plus"></i>add vehicles</a>
                        </div>
                </div>
        
        </div>
        @if(count($vehicles)==0)
            <h1 class="text-md-center">No Vehicles Found</h1>
        @endif
        @if(count($vehicles)>0)
            <div class="table-responsive table-responsive-data2">
                <table id="tableID" class="table table-data2">
                    <thead>
                    <tr>

                        <th>id</th>
                        <th>name</th>
                        <th>Size (inches)</th>
                        <th>created</th>
                        <th>updated</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($vehicles as $vehicle)
                        <tr class="tr-shadow">

                            <td>{{$vehicle->id}}</td>
                            <td class="desc">{{$vehicle->name}}</td>
                            <td>{{number_format($vehicle->size,0,'.',',')}}</td>
                            <td>{{Carbon::parse($vehicle->created_at)->format('d-m-Y')}}</td>
                            <td>
                                {{Carbon::parse($vehicle->updated_at)->format('d-m-Y')}}
                            </td>
                            <td>
                                <div class="table-data-feature">
                                    <form id="del{{$vehicle->id}}" action="/admin/vehicle/{{$vehicle->id}}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        <a href="#" onclick="clicked({{$vehicle->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                    </form>
                                    <a href="/admin/vehicle/{{$vehicle->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                        </button>

                                </div>
                            </td>
                        </tr>
                        <tr class="spacer"></tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
    </div>
    <script>
        function clicked(id){
            if(confirm("Are You Sure ?")){
                document.getElementById('del'+id).submit();
            }
            else{
            }
        }
        Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
            if (e.preventDefault) {
                e.preventDefault();
                $('#add').click();


            } else {
                // internet explorer
                e.returnValue = false;
            }

        });
    </script>
    <!-- END DATA TABLE-->
    @include('includes.search');
@endsection