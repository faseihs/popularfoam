@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::model($vehicle,['method'=>'PATCH','action'=>['AdminVehicleController@update',$vehicle->id],'class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('name','Name:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Name'])!!}
                        <span class="help-block">Please enter the Category's name</span>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('size','Size (inches) :')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::text('size',null,['class'=>'form-control','placeholder'=>'Enter Size'])!!}
                        <span class="help-block">Please enter the Vehcile's size</span>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('comments','Comments:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::textarea('comments',null,['class'=>'form-control','placeholder'=>'Comments','rows'=>3])!!}
                        <span class="help-block">Any Comments (Optional)</span>
                    </div>
                </div>
            </div>
            <div class="card-footer">

                {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @include('includes.errors')
@endsection