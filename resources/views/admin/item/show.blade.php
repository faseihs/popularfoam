@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Item Details</strong>
            </div>

            <div class="card-body card-block">
                <div class="row">
                    <div class="col-md-4"><img class="img-responsive" src="{{$item->photo? asset('images/'.$item->photo->path): 'https://placehold.co/400x400'}}" alt="">
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Item:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->name}}</div>
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Quality:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->quality->name}}</div>
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Category:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->category->name}}</div>
                </div>
                <div class="row">
                        <div class=" col-md-2 col-sm-5 font-weight-bold">Opening Date :</div>
                        <div class=" col-md-2 col-sm-5">{{$item->opening_date}}</div>
                        <div class=" col-md-2 col-sm-5 font-weight-bold">Opening Stock:</div>
                        <div class=" col-md-2 col-sm-5">{{$item->opening_quantity}}</div>
                        <div class=" col-md-2 col-sm-5 font-weight-bold">Opening Price:</div>
                        <div class=" col-md-2 col-sm-5">{{$item->opening_price}}/-</div>
                       
                    </div>
                <div class="row">
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Purchase Price:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->purchase_price}}</div>
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Retail Price:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->retail_price? $item->retail_price:'NA' }}</div>
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Sale Price:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->sale_price}}</div>
                </div>
                <div class="row">
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Sales Tax:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->sales_tax?$item->sales_tax:'NA'}}</div>
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Package Unit:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->package_unit }}</div>
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Package Amount:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->package_amount}}</div>
                </div>
                <div class="row">
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Order Level:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->default_quantity}}</div>
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Store Level:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->store_quantity }}</div>
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Current Level:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->current_quantity}}</div>
                </div>
                <div class="row">
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Bar Code:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->bar_code ? $item->bar_code :'NA'}}</div>
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Covered</div>
                    <div class=" col-md-2 col-sm-5">{{$item->covered==1?'Yes':'No' }}</div>
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Size:</div>
                    <div class=" col-md-2 col-sm-5">{{$item->size}}</div>
                </div>
                <div class="row">
                    <div class=" col-md-2 col-sm-5 font-weight-bold">Total Size:</div>
                    <div class=" col-md-2 col-sm-5">{{''.$item->size_t.' inches'}}</div>
                    <div class=" col-md-2 col-sm-4 font-weight-bold">Comments</div>
                    <div class=" col-md-6 col-sm-8">{{$item->comments?$item->comments:'NA' }}</div>

                </div>
                <div class="row">
                    <div class="col-md-2 col-sm-5 font-weight-bold">Covered Discount</div>
                    <div class=" col-md-2 col-sm-5">{{$item->CDiscount()}}</div>
                    <div class="col-md-2 col-sm-5 font-weight-bold">Uncovered Discount</div>
                    <div class=" col-md-2 col-sm-5">{{$item->UDiscount()}}</div>
                </div>


            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-md-2">
                        <a href="/admin/item/{{$item->id}}/edit" class="btn btn-dark btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Edit
                        </a>
                    </div>
                    <div class="col-md-2">
                        <form id="del{{$item->id}}" action="/admin/item/{{$item->id}}" method="POST">
                            <input name="_method" type="hidden" value="DELETE">
                            <input name="_token" type="hidden" value="{{csrf_token()}}">
                            <a href="#" onclick="clicked({{$item->id}})"  class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
                                <i class="fa fa-eraser"></i> Delete
                            </a>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script>
        function clicked(id){
            if(confirm("Are You Sure ?")){
                document.getElementById('del'+id).submit();
            }
            else{
            }
        }
    </script>
@endsection