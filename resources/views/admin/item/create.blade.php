@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::open(['method'=>'POST','action'=>'AdminItemController@store','class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('quality_id','Quality:')!!}
                    </div>
                    <div class="col-md-3 col-sm-10">
                        {!! Form::select('quality_id',$qualities,null,['class'=>'form-control','placeholder'=>'Choose Quality'])!!}

                    </div>
                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('name','Item:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Name'])!!}

                    </div>

                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('category_id','Category:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::select('category_id',$categories,null,['class'=>'form-control','placeholder'=>'Choose Category'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-1 col-sm-2">
                                {!! Form::label('opening_date','Opening Date :')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                                {!! Form::date('opening_date',null,['class'=>'form-control','required'])!!}
        
                    </div>

                    <div class="col col-md-1 col-sm-2">
                            {!! Form::label('opening_quantity','Opening Stock :')!!}
                </div>
                <div class=" col-md-3 col-sm-10">
                            {!! Form::number('opening_quantity',0,['class'=>'form-control'])!!}
    
                </div>
                <div class="col col-md-1 col-sm-2">
                        {!! Form::label('opening_price','Opening Price :')!!}
            </div>
            <div class=" col-md-3 col-sm-10">
                        {!! Form::number('opening_price',0,['class'=>'form-control','required'])!!}

            </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('purchase_price','Purchase Price:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::text('purchase_price',null,['class'=>'form-control','placeholder'=>'Purchase Price'])!!}

                    </div>

                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('retail_price','Retail Price:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::text('retail_price',null,['class'=>'form-control','placeholder'=>'Optional'])!!}

                    </div>
                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('sale_price','Sale Price:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::text('sale_price',null,['class'=>'form-control','placeholder'=>'Sale Price'])!!}

                    </div>

                </div>
                <div class="row form-group">
                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('sales_tax','Sales Tax:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::text('sales_tax',null,['class'=>'form-control','placeholder'=>'Optional'])!!}

                    </div>
                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('package_unit','Packaging Unit:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::select('package_unit',['Pcs'=>'Pieces','Ms'=>'Metres', 'Kgs'=>'Kilograms','Doz'=>'Dozen','Gms'=>'Grams'],null,['class'=>'form-control','placeholder'=>'Choose Unit'])!!}

                    </div>
                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('package_amount','Packaging Amount:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::text('package_amount',null,['class'=>'form-control','placeholder'=>'Packaging Amount'])!!}

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('default_quantity','Order Level:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::text('default_quantity',null,['class'=>'form-control','placeholder'=>'Order Level'])!!}

                    </div>
                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('current_quantity','Current Stock:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::text('current_quantity',null,['class'=>'form-control','placeholder'=>'Shop Level'])!!}

                    </div>
                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('store_quantity','Store Level:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::text('store_quantity',null,['class'=>'form-control','placeholder'=>'Store Level'])!!}

                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('bar_code','Bar Code:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::text('bar_code',null,['class'=>'form-control','placeholder'=>'Bar Code'])!!}

                    </div>
                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('covered','Covered:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::select('covered',[0=>'No',1=>'Yes'],null,['class'=>'form-control','placeholder'=>'Select Option'])!!}

                    </div>
                    <div class="col col-md-1 col-sm-2">
                        {!! Form::label('comments','Comments:')!!}
                    </div>
                    <div class=" col-md-3 col-sm-10">
                        {!! Form::text('comments',null,['class'=>'form-control','placeholder'=>'Comments'])!!}
                    </div>

                </div>
                <div class="row form-group">

                    <div class="col-md-1 col-sm-12">Size in inches</div>
                    <div class="col-1">{!! Form::label('size_l','Length:')!!}</div>
                    <div class="col-md-1 col-sm-2">

                        {!! Form::text('size_l',null,['class'=>'form-control','placeholder'=>'Size','id'=>'size_l'])!!}
                    </div>
                    <div class="col-1">{!! Form::label('size_w','Width:')!!}</div>
                    <div class="col-md-1 col-sm-2">

                        {!! Form::text('size_w',null,['class'=>'form-control','placeholder'=>'Size','id'=>'size_w'])!!}
                    </div>
                    <div class="col-1">{!! Form::label('size_h','Height:')!!}</div>
                    <div class="col-md-1 col-sm-2">

                        {!! Form::text('size_h',null,['class'=>'form-control','placeholder'=>'Size','id'=>'size_h'])!!}
                    </div>
                    <div class="col-1">{!! Form::label('size_t','Total:')!!}</div>
                    <div class="col-md-1 col-sm-2">

                        {!! Form::text('size_t',0,['class'=>'form-control','placeholder'=>'Size','id'=>'total','readonly'])!!}
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-1">{!! Form::label('photo','Picture:')!!}</div>
                    <div class=" col-md-3 col-sm-5">
                {!! Form::file('photo',null,['class'=>'form-control'])!!}
                    </div>
                </div>

            </div>
            <div class="card-footer">

                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Submit
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#size_l').keyup(function () {
                //console.log('Pressed L');
                let l = $('#size_l');
                let w = $('#size_w');
                let h = $('#size_h');
                lvalue=l.val();
                wvalue=w.val();
                hvalue=h.val();
                if(lvalue!='' && hvalue!='' && wvalue!='' ){
                    if(checkvalid(lvalue) && checkvalid(wvalue) && checkvalid(hvalue)){
                        $('#total').val(calc(lvalue,wvalue,hvalue));
                    }
                    else{
                        $('#total').val(0);
                    }
                }
                else{
                    $('#total').val(0);
                }
            });

            $('#size_w').keyup(function () {
                let l = $('#size_l');
                let w = $('#size_w');
                let h = $('#size_h');
                lvalue=l.val();
                wvalue=w.val();
                hvalue=h.val();
                if(lvalue!='' && hvalue!='' && wvalue!='' ){
                    if(checkvalid(lvalue) && checkvalid(wvalue) && checkvalid(hvalue)){
                        $('#total').val(calc(lvalue,wvalue,hvalue));
                    }
                    else{
                        $('#total').val(0);
                    }
                }
                else{
                    $('#total').val(0);
                }
            });
            $('#size_h').keyup(function () {
                let l = $('#size_l');
                let w = $('#size_w');
                let h = $('#size_h');
                lvalue=l.val();
                wvalue=w.val();
                hvalue=h.val();
                if(lvalue!='' && hvalue!='' && wvalue!='' ){
                    if(checkvalid(lvalue) && checkvalid(wvalue) && checkvalid(hvalue)){
                        $('#total').val(calc(lvalue,wvalue,hvalue));
                    }
                    else{
                        $('#total').val(0);
                    }
                }
                else{
                    $('#total').val(0);
                }
            });

            function checkvalid(value){
                if(isNaN(value))
                    return false;
                else return true;
            }

            function calc(x,y,z){
                return x*y*z;
            }
        });


    </script>
    @include('includes.errors')
@endsection