@extends('layouts.myapp')
@section("styles")

    <link rel="stylesheet" href="/css/datatables.min.css">
    {{-- <link rel="stylesheet" href="/css/dataTables.bootstrap.min.css"> --}}


    <style>
        tr{
            box-shadow: 0 .5rem 1rem rgba(0,0,0,.15) !important;
        }
        .dataTables_processing.card{
            visibility: hidden !important;
        }
        .img{
            padding: 22px 40px !important;
            padding-left: 10px !important;
        }

        /* #tableID thead tr {
        display: none !important;
    } */
    </style>

@endsection

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-1">
                    <h3 class="title-5 m-b-35">Items</h3>
                </div>
                <div class="col-md-4">
                    <a id="add" href="/admin/item/create" class="au-btn au-btn-icon au-btn--green au-btn--small float-right">
                        <i class="zmdi zmdi-plus"></i>add items</a>
                </div>
            </div>

        </div>

    </div>
            <div  class="table-responsive table-responsive-data2">
                <table id="tableID" class="table table-data2">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th  class="max-width:50px;">Photo</th>
                        <th>Quality</th>
                        <th>Item</th>
                        <th>Purchase Price</th>
                        <th >Sale Price</th>
                        <th>Category</th>
                        <th>Covered</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    {{-- <tbody>
                    @foreach($items as $item)
                    <tr class="shadow">
                        <td class="img"><img height="50px"  width="50px" class="img-circle img-responsive" src="{{$item->photo? asset('images/'.$item->photo->path): 'https://placehold.co/50x50'}}" alt=""></td>
                        <td>{{$item->quality?$item->quality->name:'Deleted / NA'}}</td>
                        <td><a href="/admin/item/{{$item->id}}">{{$item->name}}</a></td>
                        <td class="text-right">{{$item->purchase_price}}</td>
                        <td class="text-right">{{$item->sale_price}}</td>
                        <td>{{$item->category?$item->category->name:'Deleted / NA'}}</td>


                        <td>{{$item->covered==1?'Yes':'No'}}</td>
                        <td>
                            <div class="table-data-feature">
                                <form id="del{{$item->id}}" action="/admin/item/{{$item->id}}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                    <a href="#" onclick="clicked({{$item->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </a>
                                </form>
                                <a href="/admin/item/{{$item->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </a>

                            </div>

                        </td>
                    </tr>
                        @endforeach
                    </tbody> --}}
                </table>
            

            

    </div>       <!-- END DATA TABLE-->
   
@endsection


@section("scripts")
<script>
    function clicked(id){
        if(confirm("Are You Sure ?")){
            document.getElementById('del'+id).submit();
        }
        else{
        }
    }
    Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
        if (e.preventDefault) {
            e.preventDefault();
            $('#add').click();


        } else {
            // internet explorer
            e.returnValue = false;
        }

    });
</script>
<script src="/js/datatables.min.js"></script>
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var $datatable = $('#tableID');
        var table = $datatable.DataTable({
            scrollResize: true,
            scrollY: window.innerHeight-400,
            scrollCollapse: true,
            pageResize: true,
            scrollX:true,
            "autoWidth": false,
            "columns": [
                {"data": "id"},
                {"data":"id",render:function(id,type,row){
                    let data =row.photo;
                    return `<img height="50px"  width="50px" class="img-circle img-responsive" src="${data?'images/'+data.path: 'https://placehold.co/50x50'}" alt="">`
                }},
               {"data":"quality",name:"quality.name"},
                {"data": "name",render:function(data,type,row){
                    return `<a href="/admin/item/${row.id}">${data}</a>`;
                }},
                {"data": "purchase_price"},
                {"data": "sale_price"},
               
                {"data":"category",name:"category.name"},
                {"data":"covered",render:function(data){
                    return data==1?"C":"-";
                }},
                {
                    "data": "id",
                    "className": "center",
                    "render": function(id){
                        let s =``;
                        s+=` <div class="table-data-feature"><form id="del${id}" action="/admin/item/${id}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input name="_token" type="hidden" value="{{csrf_token()}}">
                                    <a href="#" onclick="clicked(${id})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                    </a>
                                </form>
                                <a href="/admin/item/${id}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </a></div>`;
                        return s;
                    }
                }
            ],
            'processing': true,
            "stateSave": false,
            'serverSide': true,
            'ajax': {
                'url': '{{ route("item-data") }}',
                'type': 'POST'
            },
            'order': [[ 0, 'asc' ]],
            'columnDefs': [
                { "orderable": false, "targets": [-1] },
                { "searchable": false, "targets": [-1,1,2,6] }
            ],
            "dom":"Bfltip"
        });
        $( table.table().container() ).removeClass( 'form-inline' );
    });
</script>

@endsection