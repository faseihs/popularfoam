@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::model($discount,['method'=>'PATCH','action'=>['AdminDiscountController@update',$discount->id],'class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
            {!! Form::text('quality_id',null,['class'=>'form-control','style'=>'display:none'])!!}
            <div class="card-body card-block">

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('item_id','Item:')!!}
                    </div>
                    <div class="col-3 col-md-3">
                        {!! Form::select('item_id',[0=>'All Items']+$items,null,['class'=>'form-control'])!!}

                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('Covered Discount','Covered Discount')!!}
                    </div>
                    <div class="col-3 col-md-2">
                        {!! Form::number('covered',null,['class'=>'form-control','min'=>0,'max'=>'100'])!!}
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('Uncovered Discount','Uncovered Discount')!!}
                    </div>
                    <div class="col-3 col-md-2">
                        {!! Form::number('uncovered',null,['class'=>'form-control','min'=>0,'max'=>'100'])!!}
                    </div>
                </div>

            </div>



        </div>
        <div class="card-footer">

            {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])!!}
        </div>
        {!! Form::close() !!}
    </div>
    @include('includes.errors')
@endsection