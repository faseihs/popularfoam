@extends('layouts.myapp')
@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <strong>Fill in the Details</strong>
            </div>
            {!! Form::open([
                'method' => 'POST',
                'action' => 'AdminQualityController@store',
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data',
            ]) !!}
            <div class="card-body card-block">
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('name', 'Name:') !!}
                    </div>
                    <div class=" col-md-3">
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter Name']) !!}
                        <span class="help-block">Please enter the Company's name</span>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('company_id', 'Company:') !!}
                    </div>
                    <div class=" col-md-3">
                        {{-- {!! Form::select('company_id',array_merge([0=>'Choose Company'],$companies),null,['class'=>'form-control'])!!} --}}
                        <select required name="company_id" class="form-control" id="company_id">
                            <option value="0">Choose Company</option>
                            @foreach ($companies as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <span class="help-block">Please select Company's name</span>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        {!! Form::label('comments', 'Comments:') !!}
                    </div>
                    <div class=" col-md-3">
                        {!! Form::textarea('comments', null, ['class' => 'form-control', 'placeholder' => 'Comments', 'rows' => 3]) !!}
                        <span class="help-block">Any Comments (Optional)</span>
                    </div>
                </div>

            </div>



        </div>
        <div class="card-footer">

            {!! Form::submit('Submit', ['class' => 'btn btn-primary btn-sm']) !!}
        </div>
        {!! Form::close() !!}
    </div>
    </div>
    @include('includes.errors')
@endsection
