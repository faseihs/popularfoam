@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                    <div class="row">
                            <div class="col-md-2">
                                <h3 class="title-5 m-b-35">Quality</h3>
                            </div>
                            <div class="col-md-6">
                                <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search Qualities...">
                            </div>
                            <div class="table-data__tool-right col-md-4">
                                <a id="add" href="/admin/quality/create" class="au-btn au-btn-icon au-btn--green au-btn--small float-right">
                                    <i class="zmdi zmdi-plus"></i>add qualities</a>
                            </div>
                        </div>
            </div>

        </div>
        @if(count($qualities)==0)
            <h1 class="text-md-center" >No Qualities Found</h1>
        @endif
        @if(count($qualities)>0)
            <div class="table-responsive table-responsive-data2">
                <table id="tableID" class="table table-data2">
                    <thead>
                    <tr>

                        <th>id</th>
                        <th>name</th>
                        <th>Company</th>
                        <th>Comments</th>
                        <th>created</th>
                        <th>updated</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($qualities as $quality)
                        <tr class="tr-shadow">

                            <td>{{$quality->id}}</td>
                            <td class="desc"><a href="/admin/quality/{{$quality->id}}">{{$quality->name}}</a></td>
                            <td>{{$quality->company?$quality->company->name:'Deleted'}}</td>
                            <td>{{$quality->comments?$quality->comments: 'None'}}</td>
                            <td>{{Carbon::parse($quality->created_at)->format('d-m-Y')}}</td>
                            <td>
                                {{Carbon::parse($quality->updated_at)->format('d-m-Y')}}
                            </td>
                            <td>
                                <div class="table-data-feature">
                                    <form id="del{{$quality->id}}" action="/admin/quality/{{$quality->id}}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        <a href="#" onclick="clicked({{$quality->id}})"  class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                    </form>
                                    <a href="/admin/quality/{{$quality->id}}/edit" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>

                                    </a>
                                </div>
                            </td>
                        </tr>
                        <tr class="spacer"></tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
    </div>
    <script>
        function clicked(id){
            if(confirm("Are You Sure ?")){
                document.getElementById('del'+id).submit();
            }
            else{
            }
        }
        Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
            if (e.preventDefault) {
                e.preventDefault();
                $('#add').click();


            } else {
                // internet explorer
                e.returnValue = false;
            }

        });
    </script>
    <!-- END DATA TABLE-->
    @include('includes.search');
@endsection