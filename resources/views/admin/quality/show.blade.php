@extends('layouts.myapp')
@section('content')

    <div class="col-12">
        <div class="default-tab">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home"
                                                     aria-selected="true">Overview</a>
                     <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-item" role="tab" aria-controls="nav-home"
                                                     aria-selected="true">Price List</a>
                     

                </div>
            </nav>
            <div class="tab-content pl-3 pt-2" id="nav-tabContent">
				<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="row text-left">
                        <div class="col-md-6">
                            ID : <strong>{{$quality->id}}</strong>
                        </div>

                        <div class="col-md-6">
                            Quality : <strong>{{$quality->name}}</strong>    
                        </div>
                    </div>

                    <div class="row text-left">
                            <div class="col-md-6">
                                Company : <strong>{{$quality->company?$quality->company->name:'Deleted / NA'}}</strong>
                            </div>
    
                            <div class="col-md-6">
                                Comments : <strong>{{$quality->comments}}</strong>    
                            </div>
                    </div>
                    <div class="row text-left">
                            <div class="col-md-6">
                                    Data Creation : <strong>{{Carbon::parse($quality->created_at)->format('d-m-Y')}}</strong>    
                                </div>
    
                            <div class="col-md-6">
                                Data Updated : <strong>{{Carbon::parse($quality->updated_at)->format('d-m-Y')}}</strong>    
                            </div>
                    </div>
                    <div class="row text-left">
                            <div class="col-md-6">
                                Total Items : <strong>{{$quality->items->count()}}</strong>
                            </div>
    
                            <div class="col-md-6">
                               
                            </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="nav-item" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="table-responsive">
                        <table class="table table-top-campaign">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Item</th>
                                    <th>Purchase Price</th>
                                    <th>Sale Price</th>
                                    <th><button id="save-all" class="btn btn-primary"><i class="fa fa-check"></i> Save All</button></th>
                                    
                                </tr>
                            </thead>
                            @foreach ($items as $item)
                                <tr>
                                    <td class="itemID">{{$item->id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td class="pprice"><input class="form-control-sm" type="number" value="{{$item->purchase_price}}"></td>
                                    <td class="sprice"><input class="form-control-sm" type="number" value="{{$item->sale_price}}"></td>
                                    <td><button class="btn btn-primary btn-sm saveBtn">Save</button> <div style="display:none" class="loader"></div></td>
                                    <td></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @include('includes.errors')
    </div>
    
    <style>
        .au-breadcrumb2 {
             padding-top: 10px;
            padding-bottom: 10px;
           
        }

        .loader,
.loader:after {
  border-radius: 50%;
  width: 10em;
  height: 10em;
}
.loader {
  margin: 0;
  font-size: 3px;
  position: relative;
  text-indent: -9999em;
  border-top: 1.1em solid #007bff;
  border-right: 1.1em solid #007bff;
  border-bottom: 1.1em solid #007bff;
  border-left: 1.1em solid #ffffff;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
  -webkit-animation: load8 1.1s infinite linear;
  animation: load8 1.1s infinite linear;
}
@-webkit-keyframes load8 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}
@keyframes load8 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(360deg);
    transform: rotate(360deg);
  }
}

    </style>

    <script>
        $('.saveBtn').on('click',function(){
            $(this).hide();
            $(this).next().show();
            var btn=$(this);
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var item={id:-1,pprice:-1,sprice:-1};
            item.id=$(this).parent().prevAll().last().html();
            item.pprice=$(this).parent().prev().prev().find('input').val();
            item.sprice=$(this).parent().prev().find('input').val();
            console.log(item);
                $.ajax({
                    /* the route pointing to the post function */
                    url: '/admin/quality/editpost',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, Item:item},
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) { 
                        btn.next().hide();
                        btn.show();
                    },
                    fail: function(xhr, textStatus, errorThrown){
                        console.log('request failed');
                    }
                }); 
            
        });

        $("#save-all").click(function(){
            $('.saveBtn').click();
        })
    </script>
@endsection