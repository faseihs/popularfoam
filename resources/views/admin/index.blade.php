@extends('layouts.myapp')

@section('content')
    <div class="container-fluid">
        <h1>Welcome to The Dashboard</h1>
        <hr/>
        <div class="row mb-5">
            <div class="col-md-9">
                    <a href="/point-of-sales" class="btn btn-block btn-dark"> Point Of Sales</a>
            </div>
        </div>
        <div class="row">
            @can('view',App\Category::class)
            <div class="col-md-3">
                <a href="/admin/category" class="btn btn-block btn-primary"> Go to Category</a>
            </div>
        @endcan
            @can('view',App\SalesInvoice::class)
                <div class="col-md-3">
                    <a href="/admin/sales_invoice" class="btn btn-block btn-warning"> Go to Sales Invoice</a>
                </div>
            @endcan

            @can('view',App\PurchaseOrder::class)
                    
                    <div class="col-md-3">
                        <a href="/admin/purchase_order" class="btn btn-block btn-dark"> Go to Purchase Order</a>
                    </div>
            @endcan

        </div>
        <br>
        <div class="row">
        @can('view',App\Company::class)
                <div class="col-md-3">
                    <a href="/admin/company" class="btn btn-block btn-info"> Go to Company</a>
                </div>
        @endcan
        @can('view',App\SalesReturn::class)
        <div class="col-md-3">
            <a href="/admin/sales_return" class="btn btn-block btn-dark"> Go to Sales Return</a>
        </div>
    @endcan
        @can('view',App\PurchaseIvoice::class)    
                <div class="col-md-3">
                    <a href="/admin/purchase_invoice" class="btn btn-block btn-primary"> Go to Purchase Invoice</a>
                </div>
        @endcan


        </div>
        <br>
        <div class="row">

            @can('view',App\Item::class)
                <div class="col-md-3">
                    <a href="/admin/item" class="btn btn-block btn-dark"> Go to Items</a>
                </div>
              
            @endcan

            @can('view',App\orderBooking::class)
                <div class="col-md-3">
                    <a href="/admin/order_booking" class="btn btn-block btn-danger"> Go to Order Booking</a>
                </div>
            @endcan

        </div>
        <br>
        @can('view',App\Quality::class)
        <div class="row">
            <div class="col-md-3">
                <a href="/admin/quality" class="btn btn-block btn-info"> Go to Quality</a>
            </div>
        </div>
        @endcan
        <br>
        @can('view',App\Vehicle::class)
        <div class="row">
            <div class="col-md-3">
                <a href="/admin/vehicle" class="btn btn-block btn-danger"> Go to Vehicle</a>
            </div>
        </div>
        @endcan
    </div>

    <style>
        .btn-warning{
            color:white !important;
        }
    </style>
@endsection