@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div id="ledger-app">
        <vue-table :customers="customers"  :companies="companies"></vue-table>
    </div>

    <script src="{{asset('js/ledger.js')}}"></script>
    <style>
        .au-breadcrumb2 {
            padding-top: 10px;
            padding-bottom: 0px;
        }
    </style>
@endsection