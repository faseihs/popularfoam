@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <h3 class="title-5 m-b-35">cash registers</h3>
                </div>
                <div class="col-md-5">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search customers...">
                </div>
                {{--<div class="table-data__tool-right col-md-4">
                    <a id="add" href="/admin/company_cash/create" class="au-btn au-btn-icon au-btn--green au-btn--small float-right">
                        <i class="zmdi zmdi-plus"></i>add company_cash</a>
                </div>--}}
            </div>
        </div>

    </div>
    
        <div class="table-responsive table-responsive-data2">
            <table id="tableID" class="table table-data2">
                <thead>
                <tr>

                    <th>id</th>
                    <th>name</th>
                    <th>Paid</th>
                    <th>To Pay</th>
                    <th>Received</th>
                    <th>To Receive</th>
                    <th>created</th>
                    <th>updated</th>
                    
                </tr>
                </thead>
                <tbody>
                @foreach($company_cash as $c)
                    <tr class="tr-shadow">

                        <td>{{$c->id}}</td>
                        <td class="desc">{{$c->owner?$c->owner->name:'Deleted / NA'}}</td>
                        <td>{{number_format($c->received,0,'.',',')}} /-</td>
                        <td>{{number_format($c->to_receive,0,'.',',')}} /-</td>
                        <td>{{number_format($c->paid,0,'.',',')}} /-</td>
                        <td>{{number_format($c->to_pay,0,'.',',')}} /-</td>
                        <td>{{Carbon::parse($c->created_at)->format('d-m-Y')}}</td>
                        <td>
                            {{Carbon::parse($c->updated_at)->format('d-m-Y')}}
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                    
                @endforeach
                <tr class="dark">
                        <td colspan="2"><strong>Company Total</strong></td>
                        <td>{{number_format($companyTotal->paid,0,'.',',')}} /-</td>
                        <td>{{number_format($companyTotal->to_pay,0,'.',',')}} /-</td>
                        <td>{{number_format($companyTotal->received,0,'.',',')}} /-</td>
                        <td>{{number_format($companyTotal->to_receive,0,'.',',')}} /-</td>
                        <td colspan="2"></td>
                    </tr>
                    <tr class="spacer"></tr>
                <tr class="spacer"></tr>
                <tr class="spacer"></tr>
                @foreach($customer_cash as $c)
                    <tr class="tr-shadow">

                        <td>{{$c->id}}</td>
                        <td class="desc">{{$c->owner?$c->owner->name:'Deleted / NA'}}</td>
                        <td>{{number_format($c->received,0,'.',',')}} /-</td>
                        <td>{{number_format($c->to_receive,0,'.',',')}} /-</td>
                        <td>{{number_format($c->paid,0,'.',',')}} /-</td>
                        <td>{{number_format($c->to_pay,0,'.',',')}} /-</td>
                        <td>{{Carbon::parse($c->created_at)->format('d-m-Y')}}</td>
                        <td>
                            {{Carbon::parse($c->updated_at)->format('d-m-Y')}}
                        </td>
                    </tr>
                    <tr class="spacer"></tr>
                @endforeach
                <tr class="dark">
                        <td colspan="2"><strong> Customer Total</strong></td>
                        <td>{{number_format($customerTotal->paid,0,'.',',')}} /-</td>
                        <td>{{number_format($customerTotal->to_pay,0,'.',',')}} /-</td>
                        <td>{{number_format($customerTotal->received,0,'.',',')}} /-</td>
                        <td>{{number_format($customerTotal->to_receive,0,'.',',')}} /-</td>
                        <td colspan="2"></td>
                    </tr>
                    <tr class="spacer"></tr>
                    <tr class="spacer"></tr>

                    <tr class="dark">
                            <td colspan="2"><strong>Final Total</strong></td>
                            <td>{{number_format($grandTotal->paid,0,'.',',')}} /-</td>
                            <td>{{number_format($grandTotal->to_pay,0,'.',',')}} /-</td>
                            <td>{{number_format($grandTotal->received,0,'.',',')}} /-</td>
                            <td>{{number_format($grandTotal->to_receive,0,'.',',')}} /-</td>
                            <td colspan="2"></td>
                        </tr>

                </tbody>
            </table>
        </div>
        
        </div>
        </div>
        <script>
            function clicked(id){
                if(confirm("Are You Sure ?")){
                    document.getElementById('del'+id).submit();
                }
                else{
                }
            }
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#add').click();
                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });
        </script>
        <style>
            .dark{
                background-color: #4272d7;;
                color:white !important;
            }
            .dark td{
                color:white !important;
            }
        </style>
        <!-- END DATA TABLE-->
        @include('includes.search');
@endsection