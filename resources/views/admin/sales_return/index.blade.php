@extends('layouts.myapp')

@section('content')
    <!-- DATA TABLE-->

    <div class="row">
        @include('includes.flash')
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2">
                    <h3 class="title-5 m-b-35">Sales Returns</h3>
                </div>
                <div class="col-md-5">
                    <input onkeyup="search()" id="searchID" class="au-input--w300 au-input--style2" type="text" placeholder="Search...">
                </div>
                <div class="table-data__tool-right col-md-4">
                    <a id="add" class="btn btn-primary" href="/admin/sales_return/create">
                        Add Sales Return
                    </a>
                </div>
            </div>

        </div>

        @if(count($sales_returns)==0)
            <h1 class="text-md-center">No Sales Returns Found</h1>
        @endif
        @if(count($sales_returns)>0)

            <div id="tableID" class="table-responsive table--no-card m-b-30">
                <table class="table table-borderless table-striped table-earning">
                    <thead>
                    <tr>
                        <th>Invoice Date</th>

                        <th>Customer Category</th>
                        <th>Customer</th>
                        <th>Customer Number</th>
                        <th class="text-right">Total Bill</th>
                        <th>Created Date</th>
                        <th>Created By</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sales_returns as $sales_return)
                        <tr>
                            <td>{{Carbon::parse($sales_return->date)->format('d-m-Y')}}</td>
                            <td>{{$sales_return->customer?$sales_return->customer->name:'Walking Customer'}}</td>
                            <td>{{$sales_return->name}}</td>
                            <td>{{$sales_return->number}}</td>
                            <td class="text-right">{{$sales_return->total_price}}</td>

                            <td>{{Carbon::parse($sales_return->updated_at)->format('d-m-Y')}}</td>
                            <td>{{$sales_return->user?$sales_return->user->name:'-'}}</td>
                            <td>
                                <div class="table-data-feature">
                                    <form id="del{{$sales_return->id}}" action="/admin/sales_return/{{$sales_return->id}}" method="POST">
                                        <input name="_method" type="hidden" value="DELETE">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        <a href="#" onclick="clicked({{$sales_return->id}})"  class="sales_return" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <i class="zmdi zmdi-delete"></i>
                                        </a>
                                    </form>
                                    <a href="/admin/sales_return/{{$sales_return->id}}/edit" class="sales_return" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>

                                </div>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        @endif

        <script>
            function clicked(id){
                if(confirm("Are You Sure ?")){
                    document.getElementById('del'+id).submit();
                }
                else{
                }
            }
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#add').click();

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });
        </script>

    </div>       <!-- END DATA TABLE-->
    @include('includes.search');
@endsection