@extends('layouts.simple')
@section('content')
    <div id="hidden" style="display:none"></div>
    <div class="col-12">
        <div class="card">
            {!! Form::model($sales_return,['method'=>'PATCH','action'=>['AdminSalesReturnController@update',$sales_return->id],'onsubmit'=>'return submitFunc()','id'=>'formID']) !!}
            <div class="card-header">
                <input id="sales_invoice_id" name="sales_invoice_id" value="{{$sales_return->sales_invoice_id}}" type="hidden">
                <input id="customer_id" name="customer_id" value="{{$customer?$customer->id:0}}"  type="hidden">
                <input id="totalPriceID" name="total_price" type="hidden">
                <div class="d-flex justify-content-around">
                    <strong class="p2">Sales Invoice # {{$sales_return->salesInvoice->id}}</strong>
                    <div id="customerID" class="p2"><strong>Customer: </strong>{{$customer?$customer->name:'Waking Customer'}}<span></span></div>
                    <div class="p2">
                        Previous Balance: <strong><span>{{$cr?$cr->to_pay:'No Previous Balance'}}</span></strong>
                    </div>
                    <div class="p2">
                        Last Payment : <strong><span>{{$payment?$payment->amount:'No Previous Sale'}}</span></strong>
                    </div>
                    <div class="p2">
                        Date :
                        {!! Form::date('date',null,['class'=>'form-control-sm']) !!}
                    </div>


                </div>


            </div>
            <div class="card-body card-block">
                <div style="overflow-y: scroll" id="itemDivID" class="table-responsive table--no-card m-b-30">
                    <table id="itemTableID" class="table table-borderless  table-earning">
                        <thead>
                        <tr>

                            <th>Quality</th>
                            <th>Item</th>
                            <th class="text-right">Covered</th>
                            <th class="text-right">Quantity</th>
                            <th class="text-right">Bonus</th>
                            <th class="text-right">Price</th>
                            <th class="text-right">Discount</th>
                            <th class="text-right">Total</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $item)
                            <tr id="{{$item->item->id}}">
                                <td>{{$item->item->quality->name}}</td>
                                <td>{{$item->item->name}}</td>
                                <td class="text-right covered">
                                    <select class="form-control-sm mousetrap">
                                        <option selected value={{$item->covered}}>{{$item->covered==0?'No':'Yes'}}</option>
                                        <option value="{{$item->covered==0?1:0}}">{{$item->covered==0?'Yes':'No'}}</option>
                                    </select>
                                </td>
                                <td style="display: none" class="size">{{$item->item->size_t}}</td>
                                <td class="udiscount" style="display: none">{{$item->item->UCDiscount($customer?$customer->id:0)}}</td>
                                <td class="cdiscount" style="display: none">{{$item->item->CCDiscount($customer?$customer->id:0)}}</td>

                                <td class="text-right quantity"><input  min="1" class="form-control-sm sm quantityIN mousetrap" type="number" value="{{$item->quantity}}"/></td>
                                <td class="text-right bonus"><input min="0" class="form-control-sm sm mousetrap" type="number" value="{{$item->bonus}}"/></td>
                                <td class="text-right price">{{$item->price}}</td>

                                <td class="text-right discount"><input min="0" max="100" class="form-control-sm sm discountIN mousetrap" type="number" value="{{$item->discount}}"/> %</td>

                                <td class="text-right total">{{$item->total_price}}</td>
                                <td style="display: none"><input class="obj" type="checkbox" name="checkBoxArray[]"  value=""></td>
                                <td><button class="btn btn-danger btn-sm deleteBtn" type="button">Delete</button></td>



                            </tr>
                        @endforeach

                        </tbody>

                    </table>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div id="total" class="col-md-2"><strong>Total : </strong><span>{{$sales_return->total_price}}</span></div>
                    <div class="col-md-3">
                        {!! Form::label('paid_amount','Paid Amount') !!}
                        {!! Form::number('paid_amount',null,['class'=>'form-control-sm col-md-4 mousetrap','min'=>0,'id'=>'paid_amount']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Form::label('returned','Returned') !!}
                        {!! Form::text('returned',null,['class'=>'form-control-sm col-md-4','readonly','id'=>'returned']) !!}
                    </div>
                    <div class="col-md-4">
                        {!! Form::submit('Submit',['class'=>'btn btn-primary btn-sm float-right','id'=>'submitID'])!!}
                    </div>

                </div>




            </div>

        </div>
        {!! Form::close() !!}
    </div>
    @include('includes.errors')
    <style>
        .card-body{
            min-height: 83vh;
            max-height: 83vh !important;
        }
        tr:focus{
            background: #b2b2b2;
        }
        tr:focus{
            background: #b2b2b2;
        }

        .selected{
            background: #b2b2b2 !important;
            color: white !important;
        }
        .selected td{
            color:white !important;
        }
        .form-control-sm{
            border:1px solid #ced4da !important;
        }
        .sm{
            width: 50px !important;
        }
        #itemDivID{
            max-height: 70vh;
        }
        #table_ID td,th{
            padding: 12px 10px !important;
        }
        #itemTableID td,th{
            padding: 12px 10px !important;
            font-size: 12px;
        }

        .card-header{
            padding: 0;
            font-size: 16px !important;
        }
        .card-footer{
            padding: 0;
            padding-top: 1px;
            padding-left: 5px;
            padding-right: 5px;
            font-size: 12px !important;
        }
        .form-control-sm{
            font-size: 10px !important;
        }
        select.form-control-sm{
            height: 1.5rem !important;
        }
        .p2{
            font-size: 12px !important;
        }
        .btn-sm{
            font-size: .5rem !important;
        }

    </style>

    {{--<script src="{{asset('js/po_create.js')}}"></script>--}}
    <script>
        let customerId={{$sales_return->customer_id}};

        function setItems(items){
            finalStr=``;
            for (let i =0;i<items.length;i++){
                let item = items[i];
                finalStr+=`<tr  tabindex="${item.id}">
                                    <!--<td><button class="btn btn-sm btn-primary">Select</button></td>-->
                                    <td>${item.id}</td>
                                     <td>${item.quality}</td>
                                    <td>${item.name}</td>
                                    <td>${item.sale_price}</td>

                                    <td>${item.size_t}</td>
                                    <td>${item.Covered}</td>

                                    <td>
                                        ${item.covered_discount}
                    </td>
                    <td>
${item.uncovered_discount}
                    </td>
                    <td style="display:none">${item.package_unit}</td>
                                    <td style="display:none">${item.current_quantity}</td>
                                </tr>`
                finalStr+=`</tr>`;

            }
            $('#itemTbody').append(finalStr);
            bindEvents();

        }

        $(document).ready(function () {
            calcTotal();

            Date.prototype.toDateInputValue = (function() {
                var local = new Date(this);
                local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
                return local.toJSON().slice(0,10);
            });
            //$('#date').val(new Date().toDateInputValue());
            Mousetrap.bind(['ctrl+a', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    //$('#search').click();
                    $('#scrollmodal').modal('toggle');

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            Mousetrap.bind(['ctrl+q', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#paid_amount').focus();

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });
            Mousetrap.bind(['ctrl+s', 'meta+s'], function(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                    $('#submitID').click();

                } else {
                    // internet explorer
                    e.returnValue = false;
                }

            });

            /*$('#pay_later select').on('change',function () {
               if($(this).val()==1) {
                   $('.card-footer').append(udhar);
                   if((!isNaN(parseInt($('#total span').html())) && parseInt($('#total span').html())>0) && !isNaN(parseInt($('#paid_amount').val()))){
                       var ret=$('#paid_amount').val() - parseInt($('#total span').html());
                       $('#to_pay').val(abs(parseInt(ret)));
                   }
               }
               else $('#udhar').remove();
            });*/


            $('#scrollmodal').on('shown.bs.modal', function () {
                $('#searchIn').focus();
            });
            $('#scrollmodal').on('hidden.bs.modal', function () {
                $('#tableID tr').each(function () {
                    $(this).removeClass('selected');
                    $('#searchIn').val('');
                    $('#table_ID tr').each(function () {
                        $(this).css('display','');
                    });
                });
                $('#search').blur();
            });
            $('#scrollmodal,.selected').on('keydown',function (e) {
                let key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
                if(key==40) {
                    //$('#scrollmodal').focus();
                    //e.preventDefault();
                    $('#searchIn').blur();
                    //console.log('Key Down Pressed');
                    if($('.selected:not(:hidden)').length==0){

                        $('#tableID tbody tr').not(':hidden').first().addClass('selected').focus();
                    }
                    else{
                        let current=$('.selected:not(:hidden)');
                        if(current.nextAll().not(':hidden').length){
                            current.nextAll().not(':hidden').first().addClass('selected').focus();
                            current.removeClass('selected');
                            //current.next().focus();
                        }
                        else{
                            current.removeClass('selected');
                            $('#searchIn').focus();
                        }
                    }

                }

                //Up Key
                if(key==38){
                    //e.preventDefault();
                    //$('#scrollmodal').focus();
                    console.log('Dabb gaya');
                    $('#searchIn').blur();
                    if($('.selected:not(:hidden)').length==0){

                        $('#tableID tbody tr').not(':hidden').last().addClass('selected').focus();
                        console.log('yahan 1');
                    }
                    else{
                        let current=$('.selected:not(:hidden)');
                        if(current.prevAll().not(':hidden').length){
                            current.prevAll().not(':hidden').first().addClass('selected').focus();
                            current.removeClass('selected');
                        }
                        else{
                            current.removeClass('selected');
                            $('#searchIn').focus();
                            console.log('yahan 3');
                        }
                    }

                }

                if(key==13){
                    e.preventDefault();
                    $('#scrollmodal').focus();
                    //console.log('Enter Pressed');
                    if($('.selected:not(:hidden)').length>0) {
                        let selected=$('.selected');
                        //$('.card-body').append(selected.html());
                        $('#scrollmodal').modal('toggle');
                        getItemFromTable(selected);
                        calcTotal();
                        

                    }

                }
            });
            $('#company').on('change', function() {
                let comp=$('#company option:selected').html();
                if(comp!=='Company')
                    $('#companyID span').html(comp);
                else $('#companyID span').html('');
            });

            $('#vehicle').on('change', function() {
                let comp=$('#vehicle option:selected').html();
                if(comp!=='Vehicle' || $('#total-coverage').hasClass('alert-danger')) {
                    $('#vehicleID span').html(comp);
                    setVehicleSize();
                   
                    $('#submitID').removeAttr('disabled');

                }
                else {$('#vehicleID span').html('');setVehicleSize();$('#submitID').attr('disabled','true');}
            });

            $('.discountIN').on('keyup',function () {

                calcTotal();
                console.log('Yahoo');
            });
            $('.quantityIN').on('keyup',function () {
                calcTotal();
                
            });

            $('.bonus input').on('change',function () {
                
            });


            $('#table_ID tbody tr').on('click',function () {
                $('#scrollmodal').modal('toggle');
                getItemFromTable($(this));
                calcTotal();
                
            });

            $('tr').on('focus',function(){
                console.log('FOvussed');
            });

            $('#paid_amount').on('keyup',function () {
                if(!isNaN(parseInt($('#total span').html()))) {
                    $('#returned').val($(this).val() - parseInt($('#total span').html()));
                    $('#to_pay').val(abs($(this).val() - parseInt($('#total span').html())));
                }
            });

            $('.deleteBtn').on('click',function(){
                $(this).parent().parent().remove();
                calcTotal();
            });


        });
        function search() {
            $('#tableID tr').each(function () {
                $(this).removeClass('selected');
            });
            var input, filter, table, tr, td, i;
            input = document.getElementById("searchIn");
            filter = input.value.toUpperCase();
            table = document.getElementById("table_ID");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";

                    } else {
                        tr[i].style.display = "none";

                    }
                }
            }
        }

        function getItemFromTable(object){
            var item={id:-1, name:"", price:-1,quality:"" ,company:"",covered:'',udiscount:-1,cdiscount:-1};
            var counter=1;
            object.children().each(function () {
                if(counter==1)
                    item.id=$(this).html();
                else if(counter==2)
                    item.name=$(this).html();
                else if(counter==3)
                    item.price=$(this).html();
                else if(counter==4)
                    item.quality=$(this).html();
                else if(counter==5)
                    item.size=$(this).html();
                else if(counter==6)
                    item.covered=$(this).html();
                else if(counter==7)
                    item.udiscount=$(this).html();
                else if(counter==8)
                    item.cdiscount=$(this).html();
                counter++;
            });

            //console.log(item);
            addItemToTable(item);
        }

        function addItemToTable(item){
            var selected;
            var selected_;
            var unselected;
            var unselected_;
            var dvalue;
            if(item.covered=='No'){
                selected=0;
                selected_='No';
                unselected=1;
                unselected_='Yes';
                dvalue=item.udiscount;
            }
            else{
                selected=1;
                selected_='Yes';
                unselected=0;
                unselected_='No';
                dvalue=item.cdiscount;
            }


            var row=`    <tr id='`+item.id+`'>
                                <td>`+item.quality+`</td>
                                <td>`+item.name+`</td>
                                <td class="text-right covered">
                                <select class="form-control-sm mousetrap">
                                <option selected value='`+selected+`'>`+selected_+`</option>
                                <option value='`+unselected+`'>`+unselected_+`</option>
                                </select>
                                 </td>
                                <td style="display:none" class="size">`+item.size+`</td>
                                <td class="cdiscount" style="display: none">`+item.cdiscount+`</td>
                                <td class="udiscount" style="display: none">`+item.udiscount+`</td>

                                <td class="text-right quantity"><input  min="1" class="form-control-sm sm quantityIN mousetrap" type="number" value="1"/></td>
                                <td class="text-right bonus"><input min="0" class="form-control-sm sm mousetrap" type="number" value="0"/></td>
                                 <td class="text-right price">`+item.price+`</td>
                                <td class="text-right discount"><input min="0" max="100" class="form-control-sm sm discountIN mousetrap" type="number" value=`+dvalue+`/> %</td>

                                <td class="text-right total">`+item.price+`</td>
                                <td style="display: none"><input class="obj" type="checkbox" name="checkBoxArray[]"  value=""></td>
                                <td><button class="btn btn-danger btn-sm deleteBtn" type="button">Delete</button></td>



                       </tr>`;
            $('#itemTableID tbody').append(row);
            $('.quantityIN').bind('change', function() {
                calcTotal();
                console.log('Yahoo');
            });
            $('.discountIN').bind('change', function() {
                calcTotal();
                console.log('Yahoo');
            });


            $('.deleteBtn').bind('click',function () {
                $(this).parent().parent().remove();
                calcTotal();
            });

            $('.covered select').on('change',function () {
                var val =$(this).val();
                var p=$(this).parent().parent();

                //console.log(parseInt(p.find('.cdiscount').html()));
                //console.log(parseInt(p.find('.udiscount').html()));
                if(val==1)
                    p.find('.discountIN').val(parseInt(p.find('.cdiscount').html()));
                else{
                    p.find('.discountIN').val(parseInt(p.find('.udiscount').html()));
                }
                calcTotal();
            });




        }

        function calcTotal(){
            var sum=0;
            $('#itemTableID tbody').children().each(function () {

                var individualSum=parseInt($(this).children('.price').html()) *parseInt($(this).find('.quantity input').val());
                console.log(individualSum);
                var discount=parseInt($(this).find('.discount input').val());
                console.log(discount);
                if(discount>0){
                    individualSum=individualSum*(1-(discount/100));
                }
                individualSum=Number((individualSum).toFixed(1));
                $(this).children('.total').html(individualSum);
                sum+=individualSum;
            });
            sum=Number((sum).toFixed(1));
            $('#total span').html(sum);
            $('#totalPriceID').val(sum);
            if((!isNaN(parseInt($('#total span').html())) && parseInt($('#total span').html())>0) && !isNaN(parseInt($('#paid_amount').val()))) {
                var ret=$('#paid_amount').val() - parseInt($('#total span').html());
                $('#returned').val(ret);

            }
            else {
                $('#returned').val(0);
            }
        }

        function submitFunc(e){
            var prev_items=JSON.parse('@php echo json_encode($Item); @endphp ');
            if($('#itemTableID tbody tr').children().length<1){
                alert("No Items Selected");
                return false;
            }

            if(($('#paid_amount').val()-parseInt($('#total span').html())<0)){
                alert('Paid Amount is less than total amount ');
                return false;
            }

            /*
            if(($('#paid_amount').val()-parseInt($('#total span').html())>0) && $('#pay_later select').val()==1){
                alert('Paid Amount is greater so Pay Later (Yes) Option cannot be used ');
                return false;
            }
            if($('#customer_id').val()==0){
                if($('#pay_later select').val()==1)
                {
                    alert('Walking Customer has to pay the whole amount now');
                    return false;
                }
            }*/
            var item_array=Array();
            var error=false;
            $('#itemTableID tbody tr').each(function () {

                var id=$(this).attr('id');
                var item={item_id:-1,quantity:-1,total_price:-1,bonus:-1,discount:-1,covered:-1};
                item.item_id=id;
                var $this=$(this);
                var count=1;
                item.price=$(this).children('.price').html();
                //console.log($(this).find(':nth-child(5)').find(':nth-child(1)').val());
                item.quantity=$(this).children('.quantity').find(':nth-child(1)').val();
                item.bonus=$(this).children('.bonus').find(':nth-child(1)').val();
                item.discount=$(this).children('.discount').find(':nth-child(1)').val();
                item.covered=$(this).children('.covered').find(':nth-child(1)').val();
                item.total_price=$(this).children('.total').html();
                $(this).find('input.obj').val(item);
                //console.log($(this).find('input.obj').val());
                item_array.push(item);
                for(var i in prev_items){
                    if(prev_items[i].item_id==item.item_id){
                        if(prev_items[i].quantity<item.quantity) {

                            error=true;
                            break;
                        }
                    }
                }
            });
            if(error){
                alert('Quantity cannot exceed purchased amount');
                return false;
            }
            var items=JSON.stringify(item_array);
            $('#formID').append("<textarea id='items' name='items' style='display:none'>"+items+"</textarea> ");
            return true;
        }



    </script>
@endsection