@extends('layouts.myapp')

@section('content')
<div class="row">
       
    <h3 class="col-md-2">Profit : Bill Wise</h3>
    {!! Form::open(['method'=>'POST','action'=>'AdminReportController@billQuery','class'=>'col-md-10','id'=>'formRow']) !!}
    <div id="fRow" class="row">
        {!! Form::select('today',['all'=>'All','3'=>'From Date'],$default
        ,['class'=>'form-control-sm col-md-2 noprint','id'=>'selectID'])!!}
        <button style="margin-left:5px;" class="btn btn-primary btn-sm col-md-1 noprint" type="submit">Go</button>




        @if(isset($on_date))
            <input id='date'  type='date' class='form-control-sm col-md-3 on_date' value="{{Carbon::parse($on_date)->format('Y-m-d')}}" name='on_date' required />
        @endif

        @if (!isset($on_date) && !isset($from_date))
            <input id='date' style="display:none;padding-left:5px;"  type='date' class='form-control-sm col-md-3 today' value="{{Carbon::now()->format('Y-m-d')}}" name='on_date' required />
        @endif
        @if(isset($from_date))
            <input  type='date' class='form-control-sm col-md-2 duration_dates' value="{{Carbon::parse($from_date)->format('Y-m-d')}}" name='from_date' required />
            <input  type='date' class='form-control-sm col-md-2 duration_dates' value="{{Carbon::parse($to_date)->format('Y-m-d')}}" name='to_date' required />

        @endif
    </div>
    {!! Form::close() !!}

</div>

    @if (count($allSales)>0)
        @foreach ($allSales as $item)


        <table  class="table table-data2">
                <thead> 
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                       
                    </tr>
                </thead>
                <tbody>
                <tr class="sales">       
                            <td>ID : <strong><a  class="si" target="_blank" href="/admin/sales_invoice/{{$item->id}}">{{$item->id}}</a></strong></td>
                            <td>Date : <strong>{{Carbon::parse($item->date)->format('d-m-Y')}}</strong></td>
                            <td>Customer : <strong>{{$item->customer}}</strong></td>
                        </tr>
                </tbody>
            </table>



            <table  class="table table-data2">
                <thead> 
                    <tr>
                        
                        <th>Quality</th>
                        <th>Item</th>
                        <th>Qty</th>
                        <th>Pur.Val</th>
                        <th>Sal.Val</th>
                        <th>Profit</th>
                        <th>Discount</th>
                       
                    </tr>
                </thead>

                <tbody>
                @foreach ($item->items as $it)
                            <tr>
                               
                                <td><strong>{{substr($it->quality,0,10)}}</strong></td>
                                <td><strong><a target="_blank" style="color:#808080;" href="/admin/item/{{$it->id}}">{{$it->name}}</a></strong></td>
                                <td><strong>{{$it->quantity}}</strong></td>
                                <td><strong>{{number_format($it->iPurchase,1,'.',',')}}/-</strong></td>
                                <td><strong>{{number_format($it->iSale,1,'.',',')}}/-</strong></td>
                                <td><strong>{{number_format($it->iProfit,1,'.',',')}}/-</strong></td>
                                <td><strong>{{number_format($it->discount,1,'.',',')}}/-</strong></td>
                            </tr>
                       
                        @endforeach
                        <tr class="sales">
                            <td colspan="2" class="text-left">Profit % : <strong>{{number_format(100-(($item->finalPurchase*100)/$item->total_price), 2, '.', ',')}}%</strong></td>
                            <td class="text-leftt">{{$item->quantity}}</td>
                            <td class="text-left">{{number_format($item->finalPurchase,0,'.',',')}}/-</td>
                            <td class="text-left">{{number_format($item->total_price,0,'.',',')}}/-</td>
                            <td> Pofit : {{number_format($item->finalProfit,0,'.',',')}}/-</td>
                            <td><strong>{{number_format($item->discount,1,'.',',')}}/-</strong></td>
                        </tr>
                <tbody>
            </table>
            

        @endforeach   
        <hr>
        <hr>
        <table class="table table-data2">
            <thead>
                <tr>
                    <th>Total Quantity</th>
                    <th>Total Purchase Value</th>
                    <th>Total Sales Value</th>
                    <th>Total Profit</th>
                </tr>
            </thead>
            <tbody>
            <strong>
                <tr class="font-weight-bold">
                    <td>{{$totalQuantity}}/-</td>
                    <td>{{number_format($totalPurchaseValue,1,'.',',')}}/-</td>
                    <td>{{number_format($totalSaleValue,'1','.',',')}}/-</td>
                    <td>{{number_format($totalProfit,'1','.',',')}}/-</td>
                </tr>
            </strong>
            </tbody>
        </table>
            

    @else
    <div class="col-md-12 text-center"><h2>No Sales Invoices</h2></div>
    @endif
    

    <style>
        .si{
            background: white;
            padding: 2px;
            border-radius: 5px;
            color:blue !important;
        }

    td{
        padding-top:10px !important;
        padding-bottom:10px !important;
        padding-right:10px !important;
        
        
    }
        .au-input--w300{
            width: 100% !important;
        }
        .sales{
            background-color: rgb(57, 57, 57);;
            color:white;
            
            
        }
        .sales td{
            color:white !important;
            
        }

        .items{
            
            border:1px solid black !important;
        }
        @media print
        {
            .noprint {display:none !important}
            .today{
                display: block !important;
            }
        }
    </style>
    <script>
        var on_date= "<input style='margin-left:5px;' class='on_date' type='date' class='form-control-sm col-md-2' name='on_date' required />";
        var from_date="<input style='margin-left:5px;' class='duration_dates' type='date' class='form-control-sm col-md-2' name='from_date' required /><input  class='duration_dates' type='date' class='form-control-sm' name='to_date' required />"

        $(document).ready(function(){
            $('#selectID').on('change',function(){

                if($(this).val()==1 || $(this).val()=='all'){
                  
                    $('.duration_dates').remove();
                }

                else if($(this).val()==3){
                    $('#fRow').append(from_date);
                   
                }


            });
        });
    </script>
@endsection