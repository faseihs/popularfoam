@extends('layouts.myapp')

@section('content')
<div class="row">
       
    <h3 class="col-md-2">Profit</h3>
    {!! Form::open(['method'=>'POST','action'=>'AdminReportController@itemQuery','class'=>'col-md-10','id'=>'formRow']) !!}
    <div id="fRow" class="row">
        {!! Form::select('today',['all'=>'All','3'=>'From Date'],$default
        ,['class'=>'form-control-sm col-md-2 noprint','id'=>'selectID'])!!}
        <button style="margin-left:5px;" class="btn btn-primary btn-sm col-md-1 noprint" type="submit">Go</button>




        @if(isset($on_date))
            <input id='date'  type='date' class='form-control-sm col-md-3 on_date' value="{{Carbon::parse($on_date)->format('Y-m-d')}}" name='on_date' required />
        @endif

        @if (!isset($on_date) && !isset($from_date))
            <input id='date' style="display:none;padding-left:5px;"  type='date' class='form-control-sm col-md-3 today' value="{{Carbon::now()->format('Y-m-d')}}" name='on_date' required />
        @endif
        @if(isset($from_date))
            <input style='margin-left:15px'; type='date' class='form-control-sm col-md-2 duration_dates' value="{{Carbon::parse($from_date)->format('Y-m-d')}}" name='from_date' required />
            <input style='margin-left:15px';  type='date' class='form-control-sm col-md-2 duration_dates' value="{{Carbon::parse($to_date)->format('Y-m-d')}}" name='to_date' required />

        @endif
    </div>
    {!! Form::close() !!}

    </div>

<div class="row">


    <div class="col-md-12 text-left"><h3>Items</h3></div>
    <div class="col-12">
            
        <div id="tableDivID" class="table-responsive table-responsive-data2">
            <table id="tableID" class="table table-data2">
                <thead>
                <tr>
                    <th>Quality</th>
                    <th>Item</th>
                    <th>S.Item</th>
                    <th>P.Value</th>

                    <th>S.Value</th>
                    <th>Profit</th>
                </tr>

                </thead>
                <tbody>
                @foreach ($items as $item)
                    <tr class="shadow">
                        <td>{{$item->quality}}</td>
                        <td><button style="color:blue" data-target="#i{{$item->id}}" data-toggle="modal">{{$item->name}}</button></td>
                        <td>{{number_format($item->sales,2,'.',',')}}</td>
                        <td >{{number_format($item->cummPurchase,0,'.',',')}}/-</td>

                        <td>{{number_format($item->cummSale,0,'.',',')}}/-</td>
                        <td>{{number_format($item->profit,0,'.',',')}}/-</td>
                        
                        <div id="i{{$item->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                          
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                <h4 class="modal-title">Individual Details</h4>
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 
                                </div>
                                <div class="modal-body">
                                 <div class="row">
                                     <div class="col-md-1">#</div>
                                     <div class="col-md-4">
                                         <strong>Purchase Price</strong>
                                     </div>
                                     <div class="col-md-3">
                                        <strong> Sale Price </strong>
                                     </div>
                                     <div class="col-md-3">
                                           <strong> Profit</strong>
                                        </div>
                                 </div>
                                 @php($count=0)
                                @foreach ($item->stats as $it)
                                    @php($count++)
                                     <div class="row">
                                         <div class="col-md-1">{{$count}}</div>
                                         
                                            <div class="col-md-4">
                                                   {{number_format($it['purchase'],0,'.',',')}}
                                                </div>
                                                <div class="col-md-3">
                                                    {{number_format($it['sale'],0,'.',',')}}
                                                </div>
                                                <div class="col-md-3">
                                                        {{number_format($it['profit'],0,'.',',')}}
                                                    </div>
                                     </div>
                                     @endforeach
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                          
                            </div>
                          </div>
                    </tr>
                   
                @endforeach
               

                <tr class="shadow">
                    <td colspan="2"></td>

                    <td><strong>{{number_format($FSales,2,'.',',')}} /-</strong></td>
                    <td><strong>{{number_format($FPVal,0,'.',',')}} /-</strong></td>
                    <td><strong>{{number_format($FSVal,0,'.',',')}} /-</strong></td>
                    <td colspan="1"><strong>{{number_format($FProfit,0,'.',',')}} /-</strong></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>



    <style>
       
        
        .au-input--w300{
            width: 100% !important;
        }
        @media print
        {
            .noprint {display:none !important}
            .today{
                display: block !important;
            }

        }
    </style>
    <script>
        var on_date= "<input style='margin-left:15px;' class='on_date' type='date' class='form-control-sm col-md-2' name='on_date' required />";
        var from_date="<input style='margin-left:15px;' class='duration_dates' type='date' class='form-control-sm col-md-2' name='from_date' required /><input  class='duration_dates' type='date' style='margin-left:15px;' class='form-control-sm' name='to_date' required />"

        $(document).ready(function(){
            $('#selectID').on('change',function(){

                if($(this).val()==1 || $(this).val()=='all'){
                  
                    $('.duration_dates').remove();
                }

                else if($(this).val()==3){
                    $('#fRow').append(from_date);
                   
                }


            });
        });
    </script>
@endsection