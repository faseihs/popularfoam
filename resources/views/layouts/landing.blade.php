<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Title Page-->
    <title>Popular Foam | Admin </title>
    <link href="{{asset('images/favicon.ico')}}" rel=icon>
    <!-- Fontfaces CSS-->
    <link href="{{ asset('css/font-face.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/font-awesome-4.7/css/font-awesome.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/font-awesome-5/css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/mdi-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ asset('vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{ asset('vendor/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/wow/animate.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/slick/slick.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{asset('css/theme.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" media="all">

</head>

<body class="animsition">

<div class="page-wrapper">
    <!-- HEADER DESKTOP-->
    <header class="header-desktop3 d-none d-lg-block">
        <div class="section__content section__content--p35">
            <div class="header3-wrap">
                <div class="header__logo">
                    <a href="/">
                        <img height="50px" width="50px" src='{{asset("images/logoT.png")}}' alt="CoolAdmin" />
                        <span style="color: white">Popular Form</span>
                    </a>
                </div>
                <div class="header__navbar">
                    <ul class="list-unstyled">
                        <li class="has-sub">
                            <a href="#">
                                <i class="fas fa-bars"></i>General
                                <span class="bot-line"></span>
                            </a>
                            <ul class="header3-sub-list list-unstyled">
                                <li>
                                    <a href="/admin/user">Users</a>
                                </li>
                                <li>
                                    <a href="/admin/company">Company</a>
                                </li>
                                <li>
                                    <a href="/admin/quality">Quality</a>
                                </li>
                                <li>
                                    <a href="/admin/item">Item</a>
                                </li>
                                <li>
                                    <a href="/admin/category">Category</a>
                                </li>
                                <li>
                                    <a href="/admin/vehicle">Vehicle</a>
                                </li>
                                <li>
                                    <a href="/admin/account">Accounts</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="#">
                                <i class="fas fa-copy"></i>
                                <span class="bot-line"></span>Inventory</a>
                            <ul class="header3-sub-list list-unstyled">
                                <li>
                                    <a href="/admin/purchase_order">Purchase Orders</a>
                                </li>
                                <li>
                                    <a href="/admin/purchase_invoice">Purchase Invoices</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="#">
                                <i class="fas fa-list-alt"></i>
                                <span class="bot-line"></span>Sales</a>
                            <ul class="header3-sub-list list-unstyled">
                                <li>
                                    <a href="#">Sales Invoice</a>
                                </li>
                                <li>
                                    <a href="/admin/customer">Customer</a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="#">
                                <i class="fas fa-eur"></i>
                                <span class="bot-line"></span>Payments</a>
                            <ul class="header3-sub-list list-unstyled">
                                <li>
                                    <a href="/admin/discount">Discounts</a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="header__tool">
                    <div class="header-button-item has-noti js-item-menu">

                    </div>
                    <div class="account-wrap">
                        <div class="account-item account-item--style2 clearfix js-item-menu">
                            <div class="image">
                                <img src='https://www.placehold.co/50x50' alt="John Doe" />
                            </div>
                            <div class="content">
                                <a class="js-acc-btn" href="#">{{Auth::user()->name}}</a>
                            </div>
                            <div class="account-dropdown js-dropdown">
                                <div class="info clearfix">
                                    <div class="image">
                                        <a href="#">
                                            <img src='{{Auth::user()->photo? Auth::user()->photo :'https://www.placehold.co/50x50'}}' alt="John Doe" />
                                        </a>
                                    </div>
                                    <div class="content">
                                        <h5 class="name">
                                            <a href="#">{{Auth::user()->name}}</a>
                                        </h5>
                                        <span class="email">{{Auth::user()->email}}</span>
                                    </div>
                                </div>
                                <div class="account-dropdown__footer">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- END HEADER DESKTOP-->

    <!-- HEADER MOBILE-->
    <header class="header-mobile header-mobile-2 d-block d-lg-none">
        <div class="header-mobile__bar">
            <div class="container-fluid">
                <div class="header-mobile-inner">
                    <a class="logo" href="index.html">
                        <img height="40px" width="40px" src="{{asset('images/logoT.png')}}"  />
                    </a>
                    <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <nav class="navbar-mobile">
            <div class="container-fluid">
                <ul class="navbar-mobile__list list-unstyled">
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-tachometer-alt"></i>General</a>
                        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                            <li>
                                <a href="/admin/company">Company</a>
                            </li>
                            <li>
                                <a href="/admin/quality">Quality</a>
                            </li>
                            <li>
                                <a href="/admin/item">Item</a>
                            </li>
                            <li>
                                <a href="/admin/category">Category</a>
                            </li>
                            <li>
                                <a href="/admin/vehicle">Vehicle</a>
                            </li>
                            <li>
                                <a href="/admin/account">Accounts</a>
                            </li>

                        </ul>
                    </li>

                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-copy"></i>Sales</a>
                        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                            <li>
                                <a href="login.html">Sales Invoice</a>
                            </li>
                            <li>
                                <a href="/admin/customer">Customer</a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-desktop"></i>Inventory</a>
                        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                            <li>
                                <a href="/admin/purchase_order">Purchase Orders</a>
                            </li>
                            <li>
                                <a href="/admin/purchase_invoice">Purchase Invoices</a>
                            </li>

                        </ul>
                    </li>
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-desktop"></i>Payments</a>
                        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                            <li>
                                <a href="/admin/discount">Discounts</a>
                            </li>

                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>
    <div class="sub-header-mobile-2 d-block d-lg-none">
        <div class="header__tool">
            <div class="header-button-item has-noti js-item-menu">

            </div>
            <div class="header-button-item js-item-menu">

            </div>
            <div class="account-wrap">
                <div class="account-item account-item--style2 clearfix js-item-menu">
                    <div class="image">
                        <img src='{{Auth::user()->photo? Auth::user()->photo :'https://www.placehold.co/50x50'}}' alt="John Doe" />
                    </div>
                    <div class="content">
                        <a class="js-acc-btn" href="#">{{Auth::user()->name}}</a>
                    </div>
                    <div class="account-dropdown js-dropdown">
                        <div class="info clearfix">
                            <div class="image">
                                <a href="#">
                                    <img src='https://www.placehold.co/40x40' alt="John Doe" />
                                </a>
                            </div>
                            <div class="content">
                                <h5 class="name">
                                    <a href="#">{{Auth::user()->name}}</a>
                                </h5>
                                <span class="email">{{Auth::user()->email}}</span>
                            </div>
                        </div>
                        <div class="account-dropdown__body">

                        </div>
                        <div class="account-dropdown__footer">
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END HEADER MOBILE -->
    <!-- Jquery JS-->
    <script src='{{asset("vendor/jquery-3.2.1.min.js")}}'></script>
    <!-- Bootstrap JS-->

    <script src='{{asset("vendor/bootstrap-4.1/popper.min.js")}}'></script>
    <script src='{{asset("vendor/bootstrap-4.1/bootstrap.min.js")}}'></script>

    <!-- Vendor JS       -->
    <script src='{{asset("vendor/slick/slick.min.js")}}'>
    </script>
    <script src='{{asset("vendor/wow/wow.min.js")}}'></script>
    <script src='{{asset("vendor/animsition/animsition.min.js")}}'></script>
    <script src='{{asset("vendor/bootstrap-progressbar/bootstrap-progressbar.min.js")}}'>
    </script>
    <script src='{{asset("vendor/counter-up/jquery.waypoints.min.js")}}'></script>
    <script src='{{asset("vendor/counter-up/jquery.counterup.min.js")}}'>
    </script>
    <script src='{{asset("vendor/circle-progress/circle-progress.min.js")}}'></script>
    <script src='{{asset("vendor/perfect-scrollbar/perfect-scrollbar.js")}}'></script>
    <script src='{{asset("vendor/chartjs/Chart.bundle.min.js")}}'></script>
    <script src='{{asset("vendor/select2/select2.min.js")}}'>
    </script>

    <!-- Main JS-->
    <script src='{{asset("js/main.js")}}'></script>
    <script src='{{asset("js/mousetrap.min.js")}}'></script>
    <!-- PAGE CONTENT-->
    <div class="page-content--bgf7">
        <!-- BREADCRUMB-->
        <section class="au-breadcrumb2">
            <div class="container-fluid">
                @yield('content')
            </div>
        </section>
        <!-- END COPYRIGHT-->
    </div>

</div>



</body>

</html>
<!-- end document-->