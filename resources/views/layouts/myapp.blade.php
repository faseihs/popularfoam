<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="SAB Solutions | Faseih Saad">
    <meta name="keywords" content="au theme template">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Title Page-->
    <title>Popular Foam | Admin </title>
    <link href="{{asset('images/favicon.ico')}}" rel=icon>
    <!-- Fontfaces CSS-->
    <link href="{{ asset('css/font-face.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/font-awesome-4.7/css/font-awesome.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/font-awesome-5/css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/mdi-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{ asset('vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{ asset('vendor/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/wow/animate.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/slick/slick.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{asset('css/theme.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" media="all">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    @yield('styles')

</head>

<body class="animsition">

<div class="page-wrapper">
    <!-- HEADER DESKTOP-->
    <header class="header-desktop3 d-none d-lg-block noprint">
        <div class="section__content section__content--p35">
            <div class="header3-wrap">
                <div class="header__logo">
                    <a href="/">
                        <img height="50px" width="50px" src='{{asset("images/logoT.png")}}' alt="CoolAdmin" />
                        <span style="color: white">Popular Foam</span>
                    </a>
                </div>
                <div class="header__navbar">
                    <ul class="list-unstyled">
                        @if(Auth::user()->role_id==1)
                            <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-bars"></i>General
                                    <span class="bot-line"></span>
                                </a>
                                <ul class="header3-sub-list list-unstyled">
                                    <li>
                                        <a href="/admin/user">Users</a>
                                    </li>
                                    <li>
                                        <a href="/admin/company">Company</a>
                                    </li>
                                    <li>
                                        <a href="/admin/quality">Quality</a>
                                    </li>
                                    <li>
                                        <a href="/admin/item">Item</a>
                                    </li>
                                    <li>
                                        <a href="/admin/category">Category</a>
                                    </li>
                                    <li>
                                        <a href="/admin/vehicle">Vehicle</a>
                                    </li>
                                    <li>
                                        <a href="/admin/account">Accounts</a>
                                    </li>
                                    <li>
                                        <a href="/admin/worker">Workers</a>
                                    </li>

                                    <li>
                                        <a href="/admin/transaction">Daily Expense</a>
                                    </li>
                                    <li>
                                        <a href="/admin/daily_credits">Daily Credits</a>
                                    </li>

                                </ul>
                            </li>
                        @endif
                        @if(Auth::user()->role_id==1 || Auth::user()->role_id==2)

                        <li class="has-sub">
                            <a href="#">
                                <i class="fas fa-copy"></i>
                                <span class="bot-line"></span>Inventory</a>
                            <ul class="header3-sub-list list-unstyled">

                                @if(Auth::user()->role_id==1)
                                    <li>
                                        <a href="/admin/purchase_order">Purchase Orders</a>
                                    </li>
                                    <li>
                                        <a href="/admin/purchase_invoice">Purchase Invoices</a>
                                    </li>
                                    <li>
                                        <a href="/admin/purchase_return">Purchase Returns</a>
                                    </li>
                                    <li>
                                        <a href="/admin/product_ledger">Ledger : <strong>Product</strong></a>
                                    </li>
                                    @else
                                    <li>
                                        <a href="/admin/purchase_order">Purchase Orders</a>
                                    </li>
                                @endif

                            </ul>
                        </li>
                            @endif


                        <li class="has-sub">
                            <a href="#">
                                <i class="fas fa-usd"></i>
                                <span class="bot-line"></span>Sales</a>
                            <ul class="header3-sub-list list-unstyled">                             
                                <li>
                                    <a href="/admin/sales_invoice">Sales Invoice</a>
                                </li>
                                <li>
                                    <a href="/admin/sales_return">Sales Return</a>
                                </li>
                                <li>
                                    <a href="/admin/order_booking">Order Booking</a>
                                </li>
                                <li>
                                    <a href="/point-of-sales">Point Of Sales</a>
                                </li>
        
                            </ul>
                        </li>
                            @if(Auth::user()->role_id==1 || Auth::user()->role_id==2)
                        <li class="has-sub">
                            <a href="#">
                                <i class="fas fa-users"></i>
                                <span class="bot-line"></span>Customer</a>
                            <ul class="header3-sub-list list-unstyled">
                                @if(Auth::user()->role_id==1)
                                <li>
                                    <a href="/admin/customer">View</a>
                                </li>
                                <li>
                                    <a href="/admin/customer_discount">Discounts : <strong>Customer</strong></a>
                                </li>
                                <li>
                                    <a href="/admin/customer_payment">Payments : <strong>Customer</strong></a>
                                </li>

                                <li>
                                    <a href="/admin/customer_cash">Cash Register :  <strong>Customer</strong></a>
                                </li>
                                <li>
                                    <a href="/admin/customer_ledger">Ledger :  <strong>Customer</strong></a>
                                </li>
                                <li>
                                    <a href="/admin/customer_print">Print Numbers</strong></a>
                                </li>
                                    @else
                                    <li>
                                        <a href="/admin/customer_payment">Payments : <strong>Customer</strong></a>
                                    </li>
                                    @endif

                            </ul>
                        </li>
                            @endif
                            @if(Auth::user()->role_id==1)
                        <li class="has-sub">
                            <a href="#">
                                <i class="fas fa-archive"></i>
                                <span class="bot-line"></span>Company</a>
                            <ul class="header3-sub-list list-unstyled">
                                <li>
                                    <a href="/admin/discount">Discounts : <strong>Company</strong></a>
                                </li>
                                <li>
                                    <a href="/admin/company_payment">Payments : <strong>Company</strong></a>
                                </li>

                                <li>
                                    <a href="/admin/company_cash">Cash Register :  <strong>Company</strong></a>
                                </li>
                                <li>
                                    <a href="/admin/company_ledger">Ledger :  <strong>Company</strong></a>
                                </li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="#">
                                <i class="fas fa-bar-chart-o"></i>
                                <span class="bot-line"></span>Reports</a>
                            <ul class="header3-sub-list list-unstyled">
                                <li>
                                    <a href="/admin/reports/sales_reports/monthly">Summary</a>
                                </li>
                                <li>
                                    <a href="/admin/reports/monthly">Overall </a>
                                </li>

                                <li>
                                    <a href="/admin/reports/qualitywise">Stock Report : <strong>Quality Wise</strong></a>
                                </li>
                                <li>
                                    <a href="/admin/reports/companywise">Stock Report : <strong>Company Wise</strong></a>
                                </li>
                                <li>
                                <a href="/admin/reports/stockvaluation">Stock Valuation  : <strong>Quality Wise</strong></a>
                            </li>
                            <li>
                                    <a href="/admin/reports/companyvaluation">Stock Valuation  : <strong>Company Wise</strong></a>
                            </li>
                            <li>
                                    <a href="/admin/reports/expense">Expense</a>
                                </li>
                                <li>
                                        <a href="/admin/reports/cash_in">Cash In</a>
                                    </li>
                                <li>
                                    <a href="/admin/profit/item">Profit  : <strong>Item Wise</strong></a>
                                </li>
                                <li>
                                    <a href="/admin/profit/bill">Profit  : <strong>Bill Wise</strong></a>
                                </li>
                                <li>
                                    <a href="/admin/Ledger">Payable and Receievable</a>
                                </li>
                                <li>
                                    <a href="/admin/purchase-report-item-summary">Purchase Report</a>
                                </li>
                            </ul>
                        </li>

                        <li class="has-sub">
                            <a href="#">
                                <i class="fas fa-user"></i>
                                <span class="bot-line"></span>Workers</a>
                            <ul class="header3-sub-list list-unstyled">
                                <li>
                                    <a href="/admin/worker">View</a>
                                </li>
                                <li>
                                    <a href="/admin/attendance">Attendance </a>
                                </li>

                                <li>
                                    <a href="/admin/salary_payment">Salary Payments</a>
                                </li>

                                <li>
                                    <a href="/admin/worker_cash">Cash Register</a>
                                </li>


                            </ul>
                        </li>

                        @endif



                    </ul>
                </div>
                <div class="header__tool">
                    <div class="header-button-item has-noti js-item-menu">

                    </div>
                    <div class="account-wrap noprint">
                        <div class="account-item account-item--style2 clearfix js-item-menu">
                            <div class="image">
                                <img src='{{Auth::user()->photo?Auth::user()->photo->getImage():'https://placehold.co/50x50'}}' alt="John Doe" />
                            </div>
                            <div class="content">
                                <a class="js-acc-btn" href="#">{{Auth::user()->name}}</a>
                            </div>
                            <div class="account-dropdown js-dropdown">
                                <div class="info clearfix">
                                    <div class="image">
                                        <a href="#">
                                            <img src='{{Auth::user()->photo?Auth::user()->photo->getImage():'https://placehold.co/50x50'}}' alt="John Doe" />
                                        </a>
                                    </div>
                                    <div class="content">
                                        <h5 class="name">
                                        <a href="#">{{Auth::user()->name}}</a>
                                        </h5>
                                        <span class="email">{{Auth::user()->email}}</span>
                                    </div>
                                </div>
                                <div class="account-dropdown__footer">
                                    <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- END HEADER DESKTOP-->

    <!-- HEADER MOBILE-->
    <header class="header-mobile header-mobile-2 d-block d-lg-none noprint">
        <div class="header-mobile__bar">
            <div class="container-fluid">
                <div class="header-mobile-inner">
                    <a class="logo" href="index.html">
                        <img height="40px" width="40px" src="{{asset('images/logoT.png')}}"  />
                    </a>
                    <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                    </button>
                </div>
            </div>
        </div>
        <nav class="navbar-mobile">
            <div class="container-fluid">
                <ul class="navbar-mobile__list list-unstyled">
                    @if(Auth::user()->role_id==1)
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-tachometer-alt"></i>General</a>
                        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                            <li>
                                <a href="/admin/user">Users</a>
                            </li>
                            <li>
                                <a href="/admin/company">Company</a>
                            </li>
                            <li>
                                <a href="/admin/quality">Quality</a>
                            </li>
                            <li>
                                <a href="/admin/item">Item</a>
                            </li>
                            <li>
                                <a href="/admin/category">Category</a>
                            </li>
                            <li>
                                <a href="/admin/vehicle">Vehicle</a>
                            </li>
                            <li>
                                <a href="/admin/account">Accounts</a>
                            </li>
                            <li>
                                <a href="/admin/worker">Workers</a>
                            </li>
                            <li>
                                <a href="/admin/transaction">Daily Expense</a>
                            </li>
                            <li>
                                <a href="/admin/daily_credits">Daily Credits</a>
                            </li>

                        </ul>
                    </li>
                    @endif

                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-copy"></i>Sales</a>
                        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                          
                                <li>
                                    <a href="/admin/sales_invoice">Sales Invoice</a>
                                </li>
                                <li>
                                    <a href="/admin/sales_return">Sales Return</a>
                                </li>
                                <li>
                                    <a href="/admin/order_booking">Order Booking</a>
                                </li>
                                <li>
                                    <a href="/point-of-sales">Point Of Sales</a>
                                </li>
                           

                        </ul>
                    </li>
                        @if(Auth::user()->role_id==1 || Auth::user()->role_id==2)
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-desktop"></i>Inventory</a>
                        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                            @if(Auth::user()->role_id==1)
                                <li>
                                    <a href="/admin/purchase_order">Purchase Orders</a>
                                </li>
                                <li>
                                    <a href="/admin/purchase_invoice">Purchase Invoices</a>
                                </li>
                                <li>
                                    <a href="/admin/purchase_return">Purchase Returns</a>
                                </li>
                                <li>
                                    <a href="/admin/product_ledger">Ledger : <strong>Product</strong></a>
                                </li>
                            @else
                                <li>
                                    <a href="/admin/purchase_order">Purchase Orders</a>
                                </li>
                            @endif

                        </ul>
                    </li>
                        @endif
                        @if(Auth::user()->role_id==1 || Auth::user()->role_id==2)
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-desktop"></i>Customer</a>
                        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                            @if(Auth::user()->role_id==1)
                                <li>
                                    <a href="/admin/customer">View</a>
                                </li>
                                <li>
                                    <a href="/admin/customer_discount">Discounts : <strong>Customer</strong></a>
                                </li>
                                <li>
                                    <a href="/admin/customer_payment">Payments : <strong>Customer</strong></a>
                                </li>

                                <li>
                                    <a href="/admin/customer_cash">Cash Register :  <strong>Customer</strong></a>
                                </li>
                                <li>
                                    <a href="/admin/customer_ledger">Ledger :  <strong>Customer</strong></a>
                                </li>
                                <li>
                                    <a href="/admin/customer_print">Print Numbers</strong></a>
                                </li>
                            @else
                                <li>
                                    <a href="/admin/customer_payment">Payments : <strong>Customer</strong></a>
                                </li>
                            @endif
                        </ul>
                    </li>
                        @endif
                        @if(Auth::user()->role_id==1)
                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-desktop"></i>Company</a>
                        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                            <li>
                                <a href="/admin/discount">Discounts : <strong>Company</strong></a>
                            </li>
                            <li>
                                <a href="/admin/company_payment">Payments : <strong>Company</strong></a>
                            </li>

                            <li>
                                <a href="/admin/company_cash">Cash Register :  <strong>Company</strong></a>
                            </li>
                            <li>
                                <a href="/admin/company_ledger">Ledger :  <strong>Company</strong></a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-desktop"></i>Reports</a>
                        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                            <li>
                                <a href="/admin/reports/sales_reports/monthly">Summary </a>
                            </li>
                            <li>
                                <a href="/admin/reports/monthly">Overall </a>
                            </li>
                            <li>
                                <a href="/admin/reports/qualitywise">Stock Report : <strong>Quality Wise</strong></a>
                            </li>
                            <li>
                                <a href="/admin/reports/companywise">Stock Report : <strong>Company Wise</strong></a>
                            </li>
                            <li>
                                    <a href="/admin/reports/stockvaluation">Stock Valuation : <strong>Quality Wise</strong></a>
                                </li>
                                <li>
                                        <a href="/admin/reports/companyvaluation">Stock Valuation  : <strong>Company Wise</strong></a>
                                </li>
                            <li>
                                <a href="/admin/reports/expense">Expense</a>
                            </li>
                            <li>
                                    <a href="/admin/reports/cash_in">Cash In</a>
                            </li>
                            <li>
                                <a href="/admin/profit/item">Profit  : <strong>Item Wise</strong></a>
                            </li>
                            <li>
                                <a href="/admin/profit/bill">Profit  : <strong>Bill Wise</strong></a>
                            </li>
                            <li>
                                    <a href="/admin/Ledger">Payable and Receievable</a>
                                </li>
                            <li>
                                <a href="/admin/purchase-report-item-summary">Purchase Report</a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-sub">
                        <a class="js-arrow" href="#">
                            <i class="fas fa-desktop"></i>Workers</a>
                        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                            <li>
                                <a href="/admin/worker">View</a>
                            </li>
                            <li>
                                <a href="/admin/attendance">Attendance </a>
                            </li>

                            <li>
                                <a href="/admin/salary_payment">Salary Payments</a>
                            </li>

                            <li>
                                <a href="/admin/worker_cash">Cash Register</a>
                            </li>
                            <li>
                                <a href="/admin/Ledger">Ledger</a>
                            </li>
                        </ul>
                    </li>
                            @endif

                </ul>
            </div>
        </nav>
    </header>
    <div class="sub-header-mobile-2 d-block d-lg-none noprint">
        <div class="header__tool">
            <div class="header-button-item has-noti js-item-menu">

            </div>
            <div class="header-button-item js-item-menu">

            </div>
            <div class="account-wrap">
                <div class="account-item account-item--style2 clearfix js-item-menu">
                    <div class="image">
                        <img src="{{Auth::user()->photo?Auth::user()->photo->getImage():'https://placehold.co/50x50'}}" alt="John Doe" />
                    </div>
                    <div class="content">
                        <a class="js-acc-btn" href="#">{{Auth::user()->name}}</a>
                    </div>
                    <div class="account-dropdown js-dropdown">
                        <div class="info clearfix">
                            <div class="image">
                                <a href="#">
                                    <img src="{{Auth::user()->photo?Auth::user()->photo->getImage():'https://placehold.co/50x50'}}" alt="John Doe" />
                                </a>
                            </div>
                            <div class="content">
                                <h5 class="name">
                                    <a href="#">{{Auth::user()->name}}</a>
                                </h5>
                                <span class="email">{{Auth::user()->email}}</span>
                            </div>
                        </div>
                        <div class="account-dropdown__body">

                        </div>
                        <div class="account-dropdown__footer">
                            <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END HEADER MOBILE -->
    <!-- Jquery JS-->
    <script src='{{asset("vendor/jquery-3.2.1.min.js")}}'></script>
    <!-- Bootstrap JS-->

    <script src='{{asset("vendor/bootstrap-4.1/popper.min.js")}}'></script>
    <script src='{{asset("vendor/bootstrap-4.1/bootstrap.min.js")}}'></script>

    <!-- Vendor JS       -->
    <script src='{{asset("vendor/slick/slick.min.js")}}'>
    </script>
    <script src='{{asset("vendor/wow/wow.min.js")}}'></script>
    <script src='{{asset("vendor/animsition/animsition.min.js")}}'></script>
    <script src='{{asset("vendor/bootstrap-progressbar/bootstrap-progressbar.min.js")}}'>
    </script>
    <script src='{{asset("vendor/counter-up/jquery.waypoints.min.js")}}'></script>
    <script src='{{asset("vendor/counter-up/jquery.counterup.min.js")}}'>
    </script>
    <script src='{{asset("vendor/circle-progress/circle-progress.min.js")}}'></script>
    <script src='{{asset("vendor/perfect-scrollbar/perfect-scrollbar.js")}}'></script>
    <script src='{{asset("vendor/chartjs/Chart.bundle.min.js")}}'></script>
    <script src='{{asset("vendor/select2/select2.min.js")}}'>
    </script>

    <!-- Main JS-->
    <script src='{{asset("js/main.js")}}'></script>
    <script src='{{asset("js/mousetrap.min.js")}}'></script>
    <!-- PAGE CONTENT-->
    <div style="min-height:100vh;" class="page-content--bgf7">
        <!-- BREADCRUMB-->
        <section class="au-breadcrumb2">
            <div class="container-fluid">
            @yield('content')
            </div>
        </section>
        <!-- END COPYRIGHT-->
    </div>

</div>
{{--<footer  style="max-height:10px;bottom:0" class="footer text-center">
        <p>Copyright &copy; SAB-Solutions <img height="30px" width="30px" src="{{asset('images/sabT.png')}}" /></p>
</footer>--}}

<script>
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();

});


</script>

@yield('scripts')
</body>

</html>
<!-- end document-->