<?php return array (
  'codeToName' => 
  array (
    13 => 'CR',
    32 => 'space',
    33 => 'exclam',
    34 => 'quotedbl',
    35 => 'numbersign',
    36 => 'dollar',
    37 => 'percent',
    38 => 'ampersand',
    39 => 'quotesingle',
    40 => 'parenleft',
    41 => 'parenright',
    42 => 'asterisk',
    43 => 'plus',
    44 => 'comma',
    45 => 'hyphen',
    46 => 'period',
    47 => 'slash',
    48 => 'zero',
    49 => 'one',
    50 => 'two',
    51 => 'three',
    52 => 'four',
    53 => 'five',
    54 => 'six',
    55 => 'seven',
    56 => 'eight',
    57 => 'nine',
    58 => 'colon',
    59 => 'semicolon',
    60 => 'less',
    61 => 'equal',
    62 => 'greater',
    63 => 'question',
    64 => 'at',
    65 => 'A',
    66 => 'B',
    67 => 'C',
    68 => 'D',
    69 => 'E',
    70 => 'F',
    71 => 'G',
    72 => 'H',
    73 => 'I',
    74 => 'J',
    75 => 'K',
    76 => 'L',
    77 => 'M',
    78 => 'N',
    79 => 'O',
    80 => 'P',
    81 => 'Q',
    82 => 'R',
    83 => 'S',
    84 => 'T',
    85 => 'U',
    86 => 'V',
    87 => 'W',
    88 => 'X',
    89 => 'Y',
    90 => 'Z',
    91 => 'bracketleft',
    92 => 'backslash',
    93 => 'bracketright',
    94 => 'asciicircum',
    95 => 'underscore',
    96 => 'grave',
    97 => 'a',
    98 => 'b',
    99 => 'c',
    100 => 'd',
    101 => 'e',
    102 => 'f',
    103 => 'g',
    104 => 'h',
    105 => 'i',
    106 => 'j',
    107 => 'k',
    108 => 'l',
    109 => 'm',
    110 => 'n',
    111 => 'o',
    112 => 'p',
    113 => 'q',
    114 => 'r',
    115 => 's',
    116 => 't',
    117 => 'u',
    118 => 'v',
    119 => 'w',
    120 => 'x',
    121 => 'y',
    122 => 'z',
    123 => 'braceleft',
    124 => 'bar',
    125 => 'braceright',
    126 => 'asciitilde',
    161 => 'exclamdown',
    162 => 'cent',
    163 => 'sterling',
    164 => 'currency',
    165 => 'yen',
    166 => 'brokenbar',
    167 => 'section',
    168 => 'dieresis',
    169 => 'copyright',
    170 => 'ordfeminine',
    171 => 'guillemotleft',
    172 => 'logicalnot',
    174 => 'registered',
    175 => 'macron',
    176 => 'degree',
    177 => 'plusminus',
    178 => 'twosuperior',
    179 => 'threesuperior',
    180 => 'acute',
    182 => 'paragraph',
    183 => 'periodcentered',
    184 => 'cedilla',
    185 => 'onesuperior',
    186 => 'ordmasculine',
    187 => 'guillemotright',
    188 => 'onequarter',
    189 => 'onehalf',
    190 => 'threequarters',
    191 => 'questiondown',
    192 => 'Agrave',
    193 => 'Aacute',
    194 => 'Acircumflex',
    195 => 'Atilde',
    196 => 'Adieresis',
    197 => 'Aring',
    198 => 'AE',
    199 => 'Ccedilla',
    200 => 'Egrave',
    201 => 'Eacute',
    202 => 'Ecircumflex',
    203 => 'Edieresis',
    204 => 'Igrave',
    205 => 'Iacute',
    206 => 'Icircumflex',
    207 => 'Idieresis',
    208 => 'Eth',
    209 => 'Ntilde',
    210 => 'Ograve',
    211 => 'Oacute',
    212 => 'Ocircumflex',
    213 => 'Otilde',
    214 => 'Odieresis',
    215 => 'multiply',
    216 => 'Oslash',
    217 => 'Ugrave',
    218 => 'Uacute',
    219 => 'Ucircumflex',
    220 => 'Udieresis',
    221 => 'Yacute',
    222 => 'Thorn',
    223 => 'germandbls',
    224 => 'agrave',
    225 => 'aacute',
    226 => 'acircumflex',
    227 => 'atilde',
    228 => 'adieresis',
    229 => 'aring',
    230 => 'ae',
    231 => 'ccedilla',
    232 => 'egrave',
    233 => 'eacute',
    234 => 'ecircumflex',
    235 => 'edieresis',
    236 => 'igrave',
    237 => 'iacute',
    238 => 'icircumflex',
    239 => 'idieresis',
    240 => 'eth',
    241 => 'ntilde',
    242 => 'ograve',
    243 => 'oacute',
    244 => 'ocircumflex',
    245 => 'otilde',
    246 => 'odieresis',
    247 => 'divide',
    248 => 'oslash',
    249 => 'ugrave',
    250 => 'uacute',
    251 => 'ucircumflex',
    252 => 'udieresis',
    253 => 'yacute',
    254 => 'thorn',
    255 => 'ydieresis',
    305 => 'dotlessi',
    338 => 'OE',
    339 => 'oe',
    710 => 'circumflex',
    730 => 'ring',
    732 => 'tilde',
    8211 => 'endash',
    8212 => 'emdash',
    8216 => 'quoteleft',
    8217 => 'quoteright',
    8218 => 'quotesinglbase',
    8220 => 'quotedblleft',
    8221 => 'quotedblright',
    8222 => 'quotedblbase',
    8226 => 'bullet',
    8230 => 'ellipsis',
    8249 => 'guilsinglleft',
    8250 => 'guilsinglright',
    8260 => 'fraction',
    8364 => 'Euro',
    8722 => 'minus',
  ),
  'isUnicode' => true,
  'EncodingScheme' => 'FontSpecific',
  'FontName' => 'Poppins Thin',
  'FullName' => 'Poppins Thin',
  'Version' => 'Version 3.010;PS 1.000;hotconv 16.6.54;makeotf.lib2.5.65590',
  'PostScriptName' => 'Poppins-Thin',
  'Weight' => 'Medium',
  'ItalicAngle' => '0',
  'IsFixedPitch' => 'false',
  'UnderlineThickness' => '50',
  'UnderlinePosition' => '-75',
  'FontHeightOffset' => '100',
  'Ascender' => '1050',
  'Descender' => '-350',
  'FontBBox' => 
  array (
    0 => '-514',
    1 => '-543',
    2 => '2649',
    3 => '1026',
  ),
  'StartCharMetrics' => '216',
  'C' => 
  array (
    0 => 0,
    13 => 290,
    32 => 290,
    33 => 224,
    34 => 196,
    35 => 743,
    36 => 536,
    37 => 529,
    38 => 672,
    39 => 119,
    40 => 357,
    41 => 357,
    42 => 429,
    43 => 589,
    44 => 120,
    45 => 444,
    46 => 113,
    47 => 359,
    48 => 588,
    49 => 226,
    50 => 567,
    51 => 575,
    52 => 574,
    53 => 592,
    54 => 614,
    55 => 502,
    56 => 608,
    57 => 614,
    58 => 113,
    59 => 140,
    60 => 381,
    61 => 564,
    62 => 381,
    63 => 482,
    64 => 1024,
    65 => 601,
    66 => 560,
    67 => 769,
    68 => 702,
    69 => 479,
    70 => 469,
    71 => 792,
    72 => 649,
    73 => 190,
    74 => 426,
    75 => 494,
    76 => 395,
    77 => 797,
    78 => 647,
    79 => 792,
    80 => 528,
    81 => 792,
    82 => 535,
    83 => 536,
    84 => 470,
    85 => 629,
    86 => 619,
    87 => 904,
    88 => 500,
    89 => 518,
    90 => 487,
    91 => 219,
    92 => 474,
    93 => 219,
    94 => 552,
    95 => 533,
    96 => 267,
    97 => 671,
    98 => 671,
    99 => 628,
    100 => 671,
    101 => 628,
    102 => 318,
    103 => 671,
    104 => 614,
    105 => 190,
    106 => 200,
    107 => 395,
    108 => 190,
    109 => 1003,
    110 => 614,
    111 => 647,
    112 => 671,
    113 => 671,
    114 => 338,
    115 => 483,
    116 => 338,
    117 => 599,
    118 => 515,
    119 => 803,
    120 => 416,
    121 => 503,
    122 => 407,
    123 => 293,
    124 => 190,
    125 => 303,
    126 => 412,
    160 => 290,
    161 => 224,
    162 => 588,
    163 => 540,
    164 => 501,
    165 => 563,
    166 => 190,
    167 => 566,
    168 => 319,
    169 => 803,
    170 => 433,
    171 => 374,
    172 => 708,
    173 => 444,
    174 => 787,
    175 => 389,
    176 => 356,
    177 => 589,
    178 => 301,
    179 => 302,
    180 => 267,
    181 => 619,
    182 => 489,
    183 => 113,
    184 => 249,
    185 => 154,
    186 => 412,
    187 => 374,
    188 => 512,
    189 => 593,
    190 => 532,
    191 => 462,
    192 => 601,
    193 => 601,
    194 => 601,
    195 => 601,
    196 => 601,
    197 => 601,
    198 => 837,
    199 => 769,
    200 => 479,
    201 => 479,
    202 => 479,
    203 => 479,
    204 => 190,
    205 => 190,
    206 => 190,
    207 => 190,
    208 => 742,
    209 => 647,
    210 => 792,
    211 => 792,
    212 => 792,
    213 => 792,
    214 => 792,
    215 => 469,
    216 => 792,
    217 => 629,
    218 => 629,
    219 => 629,
    220 => 629,
    221 => 518,
    222 => 528,
    223 => 588,
    224 => 671,
    225 => 671,
    226 => 671,
    227 => 671,
    228 => 671,
    229 => 671,
    230 => 1135,
    231 => 628,
    232 => 628,
    233 => 628,
    234 => 628,
    235 => 628,
    236 => 190,
    237 => 190,
    238 => 190,
    239 => 190,
    240 => 645,
    241 => 614,
    242 => 647,
    243 => 647,
    244 => 647,
    245 => 647,
    246 => 647,
    247 => 589,
    248 => 647,
    249 => 599,
    250 => 599,
    251 => 599,
    252 => 599,
    253 => 503,
    254 => 671,
    255 => 503,
    305 => 190,
    338 => 1106,
    339 => 1135,
    710 => 327,
    730 => 260,
    732 => 310,
    8211 => 581,
    8212 => 764,
    8216 => 122,
    8217 => 122,
    8218 => 138,
    8220 => 208,
    8221 => 208,
    8222 => 208,
    8226 => 256,
    8230 => 403,
    8249 => 229,
    8250 => 229,
    8260 => 207,
    8364 => 837,
    8722 => 444,
    8725 => 359,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJzt0FWPGEQYhtEHd4q7tBR3dyhWiru7u7tbKe7u7u7u7i5/h4bsBRcUNumSbsI5ycw3mZl8eWeaqkmZepInfzVN0zZd0zdDMzZTMzdLszZbszeiOZqzuZq7eZq3+Zq/BVqwhVq4RVq0xVq8kY1qiUa3ZEu1dMu0bMu1fCu0Yiu1cqu0aqu1emu0Zmu1duu0buu1fhu0YRu1cWPapE3brM3borFt2bi2auu2adu2a/t2aMd2aud2add2a/f2aM/2au/2ad/2a/8O6MAO6uBBvfCfHdKhHdbhHdGRHdXRHdOxHdfxndCJndTJndKpndbpndGZndXZndO5ndf5XdCFXdTFXdL4Lm1Cl3V5V3RlV3V113Rt13V9N3RjN3Vzt3Rrt3V7d3Rnd3V393Rv93V/D/RgD/Vwj/Roj/V4T/RkT/V0z/Rsz/V8L/RiL/Vyr/Rqr/V6b/Rmb/V27/Ru7/V+H/ThEPzAv/losjt83CdDkAOGt08Hee+ziePz/zIIAAAAAAAAAAAAAAAAAADD3Bd9ObD6qq/7ZmL9tu/6fmDvh4H646B6/dTPf7P7y2TlG35+ndIBYIj89uf8+xROAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD8T/wByjlatQ==',
  '_version_' => 6,
);