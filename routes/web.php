<?php

/*
|--------------------------------------------------------------------------
| Web Routes
by Faseih Saad
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//EMail Test 
Route::get('/clear-cache', function() {
    
    $exitCode = Artisan::call('cache:clear');
    
    return 'DONE'; //Return anything
});

Route::get('/clear-config', function() {
    $exitCode = Artisan::call('config:clear');
    return 'DONE'; //Return anything
});

Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

Route::get('/email',function(){
    $data=[
        'title'=>'Yo',
        'content'=>'Yoo'
    ];

    Mail::send('email',$data,function($message){
        $message->to('faseihsaad@outlook.com')->subject('Test');
    });
});


Route::get('/', function () {
    return redirect('login');
});

Auth::routes();
 


// Route::get('/admin','Admin@index');


Route::group(['middleware'=>'auth'],function() {

    

    Route::get('/home', 'Admin@index');

    // Owner's  Routes only 

    //General Routes
    Route::resource('/admin/user','AdminUserController');

    //Company Routes
    Route::resource('/admin/company','AdminCompanyController');
    Route::post("/company-data","AdminCompanyController@apiData")->name("company-data");

    Route::resource('/admin/category','AdminCategoryController');
    Route::resource('/admin/quality','AdminQualityController');

    //Item Routes
    Route::resource('/admin/item','AdminItemController');
    Route::post("/item-data","AdminItemController@apiData")->name("item-data");


    Route::get('/admin/item/query/{query}','AdminItemController@Query');
    Route::resource('/admin/vehicle','AdminVehicleController');

  
   
    //Discount Routes
    Route::resource('/admin/discount','AdminDiscountController');
    Route::post('/admin/discount/create','AdminDiscountController@createC');

    //Purchase Invoice Routes
    Route::resource('/admin/purchase_invoice','AdminPurchaseInvoiceController');
    Route::post('/admin/purchase_invoice/create','AdminPurchaseInvoiceController@createC');
    Route::post('/admin/purchase_invoice/data','AdminPurchaseInvoiceController@apiData')->name("purchase-invoice.data");

    //Customer Routes
    Route::resource('/admin/customer','AdminCustomerController');

    //Account Routes
    Route::resource('/admin/account','AdminAccountController');

    //Payments Routes
    Route::resource('/admin/customer_payment','AdminCustomerPaymentController');
    Route::resource('/admin/company_payment','AdminCompanyPaymentController');


    //Cash Register Routes
    Route::resource('/admin/customer_cash','AdminCustomerCashController');
    Route::resource('/admin/company_cash','AdminCompanyCashController');

    //Customer Discount Routes
    Route::resource('/admin/customer_discount','AdminCDiscountController');
    Route::post('/admin/customer_discount/create','AdminCDiscountController@createC');
    
 
    //Sales Invoice Routes
   
    Route::post('/admin/api-sales-Ainvoice/create','AdminSalesInvoiceController@apiCreate');
    
    
    //Monthly Sales Reports
    Route::resource('/admin/reports/sales_reports/monthly','AdminMonthlySales');

    //Overall Reports
    Route::resource('/admin/reports/monthly','AdminMonthlyOverall');

    //Worker ROutes
    Route::resource('/admin/worker','AdminWorkerController');


    //Attendance Routes
    Route::resource('/admin/attendance','AdminAttendanceController');
    Route::post('/admin/worker/query','AdminWorkerController@query');


    //Salary Payments Controller
    Route::resource('/admin/salary_payment','AdminSalaryPaymentController');
    Route::post('/admin/salary_payment/query','AdminSalaryPaymentController@query');

    //Worker Cash Register
    Route::resource('/admin/worker_cash','AdminWorkerCashController');


    //Non Cash Transactions
    Route::resource('/admin/transaction','AdminNCController');


    //Customer Ledger
    Route::resource('/admin/customer_ledger','AdminCustomerLedgerController');


    //Company Ledger
    Route::resource('/admin/company_ledger','AdminCompanyLedgerController');
    Route::get('/admin/getCol','AdminCompanyLedgerController@getAll');
    Route::get('/admin/getComp','AdminCompanyLedgerController@getComp');


    //Price List
    Route::post('/admin/quality/editpost','AdminQualityController@editpost');

    //Purchase Order Print
   



    //Stock Report Quality Wise

    Route::get('/admin/reports/qualitywise','AdminStockReportController@qualityIndex');
    Route::post('/admin/reports/qualitywise','AdminStockReportController@qualityQuery');

    

    //Stock Report Company Wise

    Route::get('/admin/reports/companywise','AdminStockReportController@companyStockIndex');
    Route::post('/admin/reports/companywise','AdminStockReportController@companyStockQuery');


    //Stock Valuation Quality Wise
    Route::get('/admin/reports/stockvaluation','AdminStockReportController@valuationIndex');
    Route::post('/admin/reports/stockvaluation','AdminStockReportController@valuationQuery');



    //Stock Valuation Company Wise

    Route::get('/admin/reports/companyvaluation','AdminStockReportController@companyIndex');
    Route::post('/admin/reports/companyvaluation','AdminStockReportController@companyQuery');

  
    //Cash Flow Routes

    Route::get('admin/reports/expense','AdminCashFlowController@expenseIndex');
    Route::post('admin/reports/expense','AdminCashFlowController@expenseQuery');
    Route::get('admin/reports/cash_in','AdminCashFlowController@cashIndex');
    Route::post('admin/reports/cash_in','AdminCashFlowController@cashQuery');


    //Customer Ledger
    Route::get('/admin/customer_ledger','AdminCustomerLedgerController@index');
    Route::get('/admin/getCul','AdminCustomerLedgerController@getAll');
    Route::get('/admin/getCust','AdminCustomerLedgerController@getCust');

    //Profit Reports Controller

    Route::get('/admin/profit/item','AdminReportController@itemIndexMain');
    Route::get('/admin/profit/item/all','AdminReportController@itemIndex');
    Route::post('/admin/profit/item','AdminReportController@itemQuery');

    Route::get('/admin/profit/bill','AdminReportController@billIndexMain');
    Route::get('/admin/profit/bill/all','AdminReportController@billIndex');
    Route::post('/admin/profit/bill','AdminReportController@billQuery');


    //Daily Credits
    Route::resource('/admin/daily_credits','AdminDailyCreditsController');
        //Print
    Route::get('/admin/print_daily_credits/{id}','AdminDailyCreditsController@printCredits');
    //Customers Number Print
    Route::get('/admin/customer_print','AdminCustomerController@printNumbers');



    //Email Test 
    



    //Products Ledger

    Route::get('/admin/product_ledger','AdminProductLedgerController@index');
    Route::get('/admin/getQuality','AdminProductLedgerController@getQuality');
    Route::get('/admin/getItem/{id}','AdminProductLedgerController@getItem');
    Route::get('/admin/getPayment','AdminProductLedgerController@getPayment');
    Route::get('/admin/getPaymentsByItem/{id}','AdminProductLedgerController@getPaymentByItem');



    //Worker Payments Route
    Route::post('/admin/worker/queryPayment','AdminWorkerController@queryPayment');


    //Purchase Return Routes
    Route::resource('/admin/purchase_return','AdminPurchaseReturnController');
    Route::post('/admin/purchase_return/create','AdminPurchaseReturnController@createC');


    //General Ledger Controller 
    Route::get('/admin/ledger','AdminLedgerController@index');
    Route::get('/admin/Ledger','AdminLedgerController@ledger');




    //Account Show Post Route
    Route::post('/admin/account/{id}','AdminAccountController@query');


    // New Point Of Sales


    //Sales Report Print

    Route::post('/admin/print-sales-report','AdminMonthlySales@printReport');


    //PURCHASE REPORT
    Route::get('/admin/purchase-report-item-summary','Admin\ReportController@purchaseReportSummary');
    Route::post('/admin/purchase-report-item-summary','Admin\ReportController@purchaseReportSummaryEvaluate');

    //Testing Routes

    Route::get('/test','TestController@index');

    // End Owner's  Routes 


    // Admins's  Routes only 

     Route::resource('/admin/purchase_order','AdminPurchaseOrderController');
     Route::post('/admin/purchase_order/create','AdminPurchaseOrderController@createC');

      //Purchase Order Print
    Route::get('/admin/print/purchaseOrder/{id}','AdminPurchaseOrderController@printOrder');
    Route::post('/admin/purchase_order/finalPrint','AdminPurchaseOrderController@finalPrint');

    // End Admin's Routes only 


    // sales's  Routes only 

    //POS
    Route::get('/point-of-sales','PointOfSalesController@index');
    Route::get('item-api/{id}','PointOfSalesController@items');
    Route::get('perm-customers','PointOfSalesController@customers');
    Route::post('invoice-create-api','PointOfSalesController@Invoice');
    Route::post('invoice-booking-api','PointOfSalesController@Booking');
    Route::post('invoice-return-api','PointOfSalesController@salesReturn');

    //Printing
    Route::get('/admin/print/salesInvoice/{id}','AdminSalesInvoiceController@printInvoice');
    Route::get('/admin/print_option/salesInvoice/{id}','AdminSalesInvoiceController@printInvoiceOption');
    Route::get('/admin/order_booking/print/{id}','AdminOrderBookingController@printOrder');
    Route::get('/admin/sales_return/print/{id}','AdminSalesReturnController@printReturn');



    Route::get('api-sales-invoice-index','ApiController@salesInvoiceIndex');


    Route::group(['prefix'=>'api'],function (){
        Route::get('sales-invoice/edit/{id}','Api\SalesInvoiceApi@SalesInvoiceEdit');
        Route::put('sales-invoice/update/{id}','Api\SalesInvoiceApi@SalesInvoiceUpdate');
        Route::get('sales-invoice-today','Api\SalesInvoiceApi@salesToday');
        Route::get('sales-invoice-old','Api\SalesInvoiceApi@salesOld');
        Route::get('sales-invoice-order-booking','Api\SalesInvoiceApi@orderBooking');
        Route::get('sales-invoice-old-pagination','Api\SalesInvoiceApi@oldPagination');
        Route::get('sales-invoice-payment-pending','Api\SalesInvoiceApi@paymentPending');
        Route::get('get-items-edit/{id}','ApiController@getAllItemsEdit');
        Route::get('get-items-company/{id}','Api\PurchseController@getItems');
        Route::get('/get-items-by-quality/{id}','Api\PurchseController@getItemsByQuality');
        Route::get('/get-qualities-by-company/{id}','Api\PurchseController@getQualitiesByCompany');
        Route::get('/get-companies','Api\PurchseController@getCompanies');

        Route::get("/get-item-stock/{id}","Api\PurchseController@itemStock");
    });


    //Sales Invoice Routes
    Route::resource('/admin/sales_invoice','AdminSalesInvoiceController');
    Route::post('/admin/sales_invoice/create','AdminSalesInvoiceController@createC');


    //Sales Return Controller
    Route::resource('/admin/sales_return','AdminSalesReturnController');
    Route::post('/admin/sales_return/create','AdminSalesReturnController@createC');
    
    Route::get('/api/admin/sales_return/edit/{id}','AdminSalesReturnController@apiEdit');
    Route::put('/api/admin/sales_return/update/{id}','AdminSalesReturnController@apiUpdate');



    //Order Booking 
    Route::resource('/admin/order_booking','AdminOrderBookingController');
    Route::post('/admin/order_booking/create','AdminOrderBookingController@createC');
    
    // apis for order booking edit and update
    Route::get('/api/admin/order_booking/edit/{id}','AdminOrderBookingController@apiEdit');
    Route::put('/api/invoice-booking/update/{id}','AdminOrderBookingController@apiUpdate');

    // End sales's  Routes  


});
