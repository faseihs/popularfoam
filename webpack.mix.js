let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
mix.js('resources/assets/js/coL.js', 'public/js');
mix.js('resources/assets/js/cul.js', 'public/js');
mix.js('resources/assets/js/productLedger.js', 'public/js');
mix.js('resources/assets/js/ledger.js', 'public/js');
mix.js('resources/assets/js/point-of-sales.js', 'public/js').version();
mix.js('resources/assets/js/sales-invoice-index', 'public/js');


