<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('quality_id');
            $table->float('sale_price');
            $table->float('retail_price')->nullable();
            $table->float('purchase_price');
            $table->integer('sales_tax')->nullable();
            $table->string('package_unit');
            $table->float('package_amount');
            $table->integer('bar_code')->nullable();
            $table->date('opening_date')->nullabe();
            $table->integer('opening_quantity')->nullabe();
            $table->integer('opening_price')->nullabe();
            $table->integer('default_quantity');
            $table->integer('current_quantity')->default(0);
            $table->integer('store_quantity')->default(0);
            $table->integer('covered');
            $table->string('size');
            $table->float('size_l');
            $table->float('size_w');
            $table->float('size_h');
            $table->float('size_t');
            $table->integer('category_id');
            $table->string('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
