<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('owner_id');
            $table->string('owner_type');
            $table->string('type');
            $table->integer('amount');
            $table->integer('pay_later');
            $table->string('comments')->nullable();
            $table->integer('account_id')->nullable();
            $table->integer('sales_id')->nullable();
            $table->string('sales_type')->nullable();
            $table->date('date')->nullable();
            $table->integer('prev_to_pay')->nullable();
            $table->integer('balance')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
