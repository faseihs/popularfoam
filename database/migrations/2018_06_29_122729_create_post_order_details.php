<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('purchase_order_id');
            $table->integer('price');
            $table->integer('quantity');
            $table->float('total_price');
            $table->float('discount')->nullable()->default(0);
            $table->float('bonus')->nullabel()->default(0);
            $table->integer('covered')->nullabel();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_order_details');
    }
}
