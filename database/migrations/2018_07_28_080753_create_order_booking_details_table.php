<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderBookingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_booking_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('order_booking_id');
            $table->integer('price');
            $table->integer('quantity');
            $table->float('total_price');
            $table->float('discount')->nullable()->default(0);
          
            $table->integer('covered')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_booking_details');
    }
}
