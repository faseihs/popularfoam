<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesInvoiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_invoice_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('sales_invoice_id');
            $table->integer('price');
            $table->integer('quantity');
            $table->float('total_price');
            $table->float('discount')->nullable()->default(0);
            $table->float('bonus')->nullabel()->default(0);
            $table->integer('covered')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_invoice_details');
    }
}
