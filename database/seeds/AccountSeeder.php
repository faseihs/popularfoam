<?php

use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $assets=\App\Account::create([
            'name'=>'Assets',
            'debit'=>'increase',
            'credit'=>'decrease',
            'type'=>'left',
            'keyword'=>'assets',
            'can_delete'=>false,
        ]);
        \App\Account::create([
            'name'=>'Expenses',
            'debit'=>'increase',
            'credit'=>'decrease',
            'type'=>'left',
            'keyword'=>'expenses',
            'can_delete'=>false
        ]);

        \App\Account::create([
            'name'=>'Receivable',
            'debit'=>'increase',
            'credit'=>'decrease',
            'type'=>'left',
            'keyword'=>'receivable',
            'can_delete'=>false,
            'head_id'=>$assets->id
        ]);
        \App\Account::create([
            'name'=>'Cash',
            'debit'=>'increase',
            'credit'=>'decrease',
            'type'=>'left',
            'keyword'=>'cash',
            'can_delete'=>false,
            'head_id'=>$assets->id
        ]);


        //Right Side

        \App\Account::create([
            'name'=>'Liabilities',
            'credit'=>'increase',
            'debit'=>'decrease',
            'type'=>'right',
            'keyword'=>'liabilities',
            'can_delete'=>false
        ]);
        \App\Account::create([
            'name'=>'Revenue',
            'credit'=>'increase',
            'debit'=>'decrease',
            'type'=>'right',
            'keyword'=>'revenue',
            'can_delete'=>false
        ]);
        \App\Account::create([
            'name'=>'Capital/Equity',
            'credit'=>'increase',
            'debit'=>'decrease',
            'type'=>'right',
            'keyword'=>'capital',
            'can_delete'=>false
        ]);
    }
}
